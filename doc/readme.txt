
******文档说明
1.服务端安装文档
《PostgreSQL数据库安装教程.pdf》
《ENTBOOST云通讯平台安装文档（Linux）.pdf》
《ENTBOOST云通讯平台安装文档（Windows）.pdf》

2.WEB应用开发文档：
《ENTBOOST_SDK_API_JQUERY.pdf》
《ENTBOOST_SDK_API_REST.pdf》

3.Android安卓手机客户端开发文档：
《恩布SDK开发指南(Android).pdf》
《恩布SDK开发指南(Android) - 企业IM.pdf》
《恩布900在线客服_SDK_Android开发入门1.0.2014509.pdf》

4.IOS苹果手机客户端开发文档：
近期推出，敬请关注！

5.PC客户端开发文档：
《ENTBOOST_SDK_C++开发入门.pdf》
《ENTBOOST_SDK_API_PC.pdf》

6.恩布互联企业IM文档：
《ENTBOOST企业IM客户端使用手册.pdf》

7.企业IM应用集成二次开发文档：
《ENTBOOST企业IM应用集成开发文档.pdf》
《ENTBOOST企业IM应用集成后台管理手册.pdf》


******如果不能正常打开PDF文档
**请搜索一个pdf reader下载安装后再打开；
http://www.pc6.com/softview/SoftView_18376.html
http://mydown.yesky.com/soft/197/197717.shtml
http://www.skycn.com/soft/appid/7719.html

================================================
更多信息请访问：www.entboost.com
问题反馈、技术交流地址：forum.entboost.com

深圳恩布网络科技有限公司，拥有所有文档、资料和源码的最终解释权！
2014-06-28
