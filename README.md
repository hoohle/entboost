
# ENTBOOST：跨平台、跨应用的实时通讯平台
+ [恩布官网](http://www.entboost.com) 
+ [恩布云通讯技术论坛](http://forum.entboost.com) 

## ENTBOOST, V2014.180 0.5.0 --2014-06-30
+ 增加企业IM应用集成功能;
+ 完善安卓手机客户端SDK功能；
+ 部分BUG修正；

## ENTBOOST, V2014.177 0.3.0 Linux --2014-06-13
+ 增加Android IM SDK，安卓企业IM SDK开发包；
+ 增加JQUERY SDK开发包；
+ PC端企业IM产品，修正部分BUG；

## entboost, version 0.2.0 (r174) --2014-05-14
+ 增加Android SDK，恩布900在线客服安卓客户端开发包；
+ 企业私有云发布版本增加900在线客服功能；
+ 企业互联IM客户端增加集成企业应用（如OA，CRM）功能；
+ 其他BUG完善；

## entboost, version 0.1.0 (r172) --2014-04-18
+ 主要功能支持文本、表情，图片，屏幕截图，文件共享，语音视频，云盘，群组，离线消息等功能；
+ 支持PC SDK API，REST API等接口；
+ 支持对接企业内部各种业务系统；
+ **Android安卓版本、IOS版本企业IM；以及基于REST API开发的900在线客服已经在内测，近日会对外开放，敬请关注！**
