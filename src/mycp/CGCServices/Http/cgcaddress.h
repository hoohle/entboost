// cgcaddress.h file here
#ifndef __cgcaddress_h__
#define __cgcaddress_h__

#include <stl/stldef.h>

class CCgcAddress
{
public:
	enum SocketType
	{
		ST_TCP = 1
		, ST_UDP
		, ST_RTP
		, ST_UNKNOWN = 0xf
	};

	CCgcAddress(const CCgcAddress & avp)
	{
		equal(avp);
	}
	const CCgcAddress & operator = (const CCgcAddress & avp)
	{
		equal(avp);
		return *this;
	}

	void address(const tstring & newv) {m_address = newv;}
	void address(const tstring & ip, unsigned int port) {
		char buffer[32];
		sprintf(buffer, "%s:%d", ip.c_str(), port);
		m_address = buffer;
	}
	const tstring & address(void) const {return m_address;}
	void socketType(SocketType newv) {m_socketType = newv;}
	SocketType socketType(void) const {return m_socketType;}

	tstring getip(void) const
	{
		tstring::size_type find = m_address.find(":");
		return (find == tstring::npos) ? "" : m_address.substr(0, find);
	}
	int getport(void) const
	{
		tstring::size_type find = m_address.find(":");
		return (find == tstring::npos) ? 0 : atoi(m_address.substr(find+1, m_address.size()-find).c_str());
	}

	void reset(void)
	{
		m_address = _T("127.0.0.1:8010");
		m_socketType = ST_UNKNOWN;
	}

protected:
	void equal(const CCgcAddress & ca)
	{
		this->m_address = ca.address();
		this->m_socketType = ca.socketType();
	}

public:
	CCgcAddress(const tstring & address=_T("127.0.0.1:8010"), SocketType socketType = ST_UDP)
		: m_address(address), m_socketType(socketType)
	{}
	CCgcAddress(const tstring & ip, unsigned int port, SocketType socketType)
		: m_socketType(ST_UNKNOWN)
	{
		address(ip, port);
	}
	~CCgcAddress(void)
	{}
private:
	tstring	m_address;			// ip:port
	SocketType m_socketType;
};

const CCgcAddress constDefaultCgcAddress;

#endif // __cgcaddress_h__
