/*
    MYCP is a HTTP and C++ Web Application Server.
    Copyright (C) 2009-2010  Akee Yang <akee.yang@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// cgcaddress.h file here
#ifndef __cgcaddress_h__
#define __cgcaddress_h__

#include "../ThirdParty/stl/stldef.h"

class CCgcAddress
{
public:
	enum SocketType
	{
		ST_TCP = 1
		, ST_UDP
		, ST_RTP
		, ST_UNKNOWN = 0xf
	};

	CCgcAddress(const CCgcAddress & avp)
	{
		equal(avp);
	}
	const CCgcAddress & operator = (const CCgcAddress & avp)
	{
		equal(avp);
		return *this;
	}

	void address(const tstring & newv) {m_address = newv;}
	void address(const tstring & ip, unsigned int port) {
		char buffer[32];
		sprintf(buffer, "%s:%d", ip.c_str(), port);
		m_address = buffer;
	}
	const tstring & address(void) const {return m_address;}
	void socketType(SocketType newv) {m_socketType = newv;}
	SocketType socketType(void) const {return m_socketType;}

	tstring getip(void) const
	{
		tstring::size_type find = m_address.find(":");
		return (find == tstring::npos) ? "" : m_address.substr(0, find);
	}
	int getport(void) const
	{
		tstring::size_type find = m_address.find(":");
		return (find == tstring::npos) ? 0 : atoi(m_address.substr(find+1, m_address.size()-find).c_str());
	}

	void reset(void)
	{
		m_address = _T("127.0.0.1:8010");
		m_socketType = ST_UNKNOWN;
	}

protected:
	void equal(const CCgcAddress & ca)
	{
		this->m_address = ca.address();
		this->m_socketType = ca.socketType();
	}

public:
	CCgcAddress(const tstring & address="127.0.0.1:8010", SocketType socketType = ST_UDP)
		: m_address(address), m_socketType(socketType)
	{}
	CCgcAddress(const tstring & ip, unsigned int port, SocketType socketType)
		: m_socketType(ST_UNKNOWN)
	{
		address(ip, port);
	}
	~CCgcAddress(void)
	{}
private:
	tstring	m_address;			// ip:port
	SocketType m_socketType;
};

const CCgcAddress constDefaultCgcAddress;

#endif // __cgcaddress_h__
