/*
    MYCP is a HTTP and C++ Web Application Server.
    Copyright (C) 2009-2010  Akee Yang <akee.yang@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// CgcClientHandler.h file here
#ifndef __CgcClientHandler_h__
#define __CgcClientHandler_h__

//
// include
#include "IncludeBase.h"
#include "CgcData.h"

namespace cgc
{
	const cgcAttachment::pointer constNullAttchment;

	///////////////////////////////////////
	// Response handler
	// CgcClientHandler class
	class CgcClientHandler
	{
	public:
		virtual void OnCgcResponse(const cgcParserSotp & response)=0;
		virtual void OnCgcResponse(const CCgcData::pointer& recvData){}	// disable CGCParser or parser Error

		virtual void OnActiveTimeout(void) {}
		//
		// bool canResendAgain:
		//  true : resend this callid data
		//  false : do not resend this callid 
		virtual void OnCidTimeout(unsigned long callid, unsigned long sign, bool canResendAgain) {}
	};

	// 
	class DoSotpClientHandler
	{
	public:
		typedef boost::shared_ptr<DoSotpClientHandler> pointer;

		// response handler
		virtual void doSetResponseHandler(CgcClientHandler * newv) = 0;
		virtual const CgcClientHandler * doGetResponseHandler(void) const  = 0;
		virtual void doSetDisableSotpParser(bool newv) = 0;		// default false

		// session 
		virtual bool doSendOpenSession(unsigned long * pOutCallId = 0) = 0;
		virtual void doSendCloseSession(unsigned long * pOutCallId = 0) = 0;
		virtual bool doIsSessionOpened(void) const = 0;
		virtual const tstring & doGetSessionId(void) const = 0;

		// app call
		virtual void doBeginCallLock(void) = 0;
		virtual bool doSendAppCall(unsigned long nCallSign, const tstring & sCallName, bool bNeedAck = true,
			cgcAttachment::pointer pAttach = constNullAttchment, unsigned long * pOutCallId = 0) = 0;

		// thread
		virtual void doSetCIDTResends(unsigned int timeoutResends=2, unsigned int timeoutSeconds=4) = 0;
		virtual void doStartRecvThreads(unsigned short nRecvThreads = 2) = 0;
		virtual void doStartActiveThread(unsigned int nActiveWaitSeconds = 60) = 0;

		// parameter
		virtual void doAddParameter(const cgcParameter::pointer& parameter, bool bAddForce = true) = 0;
		virtual void doAddParameters(const std::vector<cgcParameter::pointer>& parameters, bool bAddForce = true) = 0;
		virtual size_t doGetParameterSize(void) const = 0;

		// info
		virtual void doSetEncoding(const tstring & newv=_T("GBK")) = 0;
		virtual const tstring & doGetEncoding(void) const = 0;
		virtual void doSetAppName(const tstring & newv) = 0;
		virtual const tstring & doGetAppName(void) const = 0;
		virtual void doSetAccount(const tstring & account, const tstring & passwd) = 0;
		virtual void doGetAccount(tstring & account, tstring & passwd) const = 0;
		virtual const tstring & doGetClientType(void) const = 0;

		// other
		virtual time_t doGetLastSendRecvTime(void) const = 0;
		virtual void doSetRemoteAddr(const tstring & newv) = 0;
		virtual void doSetMediaType(unsigned short newv) = 0;	// for RTP
		virtual size_t doSendData(const unsigned char * data, size_t size) = 0;
		virtual size_t doSendData(const unsigned char * data, size_t size, unsigned int timestamp) = 0;	// for RTP
	};

	//

} // namespace cgc
#endif // __CgcClientHandler_h__
