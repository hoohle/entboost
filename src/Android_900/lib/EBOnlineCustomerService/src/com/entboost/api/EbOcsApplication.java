package com.entboost.api;

import java.io.IOException;
import java.util.Properties;

import net.yunim.service.api.Entboost;
import net.yunim.service.cache.EbApplication;

public class EbOcsApplication extends EbApplication {
	private String csaid;
	private String appid;
	private String appkey;
	private String address;

	@Override
	public void onCreate() {
		super.onCreate();
		initConfig("config.properties");
	}
	
	/**
	 * 加载配置文件
	 */
	public void initConfig(String configName) {
		Properties p = new Properties();
		try {
			p.load(EbOcsApplication.class.getResourceAsStream(configName));
			appid=p.getProperty("appid");
			appkey=p.getProperty("appkey");
			address=p.getProperty("logoncenteraddr");
			csaid = p.getProperty("csaid");
			Entboost.showSotpLog(true);
		} catch (IOException e) {
		}
	}

	public String getCsaid() {
		return csaid;
	}

	public String getAppid() {
		return appid;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setCsaid(String csaid) {
		this.csaid = csaid;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}
	

}
