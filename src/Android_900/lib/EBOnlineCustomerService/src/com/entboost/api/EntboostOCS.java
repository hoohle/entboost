package com.entboost.api;

import android.content.Context;
import android.content.Intent;

import com.entboost.ui.ebocs.view.chat.ChatActivity;

public class EntboostOCS {
	/**
	 * 打开在线咨询界面
	 * 
	 * @param context
	 */
	public static void openChatPage(final Context context) {
		Intent intent = new Intent(context, ChatActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	/**
	 * 打开在线咨询界面
	 * 
	 * @param context
	 */
	public static void openChatPage(final Context context,String csaid,String serverName) {
		Intent intent = new Intent(context, ChatActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(EbOcsConstants.CSAID, csaid);
		intent.putExtra(EbOcsConstants.SERVERNAME, serverName);
		context.startActivity(intent);
	}
}
