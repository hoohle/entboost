package com.entboost.ui.ebocs.view.chat;

import net.yunim.eb.signlistener.EntboostChatListener;
import net.yunim.eb.signlistener.EntboostIMListener;
import net.yunim.service.api.ChatCenter;
import net.yunim.service.api.Entboost;
import net.yunim.service.api.LogonCenter;
import net.yunim.service.api.UserCenter;
import net.yunim.service.cache.EbApplication;
import net.yunim.service.constants.EB_STATE_CODE;
import net.yunim.service.entity.AccountInfo;
import net.yunim.service.entity.ChatRoomRichMsg;
import net.yunim.service.entity.Resource;
import net.yunim.service.listener.LoadCSAListener;
import net.yunim.service.listener.LogonAccountListener;
import net.yunim.ui.activity.BaseActivity;
import net.yunim.utils.ExtAudioRecorder;
import net.yunim.utils.Log;
import net.yunim.utils.UIUtils;

import org.apache.commons.lang.StringUtils;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.entboost.api.EbOcsApplication;
import com.entboost.api.EbOcsConstants;
import com.entboost.ebocs.R;

public class ChatActivity extends BaseActivity {

	private Long account;
	private String csaId;
	private EditText mContentEdit;
	private BaseAdapter mChatMsgViewAdapter;
	private ListView mMsgListView;
	private LinearLayout emotionsAppPanel;
	private GridView expressionGriView;
	private EmotionsImageAdapter emotionsImageAdapter;
	private ImageButton picBtn;
	private Button sendBtn;
	private ImageView voiceImg;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Entboost.removeChatListener(this.getClass().getName());
		Entboost.removeListener(this.getClass().getName());
		Entboost.applicationMustReInit();
		LogonCenter.getInstance().logout(null);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}

	private void init() {
		Entboost.addListener(this.getClass().getName(),
				new EntboostIMListener() {

					@Override
					public void localNoNetwork() {
						pageInfo.showError("网络无法连接，请检查设置！");
					}

					@Override
					public void serviceNoNetwork() {
						pageInfo.showError("服务器无法连接，请检查设置！");
					}

				});
		Entboost.addChatListener(this.getClass().getName(),
				new EntboostChatListener() {

					@Override
					public void onReceiveUserMessage(ChatRoomRichMsg msg) {
						mChatMsgViewAdapter.notifyDataSetChanged();
						mMsgListView.setSelection(mMsgListView.getBottom());
					}

					@Override
					public void onSendStatusChanged(ChatRoomRichMsg msg) {
						mChatMsgViewAdapter.notifyDataSetChanged();
						mMsgListView.setSelection(mMsgListView.getBottom());
					}

				});
		String appid = ((EbOcsApplication) getApplication()).getAppid();
		String appkey = ((EbOcsApplication) getApplication()).getAppkey();
		String csaidIntent = this.getIntent().getStringExtra(
				EbOcsConstants.CSAID);
		String addressIntent = this.getIntent().getStringExtra(
				EbOcsConstants.SERVERNAME);
		if (StringUtils.isNotBlank(csaidIntent)) {
			csaId = csaidIntent;
		} else {
			csaId = ((EbOcsApplication) getApplication()).getCsaid();
		}
		if (StringUtils.isNotBlank(addressIntent)) {
			Entboost.setLogonCenterAddr(addressIntent);
		} else {
			Entboost.setLogonCenterAddr(((EbOcsApplication) getApplication())
					.getAddress());
		}
		LogonCenter.getInstance().defaultLogin(appid, appkey,
				new LogonAccountListener() {

					@Override
					public void onFailure(EB_STATE_CODE nState) {
						pageInfo.showError(EB_STATE_CODE.getMessage(nState
								.getValue()));
					}

					@Override
					public void onLogonSuccess(AccountInfo arg0) {
						UserCenter.getInstance().loadCSA(csaId,
								new LoadCSAListener() {

									@Override
									public void onFailure(EB_STATE_CODE nState) {
										pageInfo.showError(EB_STATE_CODE
												.getMessage(nState.getValue()));
									}

									@Override
									public void onLogonSuccess(Long uid) {
										pageInfo.hide();
										account = uid;
										ChatCenter.getInstance()
												.loadPersonChatDiscCache(
														account);
										mChatMsgViewAdapter = new ChatMsgViewAdapter(
												ChatActivity.this, ChatCenter
														.getInstance()
														.getChatMsgs(account));
										mMsgListView
												.setAdapter(mChatMsgViewAdapter);
										mMsgListView.setSelection(mMsgListView
												.getBottom());
										Entboost.addListener(this.getClass()
												.getName(),
												new EntboostIMListener() {

													@Override
													public void serviceStop() {
														pageInfo.showError("服务器停机维护中，请稍候！");
													}

													@Override
													public void localNoNetwork() {
														pageInfo.showError("网络无法连接，请检查设置！");
													}

													@Override
													public void serviceNoNetwork() {
														pageInfo.showError("服务器无法连接，请检查设置！");
													}

													@Override
													public void network() {
														pageInfo.hide();
													}
												});
									}
								});
					}
				});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setEbContentView(R.layout.activity_chat);
		titleBar.setBackgroundResource(R.drawable.title_bar);
		titleBar.setTitleText("恩布900在线客服");
		titleBar.setLogo(R.drawable.entlogo);
		pageInfo.showProgress("正在呼叫客服，请耐心等待！");
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				Entboost.reStart();
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				init();
			}

		}.execute();

		mContentEdit = (EditText) findViewById(R.id.content);
		sendBtn = (Button) findViewById(R.id.sendBtn);
		voiceImg=(ImageView)this.findViewById(R.id.imageViewvoice);
		ImageButton voiceBtn = (ImageButton) this.findViewById(R.id.voiceBtn);
		voiceBtn.setOnTouchListener(new View.OnTouchListener() {

			private ExtAudioRecorder recorder;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {
					// 按住事件发生后执行代码的区域
					voiceImg.setVisibility(View.VISIBLE);
					recorder=ChatCenter.getInstance().startRecording();
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					// 移动事件发生后执行代码的区域
					break;
				}
				case MotionEvent.ACTION_UP: {
					// 松开事件发生后执行代码的区域
					voiceImg.setVisibility(View.GONE);
					if (recorder != null) {
						recorder.stopRecord();
						ChatCenter.getInstance().sendVoice(account,
								recorder);
						mChatMsgViewAdapter.notifyDataSetChanged();
					}
					break;
				}
				default:
					break;
				}
				return false;
			}
		});
		mContentEdit.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() > 0) {
					sendBtn.setVisibility(View.VISIBLE);
					picBtn.setVisibility(View.GONE);
				} else {
					sendBtn.setVisibility(View.GONE);
					picBtn.setVisibility(View.VISIBLE);
				}
			}
		});

		sendBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String text = mContentEdit.getText().toString().trim();
				if (StringUtils.isBlank(text)) {
					UIUtils.showToast(ChatActivity.this, "不能发送空消息！");
					return;
				}
				if (account == null) {
					UIUtils.showToast(ChatActivity.this, "正在呼叫客服，请稍候！");
					return;
				}
				ChatCenter.getInstance().sendText(account, text);
				mChatMsgViewAdapter.notifyDataSetChanged();
				// 清空文本框
				mContentEdit.setText("");
				mMsgListView.setSelection(mMsgListView.getBottom());
			}

		});

		mMsgListView = (ListView) this.findViewById(R.id.mListView);
		expressionGriView = (GridView) this
				.findViewById(R.id.expressionGridView);
		emotionsImageAdapter = new EmotionsImageAdapter(this);
		expressionGriView.setAdapter(emotionsImageAdapter);
		expressionGriView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final Resource emotions = (Resource) emotionsImageAdapter
						.getItem(position);
				UIUtils.addEmotions(mContentEdit, emotions);
			}
		});

		emotionsAppPanel = (LinearLayout) this
				.findViewById(R.id.expressionAppPanel);
		picBtn = (ImageButton) this.findViewById(R.id.picBtn);
		picBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				getPicFromContent();
			}
		});
		ImageButton emotionBtn = (ImageButton) this
				.findViewById(R.id.emotionBtn);
		emotionBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (emotionsAppPanel.getVisibility() == View.GONE) {
					emotionsAppPanel.setVisibility(View.VISIBLE);
				} else {
					emotionsAppPanel.setVisibility(View.GONE);
				}
				((BaseAdapter) expressionGriView.getAdapter())
						.notifyDataSetChanged();
			}
		});
	}

	private void getPicFromContent() {
		try {
			// 选择照片的时候也一样，我们用Action为Intent.ACTION_GET_CONTENT，
			// 有些人使用其他的Action但我发现在有些机子中会出问题，所以优先选择这个
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(intent, 2);
		} catch (ActivityNotFoundException e) {
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			return;
		}
		if (data != null) {
			// 取得返回的Uri,基本上选择照片的时候返回的是以Uri形式，但是在拍照中有得机子呢Uri是空的，所以要特别注意
			Uri mImageCaptureUri = data.getData();
			Cursor cursor = ChatActivity.this.getContentResolver().query(
					mImageCaptureUri, null, null, null, null);
			String filePath = null;
			if (cursor.moveToFirst()) {
				filePath = cursor.getString(cursor.getColumnIndex("_data"));//
				cursor.close();
			}
			// 返回的Uri不为空时，那么图片信息数据都会在Uri中获得。如果为空，那么我们就进行下面的方式获取
			if (mImageCaptureUri != null) {
				try {
					// 这个方法是根据Uri获取Bitmap图片的静态方法
					if (filePath == null) {
						// image = MediaStore.Images.Media.getBitmap(
						// this.getContentResolver(), mImageCaptureUri);
					} else {
						sendPic(filePath);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void sendPic(String picUri) {
		if (account == null) {
			UIUtils.showToast(ChatActivity.this, "正在呼叫客服，请稍候！");
			return;
		}
		ChatCenter.getInstance().sendPic(account, picUri);
		mChatMsgViewAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onStop() {
		super.onStop();
		EbApplication.getInstance().getSysDataCache().save();
	}

}
