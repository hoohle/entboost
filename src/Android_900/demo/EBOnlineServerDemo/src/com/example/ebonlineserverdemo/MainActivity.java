package com.example.ebonlineserverdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.entboost.api.EntboostOCS;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.callbtn).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						String serverName = ((EditText) findViewById(R.id.serverName))
								.getText().toString();
						String csaid = ((EditText) findViewById(R.id.csaid))
								.getText().toString();
						EntboostOCS.openChatPage(MainActivity.this,csaid,serverName);
					}
				});

	}

}
