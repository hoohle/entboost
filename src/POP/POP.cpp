
// POP.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "POP.h"
#include "POPDlg.h"
//#include "DlgLogin.h"
#include "DlgOAuth.h"
#include "DlgMemberInfo.h"
#include "DlgFuncWindow.h"
//#include <excpt.h>
//#include <boost/exception/all.hpp>

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

#ifndef USES_EBCOM_TEST
#ifdef _DEBUG
#pragma comment(lib, "videoroomd.lib")
#pragma comment(lib, "libebumd.lib")
#else
#pragma comment(lib, "videoroom.lib")
#pragma comment(lib, "libebum.lib")
#endif
#endif

// CPOPApp

BEGIN_MESSAGE_MAP(CPOPApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

CString GetLineStateText(EB_USER_LINE_STATE nLineState)
{
	switch (nLineState)
	{
	case EB_LINE_STATE_ONLINE:
		return _T("在线");
	case EB_LINE_STATE_BUSY:
		return _T("忙碌");
	case EB_LINE_STATE_AWAY:
		return _T("离开");
	default:
		return _T("离线");
	}
}
CString GetGroupTypeText(EB_GROUP_TYPE nGroupType, bool bShortName)
{
	switch (nGroupType)
	{
	case EB_GROUP_TYPE_DEPARTMENT:
		return _T("部门");
	case EB_GROUP_TYPE_PROJECT:
		return bShortName?_T("项目"):_T("项目组");
	case EB_GROUP_TYPE_GROUP:
		return bShortName?_T("群组"):_T("个人群组");
	default:
		return _T("讨论组");
	}
}
// CPOPApp 构造

//#define _CRTDBG_MAP_ALLOC
//#include "stdlib.h"
//#include "crtdbg.h"

CPOPApp::CPOPApp()
{
	//time_t tnow = time(0);
	//CTime pTime(1379619455);
	//int nyead = pTime.GetYear();
	//int nmonth = pTime.GetMonth();
	//int nday = pTime.GetDay();

	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
	CString str;
	GetModuleFileName( NULL, str.GetBuffer(MAX_PATH), MAX_PATH);
	str.ReleaseBuffer( -1);
	str = str.Mid(0, str.ReverseFind('\\'));
	m_sAppPath = str;
	m_sAppDataPath = m_sAppPath + _T("\\datas");

	m_nLogonUserId = 0;

	//m_imageEbIcon = NULL;
	//m_imageEntLogo = NULL;
	m_image1Group = NULL;
	m_image1People = NULL;
	m_image1Cloud = NULL;
	m_image2Cloud = NULL;
	m_imageDefaultVisitor = NULL;
	m_imageDefaultOrg = NULL;
	m_imageDefaultContact = NULL;
	m_imageDefaultMember = NULL;
	m_imageDefaultDepartment = NULL;
	m_imageDefaultProject = NULL;
	m_imageDefaultGroup = NULL;
	m_imageDefaultTempGroup = NULL;
	m_imageStateAway = NULL;
	m_imageStateBusy = NULL;

	m_pDlgEmotionSelect = NULL;
	//m_iconAway = NULL;
	//m_iconBusy = NULL;
	//m_iconEnterprise = NULL;
	//m_iconDepartment = NULL;
	//m_iconContact = NULL;
	m_nMainColor = 0;

	m_nSelectCallId = 0;
	m_nSelectUserId = 0;
	m_nSelectGroupId = 0;

}


// 唯一的一个 CPOPApp 对象

CPOPApp theApp;
#ifdef USES_EBCOM_TEST
CComPtr<IEBParseSetting> theEBSetting;
CComPtr<IEBClientCore> theEBClientCore;
//CEBAppClient theEBAppClient;
#else
CEBParseSetting theSetting;
CEBAppClient theEBAppClient;
#endif

// CPOPApp 初始化
//#include "../include/md5.h"

ULONG_PTR theGdiplusToken=0;
BOOL CPOPApp::InitInstance()
{
	setlocale(LC_ALL, "");	// 使用默认环境 .936中文目录有问题。
	//cgc::MD5 md5;
	//	tstring strtemp("420604750@qq.com");
	//	strtemp.append("abc135246");
	//	md5.update((const unsigned char*)strtemp.c_str(),strtemp.size());
	//	md5.finalize();
	//	std::string sPassword = md5.hex_digest();
	//	//AfxMessageBoxA(sPassword.c_str());
	//	return FALSE;

	//setlocale(LC_ALL, ".936");
	//COLORREF color = RGB(255, 127, 39);
	//float h,s,l;
	//::RGBtoHSL(255,127,39,&h,&s,&l);
	//color = ::HSLtoRGB(h, s, 0.9);
	//CString stest;
	//stest.Format(_T("%f %f %f %d %d %d"), h, s, l, GetRValue(color), GetGValue(color), GetBValue(color));
	//AfxMessageBox(stest);
	//return FALSE;

	//_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(330);

	//// 如果一个运行在 Windows XP 上的应用程序清单指定要
	//// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	////则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	//INITCOMMONCONTROLSEX InitCtrls;
	//InitCtrls.dwSize = sizeof(InitCtrls);
	//// 将它设置为包括所有要在应用程序中使用的
	//// 公共控件类。
	//InitCtrls.dwICC = ICC_WIN95_CLASSES;
	//InitCommonControlsEx(&InitCtrls);

	//CWinAppEx::InitInstance();
	//::CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	//::CoInitializeEx(NULL,COINIT_MULTITHREADED);	// 没有事件
	::CoInitialize(NULL);
	AfxEnableControlContainer();
    CMenuXP::InitializeHook();
	::AfxInitRichEdit2();
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&theGdiplusToken, &gdiplusStartupInput, NULL);

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("entboost"));

	// 执行DLL注册命令
#ifndef _DEBUG
	CString sParameter;
	sParameter.Format(_T("\"%s\\eboleimage.dll\" /s"), this->GetAppPath());
	ShellExecute(NULL,  "open", "regsvr32", sParameter, GetAppPath(), SW_HIDE);
	sParameter.Format(_T("\"%s\\ebcontrol.dll\" /s"), this->GetAppPath());
	ShellExecute(NULL,  "open", "regsvr32", sParameter, GetAppPath(), SW_HIDE);
#ifdef USES_EBCOM_TEST
	sParameter.Format(_T("\"%s\\ebvideoroom.dll\" /s"), this->GetAppPath());
	ShellExecute(NULL,  "open", "regsvr32", sParameter, GetAppPath(), SW_HIDE);
	sParameter.Format(_T("\"%s\\ebcore.dll\" /s"), this->GetAppPath());
	ShellExecute(NULL,  "open", "regsvr32", sParameter, GetAppPath(), SW_HIDE);
#endif
#endif

#ifdef USES_EBCOM_TEST
	CoCreateInstance(__uuidof(EBClientCore),NULL,CLSCTX_INPROC_SERVER,__uuidof(IEBClientCore),(void**)&theEBClientCore);
	if (theEBClientCore==NULL) return FALSE;
	theEBClientCore->QueryInterface(__uuidof(IEBParseSetting),(void**)&theEBSetting);
	if (theEBSetting==NULL) return FALSE;
	// 读取配置文件
	theEBSetting->Load((LPCTSTR)GetSettingFile());
	const CEBString sVersion = theEBSetting->Version.GetBSTR();
#else
	// 读取配置文件
	theSetting.load(GetSettingFile());
	const CEBString sVersion = theSetting.GetVersion();
#endif
	// 检查自动更新文件；
	const CString sNewVersion = EBC_GetProfileString(_T("new_version"),_T("version"));
	const CString sNewVersionFile = EBC_GetProfileString(_T("new_version"),_T("file"));
	//const CString sNewVersionFile = GetAppPath()+_T("\\1.0.0.1.rar");
	if (!sNewVersion.IsEmpty())
	{
		if (::PathFileExists(sNewVersionFile) && sNewVersion != sVersion.c_str())
		{
			// 有新版本需要自动更新
			//const CString sDescription = EBC_GetProfileString(_T("new_version"),_T("desc"));
			//const CString sUpdateTime = EBC_GetProfileString(_T("new_version"),_T("time"));
			//const UINT nUpdateType = EBC_GetProfileInt(_T("new_version"),_T("type"),0);

			// 执行自动更新过程
			TCHAR lpszModuleFileName[MAX_PATH];
			GetModuleFileName( NULL, lpszModuleFileName, MAX_PATH);
			CString sUpdateExe = GetAppPath()+_T("\\update\\ebupdate2.exe");
			CString sParameter;
			sParameter.Format(_T("\"%s\" \"%s\""), sNewVersionFile, lpszModuleFileName);
			ShellExecute(NULL,  "open", sUpdateExe, sParameter, GetAppPath(), SW_SHOW);
			return FALSE;
		}else
		{
			// 自动更新完成，或其他错误
			CString sUpdateExe2 = GetAppPath()+_T("\\update\\ebupdate2.exe");
			CString sNewUpdateExe = GetAppPath()+_T("\\update\\ebupdate2_new.exe");
			if (PathFileExists(sNewUpdateExe))
			{
				// 更新ebupdate2.exe文件
				for (int i=0; i<10; i++)	// 等级ebupdate2.exe程序退出
				{
					if (CopyFile(sNewUpdateExe,sUpdateExe2,FALSE))
					{
						break;
					}
					Sleep(200);
				}
				DeleteFile(sNewUpdateExe);
			}
			//DeleteFile(GetSettingIniFile());
			EBC_SetProfileString(_T("new_version"),_T("version"),_T(""));
			EBC_SetProfileString(_T("new_version"),_T("desc"),_T(""));
			EBC_SetProfileString(_T("new_version"),_T("time"),_T(""));
			EBC_SetProfileString(_T("new_version"),_T("file"),_T(""));
			EBC_SetProfileInt(_T("new_version"),_T("type"),0);
		}
	}

	USES_CONVERSION;
	//CString sImagePath = GetAppPath()+_T("\\img\\entboost.ico");
	//m_imageEbIcon = new Gdiplus::Image((const WCHAR*)T2W(sImagePath));   
	//sImagePath = GetAppPath()+_T("\\img\\entlogo.png");
	//m_imageEntLogo = new Gdiplus::Image((const WCHAR*)T2W(sImagePath));
	libEbc::ImageFromIDResource(IDB_PNG_IMG1_GROUP,_T("png"),m_image1Group);
	libEbc::ImageFromIDResource(IDB_PNG_IMG1_PEOPLE,_T("png"),m_image1People);
	libEbc::ImageFromIDResource(IDB_PNG_IMG1_CLOUD,_T("png"),m_image1Cloud);
	libEbc::ImageFromIDResource(IDB_PNG_IMG2_CLOUD,_T("png"),m_image2Cloud);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_VISITOR,_T("png"),m_imageDefaultVisitor);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_ORG,_T("png"),m_imageDefaultOrg);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_CONTACT,_T("png"),m_imageDefaultContact);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_MEMBER,_T("png"),m_imageDefaultMember);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_DEPARTMENT,_T("png"),m_imageDefaultDepartment);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_PROJECT,_T("png"),m_imageDefaultProject);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_GROUP,_T("png"),m_imageDefaultGroup);
	libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_TEMPGROUP,_T("png"),m_imageDefaultTempGroup);
	libEbc::ImageFromIDResource(IDB_PNG_STATE_AWAY,_T("png"),m_imageStateAway);
	libEbc::ImageFromIDResource(IDB_PNG_STATE_BUSY,_T("png"),m_imageStateBusy);

	//// 实现在登录口，保持单实例。
	//HANDLE g_hMutex = CreateMutex(NULL,TRUE,_T("__entboost_mutex__"));
	//if(ERROR_ALREADY_EXISTS==GetLastError())
	//{
	//	//找到另一个实例
	//	CString stext;
	//	stext.Format(_T("%s-登录窗口"),theSetting.GetEnterprise().c_str());
	//	HWND l_hWnd = FindWindow(NULL, stext);
	//	if (l_hWnd!=NULL)
	//	{
	//		if (TRUE == IsIconic(l_hWnd))
	//		{
	//			ShowWindow(l_hWnd,SW_SHOWNORMAL); //还原窗口
	//		}
	//		SetForegroundWindow(l_hWnd); //使窗口在最前方
	//	}else
	//	{
	//		// ?
	//	}
	//	return FALSE;
	//}

	std::string sAppParameter0;
	if (__argc >= 2)
	{
		sAppParameter0 = __argv[1];
	}
	CDlgOAuth pDlgLogin;
	pDlgLogin.m_sAppParameter0 = sAppParameter0;
	pDlgLogin.DoModal();

	bool bIsVisitor = theApp.IsLogonVisitor();
#ifdef USES_EBCOM_TEST
	if (theEBClientCore->EB_IsLogoned == VARIANT_FALSE)
	{
		return FALSE;
	}
	m_sLogonAccount = CEBString(theEBClientCore->EB_LogonAccount.GetBSTR()).c_str();
	m_nLogonUserId = theEBClientCore->EB_LogonUserId;
	m_sLogonPassword = CEBString(theEBClientCore->EB_Password.GetBSTR()).c_str();
#else
	if (!theEBAppClient.EB_IsLogoned())
	{
		return FALSE;
	}
	m_sLogonAccount = theEBAppClient.EB_GetLogonAccount().c_str();
	m_nLogonUserId = theEBAppClient.EB_GetLogonUserId();
	m_sLogonPassword = theEBAppClient.EB_GetPassword().c_str();
	//bIsVisitor = theEBAppClient.EB_IsLogonVisitor();
#endif

	CString sBoPath = theApp.GetAppDataPath() + _T("\\bodb");
	m_pBoEB = bo::bodb_init(sBoPath);
	if (m_pBoEB.get() == NULL)
	{
		CDlgMessageBox::EbDoModal(NULL,_T("安装目录损坏："),_T("请重新安装！"),CDlgMessageBox::IMAGE_ERROR);
		return FALSE;
	}
	m_pBoEB->setaccount("system","");
	if (!m_pBoEB->use("eb"))
	{
		CDlgMessageBox::EbDoModal(NULL,_T("配置文件损坏："),_T("请重新安装！"),CDlgMessageBox::IMAGE_ERROR);
		return FALSE;
	}
	// 检查默认主色调
	bo::PRESULTSET pResltSet = NULL;
	m_pBoEB->execsql("SELECT value2 FROM sys_value_t WHERE name='main-color'", &pResltSet);
	if (pResltSet != NULL && pResltSet->rscount>0)
	{
		m_nMainColor = pResltSet->rsvalues[0]->fieldvalues[0]->v.uintegerVal;
	}else
	{
#ifdef USES_EBCOM_TEST
		m_nMainColor = theEBSetting->DefaultColor;
#else
		m_nMainColor = theSetting.GetDefaultColor();
#endif
		CString sSql;
		sSql.Format(_T("INSERT INTO sys_value_t(name,value2) VALUES('main-color',%d)"),(DWORD)m_nMainColor);
		m_pBoEB->execsql(sSql);
	}
	bo::bodb_free(pResltSet);
	m_pBoEB->close();

	m_sUserMainPath = m_sAppPath + _T("\\users");

	// ****************
	//m_iconAway = LoadIcon(IDI_ICON_AWAY);
	//m_iconBusy = LoadIcon(IDI_ICON_BUSY);
	//m_iconEnterprise = LoadIcon(IDI_ENTERPRISE);
	//m_iconDepartment = LoadIcon(IDI_DEPARTMENT);
	//m_iconContact = LoadIcon(IDI_CONTACT);

	//if (pDlgLogin.m_bAutoLogin && !bIsVisitor)
	//{
	//	// 普通用户，要求设置自动登录
	//	theApp.EBC_SetProfileString(_T("system"),_T("auto-run-account"),m_sLogonAccount);
	//}else if (!pDlgLogin.m_bAutoLogin)
	//{
	//	theApp.EBC_SetProfileString(_T("system"),_T("auto-run-account"),_T(""));
	//}

	if (bIsVisitor)
	{
		m_sUserMainPath += _T("\\visitor");
	}else
	{
		m_sUserMainPath = m_sUserMainPath + _T("\\") + m_sLogonAccount;
	}
	if (!::PathFileExists(m_sUserMainPath))
	{
		::CreateDirectory(m_sUserMainPath, NULL);
	}
	m_sUserImagePath = m_sUserMainPath + _T("\\image");
	if (!::PathFileExists(m_sUserImagePath))
	{
		::CreateDirectory(m_sUserImagePath, NULL);
	}
	m_sUserFilePath = m_sUserMainPath + _T("\\file");
	if (!::PathFileExists(m_sUserFilePath))
	{
		::CreateDirectory(m_sUserFilePath, NULL);
	}
	ReadUserSetting();

	sBoPath = m_sUserMainPath + _T("\\bodb");
	const std::string sDefaultUserBoFile = GetAppDataPath()+_T("\\bodb\\ebuser\\ebuser.bdf");
	const std::string sUserBoFile = sBoPath+_T("\\ebuser\\ebuser.bdf");
	const std::string sUserBoFileBk = sUserBoFile+".bk";
	if (!::PathFileExists(sBoPath))
	{
		::CreateDirectory(sBoPath, NULL);
		::CreateDirectory(sBoPath+_T("\\ebuser"), NULL);
		CopyFile(sDefaultUserBoFile.c_str(), sUserBoFile.c_str(), TRUE);
	}else if (!::PathFileExists(sUserBoFile.c_str()))
	{
		CopyFile(sDefaultUserBoFile.c_str(), sUserBoFile.c_str(), TRUE);
	}
	const CString theUserBoPath(sBoPath);
	m_pBoUsers = bo::bodb_init(sBoPath);
	if (m_pBoUsers.get() == NULL)
	{
		CDlgMessageBox::EbDoModal(NULL,_T("安装目录损坏："),_T("请重新安装！"),CDlgMessageBox::IMAGE_ERROR);
		return FALSE;
	}
	//bool bret = openebuser();
	bool bError = false;
	try
	{
		m_pBoUsers->setaccount("system","");
		if (!m_pBoUsers->use("ebuser"))
		{
			// 数据库文件错误
			bError = true;
		}
	}
	catch(std::exception&)
	{
		bError = true;
		//AfxMessageBox(_T("catch 111"));
	}
	catch(...)
	{
		// 数据库文件错误
		bError = true;
		//AfxMessageBox(_T("catch 333"));
	}
	if (bError)
	{
		m_pBoUsers->close();
		//CopyFile(sDefaultUserBoFile.c_str(), sUserBoFile.c_str(), FALSE);
		CopyFile(sUserBoFileBk.c_str(), sUserBoFile.c_str(), FALSE);
		CDlgMessageBox::EbDoModal(NULL,_T("配置文件损坏，已经修复："),_T("请重新登录！"),CDlgMessageBox::IMAGE_INFORMATION);
		TCHAR lpModuleFileName[MAX_PATH] = {'\0'};
		::GetModuleFileName( NULL, lpModuleFileName, MAX_PATH);
		ShellExecute(NULL,  "open", lpModuleFileName, "", GetAppPath(), SW_SHOW);
		return FALSE;
	}else
	{
		CopyFile(sUserBoFile.c_str(), sUserBoFileBk.c_str(), FALSE);
	}

	// **检查更新；
	int ret = m_pBoUsers->execsql("select from_type FROM call_record_t WHERE from_type=100");
	if (ret == -1)
	{
		// **该字段不存在，需要更新数据库字段 --2014-01-26
		m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE call_id VARCHAR(16) NOT NULL");
		m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE dep_code VARCHAR(16) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE emp_code VARCHAR(16) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD from_type TINYINT DEFAULT 1");
		m_pBoUsers->execsql("ALTER TABLE msg_record_t CHANGE msg_id VARCHAR(16) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE msg_record_t CHANGE dep_code VARCHAR(16) DEFAULT ''");
	}
	//m_pBoUsers->execsql("ALTER TABLE call_record_t DROP COLUMN company");
	ret = m_pBoUsers->execsql("select company FROM call_record_t WHERE company=''");
	if (ret == -1)
	{
		// **该字段不存在，需要更新数据库字段 --2014-02-08
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD company VARCHAR(128) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD title VARCHAR(64) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD phone VARCHAR(32) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD tel VARCHAR(32) DEFAULT ''");
		m_pBoUsers->execsql("ALTER TABLE call_record_t ADD email VARCHAR(64) DEFAULT ''");
	}
	ret = m_pBoUsers->execsql("select from_phone FROM call_record_t WHERE from_phone=0");
	if (ret == -1)
	{
		// 直接更换文件
		m_pBoUsers->close();
		bo::bodb_exit(m_pBoUsers);
		m_pBoUsers.reset();
		CopyFile(sDefaultUserBoFile.c_str(), sUserBoFile.c_str(), FALSE);
		m_pBoUsers = bo::bodb_init(theUserBoPath);
		if (m_pBoUsers.get() == NULL)
		{
			CDlgMessageBox::EbDoModal(NULL,_T("安装目录损坏："),_T("请重新安装！"),CDlgMessageBox::IMAGE_ERROR);
			return FALSE;
		}
		m_pBoUsers->setaccount("system","");
		m_pBoUsers->use("ebuser");

		//// **该字段不存在，需要更新数据库字段 --2014-03-07
		////m_pBoUsers->execsql("DELETE FROM call_record_t");
		////m_pBoUsers->execsql("DELETE FROM msg_record_t");
		//m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE call_id BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE dep_code BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE call_record_t CHANGE emp_code BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE call_record_t DROP from_account");
		//m_pBoUsers->execsql("ALTER TABLE call_record_t ADD from_uid BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t CHANGE msg_id BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t CHANGE dep_code BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t DROP from_account");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t DROP to_account");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t ADD from_uid BIGINT DEFAULT 0");
		//m_pBoUsers->execsql("ALTER TABLE msg_record_t ADD to_uid BIGINT DEFAULT 0");
	}


	//ShowImageWindow(false,NULL,NULL);
	CPOPDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

int CPOPApp::ExitInstance()
{
	if (m_pDlgEmotionSelect)
	{
		m_pDlgEmotionSelect->DestroyWindow();
		delete m_pDlgEmotionSelect;
		m_pDlgEmotionSelect = NULL;
	}

	//if (m_imageEbIcon)
	//	delete m_imageEbIcon;
	//if (m_imageEntLogo)
	//	delete m_imageEntLogo;
	if (m_image1Group)
		delete m_image1Group;
	if (m_image1People)
		delete m_image1People;
	if (m_image1Cloud)
		delete m_image1Cloud;
	if (m_image2Cloud)
		delete m_image2Cloud;
	if (m_imageDefaultVisitor)
		delete m_imageDefaultVisitor;
	if (m_imageDefaultOrg)
		delete m_imageDefaultOrg;
	if (m_imageDefaultContact)
		delete m_imageDefaultContact;
	if (m_imageDefaultMember)
		delete m_imageDefaultMember;
	if (m_imageDefaultDepartment)
		delete m_imageDefaultDepartment;
	if (m_imageDefaultProject)
		delete m_imageDefaultProject;
	if (m_imageDefaultGroup)
		delete m_imageDefaultGroup;
	if (m_imageDefaultTempGroup)
		delete m_imageDefaultTempGroup;
	if (m_imageStateAway)
		delete m_imageStateAway;
	if (m_imageStateBusy)
		delete m_imageStateBusy;

	m_pCallList.clear();
	{
		boost::mutex::scoped_lock lock(m_pMsgRecord.mutex());
		CLockMap<eb::bigint, CDlgMsgRecord*>::const_iterator pIterMsgRecord = m_pMsgRecord.begin();
		for (; pIterMsgRecord!=m_pMsgRecord.end(); pIterMsgRecord++)
		{
			CDlgMsgRecord * pDlgDialog = pIterMsgRecord->second;
			pDlgDialog->DestroyWindow();
			//delete pDlgDialog;
		}
		lock.unlock();
		m_pMsgRecord.clear();
	}
	{
		boost::mutex::scoped_lock lock(m_pResourceMgr.mutex());
		CLockMap<eb::bigint, CDlgResourceMgr*>::const_iterator pIterMsgRecord = m_pResourceMgr.begin();
		for (; pIterMsgRecord!=m_pResourceMgr.end(); pIterMsgRecord++)
		{
			CDlgResourceMgr * pDlgDialog = pIterMsgRecord->second;
			pDlgDialog->DestroyWindow();
			//delete pDlgDialog;
		}
		lock.unlock();
		m_pResourceMgr.clear();
	}

	while (!theApp.m_pCloseWnd.empty())
	{
		CWnd * pWnd = NULL;
		if (!theApp.m_pCloseWnd.front(pWnd))
		{
			break;
		}
		pWnd->DestroyWindow();
	}

	boost::mutex::scoped_lock lockDialog(m_pDialogList.mutex());
	CLockMap<eb::bigint, CDlgDialog::pointer>::const_iterator pIter = m_pDialogList.begin();
	for (; pIter!=m_pDialogList.end(); pIter++)
	{
		CDlgDialog::pointer pDlgDialog = pIter->second;
		//pDlgDialog->ExitRoom();
		pDlgDialog->DestroyWindow();
	}
	lockDialog.unlock();
	m_pDialogList.clear();
	SaveUserSetting();
	Gdiplus::GdiplusShutdown(theGdiplusToken);

#ifdef USES_EBCOM_TEST
	theEBSetting.Release();
	if (theEBClientCore != NULL)
	{
		theEBClientCore->EB_Logout();
		theEBClientCore->EB_UnInit();
		theEBClientCore.Release();
	}
#else
	theEBAppClient.EB_Logout();
	theEBAppClient.EB_UnInit();
#endif

	
	bo::bodb_exit(m_pBoEB);
	bo::bodb_exit(m_pBoUsers);
    CMenuXP::UninitializeHook();
	::CoUninitialize();
	return CWinApp::ExitInstance();
	//return CWinAppEx::ExitInstance();
}

CString CPOPApp::GetSettingFile(void) const
{
	return GetAppDataPath() + _T("\\setting");
}
CString CPOPApp::GetSettingIniFile(void) const
{
	return GetAppDataPath() + _T("\\setting.ini");
}
void CPOPApp::EBC_SetProfileString(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,LPCTSTR lpszValue)
{
	const CString sIniSettingFile = GetSettingIniFile();
	::WritePrivateProfileString(lpszAppName,lpszKeyName,lpszValue,sIniSettingFile);
}
CString CPOPApp::EBC_GetProfileString(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,LPCTSTR lpszDefault)
{
	const CString sIniSettingFile = GetSettingIniFile();
	TCHAR lpszBuffer[1024];
	memset(lpszBuffer,0,sizeof(lpszBuffer));
	::GetPrivateProfileString(lpszAppName,lpszKeyName,lpszDefault,lpszBuffer,sizeof(lpszBuffer),sIniSettingFile);
	return lpszBuffer;
}
void CPOPApp::EBC_SetProfileInt(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,UINT nValue)
{
	const CString sIniSettingFile = GetSettingIniFile();
	CString sValue;
	sValue.Format(_T("%d"),nValue);
	::WritePrivateProfileString(lpszAppName,lpszKeyName,sValue,sIniSettingFile);
}
UINT CPOPApp::EBC_GetProfileInt(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,UINT nDefault)
{
	const CString sIniSettingFile = GetSettingIniFile();
	return ::GetPrivateProfileInt(lpszAppName,lpszKeyName,nDefault,sIniSettingFile);
}
HICON CPOPApp::GetFileHIcon(const std::string& sFilePath)
{
	HICON result = NULL;
	std::string sFileName;
	std::string sFileExt;
	libEbc::GetFileExt(sFilePath,sFileName,sFileExt);
	if (!sFileExt.empty())
	{
		if (m_pFileHIcon.find(sFileExt,result))
		{
			return result;
		}
	}
	SHFILEINFO sfi; 
	ZeroMemory(&sfi,sizeof(sfi)); 
	DWORD ret = SHGetFileInfo(sFilePath.c_str(),
		FILE_ATTRIBUTE_NORMAL, 
		&sfi, 
		sizeof(sfi), 
		SHGFI_USEFILEATTRIBUTES|SHGFI_ICON);
	if (ret == 1)
	{
		result = sfi.hIcon;
		if (!sFileExt.empty())
		{
			m_pFileHIcon.insert(sFileExt,result);
		}
	}
	return result;
}
void CPOPApp::ClearSubscribeSelectInfo(void)
{
	m_pSubscribeFuncList.clear();
	m_nSelectCallId = 0;
	m_nSelectUserId = 0;
	m_sSelectAccount = "";
	m_nSelectGroupId = 0;
}

//std::string CPOPApp::GetFullAccount(const char* lpszAccount) const
//{
//	std::string sAccount(lpszAccount);
//	std::string::size_type find = sAccount.find("@");
//	if (find == tstring::npos)
//	{
//		sAccount.append("@");
//		sAccount.append(theSetting.GetServerAddress());
//	}
//	return sAccount;
//}
//int CPOPApp::GetManagerLevel(const char* sGroupCode) const
//{
//	EB_MemberInfo pMyEmployeeInfo;
//	if (!theEBAppClient.EB_GetMyMemberInfo(&pMyEmployeeInfo,sGroupCode))
//		return 0;
//	return pMyEmployeeInfo.m_nManagerLevel;
//}

void CPOPApp::GetScreenSize(int & pWidth, int & pHeight)
{
	pWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
	//int m_nScreenHeight = GetSystemMetrics(SM_CYSCREEN); //屏幕高度
	pHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	pHeight += GetSystemMetrics(SM_CYCAPTION);
}

void CPOPApp::ReadUserSetting(void)
{
	TCHAR szAppFullPath[MAX_PATH];
	sprintf(szAppFullPath, _T("%s\\setting.ini"), m_sUserMainPath);
	m_cfDefault.bCharSet = GetPrivateProfileInt( _T("FONT"),_T("CharSet"),134,szAppFullPath);
	m_cfDefault.dwMask  = GetPrivateProfileInt(_T("FONT"),_T("Mask"),3892314127,szAppFullPath);
	m_cfDefault.dwEffects = GetPrivateProfileInt(_T("FONT"),_T("Effects"),0,szAppFullPath);
	m_cfDefault.yHeight = GetPrivateProfileInt(_T("FONT"),_T("Height"),240,szAppFullPath);
	m_cfDefault.yOffset = GetPrivateProfileInt(_T("FONT"),_T("Offset"),0,szAppFullPath);
	m_cfDefault.crTextColor = GetPrivateProfileInt(_T("FONT"),_T("Color"),0,szAppFullPath);
	m_cfDefault.bPitchAndFamily = GetPrivateProfileInt(_T("FONT"),_T("PitchAndFamily"),2,szAppFullPath);
	m_cfDefault.cbSize = GetPrivateProfileInt(_T("FONT"),_T("Size"),0  ,szAppFullPath);
	GetPrivateProfileString(_T("FONT"),_T("FaceName"),_T("宋体"), m_cfDefault.szFaceName ,LF_FACESIZE,szAppFullPath);	
}

inline BOOL WritePrivateProfileInt(
    __in_opt LPCSTR lpAppName,
    __in_opt LPCSTR lpKeyName,
    __in_opt int nValue,
    __in_opt LPCSTR lpFileName
    )
{
	TCHAR lpString[20];
	sprintf(lpString, _T("%d"), nValue);
	return ::WritePrivateProfileString(lpAppName, lpKeyName, lpString, lpFileName);
}

void CPOPApp::SaveUserSetting(void)
{
	TCHAR szAppFullPath[MAX_PATH];
	sprintf(szAppFullPath, _T("%s\\setting.ini"), m_sUserMainPath);
	WritePrivateProfileInt( _T("FONT"),_T("CharSet"),m_cfDefault.bCharSet,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Mask"),m_cfDefault.dwMask,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Effects"),m_cfDefault.dwEffects,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Height"),m_cfDefault.yHeight,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Offset"),m_cfDefault.yOffset,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Color"),m_cfDefault.crTextColor,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("PitchAndFamily"),m_cfDefault.bPitchAndFamily,szAppFullPath);
	WritePrivateProfileInt(_T("FONT"),_T("Size"),m_cfDefault.cbSize,szAppFullPath);
	WritePrivateProfileString(_T("FONT"),_T("FaceName"),m_cfDefault.szFaceName,szAppFullPath);	
}

void CPOPApp::ShowImageWindow(bool bShow,CImageSelectCallback* pCallback,LPRECT lpRect)
{
	if (m_pDlgEmotionSelect==NULL)
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		//CWnd * pParent = this;
		m_pDlgEmotionSelect = new CDlgEmotionSelect(pParent);
		m_pDlgEmotionSelect->Create(CDlgEmotionSelect::IDD,pParent);
	}else
	{
		m_pDlgEmotionSelect->ShowImage();
	}
	m_pDlgEmotionSelect->SetCallback(pCallback);
	if (lpRect != NULL)
		m_pDlgEmotionSelect->MoveWindow(lpRect);
	if (bShow)
	{
		m_pDlgEmotionSelect->ShowWindow(SW_SHOW);
		m_pDlgEmotionSelect->SetForegroundWindow();
	}else
	{
		m_pDlgEmotionSelect->ShowWindow(SW_HIDE);
	}
	//m_pDlgEmotionSelect->SetCircle();
}

void CPOPApp::RunEBSC(const CString& sParameter)
{
	CString sEBDCFile = theApp.GetAppPath()+_T("\\ebsc.exe");
	ShellExecute(NULL, "open", sEBDCFile, sParameter, theApp.GetAppPath(), SW_SHOW);
}

void CPOPApp::InvalidateParentRect(const CWnd* pWnd)
{
	CRect rect;
	pWnd->GetWindowRect(&rect);
	pWnd->GetParent()->ScreenToClient(&rect);
	pWnd->GetParent()->InvalidateRect(&rect);
}

void CPOPApp::NewEmployeeInfo(CWnd* pParent,const CTreeItemInfo * pTreeItemInfo)
{
	if (pTreeItemInfo == NULL || pTreeItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_GROUP) return;
#ifdef USES_EBCOM_TEST
	CEBString sGroupName = theEBClientCore->EB_GetGroupName(pTreeItemInfo->m_sGroupCode).GetBSTR();
	if (sGroupName.empty()) return;
	CDlgMemberInfo pDlgMemberInfo(pParent);
	pDlgMemberInfo.m_sGroupCode = pTreeItemInfo->m_sGroupCode;
	pDlgMemberInfo.m_sGroupName = sGroupName.c_str();
	if (pDlgMemberInfo.DoModal() == IDOK)
	{
		CIEB_MemberInfo* pNewMemberInfo = new CIEB_MemberInfo();
		pNewMemberInfo->GroupCode = pTreeItemInfo->m_sGroupCode;
		pNewMemberInfo->MemberAccount = (LPCTSTR)pDlgMemberInfo.m_sMemberAccount;
		pNewMemberInfo->UserName = (LPCTSTR)pDlgMemberInfo.m_sUserName;
		pNewMemberInfo->JobTitle = (LPCTSTR)pDlgMemberInfo.m_sJobTitle;
		pNewMemberInfo->JobPosition = pDlgMemberInfo.m_nJobPosition;
		pNewMemberInfo->CellPhone = (LPCTSTR)pDlgMemberInfo.m_sCellPhone;
		pNewMemberInfo->WorkPhone = (LPCTSTR)pDlgMemberInfo.m_sWorkPhone;
		pNewMemberInfo->Email = (LPCTSTR)pDlgMemberInfo.m_sEmail;
		pNewMemberInfo->Fax = (LPCTSTR)pDlgMemberInfo.m_sFax;
		pNewMemberInfo->Address = (LPCTSTR)pDlgMemberInfo.m_sAddress;
		pNewMemberInfo->Description = (LPCTSTR)pDlgMemberInfo.m_sDescription;
		pNewMemberInfo->Gender = (EB_GENDER_TYPE)pDlgMemberInfo.m_nGender;
		pNewMemberInfo->Birthday = pDlgMemberInfo.m_nBirthday;
		CComPtr<IEB_MemberInfo> pInterface;
		pNewMemberInfo->QueryInterface(__uuidof(IEB_MemberInfo),(void**)&pInterface);
		theEBClientCore->EB_EditMember(pInterface);
	}
#else
	CEBString sGroupName;
	if (!theEBAppClient.EB_GetGroupName(pTreeItemInfo->m_sGroupCode,sGroupName)) return;
	CDlgMemberInfo pDlgMemberInfo(pParent);
	pDlgMemberInfo.m_sGroupCode = pTreeItemInfo->m_sGroupCode;
	pDlgMemberInfo.m_sGroupName = sGroupName.c_str();
	if (pDlgMemberInfo.DoModal() == IDOK)
	{
		EB_MemberInfo pEditPopMemberInfo(pTreeItemInfo->m_sGroupCode, 0);
		pEditPopMemberInfo.m_sMemberAccount = (LPCTSTR)pDlgMemberInfo.m_sMemberAccount;
		pEditPopMemberInfo.m_sUserName = (LPCTSTR)pDlgMemberInfo.m_sUserName;
		pEditPopMemberInfo.m_sJobTitle = (LPCTSTR)pDlgMemberInfo.m_sJobTitle;
		pEditPopMemberInfo.m_nJobPosition = pDlgMemberInfo.m_nJobPosition;
		pEditPopMemberInfo.m_sCellPhone = (LPCTSTR)pDlgMemberInfo.m_sCellPhone;
		pEditPopMemberInfo.m_sWorkPhone = (LPCTSTR)pDlgMemberInfo.m_sWorkPhone;
		pEditPopMemberInfo.m_sEmail = (LPCTSTR)pDlgMemberInfo.m_sEmail;
		pEditPopMemberInfo.m_sFax = (LPCTSTR)pDlgMemberInfo.m_sFax;
		pEditPopMemberInfo.m_sAddress = (LPCTSTR)pDlgMemberInfo.m_sAddress;
		pEditPopMemberInfo.m_sDescription = (LPCTSTR)pDlgMemberInfo.m_sDescription;
		pEditPopMemberInfo.m_nGender = (EB_GENDER_TYPE)pDlgMemberInfo.m_nGender;
		pEditPopMemberInfo.m_nBirthday = pDlgMemberInfo.m_nBirthday;
		theEBAppClient.EB_EditMember(&pEditPopMemberInfo);
	}
#endif
}

#ifdef USES_EBCOM_TEST
void CPOPApp::EditEmployeeInfo(CWnd* pParent,IEB_MemberInfo* pMemberInfo)
{
	if (pMemberInfo==NULL) return;
	const CEBString sGroupName = theEBClientCore->EB_GetGroupName(pMemberInfo->GroupCode).GetBSTR();
	if (sGroupName.empty()) return;
	CDlgMemberInfo pDlgMemberInfo(pParent);
	pDlgMemberInfo.m_sGroupCode = pMemberInfo->GroupCode;
	pDlgMemberInfo.m_sMemberCode = pMemberInfo->MemberCode;
	pDlgMemberInfo.m_sMemberAccount = pMemberInfo->MemberAccount.GetBSTR();
	pDlgMemberInfo.m_sUserName = pMemberInfo->UserName.GetBSTR();
	pDlgMemberInfo.m_sGroupName = sGroupName.c_str();
	pDlgMemberInfo.m_sJobTitle = pMemberInfo->JobTitle.GetBSTR();
	pDlgMemberInfo.m_nJobPosition = pMemberInfo->JobPosition;
	pDlgMemberInfo.m_sCellPhone = pMemberInfo->CellPhone.GetBSTR();
	pDlgMemberInfo.m_sWorkPhone = pMemberInfo->WorkPhone.GetBSTR();
	pDlgMemberInfo.m_sEmail = pMemberInfo->Email.GetBSTR();
	pDlgMemberInfo.m_sFax = pMemberInfo->Fax.GetBSTR();
	pDlgMemberInfo.m_sAddress = pMemberInfo->Address.GetBSTR();
	pDlgMemberInfo.m_sDescription = pMemberInfo->Description.GetBSTR();
	pDlgMemberInfo.m_nGender = pMemberInfo->Gender;
	pDlgMemberInfo.m_nBirthday = pMemberInfo->Birthday;
	pDlgMemberInfo.m_pMemberInfo = pMemberInfo;
	//pDlgMemberInfo.m_sHeadResourceFile = pMemberInfo->HeadResourceFile.c_str();
	if (pDlgMemberInfo.DoModal() == IDOK)
	{
		EB_MemberInfo pEditPopMemberInfo(pMemberInfo);
		pEditPopMemberInfo.m_sMemberAccount = (LPCTSTR)pDlgMemberInfo.m_sMemberAccount;
		pEditPopMemberInfo.m_sUserName = (LPCTSTR)pDlgMemberInfo.m_sUserName;
		pEditPopMemberInfo.m_sJobTitle = (LPCTSTR)pDlgMemberInfo.m_sJobTitle;
		pEditPopMemberInfo.m_nJobPosition = pDlgMemberInfo.m_nJobPosition;
		pEditPopMemberInfo.m_sCellPhone = (LPCTSTR)pDlgMemberInfo.m_sCellPhone;
		pEditPopMemberInfo.m_sWorkPhone = (LPCTSTR)pDlgMemberInfo.m_sWorkPhone;
		pEditPopMemberInfo.m_sEmail = (LPCTSTR)pDlgMemberInfo.m_sEmail;
		pEditPopMemberInfo.m_sFax = (LPCTSTR)pDlgMemberInfo.m_sFax;
		pEditPopMemberInfo.m_sAddress = (LPCTSTR)pDlgMemberInfo.m_sAddress;
		pEditPopMemberInfo.m_sDescription = (LPCTSTR)pDlgMemberInfo.m_sDescription;
		pEditPopMemberInfo.m_nGender = (EB_GENDER_TYPE)pDlgMemberInfo.m_nGender;
		pEditPopMemberInfo.m_nBirthday = pDlgMemberInfo.m_nBirthday;
		CIEB_MemberInfo * pIEBMemberInfo = new CIEB_MemberInfo(pEditPopMemberInfo);
		CComPtr<IEB_MemberInfo> pOutInterface;
		pIEBMemberInfo->QueryInterface(__uuidof(IEB_MemberInfo),(void**)&pOutInterface);
		theEBClientCore->EB_EditMember(pOutInterface);
	}
}
#else
void CPOPApp::EditEmployeeInfo(CWnd* pParent,const EB_MemberInfo* pMemberInfo)
{
	if (pMemberInfo==NULL) return;
	std::string sDepartmentName;
	if (!theEBAppClient.EB_GetGroupName(pMemberInfo->m_sGroupCode,sDepartmentName)) return;
	CDlgMemberInfo pDlgMemberInfo(pParent);
	pDlgMemberInfo.m_sGroupCode = pMemberInfo->m_sGroupCode;
	pDlgMemberInfo.m_sMemberCode = pMemberInfo->m_sMemberCode;
	pDlgMemberInfo.m_sMemberAccount = pMemberInfo->m_sMemberAccount.c_str();
	pDlgMemberInfo.m_sUserName = pMemberInfo->m_sUserName.c_str();
	pDlgMemberInfo.m_sGroupName = sDepartmentName.c_str();
	pDlgMemberInfo.m_sJobTitle = pMemberInfo->m_sJobTitle.c_str();
	pDlgMemberInfo.m_nJobPosition = pMemberInfo->m_nJobPosition;
	pDlgMemberInfo.m_sCellPhone = pMemberInfo->m_sCellPhone.c_str();
	pDlgMemberInfo.m_sWorkPhone = pMemberInfo->m_sWorkPhone.c_str();
	pDlgMemberInfo.m_sEmail = pMemberInfo->m_sEmail.c_str();
	pDlgMemberInfo.m_sFax = pMemberInfo->m_sFax.c_str();
	pDlgMemberInfo.m_sAddress = pMemberInfo->m_sAddress.c_str();
	pDlgMemberInfo.m_sDescription = pMemberInfo->m_sDescription.c_str();
	pDlgMemberInfo.m_nGender = pMemberInfo->m_nGender;
	pDlgMemberInfo.m_nBirthday = pMemberInfo->m_nBirthday;
	pDlgMemberInfo.m_pMemberInfo = pMemberInfo;
	//pDlgMemberInfo.m_sHeadResourceFile = pMemberInfo->m_sHeadResourceFile.c_str();
	if (pDlgMemberInfo.DoModal() == IDOK)
	{
		EB_MemberInfo pEditPopMemberInfo(pMemberInfo);
		pEditPopMemberInfo.m_sUserName = (LPCTSTR)pDlgMemberInfo.m_sUserName;
		pEditPopMemberInfo.m_sJobTitle = (LPCTSTR)pDlgMemberInfo.m_sJobTitle;
		pEditPopMemberInfo.m_nJobPosition = pDlgMemberInfo.m_nJobPosition;
		pEditPopMemberInfo.m_sCellPhone = (LPCTSTR)pDlgMemberInfo.m_sCellPhone;
		pEditPopMemberInfo.m_sWorkPhone = (LPCTSTR)pDlgMemberInfo.m_sWorkPhone;
		pEditPopMemberInfo.m_sEmail = (LPCTSTR)pDlgMemberInfo.m_sEmail;
		pEditPopMemberInfo.m_sFax = (LPCTSTR)pDlgMemberInfo.m_sFax;
		pEditPopMemberInfo.m_sAddress = (LPCTSTR)pDlgMemberInfo.m_sAddress;
		pEditPopMemberInfo.m_sDescription = (LPCTSTR)pDlgMemberInfo.m_sDescription;
		pEditPopMemberInfo.m_nGender = (EB_GENDER_TYPE)pDlgMemberInfo.m_nGender;
		pEditPopMemberInfo.m_nBirthday = pDlgMemberInfo.m_nBirthday;
		theEBAppClient.EB_EditMember(&pEditPopMemberInfo);
	}
}
#endif

bool CPOPApp::IsLogonVisitor(void) const
{
#ifdef USES_EBCOM_TEST
	return theEBClientCore != NULL && theEBClientCore->EB_IsLogonVisitor==VARIANT_TRUE;
#else
	return theEBAppClient.EB_IsLogonVisitor();
#endif

}

int CALLBACK CPOPApp::TreeCmpFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	const CTreeItemInfo * pItemInfo1 = (const CTreeItemInfo*)lParam1;
	const CTreeItemInfo * pItemInfo2 = (const CTreeItemInfo*)lParam2;
	if (pItemInfo1==NULL || pItemInfo2==NULL) return 1;
	if (pItemInfo1->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP && pItemInfo2->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
	{
		// 群（部门）比较，用名称排序
		return strcmp(pItemInfo1->m_sName.c_str(), pItemInfo2->m_sName.c_str());
	}else if (pItemInfo1->m_nItemType==CTreeItemInfo::ITEM_TYPE_MEMBER && pItemInfo2->m_nItemType==CTreeItemInfo::ITEM_TYPE_MEMBER)
	{
		// 员工比较，先比较在线状态
		if (pItemInfo1->m_dwItemData==pItemInfo2->m_dwItemData)
		{
			// 相同在线状态，比较名称
			return strcmp(pItemInfo1->m_sName.c_str(), pItemInfo2->m_sName.c_str());
		}else if (pItemInfo1->m_dwItemData==EB_LINE_STATE_ONLINE)
		{
			// 第一个在线，不用换位置
			return -1;
		}else if (pItemInfo2->m_dwItemData==EB_LINE_STATE_ONLINE)
		{
			// 第二个在线，对调位置
			return 1;
		}else if (pItemInfo1->m_dwItemData==EB_LINE_STATE_OFFLINE)
		{
			// 第一个离线，对调位置
			return 1;
		}else if (pItemInfo2->m_dwItemData==EB_LINE_STATE_OFFLINE)
		{
			// 第二个离线，不用换位置
			return -1;
		}else if (pItemInfo1->m_dwItemData!=EB_LINE_STATE_OFFLINE)
		{
			// 第一个不是离线，不用换位置
			return -1;
		}else if (pItemInfo2->m_dwItemData!=EB_LINE_STATE_OFFLINE)
		{
			// 第二个不是离线，对调位置
			return 1;
		}
		return strcmp(pItemInfo1->m_sName.c_str(), pItemInfo2->m_sName.c_str());
	}else if (pItemInfo1->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
	{
		// 第一个是部门，排前面
		return -1;
	}else if (pItemInfo2->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
	{
		// 第二个是部门，对调位置
		return -1;
	}
	return 0;
}

void CPOPApp::SetLineState2Db(EB_USER_LINE_STATE nLineState)
{
	if (m_pBoEB->use("eb"))
	{
		CString sSql;
		sSql.Format(_T("update user_login_record_t set linestate=%d where account='%s'"),nLineState,theApp.GetLogonAccount());
		m_pBoEB->execsql(sSql);
		m_pBoEB->close();
	}
}

void CPOPApp::DeleteDbRecord(eb::bigint sId, bool bIsAccount)
{
	// 先删除图片，语音，文件...
	CString sSql;
	if (bIsAccount)
	{
		sSql.Format(_T("select msg_name FROM msg_record_t WHERE dep_code=0 AND (from_uid=%lld OR to_uid=%lld) AND msg_type>=%d"),sId,sId,MRT_JPG);
	}else
	{
		sSql.Format(_T("select msg_name FROM msg_record_t WHERE dep_code=%lld AND msg_type>=%d"),sId,MRT_JPG);
	}
	bo::PRESULTSET pResltSet = NULL;
	int ret = m_pBoUsers->execsql(sSql, &pResltSet);
	CString sUserImagePath(GetUserImagePath());
	sUserImagePath.MakeLower();
	if (pResltSet != NULL)
	{
		USES_CONVERSION;
		//CString sMsgTime;
		time_t nLocalMsgTime = 0;
		for (int i=0; i<pResltSet->rscount; i++)
		{
			CString sMsgName = pResltSet->rsvalues[i]->fieldvalues[0]->v.varcharVal.size>0?pResltSet->rsvalues[i]->fieldvalues[0]->v.varcharVal.buffer:"";
			sMsgName.MakeLower();
			// 判断是临时图片，语音，文件目录
			if (sMsgName.Find(sUserImagePath)==0)
			{
				// 删除临时目录图片
				DeleteFile(sMsgName);
			}
		}
		bo::bodb_free(pResltSet);
	}
	// 删除数据
	if (bIsAccount)
	{
		sSql.Format(_T("DELETE FROM msg_record_t WHERE dep_code=0 AND (from_uid=%lld OR to_uid=%lld)"),sId,sId);
	}else
	{
		sSql.Format(_T("DELETE FROM msg_record_t WHERE dep_code=%lld"),sId);
	}
	m_pBoUsers->execsql(sSql);
}

void CPOPApp::OpenGroupShareWindow(eb::bigint sGroupCode,const CWnd* pWndLayout)
{
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (!m_pResourceMgr.find(sGroupCode,pDlgResourceMgr))
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		pDlgResourceMgr = new CDlgResourceMgr(pParent);
		pDlgResourceMgr->Create(CDlgResourceMgr::IDD,pParent);
		m_pResourceMgr.insert(sGroupCode,pDlgResourceMgr);
	}
	CRect rect;
	pWndLayout->GetWindowRect(&rect);
	rect.left = rect.right-100;
	rect.right = rect.left + 520;
	pDlgResourceMgr->MoveWindow(&rect);
	pDlgResourceMgr->SetCircle();
	pDlgResourceMgr->LoadGroupResource(sGroupCode);
}

void CPOPApp::OpenMyShareWindow(const CWnd* pWndLayout)
{
#ifdef USES_EBCOM_TEST
	if (theEBClientCore->EB_IsLogoned == VARIANT_FALSE || theApp.IsLogonVisitor())
	{
		return;
	}
#else
	if (!theEBAppClient.EB_IsLogoned() || theApp.IsLogonVisitor()) return;
#endif
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	const eb::bigint sLogonAccount = theApp.GetLogonUserId();
	if (!m_pResourceMgr.find(sLogonAccount,pDlgResourceMgr))
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		pDlgResourceMgr = new CDlgResourceMgr(pParent);
		pDlgResourceMgr->Create(CDlgResourceMgr::IDD,pParent);
		m_pResourceMgr.insert(sLogonAccount,pDlgResourceMgr);
	}
	CRect rect;
	pWndLayout->GetWindowRect(&rect);
	rect.left = rect.right-100;
	rect.right = rect.left + 520;
	pDlgResourceMgr->MoveWindow(&rect);
	pDlgResourceMgr->SetCircle();
	pDlgResourceMgr->LoadMyResource();
}
void CPOPApp::OpenSubscribeFuncWindow(const EB_SubscribeFuncInfo& pSubscribeFuncInfo)
{
	//CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
	//CDlgFuncWindow * pFuncWindow = new CDlgFuncWindow(pParent,true);
	//pFuncWindow->m_bBroadcastMsg = true;
	//pFuncWindow->m_sTitle = "aaaa";
	//pFuncWindow->m_sFuncUrl = "adsfaf";
	//pFuncWindow->m_nWidth = 250;
	//pFuncWindow->m_nHeight = 180;
	//pFuncWindow->Create(CDlgFuncWindow::IDD,pParent);
	//pFuncWindow->ShowWindow(SW_SHOW);
	//return;
#ifdef USES_EBCOM_TEST
	const CEBString sFuncUrl = theEBClientCore->EB_GetSubscribeFuncUrl(pSubscribeFuncInfo.m_nSubscribeId,m_nSelectCallId,m_nSelectUserId,m_sSelectAccount,m_nSelectGroupId).GetBSTR();
#else
	const CEBString sFuncUrl = theEBAppClient.EB_GetSubscribeFuncUrl(pSubscribeFuncInfo.m_nSubscribeId,m_nSelectCallId,m_nSelectUserId,m_sSelectAccount,m_nSelectGroupId);
#endif
	if (!sFuncUrl.empty())
	{
		//AfxMessageBox(sFuncUrl.c_str());
		if (pSubscribeFuncInfo.m_nFunctionMode == EB_FUNC_MODE_WINDOW)
		{
			CDlgFuncWindow pFuncWindow;
			pFuncWindow.m_sTitle = pSubscribeFuncInfo.m_sFunctionName;
			pFuncWindow.m_sFuncUrl = sFuncUrl;
			pFuncWindow.m_nWidth = pSubscribeFuncInfo.m_nWindowWidth;
			pFuncWindow.m_nHeight = pSubscribeFuncInfo.m_nWindowHeight;
			pFuncWindow.DoModal();
		}else if (pSubscribeFuncInfo.m_nFunctionMode == EB_FUNC_MODE_MODAL)
		{
			CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
			CDlgFuncWindow * pFuncWindow = new CDlgFuncWindow(pParent,true);
			pFuncWindow->m_sTitle = pSubscribeFuncInfo.m_sFunctionName;
			pFuncWindow->m_sFuncUrl = sFuncUrl;
			pFuncWindow->m_nWidth = pSubscribeFuncInfo.m_nWindowWidth;
			pFuncWindow->m_nHeight = pSubscribeFuncInfo.m_nWindowHeight;
			pFuncWindow->Create(CDlgFuncWindow::IDD,pParent);
			pFuncWindow->ShowWindow(SW_SHOW);
		}else
		{
			ShellExecute(NULL,  "open", sFuncUrl.c_str(), NULL, NULL, SW_SHOW);
		}
	}
}

bool CPOPApp::GetItemImage(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem,Gdiplus::Image*& pImage1,Gdiplus::Image*& pImage2,int& pState) const
{
	const CTreeItemInfo * pTreeItemInfo = (const CTreeItemInfo*)pTreeCtrl.GetItemData(hItem);
	if (pTreeItemInfo == NULL) return false;
	if (pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_ENTERPRISE)
	{
		pImage1 = m_imageDefaultOrg->Clone();
		return true;
	}
	else if (pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
	{
		return false;
	}
	else if (pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_CONTACT)
	{
		CEBString sImagePath;
		bool bIsMemberAccount = false;
		EB_USER_LINE_STATE pOutLineState = EB_LINE_STATE_UNKNOWN;
#ifdef USES_EBCOM_TEST
		CComPtr<IEB_MemberInfo> pMemberInfo = theEBClientCore->EB_GetMemberInfoByAccount(0,pTreeItemInfo->m_sAccount.c_str());
		if (pMemberInfo != NULL)
		{
			bIsMemberAccount = true;
			pOutLineState = (EB_USER_LINE_STATE)pMemberInfo->LineState;
			const CEBString sMemberHeadFile = pMemberInfo->HeadResourceFile.GetBSTR();
			if (PathFileExists(sMemberHeadFile.c_str()))
			{
				sImagePath = sMemberHeadFile;
			}
		}
#else
		EB_MemberInfo pMemberInfo;
		if (theEBAppClient.EB_GetMemberInfoByAccount2(&pMemberInfo,pTreeItemInfo->m_sAccount.c_str()))
		{
			bIsMemberAccount = true;
			pOutLineState = pMemberInfo.m_nLineState;
			if (PathFileExists(pMemberInfo.m_sHeadResourceFile.c_str()))
			{
				sImagePath = pMemberInfo.m_sHeadResourceFile;
			}
		}
#endif
		if (bIsMemberAccount)
		{
			switch (pOutLineState)
			{
			case EB_LINE_STATE_UNKNOWN:
			case EB_LINE_STATE_OFFLINE:
				pState = 0;
				break;
			case EB_LINE_STATE_ONLINE:
				break;
			case EB_LINE_STATE_BUSY:
				{
					pImage2 = m_imageStateBusy->Clone();
				}break;
			case EB_LINE_STATE_AWAY:
				{
					pImage2 = m_imageStateAway->Clone();
				}break;
			default:
				break;
			}
			if (!sImagePath.empty())
			{
				USES_CONVERSION;
				pImage1 = new Gdiplus::Image((const WCHAR*)T2W(sImagePath.c_str()));
			}else
			{
				pImage1 = m_imageDefaultMember->Clone();
			}
		}else
		{
			pState = 0;	// 非成员，全部用灰色
			pImage1 = m_imageDefaultContact->Clone();
		}
		return true;
	}
	else if (pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_MEMBER)
	{
		CEBString sImagePath;
		EB_USER_LINE_STATE pOutLineState = EB_LINE_STATE_UNKNOWN;
#ifdef USES_EBCOM_TEST
		CComPtr<IEB_MemberInfo> pMemberInfo = theEBClientCore->EB_GetMemberInfoByMemberCode(pTreeItemInfo->m_sMemberCode);
		if (pMemberInfo != NULL)
		{
			pOutLineState = (EB_USER_LINE_STATE)pMemberInfo->LineState;
			//sDescription = pMemberInfo->Description.GetBSTR();
			const CEBString sMemberHeadFile = pMemberInfo->HeadResourceFile.GetBSTR();
			if (PathFileExists(sMemberHeadFile.c_str()))
			{
				sImagePath = sMemberHeadFile;
			}
		}
#else
		EB_MemberInfo pMemberInfo;
		if (theEBAppClient.EB_GetMemberInfoByMemberCode(&pMemberInfo,pTreeItemInfo->m_sMemberCode))
		{
			//sDescription = pMemberInfo.m_sDescription;
			pOutLineState = pMemberInfo.m_nLineState;
			if (PathFileExists(pMemberInfo.m_sHeadResourceFile.c_str()))
			{
				sImagePath = pMemberInfo.m_sHeadResourceFile;
			}
		}
#endif
		switch (pOutLineState)
		{
		case EB_LINE_STATE_UNKNOWN:
		case EB_LINE_STATE_OFFLINE:
			pState = 0;
			break;
		case EB_LINE_STATE_ONLINE:
			break;
		case EB_LINE_STATE_BUSY:
			{
				pImage2 = m_imageStateBusy->Clone();
			}break;
		case EB_LINE_STATE_AWAY:
			{
				pImage2 = m_imageStateAway->Clone();
			}break;
		default:
			break;
		}
		if (!sImagePath.empty())
		{
			USES_CONVERSION;
			pImage1 = new Gdiplus::Image((const WCHAR*)T2W(sImagePath.c_str()));
		}else
		{
			pImage1 = m_imageDefaultMember->Clone();
			//return libEbc::ImageFromIDResource(IDB_PNG_DEFAULT_MEMBER,_T("png"),pImage1)?true:false;
		}
		return true;
	}
	return false;
}

bool CPOPApp::GetItemDrawOpenClose(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem) const
{
	const CTreeItemInfo * pTreeItemInfo = (const CTreeItemInfo*)pTreeCtrl.GetItemData(hItem);
	if (pTreeItemInfo == NULL) return false;
	return pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP;// || pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_ENTERPRISE;
}
