// DlgFuncWindow.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgFuncWindow.h"

#define WINDOW_TITLE_HEIGHT			30
#define TIMER_REFRESH_BROWSER		0x100
const double theBackColor = 0.96;

// CDlgFuncWindow dialog

IMPLEMENT_DYNAMIC(CDlgFuncWindow, CEbDialogBase)

CDlgFuncWindow::CDlgFuncWindow(CWnd* pParent /*=NULL*/,bool bDeleteThis)
	: CEbDialogBase(CDlgFuncWindow::IDD, pParent)
	, m_bDeleteThis(bDeleteThis)
{
	m_bBroadcastMsg = false;
	m_pBroadcastView = NULL;
	m_nWidth = 420;
	m_nHeight = 380;
}

CDlgFuncWindow::~CDlgFuncWindow()
{
}

void CDlgFuncWindow::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_MAX, m_btnMax);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	DDX_Control(pDX, IDC_EXPLORER1, m_pExplorer);
}


BEGIN_MESSAGE_MAP(CDlgFuncWindow, CEbDialogBase)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CDlgFuncWindow::OnBnClickedButtonMin)
	ON_BN_CLICKED(IDC_BUTTON_MAX, &CDlgFuncWindow::OnBnClickedButtonMax)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDlgFuncWindow::OnBnClickedButtonClose)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_TIMER()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CDlgFuncWindow, CEbDialogBase)
	//{{AFX_EVENTSINK_MAP(CDlgFuncWindow)
	ON_EVENT(CDlgFuncWindow, IDC_EXPLORER1, 250 /* BeforeNavigate2 */, OnBeforeNavigate2IeLobby, VTS_DISPATCH VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBOOL)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()


// CDlgFuncWindow message handlers

BOOL CDlgFuncWindow::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	ModifyStyle(0, WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SYSMENU);

	if (m_bBroadcastMsg)
	{
		m_btnMin.ShowWindow(SW_HIDE);
		m_btnMax.ShowWindow(SW_HIDE);
	}else
	{
		m_btnMin.SetAutoSize(false);
		m_btnMin.Load(IDB_PNG_MIN);
		m_btnMin.SetToolTipText(_T("最小化"));
		m_btnMax.SetAutoSize(false);
		m_btnMax.Load(IDB_PNG_MAX);
		m_btnMax.SetToolTipText(_T("最大化"));
	}
	m_btnClose.SetAutoSize(false);
	m_btnClose.Load(IDB_PNG_CLOSE);
	m_btnClose.SetToolTipText(_T("关闭"));

	int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
	int m_nScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	CRect rect;
	if (m_bBroadcastMsg)
	{
		m_pExplorer.ShowWindow(SW_HIDE);
		m_pBroadcastView = new CBroadcastView(this);

		float h,s,l;
		::RGBtoHSL(GetRValue(theApp.GetMainColor()), GetGValue(theApp.GetMainColor()), GetBValue(theApp.GetMainColor()),&h,&s,&l);
		COLORREF color2 = ::HSLtoRGB(h, s, theBackColor);
		m_pBroadcastView->m_strBgColor.Format(_T("#%02x%02x%02x"),GetRValue(color2), GetGValue(color2), GetBValue(color2));
		if ( !m_pBroadcastView->Create(NULL, NULL, WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN, CRect(0,0,0,0), this, 0x146, NULL))	// 176
		{
			delete m_pBroadcastView;
			m_pBroadcastView = NULL;
			AfxThrowResourceException();
			return FALSE;
		}
		rect.right = m_nScreenWidth-10;
		rect.bottom = m_nScreenHeight;
		rect.left = rect.right-m_nWidth;
		rect.top = rect.bottom-m_nHeight;
		m_pBroadcastView->WriteBlock(m_sFuncUrl.c_str());
		m_pBroadcastView->SetWindowPos(&m_btnClose,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
		SetWindowPos(&CWnd::wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}else
	{
		//m_pExplorer.ModifyStyle(WS_BORDER,0);
		m_pExplorer.ModifyStyle(0, WS_CLIPSIBLINGS|WS_CLIPCHILDREN);
		m_pExplorer.Navigate(m_sFuncUrl.c_str(),NULL,NULL,NULL,NULL);
		if (m_bDeleteThis)
		{
			m_nScreenHeight += GetSystemMetrics(SM_CYCAPTION);
			this->SetSplitterBorder();
			rect.left = (m_nScreenWidth-m_nWidth)/2;
			rect.top = (m_nScreenHeight-m_nHeight)/2;
		}else
		{
			this->GetWindowRect(&rect);
		}
		rect.right = rect.left + m_nWidth;
		rect.bottom = rect.top + m_nHeight;
	}
	this->MoveWindow(&rect);
	this->SetForegroundWindow();
	this->SetWindowText(m_sTitle.c_str());
	//SetCircle();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgFuncWindow::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);
	int btnx = 0;
	btnx = cx-m_btnClose.GetImgWidth()-2;
	m_btnClose.MovePoint(btnx, 2);
	btnx -= m_btnMin.GetImgWidth();
	m_btnMax.MovePoint(btnx, 2);
	btnx -= m_btnMin.GetImgWidth();
	m_btnMin.MovePoint(btnx, 2);

	if (m_bBroadcastMsg && m_pBroadcastView != NULL && m_pBroadcastView->GetSafeHwnd()!=NULL)
	{
		const int const_border_height = 6;
		m_pBroadcastView->MoveWindow(const_border_height,WINDOW_TITLE_HEIGHT+const_border_height,cx-const_border_height*2,cy-WINDOW_TITLE_HEIGHT-const_border_height*2);
	}else if (m_pExplorer.GetSafeHwnd())
	{
		const int const_border_height = 20;
		m_pExplorer.MoveWindow(1,WINDOW_TITLE_HEIGHT+const_border_height,cx-2,cy-WINDOW_TITLE_HEIGHT-const_border_height*2);
	}
}

void CDlgFuncWindow::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CEbDialogBase::OnClose();
}

void CDlgFuncWindow::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	CEbDialogBase::OnOK();
}

void CDlgFuncWindow::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	CEbDialogBase::OnCancel();
}

void CDlgFuncWindow::OnPaint()
{
	if (IsIconic())
	{
		//CPaintDC dc(this); // 用于绘制的设备上下文

		//SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		//// 使图标在工作区矩形中居中
		//int cxIcon = GetSystemMetrics(SM_CXICON);
		//int cyIcon = GetSystemMetrics(SM_CYICON);
		//CRect rect;
		//GetClientRect(&rect);
		//int x = (rect.Width() - cxIcon + 1) / 2;
		//int y = (rect.Height() - cyIcon + 1) / 2;

		//// 绘制图标
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this); // 用于绘制的设备上下文
		CRect rectClient;
		this->GetClientRect(&rectClient);
		CSkinMemDC memDC(&dc, rectClient);
		this->ClearBgDrawInfo();
		this->AddBgDrawInfo(CEbBackDrawInfo(WINDOW_TITLE_HEIGHT,0.7,true));
		this->AddBgDrawInfo(CEbBackDrawInfo(0,theBackColor,false));
		this->DrawPopBg(&memDC, theApp.GetMainColor());
		Gdiplus::Graphics graphics(memDC.m_hDC);

		const FontFamily fontFamily(theFontFamily.c_str());
		const Gdiplus::Font fontEbTitle(&fontFamily, 14, FontStyleBold, UnitPixel);
		Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(38,38,38));
		Gdiplus::PointF pointTitle(7,7);
		USES_CONVERSION;
		graphics.DrawString(T2W(m_sTitle.c_str()),-1,&fontEbTitle,pointTitle,&brushEbTitle);
		//if (m_pExplorer.GetSafeHwnd())
		//{
		//	//m_pExplorer.Invalidate();
		//	m_pExplorer.ShowWindow(SW_HIDE);
		//	m_pExplorer.ShowWindow(SW_SHOW);
		//}
		//if (!sDescription.empty())
		//{
		//	pointTitle.Y += 20;
		//	graphics.DrawString(T2W(sDescription.c_str()),-1,&fontEbDescription,pointTitle,&brushEbTitle);
		//}
	}
}

void CDlgFuncWindow::OnDestroy()
{
	CEbDialogBase::OnDestroy();
	if (m_pBroadcastView)
	{
		m_pBroadcastView->DestroyWindow();
		delete m_pBroadcastView;
		m_pBroadcastView = NULL;
	}
	if (m_bDeleteThis)
		delete this;
}

void CDlgFuncWindow::OnBnClickedButtonMin()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CDlgFuncWindow::OnBnClickedButtonMax()
{
	int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
	//int m_nScreenHeight = GetSystemMetrics(SM_CYSCREEN); //屏幕高度
	int m_nScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	m_nScreenHeight += GetSystemMetrics(SM_CYCAPTION);

	static CRect theRestoreRect;
	CRect rect;
	GetWindowRect(&rect);
	if (rect.Width() == m_nScreenWidth)
	{
		this->SetToolTipText(IDC_BUTTON_MAX,_T("最大化"));
		m_btnMax.Load(IDB_PNG_MAX);
		MoveWindow(&theRestoreRect);
	}else
	{
		this->SetToolTipText(IDC_BUTTON_MAX,_T("向下还原"));
		m_btnMax.Load(IDB_PNG_RESTORE);
		theRestoreRect = rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = rect.left + m_nScreenWidth;
		rect.bottom = rect.top + m_nScreenHeight;
		MoveWindow(&rect);
	}
	SetTimer(TIMER_REFRESH_BROWSER,10,NULL);
	//PostMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}

void CDlgFuncWindow::OnBnClickedButtonClose()
{
	//PostMessage(EB_MSG_EXIT_CHAT, 0, 0);
	this->PostMessage(WM_CLOSE, 0, 0);
}

void CDlgFuncWindow::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	OnBnClickedButtonMax();
	CEbDialogBase::OnLButtonDblClk(nFlags, point);
}

void CDlgFuncWindow::OnTimer(UINT_PTR nIDEvent)
{
	if (TIMER_REFRESH_BROWSER==nIDEvent)
	{
		KillTimer(TIMER_REFRESH_BROWSER);
		if (!m_bBroadcastMsg && m_pExplorer.GetSafeHwnd())
		{
			long nReadyState = m_pExplorer.get_ReadyState();
			if (nReadyState == READYSTATE_COMPLETE)
			{
				m_pExplorer.Refresh();
				//m_pExplorer.ShowWindow(SW_HIDE);
				//m_pExplorer.ShowWindow(SW_SHOW);
			}
		}
	}
	CEbDialogBase::OnTimer(nIDEvent);
}

void CDlgFuncWindow::OnBeforeNavigate(BOOL* bCancel,LPCTSTR szURL)
{
	CString csURL(szURL);
	if (csURL.Right(1)==_T("/"))
		csURL = csURL.Left(csURL.GetLength()-1);
	//csURL.TrimRight('/');
	//AfxMessageBox(csURL);
	int pos = csURL.Find(_T("ebim-call-account:"), 0); 
	if( pos != -1 )
	{
		// ebim-call-account://[to_account]
		*bCancel = TRUE;	
		int slashPos = csURL.Find(_T("//"), pos);
		if( slashPos == -1 )
		{
			return;
		}
		const CString sToAccount = csURL.Right( csURL.GetLength()-slashPos-2 );
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_CallAccount(sToAccount);
#else
		theEBAppClient.EB_CallAccount(sToAccount);
#endif
		return;
	}
	pos = csURL.Find(_T("ebim-call-group:"), 0); 
	if( pos != -1 )
	{
		// ebim-call-group://[group_id]
		*bCancel = TRUE;	
		int slashPos = csURL.Find(_T("//"), pos);
		if( slashPos == -1 )
		{
			return;
		}
		const CString sToGroupId = csURL.Right( csURL.GetLength()-slashPos-2 );
		eb::bigint nToGroupId = eb_atoi64(sToGroupId);
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_CallGroup(nToGroupId);
#else
		theEBAppClient.EB_CallGroup(nToGroupId);
#endif
		return;
	}
	pos = csURL.Find(_T("eb-close://"), 0); 
	if( pos != -1 )
	{
		*bCancel = TRUE;	
		OnBnClickedButtonClose();
		return;
	}
	csURL.MakeLower();
	if (csURL.Find(_T("http://"))>=0 ||
		csURL.Find(_T("https://"))>=0 ||
		csURL.Find(_T("www."))>=0)
	{
		*bCancel = TRUE;	
		ShellExecute(NULL,  "open", csURL, NULL, NULL, SW_SHOW);
	}
}

void CDlgFuncWindow::OnBeforeNavigate2IeLobby(LPDISPATCH pDisp, VARIANT FAR* URL, VARIANT FAR* Flags, VARIANT FAR* TargetFrameName, VARIANT FAR* PostData, VARIANT FAR* Headers, BOOL FAR* Cancel) 
{
	OnBeforeNavigate(Cancel,(CString)URL->bstrVal);

//	pos = csURL.Find(_T("tryhide:") , 0  ); 
//	if( pos != -1 )
//	{
//		*Cancel = TRUE;	
//		int slashPos = csURL.Find(_T("//") , 0 );
//		if( slashPos == -1 )
//		{
//			WCHAR szTemp[512];
//			LoadString( g_hResourceHandle , IDS_DLG_HERB_ADDRESS_ERROR , szTemp , sizeof(szTemp)/sizeof(WCHAR) );
//			MessageBox( szTemp , theApp.m_sHomeName);
//			return;
//		}
//
//		//tryvcb://53,220.166.104.78,8002/
//		CString cmd = csURL.Right( csURL.GetLength()-slashPos-2 );
//#ifdef USES_REENTER_ROOM
//		theReEnterRoomString = cmd;
//#endif
//		EnterRoom(cmd);
//		return;
//	}


}
