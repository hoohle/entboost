#pragma once
#include "afxwin.h"


// CDlgContactInfo dialog

class CDlgContactInfo : public CEbDialogBase
{
	DECLARE_DYNAMIC(CDlgContactInfo)

public:
	CDlgContactInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgContactInfo();

	CString m_sContact;
	CString m_sGroup;
	CString m_sName;
	LONGLONG m_sPhone;
	CString m_sCompany;
	CString m_sJobTitle;
	CString m_sUrl;
	CString m_sTel;
	CString m_sFax;
	CString m_sEmail;
	CString m_sAddress;
	CString m_sDescription;

// Dialog Data
	enum { IDD = IDD_DLG_CONTACT_INFO };

protected:
	CLockMap<int,std::string> m_pContactList;	// item->contact
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CHoverEdit	m_editName;
	CHoverEdit	m_editPhone;
	CHoverEdit	m_editCompany;
	CHoverEdit	m_editJobTitle;
	CHoverEdit	m_editUrl;
	CHoverEdit	m_editTel;
	CHoverEdit	m_editFax;
	CHoverEdit	m_editEmail;
	CHoverEdit	m_editAddress;
	CHoverEdit	m_editDescription;
	CBitComboBox m_comboContact;
	CBitComboBox m_comboGroup;
	CTraButton m_btnClose;
	CTraButton m_btnOk;
	CTraButton m_btnCancel;

#ifdef USES_EBCOM_TEST
	void Data2Ctrl(IEB_ContactInfo* pPopContactInfo);
#else
	void Data2Ctrl(const EB_ContactInfo* pPopContactInfo);
#endif

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeComboContact();
	afx_msg void OnCbnEditchangeComboContact();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonClose();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
