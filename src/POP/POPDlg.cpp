// POPDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "POP.h"
#include "POPDlg.h"
#include "DlgIncomingCall.h"
#include "DlgAlertingCall.h"
#include "DlgDiaRecord.h"
#include "DlgMyEnterprise.h"
#include "DlgMyGroup.h"
#include "DlgMyContacts.h"
#include "DlgMySession.h"
#include "DlgEditInfo.h"
#include "DlgFileManager.h"
#include <sys/timeb.h>
#include "DlgMessageBox.h"
#include "EBCCallInfo.h"
#include "Core/SkinMemDC.h"
#include "MMSystem.h"
#include "DlgFuncWindow.h"

////////////////////////
bool theWantExitApp = false;

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif
#define WM_TRAYICON_NOTIFICATION WM_USER+1

#define LEN_DLG_WIDTH 274
#define LEN_DLG_HEIGHT 588

#define POS_ADIMG_LEFT 13
#define POS_ADIMG_TOP 43
#define POS_ADIMG_SIGE 60	// 128
#define POS_STA_FIRST_Y (POS_ADIMG_TOP+POS_ADIMG_SIGE+18)
#define POS_LINESTATE_LEFT (POS_ADIMG_LEFT+POS_ADIMG_SIGE+10)
#define POS_LINESTATE_TOP (POS_ADIMG_TOP-1)
//#define POS_LINESTATE_WIDTH 30

#define TIMERID_LOADINFO			0x121
//#define TIMERID_RELOGIN				0x122
#define TIMERID_LOGOUT				0x123
#define TIMERID_CHECK_CLOSE_DIALOG	0x124
#define TIMERID_CHECK_UPDATE		0x125
bool theUpdateResetTimer = false;
#define TIMERID_NEW_VERSION			0x126
#define TIMERID_SHOW_EMOTIPN		0x127

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()
//
//#ifdef USES_EBCOM
//BEGIN_DISPATCH_MAP(CPOPDlg, CEbDialogBase)
//	DISP_FUNCTION_ID(CPOPDlg,"onCallConnected2",25,onCallConnected2,VT_EMPTY,VTS_DISPATCH VTS_I4)
//	//DISP_FUNCTION_ID(CPOPDlg,"onGroupInfo2",41,onGroupInfo2,VT_EMPTY,VTS_DISPATCH VTS_BOOL)
//	//DISP_FUNCTION_ID(CPOPDlg,"onMemberInfo2",45,onMemberInfo2,VT_EMPTY,VTS_DISPATCH VTS_BOOL)
//	//DISP_FUNCTION_ID(CPOPDlg,"OnBytesRecv",0x4,OnBytesRecv,VT_EMPTY,VTS_I4)
//END_DISPATCH_MAP()
//
//BEGIN_INTERFACE_MAP(CPOPDlg, CEbDialogBase)
//	INTERFACE_PART(CPOPDlg, __uuidof(_IEBClientCoreEvents), Dispatch)
//END_INTERFACE_MAP()
//#endif

//BEGIN_CONNECTION_MAP(CPOPDlg, CEbDialogBase)
//    CONNECTION_PART(CPOPDlg, __uuidof(_IEBClientCoreEvents), SampleConnPt)
//END_CONNECTION_MAP()

// CPOPDlg 对话框

//void TrayIcon(UINT state, HWND hWnd, HICON resIcon,const char *message)
//{// begin TrayIcon
//
//}// end TrayIcon


CPOPDlg::CPOPDlg(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CPOPDlg::IDD, pParent)
	//, m_sCallNumber(_T(""))
	, m_pDlgEditInfo(NULL)
	, m_pDlgMyEnterprise(NULL)
	, m_pDlgMyGroup(NULL)
	, m_pDlgMyContacts(NULL)
	, m_pDlgMySession(NULL)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pCurrentLabel = NULL;
	m_pOldCurrentLabel = NULL;
	m_pDlgFileManager = NULL;
}

void CPOPDlg::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_SKIN2, m_btnSkin);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_MAX2, m_btnMax);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	DDX_Control(pDX, IDC_BUTTON_LINESTATE, m_btnLineState);
	DDX_Control(pDX, IDC_BUTTON_MY_CENTER, m_btnMyCenter);
	DDX_Control(pDX, IDC_BUTTON_MY_SHARE, m_btnMyShare);
	DDX_Control(pDX, IDC_BUTTON_FILE_MGR, m_btnFileMgr);
	//DDX_Control(pDX, IDC_COMBO_NUMBERS, m_comboNumbers);
	//DDX_Control(pDX, IDC_BUTTON_CALLUSER, m_btnCallUser);
	//DDX_Text(pDX, IDC_EDIT_SEARCH, m_sCallNumber);
	DDX_Control(pDX, IDC_EDIT_SEARCH, m_editSearch);
	//DDX_CBString(pDX, IDC_COMBO_NUMBERS, m_sCallNumber);
	DDX_Control(pDX, IDC_BUTTON_ENTERPRISE, m_btnMyEnterprise);
	DDX_Control(pDX, IDC_BUTTON_DEPARTMENT, m_btnMyDepartment);
	DDX_Control(pDX, IDC_BUTTON_CONTACT, m_btnMyContacts);
	DDX_Control(pDX, IDC_BUTTON_SESSION, m_btnMySession);
	DDX_Control(pDX, IDC_TREE_SEARCH, m_treeSearch);

	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
}

BEGIN_MESSAGE_MAP(CPOPDlg, CEbDialogBase)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	
	ON_MESSAGE(EB_WM_BROADCAST_MSG, OnMessageBroadcastMsg)
	ON_MESSAGE(EB_WM_NEW_VERSION, OnMessageNewVersion)
	ON_MESSAGE(EB_WM_SERVER_CHENGE, OnMessageServerChange)
	ON_MESSAGE(EB_WM_ONLINE_ANOTHER, OnMessageOnlineAnother)
	ON_MESSAGE(EB_WM_LOGOUT, OnMessageLogoutResponse)
	ON_MESSAGE(EB_WM_LOGON_SUCCESS, OnMessageLogonSuccess)
	ON_MESSAGE(EB_WM_LOGON_TIMEOUT, OnMessageLogonTimeout)
	ON_MESSAGE(EB_WM_LOGON_ERROR, OnMessageLogonError)
	//ON_MESSAGE(CR_WM_ENTER_ROOM, OnMessageEnterRoom)
	//ON_MESSAGE(CR_WM_EXIT_ROOM, OnMessageExitRoom)
	ON_MESSAGE(CR_WM_USER_ENTER_ROOM, OnMessageUserEnterRoom)
	ON_MESSAGE(CR_WM_USER_EXIT_ROOM, OnMessageUserExitRoom)
	ON_MESSAGE(CR_WM_RECEIVE_RICH, OnMessageReceiveRich)
	ON_MESSAGE(CR_WM_SEND_RICH, OnMessageSendRich)
	ON_MESSAGE(CR_WM_SENDING_FILE, OnMessageSendingFile)
	ON_MESSAGE(CR_WM_SENT_FILE, OnMessageSendedFile)
	ON_MESSAGE(CR_WM_CANCEL_FILE, OnMessageCancelFile)
	ON_MESSAGE(CR_WM_RECEIVING_FILE, OnMessageReceivingFile)
	ON_MESSAGE(CR_WM_RECEIVED_FILE, OnMessageReceivedFile)
	ON_MESSAGE(CR_WM_FILE_PERCENT, OnMessageFilePercent)

	ON_MESSAGE(EB_WM_V_REQUEST_RESPONSE, OnMessageVRequestResponse)
	ON_MESSAGE(EB_WM_V_ACK_RESPONSE, OnMessageVAckResponse)
	ON_MESSAGE(EB_WM_VIDEO_REQUEST, OnMessageVideoRequest)
	ON_MESSAGE(EB_WM_VIDEO_ACCEPT, OnMessageVideoAccept)
	ON_MESSAGE(EB_WM_VIDEO_REJECT, OnMessageVideoCancel)
	ON_MESSAGE(EB_WM_VIDEO_CLOSE, OnMessageVideoEnd)

	ON_MESSAGE(EB_WM_USER_STATE_CHANGE, OnMessageUserStateChange)
	ON_MESSAGE(EB_WM_EDITINFO_RESPONSE, OnMessageSInfoResponse)
	ON_MESSAGE(EB_WM_CALL_CONNECTED, OnMessageCallConnected)
	ON_MESSAGE(EB_WM_CALL_HANGUP, OnMessageCallHangup)
	ON_MESSAGE(EB_WM_CALL_ALERTING, OnMessageAlertingCall)
	ON_MESSAGE(EB_WM_CALL_ERROR, OnMessageCallError)
	ON_MESSAGE(EB_WM_CALL_INCOMING, OnMessageIncomingCall)
	ON_MESSAGE(EB_WM_CONTACT_INFO, OnMessageContactInfo)
	ON_MESSAGE(EB_WM_CONTACT_DELETE, OnMessageContactDelete)
	ON_MESSAGE(EB_WM_ENTERPRISE_INFO, OnMessageEnterpriseInfo)
	ON_MESSAGE(EB_WM_GROUP_INFO, OnMessageGroupInfo)
	ON_MESSAGE(EB_WM_GROUP_DELETE, OnMessageGroupDelete)
	ON_MESSAGE(EB_WM_GROUP_EDIT_ERROR, OnMessageGroupEditError)
	ON_MESSAGE(EB_WM_REMOVE_GROUP, OnMessageRemoveGroup)
	ON_MESSAGE(EB_WM_EXIT_GROUP, OnMessageExitGroup)
	ON_MESSAGE(EB_WM_REQUEST_ADD2GROUP, OnMessageRequestJoin2Group)
	ON_MESSAGE(EB_WM_INVITE_ADD2GROUP, OnMessageInviteJoin2Group)
	ON_MESSAGE(EB_WM_MEMBER_INFO, OnMessageMemberInfo)
	ON_MESSAGE(EB_WM_MEMBER_DELETE, OnMessageMemberDelete)
	ON_MESSAGE(EB_WM_MEMBER_EDIT_ERROR, OnMessageMemberEditError)
	//ON_MESSAGE(EB_WM_APP_MSG, OnMessageAppMsgInfo)

	ON_MESSAGE(EB_WM_RESOURCE_INFO, OnMessageResourceInfo)
	ON_MESSAGE(EB_WM_RESOURCE_DELETE, OnMessageDeleteResource)
	ON_MESSAGE(EB_WM_RESOURCE_MOVE, OnMessageMoveResource)
	ON_MESSAGE(EB_COMMAND_FILE_MANAGER, OnMessageFileManager)

	ON_MESSAGE(WM_HOTKEY, OnMessageHotKey)

	//ON_MESSAGE(POP_WM_WINDOW_SIZE, OnMessageWindowResize)

	//ON_CBN_SELCHANGE(IDC_COMBO_NUMBERS, &CPOPDlg::OnCbnSelchangeComboNumbers)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CONTACT, &CPOPDlg::OnBnClickedButtonContact)
	ON_BN_CLICKED(IDC_BUTTON_ENTERPRISE, &CPOPDlg::OnBnClickedButtonEnterprise)
	ON_BN_CLICKED(IDC_BUTTON_DEPARTMENT, &CPOPDlg::OnBnClickedButtonDepartment)
	ON_BN_CLICKED(IDC_BUTTON_MAX2, &CPOPDlg::OnBnClickedButtonMax)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CPOPDlg::OnBnClickedButtonMin)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CPOPDlg::OnBnClickedButtonClose)
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_EDIT_SEARCH, &CPOPDlg::OnEnChangeEditSearch)
	ON_MESSAGE(WM_ITEM_DOUBLE_CLICK, OnTreeItemDoubleClieck)
	ON_MESSAGE(WM_ITEM_TRACK_HOT, OnTreeItemTrackHot)
	ON_MESSAGE(WM_TRAYICON_NOTIFICATION, OnIconNotification)

	ON_COMMAND(EB_COMMAND_EXIT_APP, &CPOPDlg::OnExitApp)
	ON_COMMAND(EB_COMMAND_LOGOUT, &CPOPDlg::OnLogout)
	ON_BN_CLICKED(IDC_BUTTON_LINESTATE, &CPOPDlg::OnBnClickedButtonLinestate)
	ON_COMMAND(EB_COMMAND_STATE_ONLINE, &CPOPDlg::OnStateOnline)
	ON_COMMAND(EB_COMMAND_STATE_AWAY, &CPOPDlg::OnStateAway)
	ON_COMMAND(EB_COMMAND_STATE_BUSY, &CPOPDlg::OnStateBusy)
	ON_COMMAND(EB_COMMAND_AUTO_LOGIN, &CPOPDlg::OnAutoLogin)
	ON_WM_MOUSEMOVE()
	ON_EN_KILLFOCUS(IDC_EDIT_DESCRIPTION, &CPOPDlg::OnEnKillfocusEditDescription)
	ON_BN_CLICKED(IDC_BUTTON_SESSION, &CPOPDlg::OnBnClickedButtonSession)
	ON_WM_LBUTTONDOWN()
	//ON_COMMAND(EB_COMMAND_MY_SETTING, &CPOPDlg::OnMySetting)
	//ON_COMMAND(EB_COMMAND_FILE_MANAGER, &CPOPDlg::OnFileManager)
	//ON_COMMAND(EB_COMMAND_MY_SHARE, &CPOPDlg::OnMyShare)
	ON_BN_CLICKED(IDC_BUTTON_SKIN2, &CPOPDlg::OnBnClickedButtonSkin2)
	ON_COMMAND_RANGE(EB_COMMAND_SKIN_SETTING,EB_COMMAND_SKIN_2,&OnSkinSelect)
	ON_COMMAND(EB_COMMAND_OPEN_MAIN, &CPOPDlg::OnOpenMain)
	ON_COMMAND_RANGE(EB_COMMAND_MY_EMPLOYEE,EB_COMMAND_MY_EMPLOYEE+0x20,OnMyEmployeeInfo)
	ON_COMMAND_RANGE(EB_COMMAND_SUBSCRIBE_FUNC,EB_COMMAND_SUBSCRIBE_FUNC+0x20,OnSubscribeFunc)

	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_BN_CLICKED(IDC_BUTTON_MY_CENTER, &CPOPDlg::OnBnClickedButtonMyCenter)
	ON_BN_CLICKED(IDC_BUTTON_MY_SHARE, &CPOPDlg::OnBnClickedButtonMyShare)
	ON_BN_CLICKED(IDC_BUTTON_FILE_MGR, &CPOPDlg::OnBnClickedButtonFileMgr)
	END_MESSAGE_MAP()


// CPOPDlg 消息处理程序
DWORD dwAdvise = 0;
BOOL CALLBACK MyEnumFonts(CONST LOGFONTW* lplf, CONST TEXTMETRICW *lptm,DWORD dwType,LPARAM lparam)
{
	const wchar_t * lpszFine = (const wchar_t*)lparam;
	const std::wstring tempFontName(lplf->lfFaceName);
	if(tempFontName.find(lpszFine)!=std::wstring::npos)
	{
		theFontFamily = lpszFine;
		return false;
	}
	return true;
}
BOOL CPOPDlg::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	ModifyStyle(0, WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SYSMENU);
    CMenuXP::SetXPLookNFeel (this);
    CMenuXP::UpdateMenuBar (this);

	CPaintDC dc((CWnd*)this);
  ::EnumFontsW(dc.m_hDC,NULL,(FONTENUMPROCW)MyEnumFonts,(LPARAM)theFontFamily1.c_str());	// 
  ::EnumFontsW(dc.m_hDC,NULL,(FONTENUMPROCW)MyEnumFonts,(LPARAM)theFontFamily0.c_str());

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	RegisterHotKey(this->m_hWnd, 0x2013, MOD_SHIFT|MOD_ALT, 'A'); 
	RegisterHotKey(this->m_hWnd, 0x2014, MOD_SHIFT|MOD_ALT, 'Z'); 
	//RegisterHotKey(this->m_hWnd, 0x2013, MOD_CONTROL|MOD_ALT, 'P'); 

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	//SetIcon(m_hIcon, TRUE);			// 设置大图标
	//SetIcon(m_hIcon, FALSE);		// 设置小图标

	SetTransparentType(CEbDialogBase::TT_STATIC);
	SetSplitterBorder();
	this->EnableToolTips();
	this->SetToolTipText(IDC_BUTTON_SKIN2,_T("换皮肤主色调"));
	this->SetToolTipText(IDC_BUTTON_MIN,_T("最小化"));
	this->SetToolTipText(IDC_BUTTON_MAX2,_T("最大化"));
	this->SetToolTipText(IDC_BUTTON_CLOSE,_T("关闭"));
	this->SetToolTipText(IDC_BUTTON_LINESTATE,_T("在线状况"));
	this->SetToolTipText(IDC_BUTTON_MY_CENTER,_T("个人中心"));
	this->SetToolTipText(IDC_BUTTON_FILE_MGR,_T("文件传输管理"));
	this->SetToolTipText(IDC_BUTTON_MY_SHARE,_T("个人云盘"));
	this->SetToolTipText(IDC_EDIT_DESCRIPTION,_T("点击修改个性签名，回车保存"));
	this->SetToolTipText(IDC_EDIT_SEARCH,_T("输入用户帐号，直接回车发起会话"));
	this->SetToolTipText(IDC_BUTTON_CALLUSER,_T("呼叫用户"));
	this->SetToolTipText(IDC_BUTTON_DEPARTMENT,_T("我的部门（群和讨论组）"));
	this->SetToolTipText(IDC_BUTTON_CONTACT,_T("个人联系人"));
	this->SetToolTipText(IDC_BUTTON_SESSION,_T("最近会话（联系人）"));
	this->SetToolTipText(IDC_BUTTON_ENTERPRISE,_T("公司组织结构"));

	m_editSearch.SetRectangleColor(RGB(0,128,255),RGB(255,255,255));

	m_btnSkin.Load(IDB_PNG_BTN_SKIN);
	m_btnMin.SetAutoSize(false);
	m_btnMin.Load(IDB_PNG_MIN);
	m_btnMax.SetAutoSize(false);
	m_btnMax.Load(IDB_PNG_MAX);
	m_btnClose.SetAutoSize(false);
	m_btnClose.Load(IDB_PNG_CLOSE);
	m_btnLineState.SetAutoSize(false);
#ifdef USES_EBCOM_TEST
	EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
	EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
	//m_btnLineState.Load(IDB_PNG_BTN);
	if (nOutLineState == EB_LINE_STATE_AWAY)
		m_btnLineState.Load(IDB_PNG_BTN_STATE_AWAY);
	else if (nOutLineState == EB_LINE_STATE_BUSY)
		m_btnLineState.Load(IDB_PNG_BTN_STATE_BUSY);
	else// if (nOutLineState == EB_LINE_STATE_ONLINE)
		m_btnLineState.Load(IDB_PNG_BTN_STATE_ONLINE);
	m_btnLineState.SetWindowText(_T(""));
	m_btnMyCenter.Load(IDB_PNG_BTN_MY_CENTER);
	m_btnMyCenter.SetWindowText(_T(""));
	m_btnMyShare.Load(IDB_PNG_BTN_MY_SHARE);
	m_btnMyShare.SetWindowText(_T(""));
	m_btnFileMgr.Load(IDB_PNG_BTN_FILE_MGR);
	m_btnFileMgr.SetWindowText(_T(""));
	if (theApp.IsLogonVisitor())
	{
		m_btnMyShare.ShowWindow(SW_HIDE);
	}

	m_editDescription.EnableWindow(TRUE);
	m_editDescription.SetReadOnly(TRUE);

	//CImageEx theBtnImage;
	//HGDIOBJ pBtnBitmap[3];

	//theBtnImage.LoadResource(IDB_BITMAP_TAB_CHATRIGHT);
	m_btnMyDepartment.SetWindowText(_T(""));
	m_btnMyDepartment.SetAutoSize(false);
	m_btnMyDepartment.Load(IDB_PNG_TAB_DEP);
	m_btnMyDepartment.Invalidate();
	m_btnMyContacts.SetWindowText(_T(""));
	m_btnMyContacts.SetAutoSize(false);
	m_btnMyContacts.Load(IDB_PNG_TAB_CONTACT);
	m_btnMySession.SetWindowText(_T(""));
	m_btnMySession.SetAutoSize(false);
	m_btnMySession.Load(IDB_PNG_TAB_SES);
	m_btnMyEnterprise.SetWindowText(_T(""));
	m_btnMyEnterprise.SetAutoSize(false);
	m_btnMyEnterprise.Load(IDB_PNG_TAB_ENT);
	m_btnMyEnterprise.ShowWindow(SW_HIDE);
	m_pDlgMyGroup = new CDlgMyGroup(this);
	m_pDlgMyGroup->Create(CDlgMyGroup::IDD, this);
	m_pDlgMyGroup->ShowWindow(SW_SHOW);
	m_pDlgMyContacts = new CDlgMyContacts(this);
	m_pDlgMyContacts->Create(CDlgMyContacts::IDD, this);
	m_pDlgMyContacts->ShowWindow(SW_HIDE);
	m_pDlgMySession = new CDlgMySession(this);
	m_pDlgMySession->Create(CDlgMySession::IDD, this);
	m_pDlgMySession->ShowWindow(SW_HIDE);
	m_pDlgMyEnterprise = new CDlgMyEnterprise(this);
	m_pDlgMyEnterprise->Create(CDlgMyEnterprise::IDD, this);
	m_pDlgMyEnterprise->ShowWindow(SW_HIDE);
	if (theApp.IsLogonVisitor())
	{
		m_btnMyDepartment.ShowWindow(SW_HIDE);
		m_btnMyContacts.ShowWindow(SW_HIDE);
		m_btnMyEnterprise.ShowWindow(SW_HIDE);
		OnBnClickedButtonSession();
	}else
	{
		OnBnClickedButtonDepartment();
	}

	m_btnSearchTrackCall.Create(_T(""),WS_CHILD|WS_VISIBLE, CRect(0,0,1,1), &m_treeSearch, 0xffff);
	m_btnSearchTrackCall.SetAutoSize(false);
	m_btnSearchTrackCall.Load(IDB_PNG_HOT_CALL);
	m_btnSearchTrackCall.SetToolTipText(_T("呼叫"));
	m_treeSearch.SetItemHeight(40);
	m_treeSearch.SetIconSize(32,32);
	//HICON m_icon = AfxGetApp()->LoadIcon(IDI_CONTACT);
	//m_treeSearch.SetRootIcon( m_icon );
	//m_treeSearch.SetItemIcon( m_icon );
	m_treeSearch.SetCallback(&theApp);
	m_treeSearch.ShowWindow(SW_HIDE);

#ifdef USES_EBCOM_TEST
	const CEBString sOutDesc = theEBClientCore->EB_Description.GetBSTR();
#else
	const std::string sOutDesc = theEBAppClient.EB_GetDescription();
#endif
	this->GetDlgItem(IDC_EDIT_DESCRIPTION)->SetWindowText(sOutDesc.c_str());

#ifdef USES_EBCOM_TEST
	(((CEBDispatch*)(CEBCoreEventsSink*)this))->AddRef();	// 增加计数，避免删除自己
	//theEBClientCore->EB_SetCallBack((IDispatch*)(CEBCoreEventsSink*)this);

	((CEBSearchEventsSink*)this)->AddRef();	// 增加计数，避免删除自己
	((CEBCoreEventsSink*)this)->m_hFireWnd = this->GetSafeHwnd();
	((CEBCoreEventsSink*)this)->DispEventAdvise(theEBClientCore);
	OnMessageLogonSuccess(0,0);
#else
	theEBAppClient.EB_SetMsgHwnd(this->GetSafeHwnd());
	OnMessageLogonSuccess(0,0);
#endif

	//theBtnImage.LoadResource(IDB_BITMAP_BTN_6821);
	//theBtnImage.Cut(3, pBtnBitmap);
	//m_btnCallUser.SetSkin(pBtnBitmap[0], pBtnBitmap[2], pBtnBitmap[1]);

#ifdef USES_EBCOM_TEST
	const CEBString sUserName = theEBClientCore->EB_UserName.GetBSTR();
	const CEBString sSettingEnterprise = theEBSetting->Enterprise.GetBSTR();
#else
	const CEBString sUserName = theEBAppClient.EB_GetUserName();
	const CEBString sSettingEnterprise = theSetting.GetEnterprise();
#endif
	CString sWindowText;
	if (theApp.IsLogonVisitor())
		sWindowText.Format(_T("游客-%s"),sUserName.c_str());
	else
		sWindowText.Format(_T("%s-%s(%s)"),sSettingEnterprise.c_str(),sUserName.c_str(),theApp.GetLogonAccount());
	this->SetWindowText(sWindowText);
	MoveWindow(0, 0, LEN_DLG_WIDTH, LEN_DLG_HEIGHT);
	SetCircle();

	// 未处理呼叫
	m_btnMySession.SetTextLeft(-40);
	m_btnMySession.SetTextTop(-2);
	m_btnMySession.SetHotTextColor(RGB(255,0,128)); 
	m_btnMySession.SetNorTextColor(RGB(255,0,128)); 

	// 系统托盘
	SetTimer(TIMERID_SHOW_EMOTIPN,12*1000,NULL);
	CString sTrayText;
	if (theApp.IsLogonVisitor())
		sTrayText.Format(_T("游客-%s-%s"), sUserName.c_str(),GetLineStateText(nOutLineState));
	else
		sTrayText.Format(_T("%s(%s)-%s"), sUserName.c_str(),theApp.GetLogonAccount(),GetLineStateText(nOutLineState));
	m_trayIconData.cbSize = sizeof(NOTIFYICONDATA);
	m_trayIconData.hIcon = m_hIcon;
	m_trayIconData.hWnd = this->GetSafeHwnd();
	m_trayIconData.uCallbackMessage = WM_TRAYICON_NOTIFICATION;
	m_trayIconData.uFlags = NIF_TIP|NIF_MESSAGE|NIF_ICON;
	m_trayIconData.uID = 120;
	lstrcpy(m_trayIconData.szTip, sTrayText);
	Shell_NotifyIcon(NIM_ADD, &m_trayIconData);

	this->SetForegroundWindow();
	SetTimer(TIMERID_CHECK_CLOSE_DIALOG,5000,NULL);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPOPDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CEbDialogBase::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPOPDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//CEbDialogBase::OnPaint();
		DrawInfo();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CPOPDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CPOPDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CEbDialogBase::OnClose();
}

void CPOPDlg::OnDestroy()
{
	Shell_NotifyIcon(NIM_DELETE, &m_trayIconData);

	UnregisterHotKey(this->m_hWnd,0x2013); 
	UnregisterHotKey(this->m_hWnd,0x2014); 

	CEbDialogBase::OnDestroy();
	m_menuState.DestroyMenu();
	m_menuTray.DestroyMenu();
	m_menuSkin.DestroyMenu();
#ifdef USES_EBCOM_TEST
	//AfxConnectionUnadvise(theEBClientCore,__uuidof(_IEBClientCoreEvents), GetIDispatch(FALSE),FALSE,dwAdvise);
	((CEBCoreEventsSink*)this)->DispEventUnadvise(theEBClientCore);
#else
	theEBAppClient.EB_SetMsgHwnd(NULL);
#endif
	m_btnSearchTrackCall.DestroyWindow();
	if (m_pDlgEditInfo)
	{
		m_pDlgEditInfo->DestroyWindow();
		delete m_pDlgEditInfo;
		m_pDlgEditInfo = NULL;
	}
	if (m_pDlgMyEnterprise)
	{
		m_pDlgMyEnterprise->DestroyWindow();
		delete m_pDlgMyEnterprise;
		m_pDlgMyEnterprise = NULL;
	}
	if (m_pDlgMyGroup)
	{
		m_pDlgMyGroup->DestroyWindow();
		delete m_pDlgMyGroup;
		m_pDlgMyGroup = NULL;
	}
	if (m_pDlgMyContacts)
	{
		m_pDlgMyContacts->DestroyWindow();
		delete m_pDlgMyContacts;
		m_pDlgMyContacts = NULL;
	}
	if (m_pDlgMySession)
	{
		m_pDlgMySession->DestroyWindow();
		delete m_pDlgMySession;
		m_pDlgMySession = NULL;
	}
	if (m_pDlgFileManager)
	{
		m_pDlgFileManager->DestroyWindow();
		delete m_pDlgFileManager;
		m_pDlgFileManager = NULL;
	}
}

void CPOPDlg::DeleteDlgIncomingCall(eb::bigint sFromAccount)
{
	CDlgIncomingCall::pointer pDlgIncomingCall;
	if (m_pIncomingCallList.find(sFromAccount, pDlgIncomingCall, true))
	{
		pDlgIncomingCall->DestroyWindow();
	}
}
void CPOPDlg::DeleteDlgAlertingCall(eb::bigint sFromAccount)
{
	CDlgAlertingCall::pointer pDlgAlertingCall;
	if (m_pAlertingCallList.find(sFromAccount, pDlgAlertingCall, true))
	{
		pDlgAlertingCall->DestroyWindow();
	}
}

void CPOPDlg::ShowDlgFileManager(void)
{
	if (m_pDlgFileManager == NULL)
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		m_pDlgFileManager = new CDlgFileManager(pParent);
		m_pDlgFileManager->Create(CDlgFileManager::IDD, pParent);
		int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
		const int const_dlg_width = 480;
		const int const_dlg_height = 360;
		CRect rect;
		rect.right = m_nScreenWidth;
		rect.left = rect.right-const_dlg_width;
		rect.top = 0;
		rect.bottom = rect.top + const_dlg_height;
		m_pDlgFileManager->MoveWindow(&rect);
	}
	m_pDlgFileManager->ShowWindow(SW_SHOW);
	m_pDlgFileManager->SetForegroundWindow();
	m_pDlgFileManager->SetCircle();
}

LRESULT CPOPDlg::OnMessageBroadcastMsg(WPARAM wParam, LPARAM lParam)
{
	// 广播消息；
#ifdef USES_EBCOM_TEST

#else
	const EB_AccountInfo* pAccountInfo = (const EB_AccountInfo*)wParam;
	const EB_APMsgInfo* pApMsgInfo = (const EB_APMsgInfo*)lParam;

	CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
	CDlgFuncWindow * pFuncWindow = new CDlgFuncWindow(pParent,true);
	pFuncWindow->m_bBroadcastMsg = true;
	pFuncWindow->m_sTitle = pApMsgInfo->m_sMsgName;
	pFuncWindow->m_sFuncUrl = pApMsgInfo->m_sMsgContent;
	pFuncWindow->m_nWidth = 250;
	pFuncWindow->m_nHeight = 180;
	pFuncWindow->Create(CDlgFuncWindow::IDD,pParent);
	pFuncWindow->ShowWindow(SW_SHOW);

#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageNewVersion(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VersionInfo* pVersionInfo = (IEB_VersionInfo*)wParam;
	// 已经下载自动更新打包文件，在下次重启时，会自动更新；
	const CString sPrevNewVersionFile = theApp.EBC_GetProfileString("new_version",_T("file"));
	if (PathFileExists(sPrevNewVersionFile) && pVersionInfo->VersionFile != (_bstr_t)(LPCTSTR)sPrevNewVersionFile)
	{
		// 删除前一个更新文件未处理
		DeleteFile(sPrevNewVersionFile);
	}
	theApp.EBC_SetProfileString(_T("new_version"),_T("version"),CEBString(pVersionInfo->Version.GetBSTR()).c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("desc"),CEBString(pVersionInfo->Description.GetBSTR()).c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("time"),CEBString(pVersionInfo->UpdateTime.GetBSTR()).c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("file"),CEBString(pVersionInfo->VersionFile.GetBSTR()).c_str());
	theApp.EBC_SetProfileInt(_T("new_version"),_T("type"),pVersionInfo->UpdateType);
	SetTimer(TIMERID_NEW_VERSION,1000,NULL);
#else
	const EB_VersionInfo* pNewVersionInfo = (const EB_VersionInfo*)wParam;
	// 已经下载自动更新打包文件，在下次重启时，会自动更新；
	const CString sPrevNewVersionFile = theApp.EBC_GetProfileString("new_version",_T("file"));
	if (PathFileExists(sPrevNewVersionFile) && sPrevNewVersionFile!=pNewVersionInfo->m_sVersionFile.c_str())
	{
		// 删除前一个更新文件未处理
		DeleteFile(sPrevNewVersionFile);
	}
	theApp.EBC_SetProfileString(_T("new_version"),_T("version"),pNewVersionInfo->m_sVersion.c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("desc"),pNewVersionInfo->m_sDescription.c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("time"),pNewVersionInfo->m_sUpdateTime.c_str());
	theApp.EBC_SetProfileString(_T("new_version"),_T("file"),pNewVersionInfo->m_sVersionFile.c_str());
	theApp.EBC_SetProfileInt(_T("new_version"),_T("type"),pNewVersionInfo->m_nUpdateType);
	SetTimer(TIMERID_NEW_VERSION,1000,NULL);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageServerChange(WPARAM wParam, LPARAM lParam)
{
	// 会话无效，在这里重新登录
#ifdef USES_EBCOM_TEST
	int nServerState = (int)wParam;
#else
	CEBCallbackInterface::SERVER_STATE nServerState = (CEBCallbackInterface::SERVER_STATE)wParam;
#endif
	if (nServerState == 1)	// CEBCallbackInterface::SERVER_STOPED
		CDlgMessageBox::EbMessageBox(this,"系统正在更新维护：",_T("成功后会自动重新登录，请稍等..."),CDlgMessageBox::IMAGE_WARNING,10);
	else if (nServerState == 3)	// CEBCallbackInterface::SERVER_RESTART)
	{
		// **SDK内部会自动重新登录；
		//CDlgMessageBox::EbMessageBox(this,"更新成功：",_T("正在重新登录..."),CDlgMessageBox::IMAGE_WARNING,10);
		//SetTimer(TIMERID_RELOGIN,5000,NULL);
	}
	return 0;
}

LRESULT CPOPDlg::OnMessageOnlineAnother(WPARAM wParam, LPARAM lParam)
{
	// 已经在其他地方登录，退出当前连接；
	SetTimer(TIMERID_LOGOUT,10,NULL);
	return 0;
}
LRESULT CPOPDlg::OnMessageLogoutResponse(WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT CPOPDlg::OnMessageLogonSuccess(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_LoadOrg();	// 加载组织结构、表情、头像
	if (theApp.IsLogonVisitor())
	{
		//this->GetDlgItem(IDC_BUTTON_MYCONTACTS)->EnableWindow(FALSE);
	}else
	{
		theEBClientCore->EB_LoadContact();
	}
#else
	theEBAppClient.EB_LoadOrg();	// 加载组织结构、表情、头像
	if (theApp.IsLogonVisitor())
	{
		//this->GetDlgItem(IDC_BUTTON_MYCONTACTS)->EnableWindow(FALSE);
	}else
	{
		theEBAppClient.EB_LoadContact();
	}
#endif
	SetTimer(TIMERID_LOADINFO, 2*1000, NULL);
	theUpdateResetTimer = false;
	KillTimer(TIMERID_CHECK_UPDATE);
	//SetTimer(TIMERID_CHECK_UPDATE,10*1000,NULL);	// 10秒测试检查
	SetTimer(TIMERID_CHECK_UPDATE,(1+rand()%5)*60*1000,NULL);	// 先1-5分钟检查
	return 0;
}

LRESULT CPOPDlg::OnMessageLogonTimeout(WPARAM wParam, LPARAM lParam)
{
	CDlgMessageBox::EbMessageBox(this,"登录超时！",_T("请重新登录。"),CDlgMessageBox::IMAGE_WARNING);
	return 0;
}

LRESULT CPOPDlg::OnMessageLogonError(WPARAM wParam, LPARAM lParam)
{
	int nErrorCode = lParam;
	if (nErrorCode == EB_STATE_UNAUTH_ERROR)
		CDlgMessageBox::EbDoModal(this,_T("该帐号邮箱未通过验证："),_T("请查收邮件，点击验证链接，完成注册！"),CDlgMessageBox::IMAGE_WARNING);
	else if (nErrorCode == EB_STATE_ACCOUNT_FREEZE)
		CDlgMessageBox::EbMessageBox(this,"该帐号已经被临时冻结：","请联系公司技术或咨询恩布网络公司（www.entboost.com）！",CDlgMessageBox::IMAGE_WARNING);
	else
		CDlgMessageBox::EbDoModal(this,_T("验证失败！"),_T("请重新登录。"),CDlgMessageBox::IMAGE_WARNING);
	return 0;
}

LRESULT CPOPDlg::OnMessageSInfoResponse(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	EB_STATE_CODE nState = (EB_STATE_CODE)wParam;
	m_editDescription.SetWindowText(CEBString(theEBClientCore->EB_Description.GetBSTR()).c_str());
	ChangeTrayText();
	this->Invalidate();
#else
	m_editDescription.SetWindowText(theEBAppClient.EB_GetDescription().c_str());
	ChangeTrayText();
	this->Invalidate();
#endif
	return 0;
}

//LRESULT CPOPDlg::OnMessageEnterRoom(WPARAM wParam, LPARAM lParam)
//{
//	const char * lpszCallId = (const char*)lParam;
//	EB_STATE_CODE nStateCode = (EB_STATE_CODE)lParam;
//	if (lpszCallId==NULL) return 1;
//	CDlgDialog::pointer pDlgDialog;
//	if (theApp.m_pDialogList.find(lpszCallId, pDlgDialog))
//	{
//		pDlgDialog->OnEnterRoom();
//	}
//	return 0;
//}
//LRESULT CPOPDlg::OnMessageExitRoom(WPARAM wParam, LPARAM lParam)
//{
//	const char * lpszCallId = (const char*)lParam;
//	if (lpszCallId==NULL) return 1;
//	theApp.m_pCallList.remove(lpszCallId);
//	theApp.m_pDialogList.remove(lpszCallId);
//	return 0;
//}

LRESULT CPOPDlg::OnMessageUserEnterRoom(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ChatAccountInfo* pChatAccountInfo = (IEB_ChatAccountInfo*)wParam;
	const eb::bigint sCallId = pChatAccountInfo->CallId;
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnUserEnterRoom(pChatAccountInfo);
	}
#else
	const CCrAccountInfo* pAccountInfo = (const CCrAccountInfo*)wParam;
	if (pAccountInfo==NULL) return 1;
	const eb::bigint sCallId = pAccountInfo->GetCallId();
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnUserEnterRoom(pAccountInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageUserExitRoom(WPARAM wParam, LPARAM lParam)
{
	// ???
	bool bHangup = lParam==1;
#ifdef USES_EBCOM_TEST
	IEB_ChatAccountInfo* pChatAccountInfo = (IEB_ChatAccountInfo*)wParam;
	const eb::bigint sCallId = pChatAccountInfo->CallId;
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnUserExitRoom(pChatAccountInfo);
	}
#else
	const CCrAccountInfo* pAccountInfo = (const CCrAccountInfo*)wParam;
	if (pAccountInfo==NULL) return 1;
	const eb::bigint sCallId = pAccountInfo->GetCallId();
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnUserExitRoom(pAccountInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageReceiveRich(WPARAM wParam, LPARAM lParam)
{
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/msg.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC);

#ifdef USES_EBCOM_TEST
	IEB_ChatRichInfo* pChatRichInfo = (IEB_ChatRichInfo*)wParam;
	const eb::bigint sCallId = pChatRichInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->OnReceiveRich(pChatRichInfo);
#else
	const CCrRichInfo * pCrMsgInfo = (const CCrRichInfo*)wParam;
	if (pCrMsgInfo==NULL) return 1;
	const eb::bigint sCallId = pCrMsgInfo->GetCallId();
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->OnReceiveRich(pCrMsgInfo);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageSendRich(WPARAM wParam, LPARAM lParam)
{
	EB_STATE_CODE nState = (EB_STATE_CODE)lParam;
#ifdef USES_EBCOM_TEST
	IEB_ChatRichInfo* pChatRichInfo = (IEB_ChatRichInfo*)wParam;
	const eb::bigint sCallId = pChatRichInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnSendRich(pChatRichInfo);
	}
#else
	const CCrRichInfo * pCrMsgInfo = (const CCrRichInfo*)wParam;
	if (pCrMsgInfo==NULL) return 1;
	const eb::bigint sCallId = pCrMsgInfo->GetCallId();
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnSendRich(pCrMsgInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageSendingFile(WPARAM wParam, LPARAM lParam)
{
	EB_STATE_CODE nState = (EB_STATE_CODE)lParam;
	switch (nState)
	{
	case EB_STATE_FILE_ALREADY_EXIST:
		return 0;
	case EB_STATE_OK:
		break;
	case EB_STATE_FILE_BIG_LONG:
		{
			CDlgMessageBox::EbMessageBox(this,"上传失败！","文件大小超过系统最大数！",CDlgMessageBox::IMAGE_WARNING);
			return 1;
		}
	default:
		{
			CString stext;
			stext.Format(_T("错误代码(%d)"),(int)nState);
			CDlgMessageBox::EbMessageBox(this,"上传失败！",stext,CDlgMessageBox::IMAGE_WARNING);
			return 1;
		}
	}
#ifdef USES_EBCOM_TEST
	IEB_ChatFileInfo* pCrFileInfo = (IEB_ChatFileInfo*)wParam;
	const eb::bigint sCallId = pCrFileInfo->CallId;
	const eb::bigint sResId = pCrFileInfo->ResId;
	if (sResId>0)
	{
		ShowDlgFileManager();
		m_pDlgFileManager->OnSendingFile(pCrFileInfo);
		m_pDlgFileManager->OnBnClickedButtonTraning();
		//return 0;
	}
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnSendingFile(pCrFileInfo);
	}
#else
	const CCrFileInfo * pCrFileInfo = (const CCrFileInfo*)wParam;
	if (pCrFileInfo==NULL) return 1;
	const eb::bigint sCallId = pCrFileInfo->GetCallId();
	const eb::bigint sResId = pCrFileInfo->m_sResId;
	if (sResId>0)
	{
		ShowDlgFileManager();
		m_pDlgFileManager->OnSendingFile(pCrFileInfo);
		m_pDlgFileManager->OnBnClickedButtonTraning();
		//return 0;
	}
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnSendingFile(pCrFileInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageSendedFile(WPARAM wParam, LPARAM lParam)
{
	EB_STATE_CODE nState = (EB_STATE_CODE)lParam;
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/msg.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC);

#ifdef USES_EBCOM_TEST
	IEB_ChatFileInfo* pCrFileInfo = (IEB_ChatFileInfo*)wParam;
	const eb::bigint sCallId = pCrFileInfo->CallId;
	const eb::bigint sReceiveAccount = pCrFileInfo->ReceiveAccount;
	const eb::bigint sResId = pCrFileInfo->ResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL && (sReceiveAccount==0 || sReceiveAccount==theApp.GetLogonUserId()))
			m_pDlgFileManager->DeleteDlgTranFile(pCrFileInfo->MsgId);
		//m_pDlgFileManager->OnSentFile(pChatRoomFileInfo);
		//return 0;
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);

	if (sReceiveAccount>0)
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
		{
			pDlgDialog->OnSentFile(pCrFileInfo);
		}
	}
#else
	const CCrFileInfo * pCrFileInfo = (const CCrFileInfo*)wParam;
	if (pCrFileInfo==NULL) return 1;
	const eb::bigint sCallId = pCrFileInfo->GetCallId();
	const eb::bigint sResId = pCrFileInfo->m_sResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL && (pCrFileInfo->m_sReceiveAccount==0 || pCrFileInfo->m_sReceiveAccount==theEBAppClient.EB_GetLogonUserId()))
			m_pDlgFileManager->DeleteDlgTranFile(pCrFileInfo->m_nMsgId);
		//m_pDlgFileManager->OnSentFile(pChatRoomFileInfo);
		//return 0;
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);
	if (pCrFileInfo->m_sReceiveAccount>0)
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
		{
			pDlgDialog->OnSentFile(pCrFileInfo);
		}
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageCancelFile(WPARAM wParam, LPARAM lParam)
{
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/msg.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC);

#ifdef USES_EBCOM_TEST
	IEB_ChatFileInfo* pCrFileInfo = (IEB_ChatFileInfo*)wParam;
	const eb::bigint sCallId = pCrFileInfo->CallId;
	const eb::bigint sReceiveAccount = pCrFileInfo->ReceiveAccount;
	const eb::bigint sResId = pCrFileInfo->ResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL && sReceiveAccount==0)
			m_pDlgFileManager->DeleteDlgTranFile(pCrFileInfo->MsgId);
		//m_pDlgFileManager->OnCancelFile(pCrFileInfo);
		//return 0;
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnCancelFile(pCrFileInfo);
	}
#else
	const CCrFileInfo * pCrFileInfo = (const CCrFileInfo*)wParam;
	if (pCrFileInfo==NULL) return 1;
	const eb::bigint sCallId = pCrFileInfo->GetCallId();
	const eb::bigint sResId = pCrFileInfo->m_sResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL && pCrFileInfo->m_sReceiveAccount==0)
			m_pDlgFileManager->DeleteDlgTranFile(pCrFileInfo->m_nMsgId);
		//m_pDlgFileManager->OnCancelFile(pCrFileInfo);
		//return 0;
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnCancelFile(pCrFileInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageReceivingFile(WPARAM wParam, LPARAM lParam)
{
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/msg.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC);

#ifdef USES_EBCOM_TEST
	IEB_ChatFileInfo* pCrFileInfo = (IEB_ChatFileInfo*)wParam;
	const eb::bigint sResId = pCrFileInfo->ResId;
	if (sResId>0)
	{
		ShowDlgFileManager();
		m_pDlgFileManager->OnReceivingFile(pCrFileInfo);
		m_pDlgFileManager->OnBnClickedButtonTraning();
	}
	const eb::bigint sCallId = pCrFileInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->OnReceivingFile(pCrFileInfo);
#else
	const CCrFileInfo * pCrFileInfo = (const CCrFileInfo*)wParam;
	if (pCrFileInfo==NULL) return 1;
	const eb::bigint sResId = pCrFileInfo->m_sResId;
	if (sResId>0)
	{
		ShowDlgFileManager();
		m_pDlgFileManager->OnReceivingFile(pCrFileInfo);
		m_pDlgFileManager->OnBnClickedButtonTraning();
	}
	const eb::bigint sCallId = pCrFileInfo->GetCallId();
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->OnReceivingFile(pCrFileInfo);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageReceivedFile(WPARAM wParam, LPARAM lParam)
{
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/msg.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC);

#ifdef USES_EBCOM_TEST
	IEB_ChatFileInfo* pCrFileInfo = (IEB_ChatFileInfo*)wParam;
	const eb::bigint sCallId = pCrFileInfo->CallId;
	const eb::bigint sResId = pCrFileInfo->ResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL)
			m_pDlgFileManager->OnReceivedFile(pCrFileInfo);
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnReceivedFile(pCrFileInfo);
	}
#else
	const CCrFileInfo * pCrFileInfo = (const CCrFileInfo*)wParam;
	if (pCrFileInfo==NULL) return 1;
	const eb::bigint sCallId = pCrFileInfo->GetCallId();
	const eb::bigint sResId = pCrFileInfo->m_sResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL)
			m_pDlgFileManager->OnReceivedFile(pCrFileInfo);
	}
	//CEBCCallInfo::pointer pEbCallInfo;
	//if (!theApp.m_pCallList.find(lpszCallId,pEbCallInfo))
	//{
	//	return 1;
	//}
	//pEbCallInfo->m_tLastTime = time(0);

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnReceivedFile(pCrFileInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageFilePercent(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ChatFilePercent* pChatFilePercent = (IEB_ChatFilePercent*)wParam;
	const eb::bigint sCallId = pChatFilePercent->CallId;
	const eb::bigint sResId = pChatFilePercent->ResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL)
			m_pDlgFileManager->SetFilePercent(pChatFilePercent);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnFilePercent(pChatFilePercent);
	}
#else
	const CChatRoomFilePercent * pChatRoomFilePercent = (const CChatRoomFilePercent*)wParam;
	if (pChatRoomFilePercent==NULL) return 1;
	const eb::bigint sCallId = pChatRoomFilePercent->GetCallId();
	const eb::bigint sResId = pChatRoomFilePercent->m_sResId;
	if (sResId>0)
	{
		if (m_pDlgFileManager!=NULL)
			m_pDlgFileManager->SetFilePercent(pChatRoomFilePercent);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->OnFilePercent(pChatRoomFilePercent);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVRequestResponse(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo * pVideoInfo = (IEB_VideoInfo*)wParam;
	EB_STATE_CODE nState = (EB_STATE_CODE)lParam;
	const eb::bigint sCallId = pVideoInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->VRequestResponse(pVideoInfo,nState);
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	int nStateValue = lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->VRequestResponse(pVideoInfo,nStateValue);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVAckResponse(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo* pVideoInfo = (IEB_VideoInfo*)wParam;
	EB_STATE_CODE nState = (EB_STATE_CODE)lParam;
	const eb::bigint sCallId = pVideoInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 1;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->VAckResponse(pVideoInfo,nState);
	}
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	int nStateValue = lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pVideoInfo->m_sCallId, pDlgDialog))
	{
		pDlgDialog->VAckResponse(pVideoInfo,nStateValue);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVideoRequest(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo* pVideoInfo = (IEB_VideoInfo*)wParam;
	IEB_UserVideoInfo* pUserVideoInfo = (IEB_UserVideoInfo*)lParam;
	const eb::bigint sCallId = pVideoInfo->CallId;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->VideoRequest(pVideoInfo,pUserVideoInfo);
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	const EB_UserVideoInfo* pUserVideoInfo = (const EB_UserVideoInfo*)lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	pEbCallInfo->m_tLastTime = time(0);
	CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
	pDlgDialog->VideoRequest(pVideoInfo,pUserVideoInfo);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVideoAccept(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo* pVideoInfo = (IEB_VideoInfo*)wParam;
	IEB_UserVideoInfo* pUserVideoInfo = (IEB_UserVideoInfo*)lParam;
	const eb::bigint sCallId(pVideoInfo->CallId);
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->VideoAccept(pVideoInfo,pUserVideoInfo);
	}
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	const EB_UserVideoInfo* pUserVideoInfo = (const EB_UserVideoInfo*)lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pVideoInfo->m_sCallId, pDlgDialog))
	{
		pDlgDialog->VideoAccept(pVideoInfo,pUserVideoInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVideoCancel(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo* pVideoInfo = (IEB_VideoInfo*)wParam;
	IEB_UserVideoInfo* pUserVideoInfo = (IEB_UserVideoInfo*)lParam;
	const eb::bigint sCallId(pVideoInfo->CallId);
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->VideoCancel(pVideoInfo,pUserVideoInfo);
	}
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	const EB_UserVideoInfo* pUserVideoInfo = (const EB_UserVideoInfo*)lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pVideoInfo->m_sCallId, pDlgDialog))
	{
		pDlgDialog->VideoCancel(pVideoInfo,pUserVideoInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageVideoEnd(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_VideoInfo* pVideoInfo = (IEB_VideoInfo*)wParam;
	IEB_UserVideoInfo* pUserVideoInfo = (IEB_UserVideoInfo*)lParam;
	const eb::bigint sCallId(pVideoInfo->CallId);
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
	{
		return 0;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		pDlgDialog->VideoEnd(pVideoInfo,pUserVideoInfo);
	}
#else
	const EB_VideoInfo* pVideoInfo = (const EB_VideoInfo*)wParam;
	const EB_UserVideoInfo* pUserVideoInfo = (const EB_UserVideoInfo*)lParam;
	CEBCCallInfo::pointer pEbCallInfo;
	if (!theApp.m_pCallList.find(pVideoInfo->m_sCallId,pEbCallInfo))
	{
		return 1;
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pVideoInfo->m_sCallId, pDlgDialog))
	{
		pDlgDialog->VideoEnd(pVideoInfo,pUserVideoInfo);
	}
#endif
	return 0;
}

void CPOPDlg::OnFileManager()
{
	if (m_pDlgFileManager == NULL || !m_pDlgFileManager->IsWindowVisible())
		this->ShowDlgFileManager();
	else if (m_pDlgFileManager->IsWindowVisible())
		m_pDlgFileManager->ShowWindow(SW_HIDE);

}
LRESULT CPOPDlg::OnMessageFileManager(WPARAM wParam, LPARAM lParam)
{
	OnFileManager();
	return 0;
}

LRESULT CPOPDlg::OnMessageUserStateChange(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_MemberInfo * pEBMemberInfo = (IEB_MemberInfo*)wParam;
	bool bIsOwnerMember = lParam==1;
	// 更新界面用户状况改变
	if (pEBMemberInfo != NULL && m_pDlgMyEnterprise != NULL)
	{
		m_pDlgMyEnterprise->EmployeeInfo(pEBMemberInfo);
	}
	if (pEBMemberInfo != NULL && m_pDlgMyGroup != NULL)
	{
		m_pDlgMyGroup->MyDepMemberInfo(pEBMemberInfo);
	}

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pEBMemberInfo->GroupCode, pDlgDialog))
	{
		// 通知会话用户状况改变
	}
#else
	const EB_MemberInfo * pMemberInfo = (const EB_MemberInfo*)wParam;
	//bool bIsOwnerMember = (bool)(lParam==1);

	// 更新界面用户状况改变
	if (pMemberInfo != NULL && m_pDlgMyEnterprise != NULL)
	{
		m_pDlgMyEnterprise->EmployeeInfo(pMemberInfo);
		//m_pDlgMyEnterprise->ChangeEmployeeInfo(pMemberInfo);
	}
	if (pMemberInfo != NULL && m_pDlgMyGroup != NULL)
	{
		m_pDlgMyGroup->MyDepMemberInfo(pMemberInfo);
		//m_pDlgMyGroup->ChangeEmployeeInfo(pMemberInfo);
	}

	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pMemberInfo->m_sGroupCode, pDlgDialog))
	{
		// 通知会话用户状况改变
	}
#endif
	return 0;
}

void CPOPDlg::SaveCallRecord(eb::bigint sCallId,eb::bigint sGroupCode,const EB_AccountInfo& pFromAccountInfo)
{
	eb::bigint sEmpCode = pFromAccountInfo.m_pFromCardInfo.m_sMemberCode;
	CEBString sDepName = pFromAccountInfo.m_pFromCardInfo.m_sGroupName;
	CEBString sFromName = pFromAccountInfo.m_pFromCardInfo.m_sName;
	int nFromType = (int)pFromAccountInfo.m_pFromCardInfo.m_nAccountType;
	if (nFromType==(int)EB_ACCOUNT_TYPE_VISITOR)
		sFromName = pFromAccountInfo.m_pFromCardInfo.m_sAddress;
	CEBString sCompany = pFromAccountInfo.m_pFromCardInfo.m_sEnterprise;
	CEBString sTitle = pFromAccountInfo.m_pFromCardInfo.m_sTitle;
	eb::bigint sPhone = eb_atoi64(pFromAccountInfo.m_pFromCardInfo.m_sPhone.c_str());
	//CEBString sPhone = pFromAccountInfo.m_pFromCardInfo.m_sPhone;
	CEBString sTel = pFromAccountInfo.m_pFromCardInfo.m_sTel;
	CEBString sEmail = pFromAccountInfo.m_pFromCardInfo.m_sEmail;
	CString sSql;
	if (sGroupCode==0)
	{
#ifdef USES_EBCOM_TEST
		_bstr_t sMemberName = theEBClientCore->EB_GetMemberNameByMemberCode(pFromAccountInfo.m_pFromCardInfo.m_sMemberCode);
		if (sMemberName.length() > 0)
		{
			sFromName = sMemberName.GetBSTR();
			char lpszMemberCode[24];
			sprintf(lpszMemberCode,"%lld",pFromAccountInfo.m_pFromCardInfo.m_sMemberCode);
			sDepName = lpszMemberCode;	// ?
		}
#else
		if (theEBAppClient.EB_GetMemberNameByMemberCode(pFromAccountInfo.m_pFromCardInfo.m_sMemberCode,sFromName))
		{
			char lpszMemberCode[24];
			sprintf(lpszMemberCode,"%lld",pFromAccountInfo.m_pFromCardInfo.m_sMemberCode);
			sDepName = lpszMemberCode;	// ?
			//EB_GroupInfo::pointer pGroupInfo = theEBAppClient.GetGroupInfo(pMemberInfo->m_sGroupCode);
			//if (pGroupInfo.get() == NULL)
			//	sDepName = sGroupCode;
			//else
			//	sDepName = pGroupInfo->m_sGroupName.c_str();
		}
#endif
	}else
	{
#ifdef USES_EBCOM_TEST
		sDepName = theEBClientCore->EB_GetGroupName(sGroupCode).GetBSTR();
		if (sDepName.empty())
		{
			char lpszBuffer[24];
			sprintf(lpszBuffer,"%lld",sGroupCode);
			sDepName = lpszBuffer;
		}
#else
		if (!theEBAppClient.EB_GetGroupName(sGroupCode,sDepName))
		{
			char lpszBuffer[24];
			sprintf(lpszBuffer,"%lld",sGroupCode);
			sDepName = lpszBuffer;
		}
#endif
	}
	if (this->m_pDlgMySession != NULL && m_pDlgMySession->GetSafeHwnd())
	{
		// 删除旧会话
		CCallRecordInfo::pointer pOldCallInfo = m_pDlgMySession->GetCallRecordInfo(sGroupCode,pFromAccountInfo.GetUserId());
		if (pOldCallInfo.get()!=NULL)
		{
			sSql.Format(_T("DELETE FROM call_record_t WHERE call_id=%lld"),pOldCallInfo->m_sCallId);
			theApp.m_pBoUsers->execsql(sSql);
			if (sPhone==0 && pOldCallInfo->m_nFromPhone!=0)
				sPhone = pOldCallInfo->m_nFromPhone;
			if (sFromName.empty() && !pOldCallInfo->m_sFromName.empty())
				sFromName = pOldCallInfo->m_sFromName;
			if (sCompany.empty() && !pOldCallInfo->m_sCompany.empty())
				sCompany = pOldCallInfo->m_sCompany;
			if (sTitle.empty() && !pOldCallInfo->m_sTitle.empty())
				sTitle = pOldCallInfo->m_sTitle;
			if (sTel.empty() && !pOldCallInfo->m_sTel.empty())
				sTel = pOldCallInfo->m_sTel;
			if (sEmail.empty() && !pOldCallInfo->m_sEmail.empty())
				sEmail = pOldCallInfo->m_sEmail;
		}
	}

	sSql.Format(_T("INSERT INTO call_record_t(call_id,dep_code,dep_name,emp_code,from_uid,from_phone,from_name,from_type,company,title,tel,email) \
				   VALUES(%lld,%lld,'%s',%lld,%lld,%lld,'%s',%d,'%s','%s','%s','%s')"),
				   sCallId,sGroupCode,sDepName.c_str(),sEmpCode,pFromAccountInfo.GetUserId(),sPhone,sFromName.c_str(),nFromType,
				   sCompany.c_str(),sTitle.c_str(),sTel.c_str(),sEmail.c_str());
	theApp.m_pBoUsers->execsql(sSql);
	if (this->m_pDlgMySession != NULL && m_pDlgMySession->GetSafeHwnd())
	{
		CCallRecordInfo::pointer pCallRecordInfo = CCallRecordInfo::create();
		pCallRecordInfo->m_sCallId = sCallId;
		pCallRecordInfo->m_sGroupCode = sGroupCode;
		pCallRecordInfo->m_sGroupName = sDepName;
		pCallRecordInfo->m_sMemberCode = sEmpCode;
		pCallRecordInfo->m_nFromUserId = pFromAccountInfo.GetUserId();
		pCallRecordInfo->m_sFromAccount = pFromAccountInfo.GetAccount();
		pCallRecordInfo->m_sFromName = sFromName;
		pCallRecordInfo->m_sCompany = sCompany;
		pCallRecordInfo->m_sTitle = sTitle;
		pCallRecordInfo->m_nFromPhone = sPhone;
		pCallRecordInfo->m_sTel = sTel;
		pCallRecordInfo->m_sEmail = sEmail;
		pCallRecordInfo->m_tTime = time(0);
		pCallRecordInfo->m_bRead = false;
		//struct timeb tbNow;
		//ftime(&tbNow);
		//tbNow.time -= (tbNow.timezone*60);
		//pCallRecordInfo->m_tTime = tbNow.time;
		m_pDlgMySession->InsertCallRecord(pCallRecordInfo);
	}
}

LRESULT CPOPDlg::OnMessageCallConnected(WPARAM wParam, LPARAM lParam)
{
	sndPlaySound(NULL,SND_NODEFAULT);
#ifdef USES_EBCOM_TEST
	IEB_CallInfo * pCallInfo = (IEB_CallInfo*)wParam;
	long nConnectFlag = lParam;
	const eb::bigint sCallId = pCallInfo->CallId;
	const eb::bigint sGroupCode = pCallInfo->GroupCode;
	const eb::bigint nFromUserId = pCallInfo->FromUserId;
	const CEBString sFromaccount(pCallInfo->FromAccount.GetBSTR());
	CComPtr<IEB_AccountInfo> pFromAccountInfo = theEBClientCore->EB_GetCallAccountInfo(sCallId,nFromUserId);
	if (pFromAccountInfo == NULL) return 0;

	if (sGroupCode==0 || sGroupCode==sCallId)// ?? || !pCallInfo->m_sChatId.empty())
	{
		const bool bOffLineUser = (nConnectFlag&EB_CONNECTED_OFFLINE_USER)==EB_CONNECTED_OFFLINE_USER;
		const bool bOwnerCall = (nConnectFlag&EB_CONNECTED_OWNER_CALL)==EB_CONNECTED_OWNER_CALL;
		//const bool bOnlineInCall = (nConnectFlag&EB_CONNECTED_ONLINE_INCALL)==EB_CONNECTED_ONLINE_INCALL;
		bool bNewCall = false;
		CEBCCallInfo::pointer pEbCallInfo;
		if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
		{
			bNewCall = true;
			pEbCallInfo = CEBCCallInfo::create();
			pEbCallInfo->m_bOffLineUser = bOffLineUser;
			pEbCallInfo->m_pCallInfo = pCallInfo;
			pEbCallInfo->m_pFromAccountInfo = pFromAccountInfo;
			theApp.m_pCallList.insert(sCallId,pEbCallInfo);
		}else
		{
			pEbCallInfo->m_bOffLineUser = bOffLineUser;
		}
#ifdef USES_EBCOM_TEST
		const int nSettingValue = theEBClientCore->EB_MyAccountSetting;
#else
		const int nSettingValue = theEBAppClient.EB_GetMyAccountSetting();
#endif
		// 对方离线转在线，本端自动呼叫，不需要处理界面
		if (/*!bOnlineInCall &&*/bOwnerCall ||
			(((bOffLineUser&&sGroupCode==0) || (nSettingValue&EB_SETTING_CONNECTED_OPEN_CHAT)==EB_SETTING_CONNECTED_OPEN_CHAT/* || nConnectFlag == EB_CONNECTED_NORMAL*/) &&
			(nConnectFlag&EB_CONNECTED_AUTO_ACK)!=EB_CONNECTED_AUTO_ACK))
		{
			// 本方发起会话，离线会话
			// 自动打开聊天界面
			CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
			::FlashWindow(pDlgDialog->GetSafeHwnd(), TRUE);
		}

		// 保存会话记录
		if (bNewCall && !theApp.IsLogonVisitor())
		{
			SaveCallRecord(sCallId,sGroupCode,pEbCallInfo->m_pFromAccountInfo);

			//eb::bigint sEmpCode = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sMemberCode;
			//CEBString sDepName = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sGroupName;
			//CEBString sFromName = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sName;
			//int nFromType = (int)pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_nAccountType;
			//if (nFromType==(int)EB_ACCOUNT_TYPE_VISITOR)
			//	sFromName = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sAddress;
			//CEBString sCompany = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sEnterprise;
			//CEBString sTitle = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sTitle;
			//eb::bigint sPhone = eb_atoi64(pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sPhone.c_str());
			//CEBString sTel = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sTel;
			//CEBString sEmail = pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sEmail;
			//CString sSql;
			//if (sGroupCode==0)
			//{
			//	_bstr_t sMemberName = theEBClientCore->EB_GetMemberNameByMemberCode(pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sMemberCode);
			//	if (sMemberName.length() > 0)
			//	{
			//		sFromName = sMemberName.GetBSTR();
			//		char lpszMemberCode[24];
			//		sprintf(lpszMemberCode,"%lld",pEbCallInfo->m_pFromAccountInfo.m_pFromCardInfo.m_sMemberCode);
			//		sDepName = lpszMemberCode;	// ?
			//	}
			//}else
			//{
			//	sDepName = theEBClientCore->EB_GetGroupName(sGroupCode).GetBSTR();
			//	if (sDepName.empty())
			//	{
			//		char lpszBuffer[24];
			//		sprintf(lpszBuffer,"%lld",sGroupCode);
			//		sDepName = lpszBuffer;
			//	}
			//}
			//if (this->m_pDlgMySession != NULL && m_pDlgMySession->GetSafeHwnd())
			//{
			//	// 删除旧会话
			//	CCallRecordInfo::pointer pOldCallInfo = m_pDlgMySession->GetCallRecordInfo(sGroupCode,nFromUserId);
			//	if (pOldCallInfo.get()!=NULL)
			//	{
			//		sSql.Format(_T("DELETE FROM call_record_t WHERE call_id=%lld"),pOldCallInfo->m_sCallId);
			//		int ret = theApp.m_pBoUsers->execsql(sSql);
			//	}
			//}
			////sSql.Format(_T("INSERT INTO call_record_t(call_id,dep_code,dep_name,emp_code,from_uid,from_phone,from_name,from_type,company,title,tel,email) \
			////			   VALUES(%lld,%lld,'%s',%lld,%lld,%lld,'%s',%d,'%s','%s','%s','%s')"),
			////			   sCallId,pConnectInfo->m_sGroupCode,sDepName.c_str(),sEmpCode,pConnectInfo->GetFromUserId(),sPhone,sFromName.c_str(),nFromType,
			////	sCompany.c_str(),sTitle.c_str(),sTel.c_str(),sEmail.c_str());

			//sSql.Format(_T("INSERT INTO call_record_t(call_id,dep_code,dep_name,emp_code,from_uid,from_phone,from_name,from_type,company,title,tel,email) \
			//			   VALUES(%lld,%lld,'%s',%lld,%lld,%lld,'%s',%d,'%s','%s','%s','%s')"),
			//	sCallId,sGroupCode,sDepName.c_str(),sEmpCode,nFromUserId,sPhone,sFromName.c_str(),nFromType,
			//	sCompany.c_str(),sTitle.c_str(),sTel.c_str(),sEmail.c_str());
			//int ret = theApp.m_pBoUsers->execsql(sSql);
			//if (this->m_pDlgMySession != NULL && m_pDlgMySession->GetSafeHwnd())
			//{
			//	CCallRecordInfo::pointer pCallRecordInfo = CCallRecordInfo::create();
			//	pCallRecordInfo->m_sCallId = sCallId;
			//	pCallRecordInfo->m_sGroupCode = sGroupCode;
			//	pCallRecordInfo->m_sGroupName = sDepName;
			//	pCallRecordInfo->m_sMemberCode = sEmpCode;
			//	pCallRecordInfo->m_nFromUserId = nFromUserId;
			//	pCallRecordInfo->m_sFromAccount = sFromaccount;
			//	pCallRecordInfo->m_sFromName = sFromName;
			//	pCallRecordInfo->m_sCompany = sCompany;
			//	pCallRecordInfo->m_sTitle = sTitle;
			//	pCallRecordInfo->m_nFromPhone = sPhone;
			//	pCallRecordInfo->m_sTel = sTel;
			//	pCallRecordInfo->m_sEmail = sEmail;
			//	pCallRecordInfo->m_tTime = time(0);
			//	//struct timeb tbNow;
			//	//ftime(&tbNow);
			//	//tbNow.time -= (tbNow.timezone*60);
			//	//pCallRecordInfo->m_tTime = tbNow.time;
			//	m_pDlgMySession->InsertCallRecord(pCallRecordInfo);
			//}
		}
	}

	DeleteDlgIncomingCall(nFromUserId);
	DeleteDlgAlertingCall(nFromUserId);
#else
	const EB_CallInfo* pConnectInfo = (const EB_CallInfo*)wParam;
	int nConnectFlag = lParam;

	const eb::bigint sCallId = pConnectInfo->GetCallId();
	EB_AccountInfo pFromAccountInfo;
	if (!theEBAppClient.EB_GetCallAccountInfo(sCallId,pConnectInfo->GetFromUserId(),&pFromAccountInfo)) return 1;

	if (pConnectInfo->m_sGroupCode==0 || pConnectInfo->m_sGroupCode==sCallId)// ?? || !pCallInfo->m_sChatId.empty())
	{
		const bool bOffLineUser = (nConnectFlag&EB_CONNECTED_OFFLINE_USER)==EB_CONNECTED_OFFLINE_USER;
		const bool bOwnerCall = (nConnectFlag&EB_CONNECTED_OWNER_CALL)==EB_CONNECTED_OWNER_CALL;
		//const bool bOnlineInCall = (nConnectFlag&EB_CONNECTED_ONLINE_INCALL)==EB_CONNECTED_ONLINE_INCALL;
		bool bNewCall = false;
		CEBCCallInfo::pointer pEbCallInfo;
		if (!theApp.m_pCallList.find(sCallId,pEbCallInfo))
		{
			bNewCall = true;
			pEbCallInfo = CEBCCallInfo::create();
			pEbCallInfo->m_bOffLineUser = bOffLineUser;
			pEbCallInfo->m_pCallInfo = pConnectInfo;
			pEbCallInfo->m_pFromAccountInfo = pFromAccountInfo;
			theApp.m_pCallList.insert(sCallId,pEbCallInfo);
		}else
		{
			pEbCallInfo->m_bOffLineUser = bOffLineUser;
			pEbCallInfo->m_tLastTime = time(0);
		}
		const int nSettingValue = theEBAppClient.EB_GetMyAccountSetting();
		// 对方离线转在线，本端自动呼叫，不需要处理界面
		if (/*!bOnlineInCall &&*/bOwnerCall ||
			(((bOffLineUser&&pConnectInfo->m_sGroupCode==0) || (nSettingValue&EB_SETTING_CONNECTED_OPEN_CHAT)==EB_SETTING_CONNECTED_OPEN_CHAT/* || nConnectFlag == EB_CONNECTED_NORMAL*/) &&
			(nConnectFlag&EB_CONNECTED_AUTO_ACK)!=EB_CONNECTED_AUTO_ACK))
		{
			// 本方发起会话，离线会话
			// 自动打开聊天界面
			CDlgDialog::pointer pDlgDialog = GetDlgDialog(pEbCallInfo);
			if (bOwnerCall)
			{
				pDlgDialog->ShowWindow(SW_SHOWNORMAL);
				pDlgDialog->SetForegroundWindow();
			}else
				::FlashWindow(pDlgDialog->GetSafeHwnd(), TRUE);
		}

		// 保存会话记录
		if (bNewCall && !theEBAppClient.EB_IsLogonVisitor())
		{
			SaveCallRecord(sCallId,pConnectInfo->m_sGroupCode,pEbCallInfo->m_pFromAccountInfo);
		}
	}

	DeleteDlgIncomingCall(pConnectInfo->GetFromUserId());
	DeleteDlgAlertingCall(pConnectInfo->GetFromUserId());
#endif

	if (!theApp.m_pDialogList.exist(sCallId))
	{
		// 没有打开聊天，设置为退出标识；
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_CallExit(sCallId);
#else
		theEBAppClient.EB_CallExit(sCallId);
#endif
	}

	return 0;
}

LRESULT CPOPDlg::OnMessageCallHangup(WPARAM wParam, LPARAM lParam)
{
	sndPlaySound(NULL,SND_NODEFAULT);

#ifdef USES_EBCOM_TEST
	IEB_CallInfo * pCallInfo = (IEB_CallInfo*)wParam;
	bool bOwner = lParam==1;
	const eb::bigint sCallId = pCallInfo->CallId;
	const eb::bigint nFromUserId = pCallInfo->FromUserId;
	bool bRemoveCall = bOwner;
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sCallId, pDlgDialog, bRemoveCall))
	{
		if (bRemoveCall)
			pDlgDialog->DestroyWindow();
		else
		{
			pDlgDialog->UserExitRoom(nFromUserId,false);
		}
	}
	if (bOwner || 
		pDlgDialog.get()==NULL)		// 本地已经关闭聊天界面
	{
		theApp.m_pCallList.remove(sCallId);
	}
	DeleteDlgAlertingCall(nFromUserId);
	DeleteDlgIncomingCall(nFromUserId);
#else
	EB_CallInfo* pCallInfo = (EB_CallInfo*)wParam;
	bool bOwner = (bool)(lParam==1);

	bool bRemoveCall = bOwner;
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pCallInfo->GetCallId(), pDlgDialog, bRemoveCall))
	{
		if (bRemoveCall)
			pDlgDialog->DestroyWindow();
		else
		{
			CCrAccountInfo pCrAccountInfo;
			pCrAccountInfo.m_sAccount = pCallInfo->GetFromUserId();
			pDlgDialog->OnUserExitRoom(&pCrAccountInfo);
		}
	}
	if (bOwner || 
		pDlgDialog.get()==NULL)		// 本地已经关闭聊天界面
	{
		theApp.m_pCallList.remove(pCallInfo->GetCallId());
	}
	DeleteDlgAlertingCall(pCallInfo->GetFromUserId());
	DeleteDlgIncomingCall(pCallInfo->GetFromUserId());
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageCallError(WPARAM wParam, LPARAM lParam)
{
	sndPlaySound(NULL,SND_NODEFAULT);

#ifdef USES_EBCOM_TEST
	IEB_CallInfo * pCallInfo = (IEB_CallInfo*)wParam;
	EB_STATE_CODE nResultValue = (EB_STATE_CODE)lParam;
	const eb::bigint sCallId = pCallInfo->CallId;
	const eb::bigint nFromUserId = pCallInfo->FromUserId;
	const CEBString sFromAccount = pCallInfo->FromAccount.GetBSTR();
	CString sErrorText;
	DeleteDlgAlertingCall(nFromUserId);
	if (nResultValue == EB_STATE_ACCOUNT_NOT_EXIST)
		sErrorText = _T("用户不存在！");
	else if (nResultValue == EB_STATE_USER_OFFLINE)
		sErrorText = _T("用户离线状态！");
	else if (nResultValue == EB_STATE_UNAUTH_ERROR)
		sErrorText = _T("该帐号未通过验证！");
	else if (nResultValue == EB_STATE_ACCOUNT_FREEZE)
		sErrorText = _T("该帐号已经被临时冻结！");
	else if (nResultValue == EB_STATE_USER_BUSY)
	{
		DeleteDlgIncomingCall(nFromUserId);
		sErrorText = _T("用户线路忙！");
	}else if (nResultValue == EB_STATE_TIMEOUT_ERROR)
	{
		// 本地太久未响应，也会有该事件
		DeleteDlgIncomingCall(nFromUserId);
		sErrorText = _T("会话超时！");
	}else if (nResultValue == EB_STATE_USER_HANGUP)
	{
		DeleteDlgIncomingCall(nFromUserId);
		sErrorText = _T("用户取消呼叫！");
	}else if (nResultValue == EB_STATE_ACCOUNT_DISABLE_OFFCALL)
	{
		sErrorText = _T("用户不在线，没有开放接收离线信息权限！");
	}else if (nResultValue==EB_STATE_ACCOUNT_DISABLE_EXTCALL)
	{
		sErrorText = _T("用户不接收外部会话！");
	}else
	{
		sErrorText.Format(_T("呼叫失败 %d"),nResultValue);
	}

	CComPtr<IEB_AccountInfo> pTempFromAccountInfo = theEBClientCore->EB_GetCallAccountInfo(sCallId,nFromUserId);
	if (pTempFromAccountInfo != NULL)
	{
		SaveCallRecord(sCallId,pCallInfo->GroupCode,EB_AccountInfo(pTempFromAccountInfo));
		m_btnMySession.SetWindowText(_T("！"));
	}

	// ??
	//if (!pCallInfo->m_bOwner)
	{
		CDlgMessageBox::EbMessageBox(this,sFromAccount.c_str(),sErrorText,CDlgMessageBox::IMAGE_WARNING,10);
	}
#else
	EB_CallInfo* pCallInfo = (EB_CallInfo*)wParam;
	const eb::bigint sCallId = pCallInfo->GetCallId();
	EB_STATE_CODE nResultValue = (EB_STATE_CODE)lParam;
	CString sErrorText;
	DeleteDlgAlertingCall(pCallInfo->GetFromUserId());
	if (nResultValue == EB_STATE_ACCOUNT_NOT_EXIST)
		sErrorText = _T("用户不存在！");
	else if (nResultValue == EB_STATE_USER_OFFLINE)
		sErrorText = _T("用户离线状态！");
	else if (nResultValue == EB_STATE_UNAUTH_ERROR)
		sErrorText = _T("该帐号未通过验证！");
	else if (nResultValue == EB_STATE_ACCOUNT_FREEZE)
		sErrorText = _T("该帐号已经被临时冻结！");
	else if (nResultValue == EB_STATE_USER_BUSY)
	{
		DeleteDlgIncomingCall(pCallInfo->GetFromUserId());
		sErrorText = _T("用户线路忙！");
	}else if (nResultValue == EB_STATE_TIMEOUT_ERROR)
	{
		// 本地太久未响应，也会有该事件
		DeleteDlgIncomingCall(pCallInfo->GetFromUserId());
		sErrorText = _T("会话超时！");
	}else if (nResultValue == EB_STATE_USER_HANGUP)
	{
		DeleteDlgIncomingCall(pCallInfo->GetFromUserId());
		sErrorText = _T("用户取消呼叫！");
	}else if (nResultValue == EB_STATE_ACCOUNT_DISABLE_OFFCALL)
	{
		sErrorText = _T("用户不在线，没有开放接收离线信息权限！");
	}else if (nResultValue==EB_STATE_ACCOUNT_DISABLE_EXTCALL)
	{
		sErrorText = _T("用户不接收外部会话！");
	}else
	{
		sErrorText.Format(_T("呼叫失败 %d"),nResultValue);
	}

	EB_AccountInfo pFromAccountInfo;
	if (theEBAppClient.EB_GetCallAccountInfo(sCallId,pCallInfo->GetFromUserId(),&pFromAccountInfo))
	{
		SaveCallRecord(sCallId,pCallInfo->m_sGroupCode,pFromAccountInfo);
		m_btnMySession.SetWindowText(_T("！"));
	}

	// ??
	//if (!pCallInfo->m_bOwner)
	{
		CDlgMessageBox::EbMessageBox(this,pCallInfo->GetFromAccount().c_str(),sErrorText,CDlgMessageBox::IMAGE_WARNING,10);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageAlertingCall(WPARAM wParam, LPARAM lParam)
{
	const int const_dlg_width = 380;
	const int const_dlg_height = 262;
	int nScreenWidth = 0;
	int nScreenHeight = 0;
	theApp.GetScreenSize(nScreenWidth, nScreenHeight);
#ifdef USES_EBCOM_TEST
	IEB_CallInfo* pCallInfo = (IEB_CallInfo*)wParam;
	const eb::bigint sCallId = pCallInfo->CallId;
	const eb::bigint nFromUserId = pCallInfo->FromUserId;
	CComPtr<IEB_AccountInfo> pFromAccountInfo = theEBClientCore->EB_GetCallAccountInfo(sCallId,nFromUserId);
	if (pFromAccountInfo == NULL) return 0;

	CDlgAlertingCall::pointer pDlgAlertingCall = CDlgAlertingCall::create(this);
	pDlgAlertingCall->m_pFromAccountInfo = pFromAccountInfo;
	pDlgAlertingCall->m_sCallId = sCallId;
	pDlgAlertingCall->Create(CDlgAlertingCall::IDD, this);
	CRect rectDlgIncomingCall;
	pDlgAlertingCall->GetWindowRect(&rectDlgIncomingCall);
	pDlgAlertingCall->MoveWindow(0, nScreenHeight-const_dlg_height, const_dlg_width, const_dlg_height);
	pDlgAlertingCall->ShowWindow(SW_SHOW);
	//pDlgAlertingCall->SetActiveWindow();
	pDlgAlertingCall->SetForegroundWindow();
	m_pAlertingCallList.insert(nFromUserId, pDlgAlertingCall);
#else
	EB_CallInfo* pCallInfo = (EB_CallInfo*)wParam;
	const eb::bigint sCallId = pCallInfo->GetCallId();
	const eb::bigint nFromUserId = pCallInfo->GetFromUserId();
	const CEBString sFromAccount = pCallInfo->GetFromAccount();
	EB_AccountInfo pFromAccountInfo;
	if (!theEBAppClient.EB_GetCallAccountInfo(sCallId,nFromUserId,&pFromAccountInfo)) return 1;

	CDlgAlertingCall::pointer pDlgAlertingCall = CDlgAlertingCall::create(this);
	pDlgAlertingCall->m_pFromAccountInfo = pFromAccountInfo;
	pDlgAlertingCall->m_sCallId = sCallId;
	pDlgAlertingCall->Create(CDlgAlertingCall::IDD, this);
	CRect rectDlgIncomingCall;
	pDlgAlertingCall->GetWindowRect(&rectDlgIncomingCall);
	pDlgAlertingCall->MoveWindow(0, nScreenHeight-const_dlg_height, const_dlg_width, const_dlg_height);
	pDlgAlertingCall->ShowWindow(SW_SHOW);
	//pDlgAlertingCall->SetActiveWindow();
	pDlgAlertingCall->SetForegroundWindow();
	m_pAlertingCallList.insert(pCallInfo->GetFromUserId(), pDlgAlertingCall);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageIncomingCall(WPARAM wParam, LPARAM lParam)
{
	CString sSoundFile;
	sSoundFile.Format(_T("%s/wav/incomingcall.wav"), theApp.GetAppDataPath());
	sndPlaySound(sSoundFile, SND_ASYNC|SND_NOWAIT|SND_LOOP);
	const int const_dlg_width = 380;
	const int const_dlg_height = 262;
	int nScreenWidth = 0;
	int nScreenHeight = 0;
	theApp.GetScreenSize(nScreenWidth, nScreenHeight);
#ifdef USES_EBCOM_TEST
	IEB_CallInfo * pCallInfo = (IEB_CallInfo*)wParam;
	IEB_AccountInfo* pFromAccount = (IEB_AccountInfo*)lParam;
	const eb::bigint sCallId = pCallInfo->CallId;
	const eb::bigint nFromUserId = pFromAccount->UserId;
	//CEBCCallInfo::pointer pCallInfo = theEBAppClient.GetCallInfo(sCallId);;
	//if (pCallInfo.get() == NULL) return 1;

	CDlgIncomingCall::pointer pDlgIncomingCall = CDlgIncomingCall::create(this);
	pDlgIncomingCall->m_pFromAccountInfo = pFromAccount;
	pDlgIncomingCall->m_pCallInfo = pCallInfo;
	pDlgIncomingCall->Create(CDlgIncomingCall::IDD, this);
	pDlgIncomingCall->MoveWindow(nScreenWidth-const_dlg_width, nScreenHeight-const_dlg_height, const_dlg_width, const_dlg_height);
	pDlgIncomingCall->SetCircle();
	pDlgIncomingCall->ShowWindow(SW_SHOW);
	//pDlgIncomingCall->SetActiveWindow();
	pDlgIncomingCall->SetForegroundWindow();
	m_pIncomingCallList.insert(nFromUserId, pDlgIncomingCall);
#else
	const EB_CallInfo* pCallInfo = (const EB_CallInfo*)wParam;
	const EB_AccountInfo* pFromAccount = (const EB_AccountInfo*)lParam;

	const eb::bigint sCallId = pCallInfo->GetCallId();
	//CEBCCallInfo::pointer pCallInfo = theEBAppClient.GetCallInfo(sCallId);;
	//if (pCallInfo.get() == NULL) return 1;

	CDlgIncomingCall::pointer pDlgIncomingCall = CDlgIncomingCall::create(this);
	pDlgIncomingCall->m_pFromAccountInfo = pFromAccount;
	pDlgIncomingCall->m_pCallInfo = pCallInfo;
	pDlgIncomingCall->Create(CDlgIncomingCall::IDD, this);
	pDlgIncomingCall->MoveWindow(nScreenWidth-const_dlg_width, nScreenHeight-const_dlg_height, const_dlg_width, const_dlg_height);
	pDlgIncomingCall->SetCircle();
	pDlgIncomingCall->ShowWindow(SW_SHOW);
	//pDlgIncomingCall->SetActiveWindow();
	pDlgIncomingCall->SetForegroundWindow();
	m_pIncomingCallList.insert(pCallInfo->GetFromUserId(), pDlgIncomingCall);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageContactInfo(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ContactInfo * pContactInfo = (IEB_ContactInfo*)wParam;
	if (m_pDlgMyContacts != NULL)
		m_pDlgMyContacts->ContactInfo(pContactInfo);
#else
	const EB_ContactInfo* pPopContactInfo = (const EB_ContactInfo*)wParam;
	if (m_pDlgMyContacts != NULL)
		m_pDlgMyContacts->ContactInfo(pPopContactInfo);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageContactDelete(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ContactInfo * pContactInfo = (IEB_ContactInfo*)wParam;
	if (m_pDlgMyContacts != NULL)
		m_pDlgMyContacts->DeleteContact(pContactInfo);
#else
	const EB_ContactInfo* pPopContactInfo = (const EB_ContactInfo*)wParam;
	if (m_pDlgMyContacts != NULL)
		m_pDlgMyContacts->DeleteContact(pPopContactInfo);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageEnterpriseInfo(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_EnterpriseInfo * pEnterpriseInfo = (IEB_EnterpriseInfo*)wParam;
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->EnterpriseInfo(pEnterpriseInfo);

	if (m_btnMyEnterprise.GetSafeHwnd() && !m_btnMyEnterprise.IsWindowVisible())
	{
		m_btnMyEnterprise.ShowWindow(SW_SHOW);
		m_btnMyEnterprise.Invalidate();
	}

	ChangeTrayText();
#else
	const EB_EnterpriseInfo* pEnterpriseInfo = (const EB_EnterpriseInfo*)wParam;
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->EnterpriseInfo(pEnterpriseInfo);

	if (m_btnMyEnterprise.GetSafeHwnd() && !m_btnMyEnterprise.IsWindowVisible())
	{
		m_btnMyEnterprise.ShowWindow(SW_SHOW);
		m_btnMyEnterprise.Invalidate();
	}

	ChangeTrayText();
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageGroupInfo(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_GroupInfo * pGroupInfo = (IEB_GroupInfo*)wParam;
	bool bIsMyGroup = lParam==1;
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->DepartmentInfo(pGroupInfo);
	if (bIsMyGroup && m_pDlgMyGroup != NULL)
	{
		m_pDlgMyGroup->MyDepartmentInfo(pGroupInfo);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pGroupInfo->GroupCode, pDlgDialog))
	{
		// 群资料已经修改
		pDlgDialog->ChangeDepartmentInfo(pGroupInfo);
	}
#else
	const EB_GroupInfo* pGroupInfo = (const EB_GroupInfo*)wParam;
	bool bIsMyDepartment = (bool)(lParam==1);
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->DepartmentInfo(pGroupInfo);
	if (bIsMyDepartment && m_pDlgMyGroup != NULL)
	{
		m_pDlgMyGroup->MyDepartmentInfo(pGroupInfo);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog))
	{
		// 群资料已经修改
		pDlgDialog->ChangeDepartmentInfo(pGroupInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageGroupDelete(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_GroupInfo * pGroupInfo = (IEB_GroupInfo*)wParam;
	bool bIsMyGroup = lParam==1;
	const eb::bigint sGroupCode = pGroupInfo->GroupCode;
	const CEBString sGroupName = pGroupInfo->GroupName.GetBSTR();
	if (m_pDlgMyEnterprise != NULL)
	{
		m_pDlgMyEnterprise->DeleteDepartmentInfo(sGroupCode);
	}
	if (m_pDlgMyGroup != NULL)
		m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);

	// 关闭现在会话窗口
	theApp.m_pCallList.remove(sGroupCode);
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog, true))
	{
		//pDlgDialog->ExitRoom(1);
		pDlgDialog->DestroyWindow();
	}
	CString stext;
	stext.Format(_T("解散%s"), sGroupName.c_str());
	CDlgMessageBox::EbMessageBox(this,_T("管理员"),stext,CDlgMessageBox::IMAGE_WARNING,10);

	// ?? remove
	//CDlgDialog::pointer pDlgDialog;
	//if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog))
	//{
	//	// 群资料已经修改
	//	pDlgDialog->ChangeDepartmentInfo(pGroupInfo);
	//}
#else
	const EB_GroupInfo* pGroupInfo = (const EB_GroupInfo*)wParam;
	bool bIsMyDepartment = (bool)(lParam==1);
	if (m_pDlgMyEnterprise != NULL)
	{
		m_pDlgMyEnterprise->DeleteDepartmentInfo(pGroupInfo->m_sGroupCode);
	}
	if (m_pDlgMyGroup != NULL)
		m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);

	// 关闭现在会话窗口
	theApp.m_pCallList.remove(pGroupInfo->m_sGroupCode);
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog, true))
	{
		//pDlgDialog->ExitRoom(1);
		pDlgDialog->DestroyWindow();
	}
	CString stext;
	stext.Format(_T("解散%s"), pGroupInfo->m_sGroupName.c_str());
	CDlgMessageBox::EbMessageBox(this,_T("管理员"),stext,CDlgMessageBox::IMAGE_INFORMATION,10);

	// ?? remove
	//CDlgDialog::pointer pDlgDialog;
	//if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog))
	//{
	//	// 群资料已经修改
	//	pDlgDialog->ChangeDepartmentInfo(pGroupInfo);
	//}
#endif
	return 0;
}
LRESULT CPOPDlg::OnMessageGroupEditError(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_GroupInfo * pGroupInfo = (IEB_GroupInfo*)wParam;
	EB_STATE_CODE nErrorCode = (EB_STATE_CODE)lParam;
#else
	const EB_GroupInfo* pGroupInfo = (const EB_GroupInfo*)wParam;
	EB_STATE_CODE nErrorCode = (EB_STATE_CODE)lParam;
#endif
	switch (nErrorCode)
	{
	case EB_STATE_NOT_AUTH_ERROR:
		CDlgMessageBox::EbDoModal(this,_T("对不起，你没有权限进行操作！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		break;
	default:
		CDlgMessageBox::EbDoModal(this,_T("编辑失败："),_T("请重试或联系客服！"),CDlgMessageBox::IMAGE_ERROR);
		break;
	}
	return 0;
}

LRESULT CPOPDlg::OnMessageRemoveGroup(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_GroupInfo * pGroupInfo = (IEB_GroupInfo*)wParam;
	IEB_MemberInfo* pMemberInfo = (IEB_MemberInfo*)lParam;
	const eb::bigint sGroupCode(pGroupInfo->GroupCode);
	const CEBString sGroupName(pGroupInfo->GroupName.GetBSTR());
	const eb::bigint sRemoveMemberCode(pMemberInfo->MemberCode);
	const eb::bigint sRemoveAccount(pMemberInfo->MemberUserId);
	// 找到现在会话，移除用户数据
	CString stext;
	if (sRemoveAccount == theApp.GetLogonUserId())
	{
		theApp.m_pCallList.remove(sGroupCode);

		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog, true))
		{
			//pDlgDialog->ExitRoom(1);
			pDlgDialog->DestroyWindow();
		}
		if (m_pDlgMyGroup)
			m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);
		stext.Format(_T("你被管理员移除出%s"), sGroupName.c_str());
		CDlgMessageBox::EbMessageBox(this,"EB2014",stext,CDlgMessageBox::IMAGE_WARNING,10);
	}else
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog))
		{
			pDlgDialog->UserExitRoom(sRemoveAccount,true);
		}
		if (m_pDlgMyEnterprise != NULL)
			m_pDlgMyEnterprise->DeleteEmployeeInfo(pGroupInfo,sRemoveMemberCode);
		if (m_pDlgMyGroup != NULL)// && theEBClientCore->EB_IsMyGroup(pMemberInfo->GroupCode))
			m_pDlgMyGroup->DeleteEmployeeInfo(pGroupInfo,sRemoveMemberCode);
		//if (m_pDlgMyGroup)
		//	m_pDlgMyGroup->ExitDepartment(sGroupCode, sRemoveAccount);
	}
#else
	const EB_GroupInfo* pGroupInfo = (const EB_GroupInfo*)wParam;
	const EB_MemberInfo* pMemberInfo = (const EB_MemberInfo*)lParam;

	const eb::bigint sRemoveUserId(pMemberInfo->m_nMemberUserId);
	// 找到现在会话，移除用户数据
	CString stext;
	if (sRemoveUserId == theEBAppClient.EB_GetLogonUserId())
	{
		theApp.m_pCallList.remove(pGroupInfo->m_sGroupCode);

		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog, true))
		{
			//pDlgDialog->ExitRoom(1);
			pDlgDialog->DestroyWindow();
		}
		if (m_pDlgMyGroup)
			m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);
		stext.Format(_T("你被管理员移除出%s"), pGroupInfo->m_sGroupName.c_str());
		CDlgMessageBox::EbMessageBox(this,"EB2014",stext,CDlgMessageBox::IMAGE_INFORMATION,10);
	}else
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog))
		{
			pDlgDialog->UserExitRoom(sRemoveUserId,true);
		}
		if (m_pDlgMyGroup != NULL)// && theEBAppClient.EB_IsMyGroup(pMemberInfo->m_sGroupCode))
			m_pDlgMyGroup->DeleteEmployeeInfo(pGroupInfo,pMemberInfo->m_sMemberCode);
		//if (m_pDlgMyGroup)
		//	m_pDlgMyGroup->ExitDepartment(pGroupInfo->m_sGroupCode, sRemoveUserId);
	}
	if (pGroupInfo->m_sEnterpriseCode>0 && m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->DeleteEmployeeInfo(pGroupInfo,pMemberInfo->m_sMemberCode);
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageExitGroup(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_GroupInfo* pGroupInfo = (IEB_GroupInfo*)wParam;
	IEB_MemberInfo* pMemberInfo = (IEB_MemberInfo*)lParam;
	const eb::bigint sGroupCode(pGroupInfo->GroupCode);
	const CEBString sGroupName(pGroupInfo->GroupName.GetBSTR());
	const eb::bigint nCreateUserId = pGroupInfo->CreateUserId;
	const eb::bigint sExitAccount(pMemberInfo->MemberUserId);
	const CEBString sUserName(pMemberInfo->UserName.GetBSTR());
	const eb::bigint sMemberCode(pMemberInfo->MemberCode);
	const CEBString sMemberAccount(pMemberInfo->MemberAccount.GetBSTR());
	// 找到现在会话，移除用户数据
	if (sExitAccount == theApp.GetLogonUserId())
	{
		theApp.m_pCallList.remove(sGroupCode);
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog, true))
		{
			//pDlgDialog->ExitRoom(1);
			pDlgDialog->DestroyWindow();
		}
		if (m_pDlgMyGroup)
			m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);
		return 0;
	}else
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog))
		{
			pDlgDialog->UserExitRoom(sExitAccount,true);
		}
		if (m_pDlgMyEnterprise != NULL)
			m_pDlgMyEnterprise->DeleteEmployeeInfo(pGroupInfo, sMemberCode);
		if (m_pDlgMyGroup != NULL)// && theEBClientCore->EB_IsMyGroup(pMemberInfo->GroupCode))
			m_pDlgMyGroup->DeleteEmployeeInfo(pGroupInfo,sMemberCode);
		//if (m_pDlgMyGroup)
		//	m_pDlgMyGroup->ExitDepartment(sGroupCode, sExitAccount);
	}
	if (nCreateUserId==theApp.GetLogonUserId())
	{
		CString swindow;
		swindow.Format(_T("%s(%s)"), sUserName.c_str(),sMemberAccount.c_str());
		CString stext;
		stext.Format(_T("已经退出%s"), sGroupName.c_str());
		CDlgMessageBox::EbMessageBox(this,swindow,stext,CDlgMessageBox::IMAGE_WARNING);
	}
#else
	const EB_GroupInfo* pGroupInfo = (const EB_GroupInfo*)wParam;
	const EB_MemberInfo* pMemberInfo = (const EB_MemberInfo*)lParam;

	const eb::bigint sExitUserId(pMemberInfo->m_nMemberUserId);
	// 找到现在会话，移除用户数据
	if (sExitUserId == theEBAppClient.EB_GetLogonUserId())
	{
		theApp.m_pCallList.remove(pGroupInfo->m_sGroupCode);
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog, true))
		{
			//pDlgDialog->ExitRoom(1);
			pDlgDialog->DestroyWindow();
		}
		if (m_pDlgMyGroup)
			m_pDlgMyGroup->DeleteDepartmentInfo(pGroupInfo);
		return 0;
	}else
	{
		CDlgDialog::pointer pDlgDialog;
		if (theApp.m_pDialogList.find(pGroupInfo->m_sGroupCode, pDlgDialog))
		{
			pDlgDialog->UserExitRoom(sExitUserId,true);
		}
		if (m_pDlgMyEnterprise != NULL)
			m_pDlgMyEnterprise->DeleteEmployeeInfo(pGroupInfo,pMemberInfo->m_sMemberCode);
		if (m_pDlgMyGroup != NULL)// && theEBAppClient.EB_IsMyGroup(pMemberInfo->m_sGroupCode))
			m_pDlgMyGroup->DeleteEmployeeInfo(pGroupInfo,pMemberInfo->m_sMemberCode);
		//if (m_pDlgMyGroup)
		//	m_pDlgMyGroup->ExitDepartment(pGroupInfo->m_sGroupCode, sExitUserId);
	}
	if (pGroupInfo->m_nCreateUserId==theEBAppClient.EB_GetLogonUserId())
	{
		CString swindow;
		swindow.Format(_T("%s(%s)"), pMemberInfo->m_sUserName.c_str(),pMemberInfo->m_sMemberAccount.c_str());
		CString stext;
		stext.Format(_T("已经退出%s"), pGroupInfo->m_sGroupName.c_str());
		CDlgMessageBox::EbMessageBox(this,swindow,stext,CDlgMessageBox::IMAGE_INFORMATION, 10);
	}
#endif
	return 0;
}
LRESULT CPOPDlg::OnMessageRequestJoin2Group(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST

#else
	const EB_AccountInfo* pRequestAccountInfo = (const EB_AccountInfo*)wParam;
	const EB_APMsgInfo* pApMsgInfo = (const EB_APMsgInfo*)lParam;

	//CString stext;
	//stext.Format(_T("%lld(%s)申请加入群组%s(%lld):%s"),
	//	pRequestAccountInfo->GetUserId(),pRequestAccountInfo->GetAccount().c_str(),pApMsgInfo->m_sMsgName.c_str(),pApMsgInfo->m_nGroupId,pApMsgInfo->m_sMsgContent.c_str());
	//CString swindow;
	//swindow.Format(_T("MSG-ID:%lld"), pApMsgInfo->m_nMsgId);
	//CDlgMessageBox::EbMessageBox(this,swindow,stext,CDlgMessageBox::IMAGE_INFORMATION);

	//theEBAppClient.EB_AckMsg(pApMsgInfo->m_nMsgId,false);

#endif
	return 0;
}
LRESULT CPOPDlg::OnMessageInviteJoin2Group(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST

#else
	const EB_AccountInfo* pRequestAccountInfo = (const EB_AccountInfo*)wParam;
	const EB_APMsgInfo* pApMsgInfo = (const EB_APMsgInfo*)lParam;

	//CString stext;
	//stext.Format(_T("%lld(%s)邀请加入群组%s(%lld):%s"),
	//	pRequestAccountInfo->GetUserId(),pRequestAccountInfo->GetAccount().c_str(),pApMsgInfo->m_sMsgName.c_str(),pApMsgInfo->m_nGroupId,pApMsgInfo->m_sMsgContent.c_str());
	//CString swindow;
	//swindow.Format(_T("MSG-ID:%lld"), pApMsgInfo->m_nMsgId);
	//CDlgMessageBox::EbMessageBox(this,swindow,stext,CDlgMessageBox::IMAGE_INFORMATION);

	//theEBAppClient.EB_AckMsg(pApMsgInfo->m_nMsgId,true);

#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageMemberInfo(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_MemberInfo * pMemberInfo = (IEB_MemberInfo*)wParam;
	bool bIsMyDefaultMember = lParam==1;
	const eb::bigint sGroupCode(pMemberInfo->GroupCode);
	const CEBString sUserName(pMemberInfo->UserName.GetBSTR());
	const CEBString sMemberAccount(pMemberInfo->MemberAccount.GetBSTR());
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->EmployeeInfo(pMemberInfo);
	if (m_pDlgMyGroup != NULL)
	{
		if (theEBClientCore->EB_IsMyGroup(sGroupCode))
		{
			m_pDlgMyGroup->MyDepMemberInfo(pMemberInfo);
		}
	}
	if (bIsMyDefaultMember)
	{
		this->Invalidate();	// 刷新重画自己头像
		const CEBString sSettingEnterprise = theEBSetting->Enterprise.GetBSTR();
		CString sWindowText;
		CComPtr<IEB_EnterpriseInfo> pEnterpriseInfo = theEBClientCore->EB_GetEnterpriseInfo(0);
		if (pEnterpriseInfo != NULL)
		{
			sWindowText.Format(_T("%s-%s(%s) %s"),sSettingEnterprise.c_str(),sUserName.c_str(),
				sMemberAccount.c_str(),CEBString(pEnterpriseInfo->EnterpriseName.GetBSTR()).c_str());
		}else
		{
			sWindowText.Format(_T("%s-%s(%s)"),sSettingEnterprise.c_str(),sUserName.c_str(),
				sMemberAccount.c_str());
		}
		this->SetWindowText(sWindowText);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(sGroupCode, pDlgDialog))
	{
		pDlgDialog->OnUserEmpInfo(pMemberInfo);
	}
#else
	const EB_MemberInfo* pMemberInfo = (const EB_MemberInfo*)wParam;
	bool bIsMyDefaultMember = (bool)(lParam==1);
	if (m_pDlgMyEnterprise != NULL)
		m_pDlgMyEnterprise->EmployeeInfo(pMemberInfo);
	if (m_pDlgMyGroup != NULL)
	{
		if (theEBAppClient.EB_IsMyGroup(pMemberInfo->m_sGroupCode))
		{
			m_pDlgMyGroup->MyDepMemberInfo(pMemberInfo);
		}
	}
	if (bIsMyDefaultMember)
	{
		this->Invalidate();	// 刷新重画自己头像

		CString sWindowText;
		EB_EnterpriseInfo pEnterpriseInfo;
		if (theEBAppClient.EB_GetEnterpriseInfo(&pEnterpriseInfo))
		{
			sWindowText.Format(_T("%s-%s(%s) %s"),theSetting.GetEnterprise().c_str(),pMemberInfo->m_sUserName.c_str(),
				pMemberInfo->m_sMemberAccount.c_str(),pEnterpriseInfo.m_sEnterpriseName.c_str());
		}else
		{
			sWindowText.Format(_T("%s-%s(%s)"),theSetting.GetEnterprise().c_str(),pMemberInfo->m_sUserName.c_str(),
				pMemberInfo->m_sMemberAccount.c_str());
		}
		this->SetWindowText(sWindowText);
	}
	CDlgDialog::pointer pDlgDialog;
	if (theApp.m_pDialogList.find(pMemberInfo->m_sGroupCode, pDlgDialog))
	{
		pDlgDialog->OnUserEmpInfo(pMemberInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageMemberDelete(WPARAM wParam, LPARAM lParam)
{
	// ***统一在EB_WM_REMOVE_GROUP消息处理
//#ifdef USES_EBCOM_TEST
//	IEB_MemberInfo* pMemberInfo = (IEB_MemberInfo*)wParam;
//	bool bIsMyDefaultMember = (bool)(lParam==1);
//	if (m_pDlgMyEnterprise != NULL)
//		m_pDlgMyEnterprise->DeleteEmployeeInfo(pMemberInfo);
//	// 我的部门
//	if (m_pDlgMyGroup != NULL && theEBClientCore->EB_IsMyGroup(pMemberInfo->GroupCode))
//		m_pDlgMyGroup->DeleteEmployeeInfo(pMemberInfo);
//#else
//	const EB_MemberInfo* pMemberInfo = (const EB_MemberInfo*)wParam;
//	bool bIsMyDefaultMember = (bool)(lParam==1);
//	if (m_pDlgMyEnterprise != NULL)
//		m_pDlgMyEnterprise->DeleteEmployeeInfo(pMemberInfo);
//	// 我的部门
//	if (m_pDlgMyGroup != NULL && theEBAppClient.EB_IsMyGroup(pMemberInfo->m_sGroupCode))
//		m_pDlgMyGroup->DeleteEmployeeInfo(pMemberInfo);
//#endif
	return 0;
}
LRESULT CPOPDlg::OnMessageMemberEditError(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_MemberInfo* pMemberInfo = (IEB_MemberInfo*)wParam;
	EB_STATE_CODE nErrorCode = (EB_STATE_CODE)lParam;
#else
	const EB_MemberInfo* pMemberInfo = (const EB_MemberInfo*)wParam;
	EB_STATE_CODE nErrorCode = (EB_STATE_CODE)lParam;
#endif
	switch (nErrorCode)
	{
	case EB_STATE_OAUTH_FORWARD:
		CDlgMessageBox::EbDoModal(this,_T("邀请成员成功，等待对方接受！"),_T(""),CDlgMessageBox::IMAGE_INFORMATION);
		break;
	case EB_STATE_NOT_AUTH_ERROR:
		CDlgMessageBox::EbDoModal(this,_T("对不起，你没有权限进行操作！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		break;
	case EB_STATE_ACCOUNT_ALREADY_EXIST:
		CDlgMessageBox::EbDoModal(this,_T("添加失败："),_T("该邮箱帐号已经存在"),CDlgMessageBox::IMAGE_ERROR);
		break;
	default:
		CDlgMessageBox::EbDoModal(this,_T("编辑失败："),_T("请重试或联系客服！"),CDlgMessageBox::IMAGE_ERROR);
		break;
	}
	return 0;
}

//LRESULT CPOPDlg::OnMessageAppMsgInfo(WPARAM wParam, LPARAM lParam)
//{
//	const EB_AppMsgInfo * pMsgInfo = (const EB_AppMsgInfo*)wParam;
//	if (pMsgInfo == NULL) return 1;
//	switch (pMsgInfo->GetMsgType())
//	{
//	case EB_MSG_REMOVE_GROUP:
//		{
//			//const tstring sRemoveAccount = pMsgInfo->GetMsgContent();
//			//EB_GroupInfo pGroupInfo;
//			//if (!theEBAppClient.EB_GetGroupInfo(pMsgInfo->m_sGroupCode,&pGroupInfo))
//			//	break;
//			//// 找到现在会话，移除用户数据
//			//CString stext;
//			//if (sRemoveAccount == theEBAppClient.EB_GetLogonAccount())
//			//{
//			//	theApp.m_pCallList.remove(pMsgInfo->m_sGroupCode);
//
//			//	CDlgDialog::pointer pDlgDialog;
//			//	if (theApp.m_pDialogList.find(pMsgInfo->m_sGroupCode, pDlgDialog, true))
//			//	{
//			//		//pDlgDialog->ExitRoom(1);
//			//		pDlgDialog->DestroyWindow();
//			//	}
//			//	if (m_pDlgMyGroup)
//			//		m_pDlgMyGroup->DeleteDepartment(pMsgInfo->m_sGroupCode);
//			//	stext.Format(_T("你被管理员移除出%s"), pGroupInfo.m_sGroupName.c_str());
//			//	CDlgMessageBox::EbMessageBox(this,"EB2014",stext);
//			//}else
//			//{
//			//	CDlgDialog::pointer pDlgDialog;
//			//	if (theApp.m_pDialogList.find(pMsgInfo->m_sGroupCode, pDlgDialog))
//			//	{
//			//		pDlgDialog->UserExitRoom(sRemoveAccount.c_str(),true);
//			//	}
//			//	if (m_pDlgMyGroup)
//			//		m_pDlgMyGroup->ExitDepartment(pMsgInfo->m_sGroupCode, sRemoveAccount);
//			//	//stext.Format(_T("%s用户被管理员移除出%s"), sRemoveAccount.c_str(), pGroupInfo->m_sGroupName.c_str());
//			//	//CDlgMessageBox::EbMessageBox(this,"",stext);
//			//}
//		}break;
//	case EB_MSG_EXIT_GROUP:
//		{
//			//const tstring sExitAccount = pMsgInfo->GetMsgContent();
//			//// 找到现在会话，移除用户数据
//			//if (sExitAccount == theEBAppClient.EB_GetLogonAccount())
//			//{
//			//	theApp.m_pCallList.remove(pMsgInfo->m_sGroupCode);
//			//	CDlgDialog::pointer pDlgDialog;
//			//	if (theApp.m_pDialogList.find(pMsgInfo->m_sGroupCode, pDlgDialog, true))
//			//	{
//			//		//pDlgDialog->ExitRoom(1);
//			//		pDlgDialog->DestroyWindow();
//			//	}
//			//	if (m_pDlgMyGroup)
//			//		m_pDlgMyGroup->DeleteDepartment(pMsgInfo->m_sGroupCode);
//			//	break;
//			//}else
//			//{
//			//	CDlgDialog::pointer pDlgDialog;
//			//	if (theApp.m_pDialogList.find(pMsgInfo->m_sGroupCode, pDlgDialog))
//			//	{
//			//		pDlgDialog->UserExitRoom(sExitAccount.c_str(),true);
//			//	}
//			//	if (m_pDlgMyGroup)
//			//		m_pDlgMyGroup->ExitDepartment(pMsgInfo->m_sGroupCode, sExitAccount);
//			//}
//
//			//EB_GroupInfo pGroupInfo;
//			//if (!theEBAppClient.EB_GetGroupInfo(pMsgInfo->m_sGroupCode,&pGroupInfo))
//			//	break;
//			//if (pGroupInfo.m_sCreator==theEBAppClient.EB_GetLogonAccount())
//			//{
//			//	CString stext;
//			//	stext.Format(_T("已经退出%s"), pGroupInfo.m_sGroupName.c_str());
//			//	CDlgMessageBox::EbMessageBox(this,sExitAccount.c_str(),stext);
//			//}
//		}break;
//	case EB_MSG_DELETE_GROUP:
//		{
//			if (m_pDlgMyGroup)
//				m_pDlgMyGroup->DeleteDepartment(pMsgInfo->m_sGroupCode);
//			// 关闭现在会话窗口
//			theApp.m_pCallList.remove(pMsgInfo->m_sGroupCode);
//			CDlgDialog::pointer pDlgDialog;
//			if (theApp.m_pDialogList.find(pMsgInfo->m_sGroupCode, pDlgDialog, true))
//			{
//				//pDlgDialog->ExitRoom(1);
//				pDlgDialog->DestroyWindow();
//			}
//			CString stext;
//			if (pMsgInfo->m_sFromAccount == theEBAppClient.EB_GetLogonAccount())
//			{
//				stext.Format(_T("成功解散%s"), pMsgInfo->GetMsgContent().c_str());
//			}else
//			{
//				stext.Format(_T("已经解散%s"), pMsgInfo->GetMsgContent().c_str());
//			}
//			CDlgMessageBox::EbMessageBox(this,pMsgInfo->m_sFromAccount.c_str(),stext);
//		}break;
//	default:
//		break;
//	}
//	return 0;
//}

LRESULT CPOPDlg::OnMessageResourceInfo(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ResourceInfo* pResourceInfo = (IEB_ResourceInfo*)wParam;
	const eb::bigint sGroupCode = pResourceInfo->GroupCode;
	const eb::bigint sEnterpriseCode = pResourceInfo->EnterpriseCode;
	eb::bigint sId = 0;
	if (sGroupCode>0)
	{
		sId = sGroupCode;
	}else if (sEnterpriseCode>0)
	{
		sId = sEnterpriseCode;
	}else
	{
		sId = theApp.GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceInfo(pResourceInfo);
	}
#else
	const EB_ResourceInfo* pResourceInfo = (const EB_ResourceInfo*)wParam;
	eb::bigint sId = 0;
	if (pResourceInfo->m_sGroupCode>0)
	{
		sId = pResourceInfo->m_sGroupCode;
	}else if (pResourceInfo->m_sEnterpriseCode>0)
	{
		sId = pResourceInfo->m_sEnterpriseCode;
	}else
	{
		sId = theEBAppClient.EB_GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceInfo(*pResourceInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageDeleteResource(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ResourceInfo* pResourceInfo = (IEB_ResourceInfo*)wParam;
	const eb::bigint sGroupCode = pResourceInfo->GroupCode;
	const eb::bigint sEnterpriseCode = pResourceInfo->EnterpriseCode;
	eb::bigint sId = 0;
	if (sGroupCode>0)
	{
		sId = sGroupCode;
	}else if (sEnterpriseCode>0)
	{
		sId = sEnterpriseCode;
	}else
	{
		sId = theApp.GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceDelete(pResourceInfo);
	}
#else
	const EB_ResourceInfo* pResourceInfo = (const EB_ResourceInfo*)wParam;
	eb::bigint sId = 0;
	if (pResourceInfo->m_sGroupCode>0)
	{
		sId = pResourceInfo->m_sGroupCode;
	}else if (pResourceInfo->m_sEnterpriseCode>0)
	{
		sId = pResourceInfo->m_sEnterpriseCode;
	}else
	{
		sId = theEBAppClient.EB_GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceDelete(*pResourceInfo);
	}
#endif
	return 0;
}

LRESULT CPOPDlg::OnMessageMoveResource(WPARAM wParam, LPARAM lParam)
{
#ifdef USES_EBCOM_TEST
	IEB_ResourceInfo* pResourceInfo = (IEB_ResourceInfo*)wParam;
	const CEBString sOldParentResId = (BSTR)lParam;
	const eb::bigint sGroupCode = pResourceInfo->GroupCode;
	const eb::bigint sEnterpriseCode = pResourceInfo->EnterpriseCode;
	eb::bigint sId = 0;
	if (sGroupCode>0)
	{
		sId = sGroupCode;
	}else if (sEnterpriseCode>0)
	{
		sId = sEnterpriseCode;
	}else
	{
		sId = theApp.GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceMove(pResourceInfo,sOldParentResId);
	}
#else
	const EB_ResourceInfo* pResourceInfo = (const EB_ResourceInfo*)wParam;
	const char* sOldParentResId = (const char*)lParam;
	eb::bigint sId = 0;
	if (pResourceInfo->m_sGroupCode>0)
	{
		sId = pResourceInfo->m_sGroupCode;
	}else if (pResourceInfo->m_sEnterpriseCode>0)
	{
		sId = pResourceInfo->m_sEnterpriseCode;
	}else
	{
		sId = theEBAppClient.EB_GetLogonUserId();
	}
	CDlgResourceMgr * pDlgResourceMgr = NULL;
	if (theApp.m_pResourceMgr.find(sId,pDlgResourceMgr))
	{
		pDlgResourceMgr->OnResourceMove(*pResourceInfo,sOldParentResId);
	}
#endif
	return 0;
}

CDlgDialog::pointer CPOPDlg::GetDlgDialog(const CEBCCallInfo::pointer & pEbCallInfo)
{
	const eb::bigint sCallId = pEbCallInfo->m_pCallInfo.GetCallId();
	CDlgDialog::pointer pDlgDialog;
	if (!theApp.m_pDialogList.find(sCallId, pDlgDialog))
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		pDlgDialog = CDlgDialog::create(pParent);
		pDlgDialog->m_pEbCallInfo = pEbCallInfo;
		pDlgDialog->Create(CDlgDialog::IDD, pParent);
		//pDlgDialog->SetForegroundWindow();	// **不弹最前面，由外面自动判断处理
		theApp.m_pDialogList.insert(sCallId, pDlgDialog);
	}
	pDlgDialog->ShowWindow(SW_SHOW);
	return pDlgDialog;
}

LRESULT CPOPDlg::OnMessageHotKey(WPARAM wParam, LPARAM lParam)
{
	UINT fuModifiers = (UINT) LOWORD(lParam);  // key-modifier flags   
	UINT uVirtKey = (UINT) HIWORD(lParam);     // virtual-key code   

	//判断响应了什么热键  
	if( (MOD_ALT|MOD_SHIFT) == fuModifiers && 'A' == uVirtKey )  
	{  
		CString sWindowText;
		this->GetForegroundWindow()->GetWindowText(sWindowText);
		CString sParameter;
		sParameter.Format(_T("\"%s\" %d"),sWindowText,(int)EB_MSG_EBSC_OK);
		theApp.RunEBSC(sParameter);
	}else if( (MOD_ALT|MOD_SHIFT) == fuModifiers && 'Z' == uVirtKey ) 
	{
		if (this->IsWindowVisible())
			this->ShowWindow(SW_HIDE);
		else
		{
			OnOpenMain();
		}
	}
	return 0;
}

//LRESULT CPOPDlg::OnMessageWindowResize(WPARAM wParam, LPARAM lParam)
//{
//	SetCircle();
//	this->Invalidate();
//	return 0;
//}


//void CPOPDlg::OnBnClickedButtonSelectuser()
//{
//	CDlgDiaRecord pDlgCallNumber(this);
//	if (pDlgCallNumber.DoModal() == IDOK)
//	{
//		m_sCallNumber = pDlgCallNumber.m_sCallNumber;
//		UpdateData(FALSE);
//		m_comboNumbers.InsertString(0, m_sCallNumber);
//	}
//}
void CPOPDlg::InitData(void)
{
	//
}


//void CPOPDlg::OnCbnSelchangeComboNumbers()
//{
//	int ncursel = m_comboNumbers.GetCurSel();
//	if (ncursel >= 0)
//	{
//		CString sAccount;
//		m_comboNumbers.GetLBText(ncursel, sAccount);
//		//m_sCallNumber = sAccount;
//		UpdateData(FALSE);
//	}
//}

void CPOPDlg::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);
	const int const_minbtn_width = m_btnMin.GetImgWidth();
	const int const_minbtn_height = m_btnMin.GetImgHeight();
	int btnx = 0;
	btnx = cx-m_btnClose.GetImgWidth()-2;
	m_btnClose.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;
	m_btnMax.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;
	m_btnMin.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;
	if (m_btnSkin.GetSafeHwnd())
		m_btnSkin.MoveWindow(btnx,2,const_minbtn_width,const_minbtn_height);

	int y = POS_LINESTATE_TOP;
	m_btnLineState.MovePoint(POS_LINESTATE_LEFT,y);
	if (this->GetDlgItem(IDC_EDIT_DESCRIPTION)->GetSafeHwnd())
	{
		y += 22;
		this->GetDlgItem(IDC_EDIT_DESCRIPTION)->MoveWindow(POS_LINESTATE_LEFT,y,cx-(POS_LINESTATE_LEFT)-3,18);
	}
	y += 20;
	int x = POS_LINESTATE_LEFT+2;
	m_btnMyCenter.MovePoint(x,y);
	x += m_btnMyCenter.GetImgWidth();
	m_btnFileMgr.MovePoint(x,y);
	x += m_btnMyCenter.GetImgWidth();
	m_btnMyShare.MovePoint(x,y);

	const int const_text_intever = 1;
	const int const_number_height = 22;
	y = POS_STA_FIRST_Y;
	if (m_editSearch.GetSafeHwnd())
		m_editSearch.MoveWindow(const_text_intever, y, cx-const_text_intever*2, const_number_height);
	y += const_number_height;
	const int const_Tab_Width	= m_btnMyDepartment.GetImgWidth();
	const int const_Tab_intever = 0;
	x = const_text_intever;
	if (theApp.IsLogonVisitor())
	{
		m_btnMySession.MovePoint(x, y);
	}else
	{
		m_btnMyDepartment.MovePoint(x, y);
		x += (const_Tab_Width+const_Tab_intever);
		m_btnMyContacts.MovePoint(x, y);
		x += (const_Tab_Width+const_Tab_intever);
		m_btnMySession.MovePoint(x, y);
		x += (const_Tab_Width+const_Tab_intever);
		m_btnMyEnterprise.MovePoint(x, y);
	}
	y += m_btnMyDepartment.GetImgHeight();
	const int const_left_intever = 1;
	const int const_bottom_intever = 22;
	CRect rectDlg(const_left_intever, y, cx-const_left_intever, cy-const_bottom_intever);
	if (m_pDlgMyGroup != NULL && m_pDlgMyGroup->GetSafeHwnd())
	{
		m_pDlgMyGroup->MoveWindow(&rectDlg);
	}
	if (m_pDlgMyContacts != NULL && m_pDlgMyContacts->GetSafeHwnd())
	{
		m_pDlgMyContacts->MoveWindow(&rectDlg);
	}
	if (m_pDlgMySession != NULL && m_pDlgMySession->GetSafeHwnd())
	{
		m_pDlgMySession->MoveWindow(&rectDlg);
	}
	if (m_pDlgMyEnterprise != NULL && m_pDlgMyEnterprise->GetSafeHwnd())
	{
		m_pDlgMyEnterprise->MoveWindow(&rectDlg);
	}

}

//void CPOPDlg::OnBnClickedButtonCalluser()
//{
//	UpdateData();
//	if (m_sCallNumber.IsEmpty())
//	{
//		m_comboNumbers.SetFocus();
//		AfxMessageBox(_T("请输入呼叫用户帐号！"));
//		return;
//	}
//	//m_comboNumbers.InsertString(0, m_sCallNumber);
//	theEBAppClient.CallUser(m_sCallNumber,"","");	// ??
//}

void CPOPDlg::RefreshLabelWindow(void)
{
	m_btnMyDepartment.SetChecked(m_btnMyDepartment.GetSafeHwnd()==m_pCurrentLabel);
	if (m_pOldCurrentLabel==m_btnMyDepartment.GetSafeHwnd() || m_pCurrentLabel==m_btnMyDepartment.GetSafeHwnd())
		theApp.InvalidateParentRect(&m_btnMyDepartment);
	m_btnMyContacts.SetChecked(m_btnMyContacts.GetSafeHwnd()==m_pCurrentLabel);
	if (m_pOldCurrentLabel==m_btnMyContacts.GetSafeHwnd() || m_pCurrentLabel==m_btnMyContacts.GetSafeHwnd())
		theApp.InvalidateParentRect(&m_btnMyContacts);
	m_btnMySession.SetChecked(m_btnMySession.GetSafeHwnd()==m_pCurrentLabel);
	if (m_pOldCurrentLabel==m_btnMySession.GetSafeHwnd() || m_pCurrentLabel==m_btnMySession.GetSafeHwnd())
		theApp.InvalidateParentRect(&m_btnMySession);
	m_btnMyEnterprise.SetChecked(m_btnMyEnterprise.GetSafeHwnd()==m_pCurrentLabel);
	if (m_pOldCurrentLabel==m_btnMyEnterprise.GetSafeHwnd() || m_pCurrentLabel==m_btnMyEnterprise.GetSafeHwnd())
		theApp.InvalidateParentRect(&m_btnMyEnterprise);
	m_pDlgMyGroup->ShowWindow(m_btnMyDepartment.GetChecked()?SW_SHOW:SW_HIDE);
	m_pDlgMyContacts->ShowWindow(m_btnMyContacts.GetChecked()?SW_SHOW:SW_HIDE);
	m_pDlgMySession->ShowWindow(m_btnMySession.GetChecked()?SW_SHOW:SW_HIDE);
	m_pDlgMyEnterprise->ShowWindow(m_btnMyEnterprise.GetChecked()?SW_SHOW:SW_HIDE);
}

void CPOPDlg::DrawInfo(void)
{
	CPaintDC dc(this); // 用于绘制的设备上下文
	CRect rectClient;
	this->GetClientRect(&rectClient);
	CSkinMemDC memDC(&dc, rectClient);
	this->ClearBgDrawInfo();
	this->AddBgDrawInfo(CEbBackDrawInfo(120,0.7,true));
	this->AddBgDrawInfo(CEbBackDrawInfo(62,0.85,false));
	this->AddBgDrawInfo(CEbBackDrawInfo(0,0.9,false));
	this->DrawPopBg(&memDC, theApp.GetMainColor());
	Gdiplus::Graphics graphics(memDC.m_hDC);

	USES_CONVERSION;
	const FontFamily fontFamily(theFontFamily.c_str());
	const Gdiplus::Font fontEbUserName(&fontFamily, 14, FontStyleRegular, UnitPixel);

	const Gdiplus::Font fontEbTitle(&fontFamily, 13, FontStyleBold, UnitPixel);
	Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(255,255,255));
	//Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(38,38,38));
	//graphics.DrawImage(theApp.m_imageEbIcon, Gdiplus::RectF(3,3,20,20));
	//const Gdiplus::PointF pointTitle(25,7);
	const Gdiplus::PointF pointTitle(10,10);
#ifdef USES_EBCOM_TEST
	const CEBString sSettingEnterprise = theEBSetting->Enterprise.GetBSTR();
	const CEBString sSettingVersion = theEBSetting->Version.GetBSTR();
#else
	const CEBString sSettingEnterprise = theSetting.GetEnterprise();
	const CEBString sSettingVersion = theSetting.GetVersion();
#endif
	//graphics.SetTextRenderingHint(TextRenderingHintAntiAlias);	// 消除锯齿效果
	if (sSettingEnterprise.empty())
		graphics.DrawString(L"EB2014",-1,&fontEbTitle,pointTitle,&brushEbTitle);
	else
		graphics.DrawString(A2W(sSettingEnterprise.c_str()),-1,&fontEbTitle,pointTitle,&brushEbTitle);
	// beta
	const Gdiplus::Font fontEbBeta(&fontFamily, 12, FontStyleBold, UnitPixel);
	const Gdiplus::PointF pointBeta(rectClient.Width()-70,rectClient.Height()-20);
	graphics.DrawString(L"V2014.180",-1,&fontEbBeta,pointBeta,&brushEbTitle);

	// 写标题
	//CFont pNewFontTitle;
	//pNewFontTitle.CreatePointFont(115, _T("宋体"));//创建显示文本的字体
	//HGDIOBJ pOldFond = SelectObject(m_hdcMemory, pNewFontTitle.m_hObject);
	//SetTextColor(m_hdcMemory, RGB(0, 0, 0));	// 黑色
	//CString sOutText = _T("EB2014");//theSetting.GetEnterprise().c_str();//_T("POP-2013");
	//TextOut(m_hdcMemory, 6, 5, sOutText, sOutText.GetLength());
	//SelectObject(m_hdcMemory, pOldFond);

	// MY IMG
#ifdef USES_EBCOM_TEST
	const CEBString sHeadFile = theEBClientCore->EB_GetMyDefaultMemberHeadFile().GetBSTR();
	const CEBString sUserName = theEBClientCore->EB_UserName.GetBSTR();
#else
	const CEBString sHeadFile = theEBAppClient.EB_GetMyDefaultMemberHeadFile();
	const CEBString sUserName = theEBAppClient.EB_GetUserName();;
#endif
	Gdiplus::Image * pImage;
	if (theApp.IsLogonVisitor())
	{
		pImage = theApp.m_imageDefaultVisitor->Clone();
	}else
	{
		if (PathFileExists(sHeadFile.c_str()))
		{
			pImage = new Gdiplus::Image((const WCHAR*)T2W(sHeadFile.c_str()));
		}else 
		{
			pImage = theApp.m_imageDefaultMember->Clone();
		}
	}
	graphics.DrawImage(pImage, POS_ADIMG_LEFT, POS_ADIMG_TOP, POS_ADIMG_SIGE, POS_ADIMG_SIGE);
	delete pImage;

	// 写字
	const int POS_LINESTATE_WIDTH = m_btnLineState.GetImgWidth();
	Gdiplus::SolidBrush brushString(Gdiplus::Color::Black);
	graphics.DrawString(A2W(sUserName.c_str()),-1,&fontEbUserName,Gdiplus::PointF(POS_LINESTATE_LEFT+POS_LINESTATE_WIDTH+1,POS_LINESTATE_TOP-2),&brushString);
}

void CPOPDlg::OnBnClickedButtonEnterprise()
{
	//if (m_pCurrentLabel != m_btnMyEnterprise.GetSafeHwnd())
	{
		m_pOldCurrentLabel = m_pCurrentLabel;
		m_pCurrentLabel = m_btnMyEnterprise.GetSafeHwnd();
		RefreshLabelWindow();
	}
}

void CPOPDlg::OnBnClickedButtonDepartment()
{
	//if (m_pCurrentLabel != m_btnMyDepartment.GetSafeHwnd())
	{
		m_pOldCurrentLabel = m_pCurrentLabel;
		m_pCurrentLabel = m_btnMyDepartment.GetSafeHwnd();
		RefreshLabelWindow();
	}
}

void CPOPDlg::OnBnClickedButtonContact()
{
	//if (m_pCurrentLabel != m_btnMyContacts.GetSafeHwnd())
	{
		m_pOldCurrentLabel = m_pCurrentLabel;
		m_pCurrentLabel = m_btnMyContacts.GetSafeHwnd();
		RefreshLabelWindow();
	}
}

void CPOPDlg::OnBnClickedButtonSession()
{
	m_btnMySession.SetWindowText(_T(""));
	m_pOldCurrentLabel = m_pCurrentLabel;
	m_pCurrentLabel = m_btnMySession.GetSafeHwnd();
	RefreshLabelWindow();
}

void CPOPDlg::OnBnClickedButtonMax()
{
	int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
	//int m_nScreenHeight = GetSystemMetrics(SM_CYSCREEN); //屏幕高度
	int m_nScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	m_nScreenHeight += GetSystemMetrics(SM_CYCAPTION);

	static CRect theRestoreRect;
	CRect rect;
	GetWindowRect(&rect);
	if (rect.Width() == m_nScreenWidth)
	{
		this->SetToolTipText(IDC_BUTTON_MAX2,_T("最大化"));
		m_btnMax.Load(IDB_PNG_MAX);
		MoveWindow(&theRestoreRect);
	}else
	{
		this->SetToolTipText(IDC_BUTTON_MAX2,_T("向下还原"));
		m_btnMax.Load(IDB_PNG_RESTORE);
		theRestoreRect = rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = rect.left + m_nScreenWidth;
		rect.bottom = rect.top + m_nScreenHeight;
		MoveWindow(&rect);
	}
}

void CPOPDlg::OnBnClickedButtonMin()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CPOPDlg::OnBnClickedButtonClose()
{
	this->ShowWindow(SW_HIDE);
	this->SetFocus();	// 解决按钮背景刷新问题；
}

void CPOPDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case TIMERID_NEW_VERSION:
		{
			KillTimer(nIDEvent);
			const CString sNewVersion = theApp.EBC_GetProfileString(_T("new_version"),_T("version"));
			const CString sNewVersionFile = theApp.EBC_GetProfileString(_T("new_version"),_T("file"));
			if (!sNewVersion.IsEmpty())
			{
#ifdef USES_EBCOM_TEST
				const CEBString sVersion = theEBSetting->Version.GetBSTR();
#else
				const CEBString sVersion = theSetting.GetVersion();
#endif
				if (::PathFileExists(sNewVersionFile) && sNewVersion != sVersion.c_str())
				{
					CString sText;
					sText.Format(_T("[%s]，是否立即更新？"),sNewVersion);
					if (CDlgMessageBox::EbDoModal(this,"检测到新版本：",sText,CDlgMessageBox::IMAGE_QUESTION)==IDOK)
					//if (MessageBox(sText, _T("恩布新版本"), MB_YESNO|MB_ICONQUESTION) == IDYES)
					{
						// 执行自动更新过程
						TCHAR lpszModuleFileName[MAX_PATH];
						GetModuleFileName( NULL, lpszModuleFileName, MAX_PATH);
						CString sUpdateExe = theApp.GetAppPath()+_T("\\update\\ebupdate2.exe");
						CString sParameter;
						sParameter.Format(_T("\"%s\" \"%s\""), sNewVersionFile, lpszModuleFileName);
						ShellExecute(NULL,  "open", sUpdateExe, sParameter, theApp.GetAppPath(), SW_SHOW);

						// 退出应用程序
						theWantExitApp = true;
						PostMessage(WM_CLOSE, 0, 0);
					}
				}
			}
		}break;
	case TIMERID_CHECK_UPDATE:
		{
#ifdef USES_EBCOM_TEST
			theEBClientCore->EB_CheckVersion(theEBSetting->Version);
#else
			theEBAppClient.EB_CheckVersion(theSetting.GetVersion().c_str());
#endif
			if (!theUpdateResetTimer)
			{
				theUpdateResetTimer = true;
				KillTimer(TIMERID_CHECK_UPDATE);
				SetTimer(TIMERID_CHECK_UPDATE,60*60*1000,NULL);	// 一小时检查一次
			}
		}break;
	case TIMERID_CHECK_CLOSE_DIALOG:
		{
			// 定期检查超过５分钟没用会话
			{
				boost::mutex::scoped_lock lockCallList(theApp.m_pCallList.mutex());
				CLockMap<eb::bigint, CEBCCallInfo::pointer>::const_iterator pIterCallList = theApp.m_pCallList.begin();
				for (; pIterCallList!=theApp.m_pCallList.end(); pIterCallList++)
				{
					const CEBCCallInfo::pointer pEbCallInfo = pIterCallList->second;
					const eb::bigint sCallid = pEbCallInfo->m_pCallInfo.GetCallId();
					if ((time(0)-pEbCallInfo->m_tLastTime)>5*60 && !theApp.m_pDialogList.exist(sCallid))
					{
						theApp.m_pCallList.erase(pIterCallList);
						// 这里主要用于处理自动响应，没有打开聊天界面会话
						// *其实在OnMessageCallConnected已经有处理；
#ifdef USES_EBCOM_TEST
						theEBClientCore->EB_CallExit(pEbCallInfo->m_pCallInfo.GetCallId());
#else
						theEBAppClient.EB_CallExit(pEbCallInfo->m_pCallInfo.GetCallId());
#endif
						break;
					}
				}
			}

			CDlgDialog::pointer pDialog;
			if (theApp.m_pCloseDialog.front(pDialog))
			{
				pDialog->DestroyWindow();
			}
			CWnd * pWnd = NULL;
			if (theApp.m_pCloseWnd.front(pWnd))
			{
				pWnd->DestroyWindow();
			}
			
		}break;
	case TIMERID_LOGOUT:
		{
			KillTimer(nIDEvent);
			OnLogout();
		}break;
//	case TIMERID_RELOGIN:
//		{
//			KillTimer(TIMERID_RELOGIN);
//			static time_t theReloginTime = 0;
//			if (theReloginTime == 0 || (time(0)-theReloginTime)>30)	// 第一次，或超过30秒才处理一次
//			{
//				theReloginTime = time(0);
//#ifdef USES_EBCOM_TEST
//				theEBClientCore->EB_ReLogon();
//#else
//				theEBAppClient.EB_ReLogon();
//#endif
//			}
//		}break;
	case TIMERID_LOADINFO:
		{
			KillTimer(nIDEvent);
#ifdef USES_EBCOM_TEST
			theEBClientCore->EB_LoadInfo();
#else
			theEBAppClient.EB_LoadInfo();
#endif
		}break;
	case TIMERID_SHOW_EMOTIPN:
		{
			KillTimer(nIDEvent);
			theApp.ShowImageWindow(false,NULL,NULL);
		}break;
	default:
		break;
	}

	CEbDialogBase::OnTimer(nIDEvent);
}
void CPOPDlg::CallItem(HTREEITEM hItem)
{
	const CTreeItemInfo * pTreeItemInfo = (const CTreeItemInfo*)m_treeSearch.GetItemData(hItem);
	if (pTreeItemInfo == NULL) return;
	if (pTreeItemInfo->m_sMemberCode==0)
	{
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_CallAccount(pTreeItemInfo->m_sAccount.c_str(),0);
#else
		theEBAppClient.EB_CallAccount(pTreeItemInfo->m_sAccount.c_str(),0);
#endif
	}else
	{
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_CallMember(pTreeItemInfo->m_sMemberCode,0);
#else
		theEBAppClient.EB_CallMember(pTreeItemInfo->m_sMemberCode,0);
#endif
	}
}

LRESULT CPOPDlg::OnTreeItemDoubleClieck(WPARAM wp, LPARAM lp)
{
	HTREEITEM item = (HTREEITEM)wp;
	VividTree* pOwner = (VividTree*)lp;
	if (pOwner == &m_treeSearch)
	{
		m_editSearch.SetFocus();
		CallItem(item);
//		std::string sSelAccount;
//		if (m_pSearchItem2.find(item, sSelAccount))
//		{
//#ifdef USES_EBCOM_TEST
//			theEBClientCore->EB_CallAccount(sSelAccount.c_str(),"");
//#else
//			theEBAppClient.EB_CallAccount(sSelAccount.c_str(),"");	// ??
//#endif
//		}
	}
	return 0;
}

LRESULT CPOPDlg::OnTreeItemTrackHot(WPARAM wp, LPARAM lp)
{
	HTREEITEM item = (HTREEITEM)wp;
	VividTree * pOwner = (VividTree*)lp;
	if (pOwner == &m_treeSearch)
	{
		if (item == NULL)
		{
			m_btnSearchTrackCall.ShowWindow(SW_HIDE);
		}else
		{
			const int const_btn_width = m_btnSearchTrackCall.GetImgWidth();
			CRect rect;
			m_treeSearch.GetItemRect(item, &rect, TRUE);
			rect.right = m_treeSearch.GetHSize();
			m_btnSearchTrackCall.MovePoint(rect.right-const_btn_width, rect.top);
			m_btnSearchTrackCall.ShowWindow(SW_SHOW);
			m_btnSearchTrackCall.Invalidate();
		}
	}
	return 0;
}
BOOL CPOPDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (pMsg->message == WM_LBUTTONUP && pMsg->hwnd == m_editDescription.GetSafeHwnd())
	{
		m_editDescription.SetReadOnly(FALSE);
		// 以上没用
		//if (m_editDescription.GetSafeHwnd() && this->GetFocus() != &m_editDescription)
		//{
		//	m_editDescription.SetFocus();
		//	m_editDescription.SetSel(0,-1);
		//}
	}else if (pMsg->message == WM_LBUTTONUP && pMsg->hwnd == m_btnSearchTrackCall.GetSafeHwnd())
	{
		m_editSearch.SetFocus();
		CallItem(m_treeSearch.GetTrackItem());
//		std::string sSelAccount;
//		if (m_pSearchItem2.find(m_treeSearch.GetTrackItem(), sSelAccount))
//		{
//#ifdef USES_EBCOM_TEST
//			theEBClientCore->EB_CallAccount(sSelAccount.c_str(),"");
//#else
//			theEBAppClient.EB_CallAccount(sSelAccount.c_str(),"");	// ??
//#endif
//		}
	}else if (pMsg->message == WM_KEYDOWN && pMsg->hwnd == m_editSearch.GetSafeHwnd())
	{
		if (pMsg->wParam == VK_RETURN)
		{
			// 回车请求会话
			// ??
			CString sCallNumber;
			m_editSearch.GetWindowText(sCallNumber);
			if (!sCallNumber.IsEmpty())
			{
#ifdef USES_EBCOM_TEST
				theEBClientCore->EB_CallAccount((LPCTSTR)sCallNumber,0);
#else
				theEBAppClient.EB_CallAccount(sCallNumber,0);
#endif
			}
		}
	}else if (pMsg->message == WM_KEYDOWN && pMsg->hwnd == m_editDescription.GetSafeHwnd())
	{
		if (pMsg->wParam == VK_RETURN)
		{
			// 回车，保存
#ifdef USES_EBCOM_TEST
			EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
			const CEBString sOutDesc = theEBClientCore->EB_Description.GetBSTR();
#else
			EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
			std::string sOutDesc = theEBAppClient.EB_GetDescription();
#endif
			CString sDesc;
			m_editDescription.GetWindowText(sDesc);
			if (sOutDesc != (LPCTSTR)sDesc)
			{
#ifdef USES_EBCOM_TEST
				theEBClientCore->EB_Description = (LPCTSTR)sDesc;
#else
				theEBAppClient.EB_SetDescription(sDesc);
#endif
			}
			RefreshEditDescription();
		}else if (pMsg->wParam == VK_ESCAPE)
		{
			// 退出，取消
			OnEnKillfocusEditDescription();
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
LRESULT CPOPDlg::OnIconNotification(WPARAM wParam, LPARAM lParam)
{
	if(wParam != m_trayIconData.uID)    // 不是自己的图标发来的消息，退出
		return FALSE;
	switch(lParam)  // 过滤消息
	{
	case WM_RBUTTONDOWN:
		{
			if (m_menuTray.GetSafeHmenu()==NULL)
			{
				m_menuTray.CreatePopupMenu();
				if (!theApp.IsLogonVisitor())
				{
					m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_ONLINE,_T("在线"));
					m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_AWAY,_T("离开"));
					m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_BUSY,_T("忙碌"));
					m_menuTray.AppendMenu(MF_SEPARATOR);
					m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_MY_SHARE,_T("我的云盘"));
					m_menuTray.AppendMenu(MF_SEPARATOR);
					m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_OPEN_MAIN,_T("打开主面板"));
					m_menuTray.AppendMenu(MF_SEPARATOR);
				}
				m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_LOGOUT,_T("注销"));
				m_menuTray.AppendMenu(MF_BYCOMMAND,EB_COMMAND_EXIT_APP,_T("退出"));
			}
			if (!theApp.IsLogonVisitor())
			{
#ifdef USES_EBCOM_TEST
				EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
				EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
				m_menuTray.CheckMenuItem(EB_COMMAND_STATE_ONLINE,nOutLineState==EB_LINE_STATE_ONLINE?MF_CHECKED:MF_UNCHECKED);
				m_menuTray.CheckMenuItem(EB_COMMAND_STATE_AWAY,nOutLineState==EB_LINE_STATE_AWAY?MF_CHECKED:MF_UNCHECKED);
				m_menuTray.CheckMenuItem(EB_COMMAND_LOGOUT,nOutLineState==EB_LINE_STATE_BUSY?MF_CHECKED:MF_UNCHECKED);
			}
			CPoint point;
			GetCursorPos(&point);
			m_menuTray.TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);

			//m_menu2.LoadMenu( IDR_MENU_TRAY );
			//CMenu *pMenu = m_menu2.GetSubMenu(0);
			//pMenu->CheckMenuItem(ID_STATE_ONLINE,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_ONLINE?MF_CHECKED:MF_UNCHECKED);
			//pMenu->CheckMenuItem(ID_STATE_AWAY,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_AWAY?MF_CHECKED:MF_UNCHECKED);
			//pMenu->CheckMenuItem(ID_STATE_BUSY,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_BUSY?MF_CHECKED:MF_UNCHECKED);
			//CPoint point;
			//GetCursorPos(&point);
			//pMenu->TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);
		}break;
	case WM_LBUTTONDOWN:
		{
			if (this->IsWindowVisible())
				this->ShowWindow(SW_HIDE);
			else
			{
				OnOpenMain();
			}
		}break; 
	default:
		break;
	}
	return TRUE;
}

#ifdef USES_EBCOM_TEST
void CPOPDlg::Fire_onSearchMemberInfo(IEB_GroupInfo* pGroupInfo, IEB_MemberInfo* pMemberInfo, ULONG dwParam)
{
	if (dwParam == 1)
	{
		// search
		const eb::bigint sMemberCode(pMemberInfo->MemberCode);
		const CEBString sMemberAccount(pMemberInfo->MemberAccount.GetBSTR());
		const CEBString sUserName(pMemberInfo->UserName.GetBSTR());
		if (sMemberAccount==(LPCTSTR)theApp.GetLogonAccount())
			return;
		if (!m_pMemberItem.exist(sMemberCode))
		//if (!m_pSearchItem1.exist(sMemberAccount))
		{
			CString sSearchName;
			sSearchName.Format(_T("%s - %s"),sUserName.c_str(),sMemberAccount.c_str());
			HTREEITEM hEmpItem = m_treeSearch.InsertItem(sSearchName);
			CTreeItemInfo::pointer pEmpItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_MEMBER,hEmpItem);
			pEmpItemInfo->m_sMemberCode = pMemberInfo->MemberCode;
			pEmpItemInfo->m_sAccount = pMemberInfo->MemberAccount.GetBSTR();
			pEmpItemInfo->m_sGroupCode = pMemberInfo->GroupCode;
			pEmpItemInfo->m_sName = pMemberInfo->UserName.GetBSTR();
			m_pMemberItem.insert(pEmpItemInfo->m_sMemberCode,pEmpItemInfo);
			m_treeSearch.SetItemData(hEmpItem,(DWORD)pEmpItemInfo.get());

			//HTREEITEM hSearchItem = m_treeSearch.InsertItem(sSearchName);
			//m_treeSearch.SetItemData(hSearchItem, 1);
			//m_pSearchItem1.insert(sMemberAccount, hSearchItem);
			//m_pSearchItem2.insert(hSearchItem, sMemberAccount);
		}
	}
}
#else
void CPOPDlg::onMemberInfo(const EB_GroupInfo* pGroupInfo, const EB_MemberInfo* pMemberInfo, unsigned long dwParam)
{
	if (dwParam == 1)
	{
		// search
		if (pMemberInfo->m_sMemberAccount==(LPCTSTR)theApp.GetLogonAccount())
			return;
		if (!m_pMemberItem.exist(pMemberInfo->m_sMemberCode))
		{
			CString sSearchName;
			sSearchName.Format(_T("%s - %s"),pMemberInfo->m_sUserName.c_str(),pMemberInfo->m_sMemberAccount.c_str());
			HTREEITEM hEmpItem = m_treeSearch.InsertItem(sSearchName);
			CTreeItemInfo::pointer pEmpItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_MEMBER,hEmpItem);
			pEmpItemInfo->m_sMemberCode = pMemberInfo->m_sMemberCode;
			pEmpItemInfo->m_sAccount = pMemberInfo->m_sMemberAccount;
			pEmpItemInfo->m_sGroupCode = pMemberInfo->m_sGroupCode;
			pEmpItemInfo->m_sName = pMemberInfo->m_sUserName;
			m_pMemberItem.insert(pEmpItemInfo->m_sMemberCode,pEmpItemInfo);
			m_treeSearch.SetItemData(hEmpItem,(DWORD)pEmpItemInfo.get());

			//HTREEITEM hSearchItem = m_treeSearch.InsertItem(sSearchName);
			//m_treeSearch.SetItemData(hSearchItem, 1);
			//m_pSearchItem1.insert(pMemberInfo->m_sMemberAccount, hSearchItem);
			//m_pSearchItem2.insert(hSearchItem, pMemberInfo->m_sMemberAccount);
		}
	}
}
#endif

#ifdef USES_EBCOM_TEST
void CPOPDlg::Fire_onSearchContactInfo(IEB_ContactInfo* pContactInfo, ULONG dwParam)
{
	 if (dwParam == 1)
	{
		// search
		const CEBString sContact(pContactInfo->Contact.GetBSTR());
		const CEBString sName(pContactInfo->Name.GetBSTR());
		if (!m_pContactItem.exist(sContact))
		{
			CString sSearchName;
			sSearchName.Format(_T("%s - %s"),sName.c_str(),sContact.c_str());
			HTREEITEM hContactItem = m_treeSearch.InsertItem(sSearchName);
			CTreeItemInfo::pointer pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
			pContactItemInfo->m_sAccount = sContact;
			pContactItemInfo->m_sName = sName;
			m_pContactItem.insert(pContactItemInfo->m_sAccount,pContactItemInfo);
			m_treeSearch.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());

			//CString sSearchName;
			//sSearchName.Format(_T("%s(%s)"),sName.c_str(),sContact.c_str());
			//HTREEITEM hSearchItem = m_treeSearch.InsertItem(sSearchName);
			//m_treeSearch.SetItemData(hSearchItem, 2);
			//m_pSearchItem1.insert(sContact, hSearchItem);
			//m_pSearchItem2.insert(hSearchItem, sContact);
		}
	}
}
#else
void CPOPDlg::onContactInfo(const EB_ContactInfo* pContactInfo, unsigned long dwParam)
{
	 if (dwParam == 1)
	{
		// search
		if (!m_pContactItem.exist(pContactInfo->m_sContact))
		{
			CString sSearchName;
			sSearchName.Format(_T("%s - %s"),pContactInfo->m_sName.c_str(),pContactInfo->m_sContact.c_str());
			HTREEITEM hContactItem = m_treeSearch.InsertItem(sSearchName);
			CTreeItemInfo::pointer pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
			pContactItemInfo->m_sAccount = pContactInfo->m_sContact;
			pContactItemInfo->m_sName = pContactInfo->m_sName;
			m_pContactItem.insert(pContactItemInfo->m_sAccount,pContactItemInfo);
			m_treeSearch.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());

			//CString sSearchName;
			//sSearchName.Format(_T("%s(%s)"),pContactInfo->m_sName.c_str(),pContactInfo->m_sContact.c_str());
			//HTREEITEM hSearchItem = m_treeSearch.InsertItem(sSearchName);
			//m_treeSearch.SetItemData(hSearchItem, 2);
			//m_pSearchItem1.insert(pContactInfo->m_sContact, hSearchItem);
			//m_pSearchItem2.insert(hSearchItem, pContactInfo->m_sContact);
		}
	}
}
#endif

void CPOPDlg::OnEnChangeEditSearch()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CEbDialogBase::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sSearchString;
	m_editSearch.GetWindowText(sSearchString);
	if (!sSearchString.IsEmpty())
	{
		m_pContactItem.clear();
		m_pMemberItem.clear();
		m_treeSearch.DeleteAllItems();
#ifdef USES_EBCOM_TEST
		//IDispatch* pCallback;
		//((CEBSearchEventsSink*)this)->QueryInterface(IID_IDispatch,(void**)&pCallback);
		theEBClientCore->EB_SearchUserInfo((IDispatch*)(CEBSearchEventsSink*)this, (LPCTSTR)sSearchString, 1);
#else
		theEBAppClient.EB_SearchUserInfo((CEBSearchCallback*)this, sSearchString, 1);
#endif

		if (m_treeSearch.GetCount() > 0)
		{
			CRect searchRect;
			m_editSearch.GetWindowRect(&searchRect);
			CRect rect;
			m_pDlgMyContacts->GetWindowRect(&rect);
			searchRect.top = searchRect.bottom+1;
			searchRect.bottom = rect.bottom;
			this->ScreenToClient(&searchRect);
			m_treeSearch.MoveWindow(&searchRect);
			m_treeSearch.ShowWindow(SW_SHOW);
			m_pDlgMyEnterprise->ShowWindow(SW_HIDE);
			m_pDlgMyGroup->ShowWindow(SW_HIDE);
			m_pDlgMyContacts->ShowWindow(SW_HIDE);
			m_pDlgMySession->ShowWindow(SW_HIDE);
			m_btnMyEnterprise.ShowWindow(SW_HIDE);
			m_btnMyDepartment.ShowWindow(SW_HIDE);
			m_btnMyContacts.ShowWindow(SW_HIDE);
			m_btnMySession.ShowWindow(SW_HIDE);
		}
	}
	if (m_treeSearch.GetCount() == 0 || sSearchString.IsEmpty())
	{
		m_treeSearch.ShowWindow(SW_HIDE);
		m_btnMyEnterprise.ShowWindow(SW_SHOW);
		m_btnMyDepartment.ShowWindow(SW_SHOW);
		m_btnMyContacts.ShowWindow(SW_SHOW);
		m_btnMySession.ShowWindow(SW_SHOW);
		if (m_btnMyEnterprise.GetChecked())
			m_pDlgMyEnterprise->ShowWindow(SW_SHOW);
		else if (m_btnMyDepartment.GetChecked())
			m_pDlgMyGroup->ShowWindow(SW_SHOW);
		else if (m_btnMyContacts.GetChecked())
			m_pDlgMyContacts->ShowWindow(SW_SHOW);
		else if (m_btnMySession.GetChecked())
			m_pDlgMySession->ShowWindow(SW_SHOW);
	}
}

void CPOPDlg::OnExitApp()
{
	theWantExitApp = true;
	PostMessage(WM_CLOSE, 0, 0);
}

void CPOPDlg::OnLogout()
{
	theWantExitApp = true;
	PostMessage(WM_CLOSE, 0, 0);

	// 重新启动新应用
	CString strExe;
	GetModuleFileName( NULL, strExe.GetBuffer(MAX_PATH), MAX_PATH);
	strExe.ReleaseBuffer(-1);
	ShellExecute(m_hWnd,_T("open"),strExe,_T("logout"),theApp.GetAppPath(),SW_SHOW);
}

void CPOPDlg::OnBnClickedButtonLinestate()
{
	const CString sAutoRunAccount = theApp.EBC_GetProfileString(_T("system"),_T("auto-run-account"));
	const int const_my_ecards_index = 4;						// ***修改菜单项，需要修改这个地方
	const int const_app_func_index = const_my_ecards_index+1;	// ***修改菜单项，需要修改这个地方
	if (m_menuState.GetSafeHmenu()==NULL)
	{
		m_menuState.CreatePopupMenu();
		if (!theApp.IsLogonVisitor())
		{
			m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_ONLINE,_T("在线"));
			m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_AWAY,_T("离开"));
			m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_STATE_BUSY,_T("忙碌"));
			m_menuState.AppendMenu(MF_SEPARATOR);
			if (!sAutoRunAccount.IsEmpty())
				m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_AUTO_LOGIN,_T("自动登录"));
			m_menuState.AppendMenu(MF_SEPARATOR);
		}
		m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_LOGOUT,_T("注销"));
		m_menuState.AppendMenu(MF_BYCOMMAND,EB_COMMAND_EXIT_APP,_T("退出"));
	}else
	{
		if (!m_pSubscribeFuncList.empty())
			m_menuState.RemoveMenu(const_app_func_index,MF_BYPOSITION);
		if (!theApp.IsLogonVisitor())
			m_menuState.RemoveMenu(const_my_ecards_index,MF_BYPOSITION);
	}

	if (!theApp.IsLogonVisitor())
	{
#ifdef USES_EBCOM_TEST
		EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
		EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
		m_menuState.CheckMenuItem(EB_COMMAND_AUTO_LOGIN,sAutoRunAccount.IsEmpty()?MF_UNCHECKED:MF_CHECKED);
		m_menuState.CheckMenuItem(EB_COMMAND_STATE_ONLINE,nOutLineState==EB_LINE_STATE_ONLINE?MF_CHECKED:MF_UNCHECKED);
		m_menuState.CheckMenuItem(EB_COMMAND_STATE_AWAY,nOutLineState==EB_LINE_STATE_AWAY?MF_CHECKED:MF_UNCHECKED);
		m_menuState.CheckMenuItem(EB_COMMAND_STATE_BUSY,nOutLineState==EB_LINE_STATE_BUSY?MF_CHECKED:MF_UNCHECKED);
		m_pMyEmployeeInfo.clear();
#ifdef USES_EBCOM_TEST
		_variant_t pMyMemberList = theEBClientCore->EB_GetMyMemberList();
		if (pMyMemberList.vt!=VT_EMPTY && pMyMemberList.parray != NULL)
		{
			CComSafeArray<VARIANT> m_sa(pMyMemberList.parray);
			for (ULONG i=0;i<m_sa.GetCount();i++)
			{
				CComVariant var = m_sa.GetAt(i);
				if (var.vt != VT_DISPATCH)
					continue;
				CComPtr<IEB_MemberInfo> pInterface;
				var.pdispVal->QueryInterface(__uuidof(IEB_MemberInfo),(void**)&pInterface);
				m_pMyEmployeeInfo.push_back(EB_MemberInfo(pInterface));
			}
		}
#else
		theEBAppClient.EB_GetMyMemberList(m_pMyEmployeeInfo);
#endif

#ifdef USES_EBCOM_TEST
		const eb::bigint sDefaultMemberCode = theEBClientCore->EB_MyDefaultMemberCode;
#else
		eb::bigint sDefaultMemberCode;
		theEBAppClient.EB_GetMyDefaultMemberCode(sDefaultMemberCode);
#endif
		CMenu pPopupMenu;
		pPopupMenu.CreatePopupMenu();
		for (size_t i=0;i<m_pMyEmployeeInfo.size();i++)
		{
			EB_MemberInfo pMemberInfo = m_pMyEmployeeInfo[i];
#ifdef USES_EBCOM_TEST
			const CEBString sGroupName = theEBClientCore->EB_GetGroupName(pMemberInfo.m_sGroupCode).GetBSTR();
			if (!sGroupName.empty())
#else
			CEBString sGroupName;
			if (theEBAppClient.EB_GetGroupName(pMemberInfo.m_sGroupCode,sGroupName))
#endif
			{
				CString stext;
				stext.Format(_T("%s-%s"),pMemberInfo.m_sUserName.c_str(),sGroupName.c_str());
				pPopupMenu.AppendMenu(MF_BYCOMMAND,EB_COMMAND_MY_EMPLOYEE+i,stext);
				if (sDefaultMemberCode == pMemberInfo.m_sMemberCode)
				{
					pPopupMenu.CheckMenuItem(EB_COMMAND_MY_EMPLOYEE+i,MF_CHECKED);
				}
			}
		}
		m_menuState.InsertMenu(const_my_ecards_index,MF_POPUP|MF_BYPOSITION,(UINT)pPopupMenu.m_hMenu,_T("我的名片"));
	}

	// 应用功能菜单
	CMenu pPopupMenu;
	pPopupMenu.CreatePopupMenu();
	theApp.ClearSubscribeSelectInfo();
	m_pSubscribeFuncList.clear();
#ifdef USES_EBCOM_TEST
	_variant_t pSubscribeFuncList = theEBClientCore->EB_GetSubscribeFuncList(EB_FUNC_LOCATION_RIGHT_CLICK_MENU_MAINFRAME);
	if (pSubscribeFuncList.vt!=VT_EMPTY && pSubscribeFuncList.parray != NULL)
	{
		CComSafeArray<VARIANT> m_sa(pSubscribeFuncList.parray);
		for (ULONG i=0;i<m_sa.GetCount();i++)
		{
			CComVariant var = m_sa.GetAt(i);
			if (var.vt != VT_DISPATCH)
				continue;
			CComPtr<IEB_SubscribeFuncInfo> pSubscribeFuncInfo;
			var.pdispVal->QueryInterface(__uuidof(IEB_SubscribeFuncInfo),(void**)&pSubscribeFuncInfo);
			if (pSubscribeFuncInfo == NULL) continue;
			m_pSubscribeFuncList.push_back(EB_SubscribeFuncInfo(pSubscribeFuncInfo));
		}
	}
#else
	theEBAppClient.EB_GetSubscribeFuncList(EB_FUNC_LOCATION_RIGHT_CLICK_MENU_MAINFRAME,m_pSubscribeFuncList);
#endif
	if (!m_pSubscribeFuncList.empty())
	{
		for (size_t i=0;i<m_pSubscribeFuncList.size();i++)
		{
			const EB_SubscribeFuncInfo & pSubscribeFuncInfo = m_pSubscribeFuncList[i];
			pPopupMenu.AppendMenu(MF_BYCOMMAND,EB_COMMAND_SUBSCRIBE_FUNC+i,pSubscribeFuncInfo.m_sFunctionName.c_str());
		}
		m_menuState.InsertMenu(const_app_func_index,MF_POPUP|MF_BYPOSITION,(UINT)pPopupMenu.m_hMenu,_T("应用功能"));
	}

	CPoint point;
	GetCursorPos(&point);
	m_menuState.TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);

	//CMenu m_menu2;
	//m_menu2.LoadMenu( IDR_MENU_LINESTATE );
	//CMenu *pMenu = m_menu2.GetSubMenu(0);
	//pMenu->CheckMenuItem(ID_STATE_ONLINE,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_ONLINE?MF_CHECKED:MF_UNCHECKED);
	//pMenu->CheckMenuItem(ID_STATE_AWAY,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_AWAY?MF_CHECKED:MF_UNCHECKED);
	//pMenu->CheckMenuItem(ID_STATE_BUSY,theEBAppClient.EB_GetLineState()==EB_LINE_STATE_BUSY?MF_CHECKED:MF_UNCHECKED);
	//CPoint point;
	//GetCursorPos(&point);
	//pMenu->TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);
}
void CPOPDlg::OnMyEmployeeInfo(UINT nID)
{
	size_t nIndex = nID-EB_COMMAND_MY_EMPLOYEE;
	if (nIndex>=0 && nIndex<m_pMyEmployeeInfo.size())
	{
		EB_MemberInfo pMemberInfo = m_pMyEmployeeInfo[nIndex];
#ifdef USES_EBCOM_TEST
		CIEB_MemberInfo * pIEBMemberInfo = new CIEB_MemberInfo(pMemberInfo);
		CComPtr<IEB_MemberInfo> pOutInterface;
		pIEBMemberInfo->QueryInterface(__uuidof(IEB_MemberInfo),(void**)&pOutInterface);
		theApp.EditEmployeeInfo(this,pOutInterface);
#else
		theApp.EditEmployeeInfo(this,&pMemberInfo);
#endif
	}
}
void CPOPDlg::OnSubscribeFunc(UINT nID)
{
	size_t nIndex = nID-EB_COMMAND_SUBSCRIBE_FUNC;
	if (nIndex>=0 && nIndex<m_pSubscribeFuncList.size())
	{
		const EB_SubscribeFuncInfo& pSubscribeFuncInfo = m_pSubscribeFuncList[nIndex];
		theApp.OpenSubscribeFuncWindow(pSubscribeFuncInfo);
	}
}

void CPOPDlg::ChangeTrayText(void)
{
	CString sTrayText;
#ifdef USES_EBCOM_TEST
	const CEBString sUserName = theEBClientCore->EB_UserName.GetBSTR();
	EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
	const CEBString sEnterpriseName = theEBClientCore->EB_GetEnterpriseName(0).GetBSTR();
#else
	std::string sUserName = theEBAppClient.EB_GetUserName();
	EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
	std::string sEnterpriseName;
	theEBAppClient.EB_GetEnterpriseName(sEnterpriseName);
#endif
	if (theApp.IsLogonVisitor())
		sTrayText.Format(_T("游客-%s-%s"),theApp.GetLogonAccount(),GetLineStateText(nOutLineState));
	else if (!sEnterpriseName.empty())
		sTrayText.Format(_T("%s(%s)-%s\n%s"),sUserName.c_str(),theApp.GetLogonAccount(),GetLineStateText(nOutLineState),sEnterpriseName.c_str());
	else
		sTrayText.Format(_T("%s(%s)-%s"),sUserName.c_str(),theApp.GetLogonAccount(),GetLineStateText(nOutLineState));
	lstrcpy(m_trayIconData.szTip, sTrayText);
	Shell_NotifyIcon(NIM_MODIFY, &m_trayIconData);
}

void CPOPDlg::OnStateOnline()
{
#ifdef USES_EBCOM_TEST
	EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
	EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
	if (nOutLineState == EB_LINE_STATE_ONLINE)
	{
		return;
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_LineState = EB_LINE_STATE_ONLINE;
#else
	theEBAppClient.EB_SetLineState(EB_LINE_STATE_ONLINE);
#endif
	m_btnLineState.Load(IDB_PNG_BTN_STATE_ONLINE);
	//m_btnLineState.SetWindowText(_T("在线"));
	m_btnLineState.Invalidate();
	theApp.SetLineState2Db(EB_LINE_STATE_ONLINE);
	ChangeTrayText();
}

void CPOPDlg::OnStateAway()
{
#ifdef USES_EBCOM_TEST
	EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
	EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
	if (nOutLineState == EB_LINE_STATE_AWAY)
	{
		return;
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_LineState = EB_LINE_STATE_AWAY;
#else
	theEBAppClient.EB_SetLineState(EB_LINE_STATE_AWAY);
#endif
	m_btnLineState.Load(IDB_PNG_BTN_STATE_AWAY);
	//m_btnLineState.SetWindowText(_T("离开"));
	m_btnLineState.Invalidate();
	theApp.SetLineState2Db(EB_LINE_STATE_AWAY);
	ChangeTrayText();
}

void CPOPDlg::OnStateBusy()
{
#ifdef USES_EBCOM_TEST
	EB_USER_LINE_STATE nOutLineState = (EB_USER_LINE_STATE)theEBClientCore->EB_LineState;
#else
	EB_USER_LINE_STATE nOutLineState = theEBAppClient.EB_GetLineState();
#endif
	if (nOutLineState == EB_LINE_STATE_BUSY)
	{
		return;
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_LineState = EB_LINE_STATE_BUSY;
#else
	theEBAppClient.EB_SetLineState(EB_LINE_STATE_BUSY);
#endif
	m_btnLineState.Load(IDB_PNG_BTN_STATE_BUSY);
	//m_btnLineState.SetWindowText(_T("忙"));
	m_btnLineState.Invalidate();
	theApp.SetLineState2Db(EB_LINE_STATE_BUSY);
	ChangeTrayText();
}
void CPOPDlg::OnAutoLogin()
{
	if (!theApp.IsLogonVisitor())
	{
		const CString sAutoRunExeName = _T("entboost");
		HKEY hKey = NULL;
		LPCTSTR lpRun = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
		long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE|KEY_READ, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			char pFileName[MAX_PATH] = {0};
			GetModuleFileName(NULL, pFileName, MAX_PATH);
			const std::string sModuleFileName(pFileName);
			DWORD dwDataSize = sModuleFileName.size();
			lRet = ::RegQueryValueEx(hKey,sAutoRunExeName,0,0,(BYTE*)pFileName,&dwDataSize);
			if(lRet != ERROR_SUCCESS || sModuleFileName != pFileName)
			{
				lRet = RegSetValueEx(hKey,sAutoRunExeName,0,REG_SZ,(BYTE *)sModuleFileName.c_str(),sModuleFileName.size());
			}
			RegCloseKey(hKey);
		}

		const CString sAutoRunAccount = theApp.EBC_GetProfileString(_T("system"),_T("auto-run-account"));
		if (sAutoRunAccount.IsEmpty())
		{
			theApp.EBC_SetProfileString(_T("system"),_T("auto-run-account"),theApp.GetLogonAccount());
		}else
		{
			theApp.EBC_SetProfileString(_T("system"),_T("auto-run-account"),_T(""));
		}
	}
}

void CPOPDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_editDescription.GetSafeHwnd() && this->GetFocus() != &m_editDescription)
	{
		CPoint pos;
		GetCursorPos(&pos);
		ScreenToClient(&pos);
		CRect rect;
		m_editDescription.GetWindowRect(&rect);
		this->ScreenToClient(&rect);
		if (rect.PtInRect(pos))
		{
			::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_HAND));
			return;
		}
	}
	__super::OnMouseMove(nFlags, point);
}
void CPOPDlg::OnEnKillfocusEditDescription()
{
#ifdef USES_EBCOM_TEST
	const CEBString sOutDesc = theEBClientCore->EB_Description.GetBSTR();
#else
	std::string sOutDesc = theEBAppClient.EB_GetDescription();
#endif
	m_editDescription.SetWindowText(sOutDesc.c_str());
	RefreshEditDescription();
}
void CPOPDlg::RefreshEditDescription(void)
{
	//m_editDescription.EnableWindow(FALSE);
	m_editDescription.SetReadOnly(TRUE);
	CRect rect;
	m_editDescription.GetWindowRect(&rect);
	this->ScreenToClient(&rect);
	this->InvalidateRect(rect);
}

void CPOPDlg::OnOK()
{
	//__super::OnOK();
}

void CPOPDlg::OnCancel()
{
	if (!theWantExitApp)
	{
		OnBnClickedButtonClose();
		return;
	}
	__super::OnCancel();
}



void CPOPDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_editDescription.GetSafeHwnd() && this->GetFocus() == &m_editDescription)
	{
		RefreshEditDescription();
		m_editSearch.SetFocus();
	}
	__super::OnLButtonDown(nFlags, point);
}

void CPOPDlg::OnMySetting()
{
	// TODO: Add your command handler code here
	bool bIsNewWindow = false;
	if (m_pDlgEditInfo == NULL)
	{
		bIsNewWindow = true;
		m_pDlgEditInfo = new CDlgEditInfo(this);
		m_pDlgEditInfo->Create(CDlgEditInfo::IDD, this);
	}
	if (bIsNewWindow || !m_pDlgEditInfo->IsWindowVisible())
	{
		const int const_dlg_width = 508;
		const int const_dlg_height = 452;
		int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
		int m_nScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
		m_nScreenHeight += GetSystemMetrics(SM_CYCAPTION);
		CRect rect;
		rect.left = (m_nScreenWidth-const_dlg_width)/2;
		rect.top = (m_nScreenHeight-const_dlg_height)/2;
		rect.right = rect.left+const_dlg_width;
		rect.bottom = rect.top+const_dlg_height;
		m_pDlgEditInfo->MoveWindow(&rect);
	}
	m_pDlgEditInfo->ShowWindow(SW_SHOW);
}
void CPOPDlg::OnMyShare()
{
	theApp.OpenMyShareWindow(this);
}

void CPOPDlg::OnSkinSelect(UINT nID)
{
	const size_t nIndex = nID-EB_COMMAND_SKIN_SETTING;
	if (nIndex == 0)
	{
		CColorDialog dlg2(theApp.GetMainColor(), CC_FULLOPEN);
		if (dlg2.DoModal() != IDOK)
		{
			return;
		}
		theApp.SetMainColor(dlg2.GetColor());
	}else if (nIndex<=theColorSkinSize)
	{
		theApp.SetMainColor(theColorSkinsValue[nIndex-1]);
	}else
	{
		return;
	}
	if (theApp.m_pBoEB->use("eb"))
	{
		CString sSql;
		sSql.Format(_T("UPDATE sys_value_t SET value2=%d WHERE name='main-color'"),(DWORD)theApp.GetMainColor());
		theApp.m_pBoEB->execsql(sSql);
		theApp.m_pBoEB->close();
	}
	this->Invalidate();
}
void CPOPDlg::OnBnClickedButtonSkin2()
{
	if (m_menuSkin.GetSafeHmenu()==NULL)
	{
		m_menuSkin.CreatePopupMenu();
		for (int i=0; i<theColorSkinSize; i++)
		{
			m_menuSkin.AppendMenu(MF_BYCOMMAND,EB_COMMAND_SKIN_1+i,theColorSkinsString[i]);
		}
		m_menuSkin.AppendMenu(MF_SEPARATOR);
		m_menuSkin.AppendMenu(MF_BYCOMMAND,EB_COMMAND_SKIN_SETTING,_T("更多"));
	}
	bool bFindedChecked = false;
	for (int i=0; i<theColorSkinSize; i++)
	{
		if (theApp.GetMainColor()==theColorSkinsValue[i])
		{
			m_menuSkin.CheckMenuItem(EB_COMMAND_SKIN_1+i,MF_CHECKED);
			bFindedChecked = true; // **这里不能break退出
		}else
		{
			m_menuSkin.CheckMenuItem(EB_COMMAND_SKIN_1+i,MF_UNCHECKED);
		}
	}
	m_menuSkin.CheckMenuItem(EB_COMMAND_SKIN_SETTING,bFindedChecked?MF_UNCHECKED:MF_CHECKED);
	CPoint point;
	GetCursorPos(&point);
	m_menuSkin.TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);
}

void CPOPDlg::OnOpenMain()
{
	this->ShowWindow(SW_SHOWNORMAL);
	this->SetForegroundWindow();
}

void CPOPDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	OnBnClickedButtonLinestate();
	__super::OnRButtonUp(nFlags, point);
}

void CPOPDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	OnBnClickedButtonMax();
	__super::OnLButtonDblClk(nFlags, point);
}

void CPOPDlg::OnBnClickedButtonMyCenter()
{
	OnMySetting();
}

void CPOPDlg::OnBnClickedButtonMyShare()
{
	OnMyShare();
}

void CPOPDlg::OnBnClickedButtonFileMgr()
{
	OnFileManager();
}
