// PanelVideos.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "PanelVideos.h"


#define WM_BUILD_USER_VIEDO (WM_USER+10)

// CPanelVideos dialog

IMPLEMENT_DYNAMIC(CPanelVideos, CEbDialogBase)

CPanelVideos::CPanelVideos(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CPanelVideos::IDD, pParent)
{
	m_nLocalUserId = 0;
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		m_pUserVideo[i] = NULL;
		m_nVideoUserId[i] = 0;
	}
}

CPanelVideos::~CPanelVideos()
{
}

void CPanelVideos::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_VIDEO_END, m_btnVideoEnd);
	DDX_Control(pDX, IDC_BUTTON_VIDEO_ACCEPT, m_btnVideoAccept);
	DDX_Control(pDX, IDC_BUTTON_VIDEO_CANCEL, m_btnVideoCancel);
}


BEGIN_MESSAGE_MAP(CPanelVideos, CEbDialogBase)
	ON_WM_SIZE()
	ON_WM_DESTROY()
//#ifndef USES_EBCOM_TEST
//	ON_MESSAGE(EB_WM_V_REQUEST_RESPONSE, OnMessageVRequestResponse)
//	ON_MESSAGE(EB_WM_V_ACK_RESPONSE, OnMessageVAckResponse)
//	ON_MESSAGE(EB_WM_VIDEO_REQUEST, OnMessageVideoRequest)
//	ON_MESSAGE(EB_WM_VIDEO_ACCEPT, OnMessageVideoAccept)
//	ON_MESSAGE(EB_WM_VIDEO_REJECT, OnMessageVideoCancel)
//	ON_MESSAGE(EB_WM_VIDEO_CLOSE, OnMessageVideoEnd)
//#endif
	ON_MESSAGE(WM_BUILD_USER_VIEDO, OnMsgBuildUserVideo)

	ON_BN_CLICKED(IDC_BUTTON_VIDEO_END, &CPanelVideos::OnBnClickedButtonVideoEnd)
	ON_BN_CLICKED(IDC_BUTTON_VIDEO_ACCEPT, &CPanelVideos::OnBnClickedButtonVideoAccept)
	ON_BN_CLICKED(IDC_BUTTON_VIDEO_CANCEL, &CPanelVideos::OnBnClickedButtonVideoCancel)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CPanelVideos message handlers
#ifdef USES_EBCOM_TEST
//void CPanelVideos::Fire_onVideoStream(ULONG nUserId, BYTE* pVideoData, ULONG nVideoSize, ULONG nUserData)
void CPanelVideos::Fire_onVideoData(unsigned long nUserVideoId, const BYTE* pData, DWORD nSize, DWORD dwParam)
{
	if (dwParam == 0) return;
	((CPanelVideos*)dwParam)->DrawVideoData(nUserVideoId,(void*)pData,nSize);
}
void CPanelVideos::Fire_onAudioData(const BYTE* pData, DWORD nSize, DWORD dwParam)
{
	CDlgVideo * pUserVideo = (CDlgVideo*)dwParam;
	if(pUserVideo) {
		pUserVideo->AddFFTSampleData((const char*)pData, nSize);
	}
}

#else
//void CPanelVideos::ProcessVideoStream(DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD udata)
//{
//	//if (theHerbDlg == NULL || theHerbDlg->m_pdlgRoom == NULL || theHerbDlg->m_pdlgRoom->GetSafeHwnd() == NULL)
//	//	return;
//	//if (theHerbDlg->m_pdlgRoom->IsIconic())
//	//	return;
//	((CPanelVideos*)udata)->DrawVideoData(nId, pFrame->data, pFrame->usedlen);
//}
void VideoDataCallBack(int nUserVideoId, BYTE* pData, DWORD dwSize, DWORD dwParam)
{
	((CPanelVideos*)dwParam)->DrawVideoData(nUserVideoId, pData, dwSize);
}
#endif
//void CPanelVideos::FFTdataProcess(char *pdata, int length, void* udata)
//{
//	//CDlgVideo *pDlg = reinterpret_cast<CDlgVideo *>(udata);
//	//if(pDlg) {
//	//	pDlg->AddFFTSampleData( pdata, length);
//	//}
//}
void CPanelVideos::DrawVideoData(unsigned int nuserid, void * pdata, int len)
{
	int index = GetVideoIndex(nuserid);
	if (index >= 0 && m_pUserVideo[index]!=NULL)
	{
		m_pUserVideo[index]->DrawVideo(pdata, len);
	}else
		PostMessage(WM_BUILD_USER_VIEDO,nuserid);
}

BOOL CPanelVideos::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();

	this->SetMouseMove(FALSE);
	this->SetTransparentType(CEbDialogBase::TT_STATIC);

#ifdef USES_EBCOM_TEST
	_variant_t pVideoDeviceList = theEBClientCore->EB_GetVideoDeviceList();
	if (pVideoDeviceList.vt!=VT_EMPTY && pVideoDeviceList.parray != NULL)
	{
		CComSafeArray<VARIANT> m_sa(pVideoDeviceList.parray);
		for (ULONG i=0;i<m_sa.GetCount();i++)
		{
			CComVariant var = m_sa.GetAt(i);
			if (var.vt != VT_BSTR)
				continue;
			const CEBString sVideoDevice(var.bstrVal);
			m_pVideoDevices.push_back(sVideoDevice);
		}
	}
#else
	CEBAppClient::EB_GetVideoDeviceList(m_pVideoDevices);
#endif
	m_btnVideoEnd.Load(IDB_PNG_71X32G);
	m_btnVideoAccept.Load(IDB_PNG_71X32G);
	m_btnVideoCancel.Load(IDB_PNG_71X32G);
	m_btnVideoEnd.EnableWindow(FALSE);
	m_btnVideoAccept.EnableWindow(FALSE);
	m_btnVideoCancel.EnableWindow(FALSE);

#ifdef USES_EBCOM_TEST
	((CEBVideoDataEventsSink*)this)->AddRef();	// 增加计数，避免删除自己
	theEBClientCore->EB_SetVideoCallback(m_pCallInfo.GetCallId(),(IDispatch*)this,(DWORD)this);
	//theEBAppClient.EB_SetVideoHwnd(m_pCallInfo.GetCallId(),this->GetSafeHwnd());
#else
	theEBAppClient.EB_SetVideoCallback(m_pCallInfo.GetCallId(),VideoDataCallBack,(DWORD)this);
	//theEBAppClient.EB_SetVideoHwnd(m_pCallInfo.GetCallId(),this->GetSafeHwnd());
#endif

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPanelVideos::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}

void CPanelVideos::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CPanelVideos::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);

	const int const_btn_width = m_btnVideoEnd.GetImgWidth();
	int x = 2;
	int y = 5;
	m_btnVideoAccept.MovePoint(x,y);
	x += (const_btn_width+2);
	m_btnVideoCancel.MovePoint(x,y);
	x += (const_btn_width+2);
	m_btnVideoEnd.MovePoint(x,y);
}

void CPanelVideos::OnDestroy()
{
//#ifdef USES_EBCOM1
//#else
//	theEBAppClient.EB_SetVideoHwnd(m_pCallInfo.GetCallId(),NULL);
//#endif
	DoVideoDisonnecte();
	__super::OnDestroy();
}

void CPanelVideos::OnMove(void)
{
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		if (m_pUserVideo[i]!=NULL)
		{
			m_pUserVideo[i]->OnMove();
		}
	}
}

void CPanelVideos::ExitChat(bool bHangup)
{
	OnBnClickedButtonVideoEnd();
	//DoVideoDisonnecte();
}

bool CPanelVideos::IsMyOnVideo(void) const
{
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		if (m_nLocalUserId>0 && m_nVideoUserId[i]==m_nLocalUserId)
		{
			return true;
		}
	}
	return false;
}
int CPanelVideos::GetVideoIndex(int nVideoUserId) const
{
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		if (m_nVideoUserId[i]==nVideoUserId)
		{
			return i;
		}
	}
	return -1;
}
int CPanelVideos::GetVideoCount(void) const
{
	int result = 0;
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		if (m_nVideoUserId[i]>0)
		{
			result++;
		}
	}
	return result;
}
int CPanelVideos::GetNullVideoIndex(void) const
{
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		if (m_nVideoUserId[i]==0)
		{
			return i;
		}
	}
	return -1;
}
void CPanelVideos::OpenUserVideo(int nVideoUserId, bool bLocalUser)
{
	if (GetVideoIndex(nVideoUserId)>=0)
		return;
	int index = GetNullVideoIndex();
	if (index < 0)
		return;
	m_nVideoUserId[index] = nVideoUserId;

	const int const_view_width = 216;
	const int const_view_height = 168;
	CRect m_rectUser1;
	m_rectUser1.left = 2;
	m_rectUser1.right = m_rectUser1.left + const_view_width;
	m_rectUser1.top = 45+(const_view_height+6)*index;		// 视频间隔
	m_rectUser1.bottom = m_rectUser1.top + const_view_height;
	m_rectUser[index] = m_rectUser1;

	CDlgVideo * pUserVideo = new CDlgVideo(this);
	pUserVideo->Create(CDlgVideo::IDD,this);
#ifdef USES_EBCOM_TEST
	SHORT videoType = (SHORT)theEBClientCore->EB_GetDefaultVideoMediaType();
#else
	SHORT videoType = (SHORT)CEBAppClient::EB_GetDefaultVideoMediaType();
#endif
	if (bLocalUser && !m_pVideoDevices.empty())
	{
		size_t nLocalVideoIndex = (size_t)theApp.GetProfileInt(_T("param"),_T("local-video-index"),0);
		if (nLocalVideoIndex >= m_pVideoDevices.size())
			nLocalVideoIndex = 0;
#ifdef USES_EBCOM_TEST
		videoType = theEBClientCore->EB_OpenLocalVideo(m_pCallInfo.GetCallId(),nLocalVideoIndex,(IDispatch*)this,(ULONG)pUserVideo);
#else
		int nVideoType = 0;
		theEBAppClient.EB_OpenLocalVideo(m_pCallInfo.GetCallId(),nLocalVideoIndex,(PAudioDataCallBack)&CDlgVideo::FFTdataProcess,(void*)pUserVideo,nVideoType);
		videoType = nVideoType;
#endif
	}else if (!bLocalUser)
	{
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_OpenVideoByUserVideoId(m_pCallInfo.GetCallId(),nVideoUserId, (IDispatch*)this,(ULONG)pUserVideo);
#else
		theEBAppClient.EB_OpenVideoByUserVideoId(m_pCallInfo.GetCallId(),nVideoUserId, (PAudioDataCallBack)&CDlgVideo::FFTdataProcess,(void*)pUserVideo);
#endif
	}
	pUserVideo->BuildVideoWindow(videoType);
	pUserVideo->MoveWindow(&m_rectUser1);
	pUserVideo->ShowWindow(SW_SHOW);
	m_pUserVideo[index] = pUserVideo;
}
LRESULT CPanelVideos::OnMsgBuildUserVideo(WPARAM wParam, LPARAM lParam)
{
	int nVideoUserId = wParam;
	bool bLocalVideo = (bool)(nVideoUserId==m_nLocalUserId);
	OpenUserVideo(nVideoUserId,bLocalVideo);
	return 0;
}

#ifdef USES_EBCOM_TEST
void CPanelVideos::VRequestResponse(IEB_VideoInfo* pVideoInfo,int nStateValue)
{
	bool bOk = nStateValue==EB_STATE_OK;
	if (bOk)
	{
		theEBClientCore->EB_SetVideoCallback(m_pCallInfo.GetCallId(),(IDispatch*)this,(DWORD)this);
		if (m_pCallInfo.m_sGroupCode==0)
		{
			//m_staDesc.SetWindowText(_T("正在请求视频通话，等待对方连接..."));
			//theApp.InvalidateParentRect(&m_staDesc);
		}else
		{
			// 群组视频，打开视频
			DoVideoConnected();
		}
	}else
	{
		//m_staDesc.SetWindowText(_T("请求失败！"));
		//theApp.InvalidateParentRect(&m_staDesc);
	}

	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
	}
	m_btnVideoEnd.EnableWindow(TRUE);
}
#else
void CPanelVideos::VRequestResponse(const EB_VideoInfo* pVideoInfo,int nStateValue)
{
	bool bOk = nStateValue==0;
	if (bOk)
	{
		theEBAppClient.EB_SetVideoCallback(m_pCallInfo.GetCallId(),VideoDataCallBack,(DWORD)this);
		if (m_pCallInfo.m_sGroupCode==0)
		{
			//m_staDesc.SetWindowText(_T("正在请求视频通话，等待对方连接..."));
			//theApp.InvalidateParentRect(&m_staDesc);
		}else
		{
			// 群组视频，打开视频
			DoVideoConnected();
		}
	}else
	{
		//m_staDesc.SetWindowText(_T("请求失败！"));
		//theApp.InvalidateParentRect(&m_staDesc);
	}

	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
	}
	m_btnVideoEnd.EnableWindow(TRUE);
}
#endif

void CPanelVideos::DoVideoConnected(void)
{
	//DoVideoDisonnecte();
	std::vector<EB_UserVideoInfo> pFromVideoUserId;
	std::vector<EB_UserVideoInfo> pOnVideoUserId;
#ifdef USES_EBCOM_TEST
	_variant_t pMemberUserVideoId;
	_variant_t pOnVideoUserVideoId;
	m_nLocalUserId = theEBClientCore->EB_GetUserVideoId(m_pCallInfo.GetCallId(),&pMemberUserVideoId,&pOnVideoUserVideoId);
	if (pMemberUserVideoId.vt!=VT_EMPTY && pMemberUserVideoId.parray != NULL)
	{
		CComSafeArray<VARIANT> m_sa(pMemberUserVideoId.parray);
		for (ULONG i=0;i<m_sa.GetCount();i++)
		{
			CComVariant var = m_sa.GetAt(i);
			if (var.vt != VT_DISPATCH)
				continue;
			IEB_UserVideoInfoPtr pInterface;
			var.pdispVal->QueryInterface(__uuidof(IEB_UserVideoInfo),(void**)&pInterface);
			pFromVideoUserId.push_back(EB_UserVideoInfo(pInterface));
		}
	}
	if (pOnVideoUserVideoId.vt!=VT_EMPTY && pOnVideoUserVideoId.parray != NULL)
	{
		CComSafeArray<VARIANT> m_sa(pOnVideoUserVideoId.parray);
		for (ULONG i=0;i<m_sa.GetCount();i++)
		{
			CComVariant var = m_sa.GetAt(i);
			if (var.vt != VT_DISPATCH)
				continue;
			IEB_UserVideoInfoPtr pInterface;
			var.pdispVal->QueryInterface(__uuidof(IEB_UserVideoInfo),(void**)&pInterface);
			pOnVideoUserId.push_back(EB_UserVideoInfo(pInterface));
		}
	}
#else
	theEBAppClient.EB_GetUserVideoId(m_pCallInfo.GetCallId(),pFromVideoUserId,pOnVideoUserId,m_nLocalUserId);
#endif

	// 生成上麦视频
	for (size_t i=0; i<pOnVideoUserId.size(); i++)
	{
		int nVideoUserId = pOnVideoUserId[i].m_nUserVideoId;
		bool bLocalVideo = (bool)(nVideoUserId==m_nLocalUserId);
		if (bLocalVideo)
		{
			OpenUserVideo(nVideoUserId,bLocalVideo);
		}else
		{
			// 群接收时，不打开，没有视频
			//OpenUserVideo(nVideoUserId,bLocalVideo);	// 这里打开，界面会黑，在收到数据数据后再自动打开
		}
	}
}

void CPanelVideos::DoVideoDisonnecte(void)
{
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_CloseAllVideo(m_pCallInfo.GetCallId());
#else
	theEBAppClient.EB_CloseAllVideo(m_pCallInfo.GetCallId());
#endif
	for (int i=0; i<MAX_VIDEO_COUNT; i++)
	{
		m_nVideoUserId[i] = 0;
		if (m_pUserVideo[i]!=NULL)
		{
			m_pUserVideo[i]->DestroyWindow();
			delete m_pUserVideo[i];
			m_pUserVideo[i] = NULL;
		}
	}
	theApp.InvalidateParentRect(this);
}

#ifdef USES_EBCOM_TEST
void CPanelVideos::VAckResponse(IEB_VideoInfo* pVideoInfo,int nStateValue)
{
	// 接受才发送窗口消息
	//m_staDesc.SetWindowText(_T("接受视频通话，正在视频中..."));
	//theApp.InvalidateParentRect(&m_staDesc);
	DoVideoConnected();
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(TRUE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}
}
#else
void CPanelVideos::VAckResponse(const EB_VideoInfo* pVideoInfo,int nStateValue)
{
	// 接受才发送窗口消息
	//m_staDesc.SetWindowText(_T("接受视频通话，正在视频中..."));
	//theApp.InvalidateParentRect(&m_staDesc);
	DoVideoConnected();
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(TRUE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}
}
#endif

#ifdef USES_EBCOM_TEST
void CPanelVideos::VideoRequest(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		//CString stext;
		//stext.Format(_T("%s请求视频通话..."),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
	}else
	{
		//CString stext;
		//stext.Format(_T("%s打开视频会议..."),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		if (this->GetVideoCount()>0)
		{
			// 自动接收视频
			OnBnClickedButtonVideoAccept();
		}
	}
	m_btnVideoAccept.EnableWindow(TRUE);
	m_btnVideoCancel.EnableWindow(TRUE);
	if (IsMyOnVideo())
		m_btnVideoEnd.EnableWindow(TRUE);
	else
		m_btnVideoEnd.EnableWindow(FALSE);
}
#else
void CPanelVideos::VideoRequest(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		//CString stext;
		//stext.Format(_T("%s请求视频通话..."),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
	}else
	{
		//CString stext;
		//stext.Format(_T("%s打开视频会议..."),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		if (this->GetVideoCount()>0)
		{
			// 自动接收视频
			OnBnClickedButtonVideoAccept();
		}
	}
	m_btnVideoAccept.EnableWindow(TRUE);
	m_btnVideoCancel.EnableWindow(TRUE);
	if (IsMyOnVideo())
		m_btnVideoEnd.EnableWindow(TRUE);
	else
		m_btnVideoEnd.EnableWindow(FALSE);
}
#endif

#ifdef USES_EBCOM_TEST
void CPanelVideos::VideoAccept(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(TRUE);
		//CString stext;
		//stext.Format(_T("对方接受视频通话，正在视频中..."));
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		DoVideoConnected();
	}
}
#else
void CPanelVideos::VideoAccept(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(TRUE);
		//CString stext;
		//stext.Format(_T("对方接受视频通话，正在视频中..."));
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		DoVideoConnected();
	}
}
#endif

#ifdef USES_EBCOM_TEST
void CPanelVideos::VideoCancel(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoEnd.EnableWindow(FALSE);
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		//CString stext;
		//stext.Format(_T("%s拒绝你的视频通话请求"),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
	}
}
#else
void CPanelVideos::VideoCancel(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoEnd.EnableWindow(FALSE);
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		//CString stext;
		//stext.Format(_T("%s拒绝你的视频通话请求"),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
	}
}
#endif

#ifdef USES_EBCOM_TEST
void CPanelVideos::VideoEnd(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)// || pFromAccountInfo->GetAccount() == theEBAppClient.EB_GetLogonAccount())
	{
		// 一对一会话，或者本端关闭
		//CString stext;
		//stext.Format(_T("%s取消视频会话"),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		DoVideoDisonnecte();
	}else
	{
		// 有人关闭我的视频；
		const int nVideoUserId = pUserVideoInfo->UserVideoId;
		if (nVideoUserId == 0) return;

		int index = GetVideoIndex(nVideoUserId);
		if (index >= 0)
		{
			if (m_pUserVideo[index]!=NULL)
			{
				m_pUserVideo[index]->DestroyWindow();
				delete m_pUserVideo[index];
				m_pUserVideo[index] = NULL;
			}
			m_nVideoUserId[index]=0;
		}

		if (GetVideoCount() == 0)
		{
			DoVideoDisonnecte();
		}
	}
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
	}else
	{
		// 群组可以继续请求查看视频
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
	}
	if (!IsMyOnVideo())
	{
		m_btnVideoEnd.EnableWindow(FALSE);
	}
}
#else
void CPanelVideos::VideoEnd(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo)
{
	if (m_pCallInfo.m_sGroupCode==0)// || pFromAccountInfo->GetAccount() == theEBAppClient.EB_GetLogonAccount())
	{
		// 一对一会话，或者本端关闭
		//CString stext;
		//stext.Format(_T("%s取消视频会话"),pFromAccountInfo->GetAccount().c_str());
		//m_staDesc.SetWindowText(stext);
		//theApp.InvalidateParentRect(&m_staDesc);
		DoVideoDisonnecte();
	}else
	{
		// 有人关闭我的视频；
		const int nVideoUserId = pUserVideoInfo->m_nUserVideoId;
		if (nVideoUserId == 0) return;

		int index = GetVideoIndex(nVideoUserId);
		if (index >= 0)
		{
			if (m_pUserVideo[index]!=NULL)
			{
				m_pUserVideo[index]->DestroyWindow();
				delete m_pUserVideo[index];
				m_pUserVideo[index] = NULL;
			}
			m_nVideoUserId[index]=0;
		}

		if (GetVideoCount() == 0)
		{
			DoVideoDisonnecte();
		}
	}
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
	}else
	{
		// 群组可以继续请求查看视频
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
	}
	if (!IsMyOnVideo())
	{
		m_btnVideoEnd.EnableWindow(FALSE);
	}
}
#endif

void CPanelVideos::OnBnClickedButtonVideoEnd()
{
	//DoVideoDisonnecte();	// 自己会收到video_close事件
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_VideoEnd(m_pCallInfo.GetCallId());
#else
	theEBAppClient.EB_VideoEnd(m_pCallInfo.GetCallId());
#endif
}

void CPanelVideos::OnBnClickedButtonVideoAccept()
{
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_SetVideoCallback(m_pCallInfo.GetCallId(),(IDispatch*)this,(DWORD)this);
	theEBClientCore->EB_VideoAck(m_pCallInfo.GetCallId(),VARIANT_TRUE);
#else
	theEBAppClient.EB_SetVideoCallback(m_pCallInfo.GetCallId(),VideoDataCallBack,(DWORD)this);
	theEBAppClient.EB_VideoAck(m_pCallInfo.GetCallId(),true);
#endif
}

void CPanelVideos::OnBnClickedButtonVideoCancel()
{
	DoVideoDisonnecte();
	if (m_pCallInfo.m_sGroupCode==0)
	{
		m_btnVideoAccept.EnableWindow(FALSE);
		m_btnVideoCancel.EnableWindow(FALSE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}else
	{
		m_btnVideoAccept.EnableWindow(TRUE);
		m_btnVideoCancel.EnableWindow(TRUE);
		m_btnVideoEnd.EnableWindow(FALSE);
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_VideoAck(m_pCallInfo.GetCallId(),VARIANT_FALSE);
	theEBClientCore->EB_VideoEnd(m_pCallInfo.GetCallId());
#else
	theEBAppClient.EB_VideoAck(m_pCallInfo.GetCallId(),false);
	theEBAppClient.EB_VideoEnd(m_pCallInfo.GetCallId());
#endif
}

void CPanelVideos::OnLButtonDown(UINT nFlags, CPoint point)
{
	// this->chatright->dialog
	this->GetParent()->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}
