#pragma once


// CDlgEditAccInfo dialog

class CDlgEditAccInfo : public CEbDialogBase
{
	DECLARE_DYNAMIC(CDlgEditAccInfo)

public:
	CDlgEditAccInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgEditAccInfo();

	void Load(void);
	void Save(void);

// Dialog Data
	enum { IDD = IDD_DLG_EDIT_ACCINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CHoverEdit	m_editAccount;
	CHoverEdit	m_editUserName;
	CHoverEdit	m_editDescription;

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
