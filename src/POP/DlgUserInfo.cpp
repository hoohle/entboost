// DlgUserInfo.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgUserInfo.h"


// CDlgUserInfo dialog

IMPLEMENT_DYNAMIC(CDlgUserInfo, CEbDialogBase)

CDlgUserInfo::CDlgUserInfo(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgUserInfo::IDD, pParent)
{

}

CDlgUserInfo::~CDlgUserInfo()
{
}

void CDlgUserInfo::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_EDIT_NAME, m_editName);
}


BEGIN_MESSAGE_MAP(CDlgUserInfo, CEbDialogBase)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT_NAME, &CDlgUserInfo::OnEnKillfocusEditName)
	ON_EN_KILLFOCUS(IDC_EDIT_ENTERPRISE, &CDlgUserInfo::OnEnKillfocusEditEnterprise)
	ON_EN_KILLFOCUS(IDC_EDIT_TEL, &CDlgUserInfo::OnEnKillfocusEditTel)
	ON_EN_KILLFOCUS(IDC_EDIT_EMAIL, &CDlgUserInfo::OnEnKillfocusEditEmail)
	ON_EN_KILLFOCUS(IDC_EDIT_JOBTITLE, &CDlgUserInfo::OnEnKillfocusEditJobtitle)
	ON_EN_KILLFOCUS(IDC_EDIT_DEPARTMENT, &CDlgUserInfo::OnEnKillfocusEditDepartment)
END_MESSAGE_MAP()


// CDlgUserInfo message handlers

BOOL CDlgUserInfo::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	this->SetTransparentType(CEbDialogBase::TT_STATIC);
	this->SetMouseMove(FALSE);
	this->SetBgColor(RGB(255,255,255));	// 白色背景

	// test
	//m_editName.Create(WS_CHILD|WS_VISIBLE|ES_LEFT|ES_READONLY, CRect(0,0,300, 12), this, 0xffff);
	//m_editName.SetWindowText(_T("陆游 13512345678"));
	//m_editName.ModifyStyle(0, ES_READONLY);
	//m_editName.SetReadOnly();
	//CEBAccountInfo::pointer pFromAccount = theEBAppClient.GetCallInfo(this->m_sCallId, this->m_sFromAccount); 

	this->GetDlgItem(IDC_EDIT_NAME)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sName.c_str());
	this->GetDlgItem(IDC_EDIT_TEL)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sPhone.c_str());
	this->GetDlgItem(IDC_EDIT_EMAIL)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sEmail.c_str());
	this->GetDlgItem(IDC_EDIT_JOBTITLE)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sTitle.c_str());
	this->GetDlgItem(IDC_EDIT_DEPARTMENT)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sGroupName.c_str());
	this->GetDlgItem(IDC_EDIT_ENTERPRISE)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sEnterprise.c_str());
	this->GetDlgItem(IDC_EDIT_ADDRESS)->SetWindowText(m_pFromAccountInfo.m_pFromCardInfo.m_sAddress.c_str());

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgUserInfo::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);

	const int const_sta_left = 15;
	const int const_sta_top = 18;
	const int const_sta_width = 35;
	const int const_sta_height = 14;
	const int const_test_width = cx-const_sta_width;
	const int const_line_intever = 10;					// 行间隔
	// 姓名
	int x = const_sta_left;
	int y = const_sta_top;
	if (this->GetDlgItem(IDC_STATIC_NAME)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_NAME)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_NAME)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_NAME)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	// 电话
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_TEL)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_TEL)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_TEL)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_TEL)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	// 邮箱
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_EMAIL)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_EMAIL)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_EMAIL)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_EMAIL)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	y += 5;	// 上下分隔
	// 职务
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_JOBTITLE)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_JOBTITLE)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_JOBTITLE)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_JOBTITLE)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	// 部门
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_DEPARTMENT)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_DEPARTMENT)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_DEPARTMENT)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_DEPARTMENT)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	// 公司
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_ENTERPRISE)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_ENTERPRISE)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_ENTERPRISE)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_ENTERPRISE)->MoveWindow(x, y, const_test_width, const_sta_height);
	}
	// 地址
	x = const_sta_left;
	y += (const_sta_height+const_line_intever);
	if (this->GetDlgItem(IDC_STATIC_ADDRESS)->GetSafeHwnd())
	{
		this->GetDlgItem(IDC_STATIC_ADDRESS)->MoveWindow(x, y, const_sta_width, const_sta_height);
	}
	if (this->GetDlgItem(IDC_EDIT_ADDRESS)->GetSafeHwnd())
	{
		x += (const_sta_width);
		this->GetDlgItem(IDC_EDIT_ADDRESS)->MoveWindow(x, y, const_test_width, const_sta_height);
	}

}

void CDlgUserInfo::OnLButtonDown(UINT nFlags, CPoint point)
{
	// this->chatright->dialog
	this->GetParent()->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgUserInfo::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}

void CDlgUserInfo::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CDlgUserInfo::OnDestroy()
{
	CEbDialogBase::OnDestroy();

	// TODO: Add your message handler code here
}

void CDlgUserInfo::InvalidateParentRect(UINT nID)
{
	CRect rect;
	this->GetDlgItem(nID)->GetWindowRect(&rect);
	this->GetParent()->ScreenToClient(&rect);
	this->GetParent()->InvalidateRect(&rect);
}

void CDlgUserInfo::OnEnKillfocusEditName()
{
	InvalidateParentRect(IDC_EDIT_NAME);
}

void CDlgUserInfo::OnEnKillfocusEditEnterprise()
{
	InvalidateParentRect(IDC_EDIT_ENTERPRISE);
}

void CDlgUserInfo::OnEnKillfocusEditTel()
{
	InvalidateParentRect(IDC_EDIT_TEL);
}

void CDlgUserInfo::OnEnKillfocusEditEmail()
{
	InvalidateParentRect(IDC_EDIT_EMAIL);
}

void CDlgUserInfo::OnEnKillfocusEditJobtitle()
{
	InvalidateParentRect(IDC_EDIT_JOBTITLE);
}

void CDlgUserInfo::OnEnKillfocusEditDepartment()
{
	InvalidateParentRect(IDC_EDIT_DEPARTMENT);
}
