#pragma once
//#include "Core/MrFrame.h"


// CDlgMsgRecord dialog

class CDlgMsgRecord : public CEbDialogBase
{
	DECLARE_DYNAMIC(CDlgMsgRecord)

public:
	CDlgMsgRecord(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgMsgRecord();

	void LoadAccountMsgRecord(eb::bigint sAccount, const tstring& sName);
	void LoadDepartmentMsgRecord(eb::bigint sDepCode, const tstring& sName);

// Dialog Data
	enum { IDD = IDD_DLG_MSGRECORD };

protected:
	eb::bigint m_sAccount;
	eb::bigint m_sGroupCode;
	tstring m_sName;
	void LoadMsgRecord(const CString& sSql, bool bIsDepartment);
	void WriteFileHICON(const char* lpszFilePath);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CTraButton m_btnMin;
	CTraButton m_btnMax;
	CTraButton m_btnClose;
	//CMrFrame m_pMrFrame;
	CWnd m_pMrFrameControl;
	CComPtr<IEBRichMessage> m_pMrFrameInterface;
	CHoverEdit m_editSearch;
	CTraButton m_btnDeleteAll;

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonMin();
	afx_msg void OnBnClickedButtonMax();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnPaint();
	afx_msg void OnClose();
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedButtonDeleteall();
	afx_msg void OnEnChangeEditSearch();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};
