#pragma once
//#include "videoroom.h"
#include "Core/DlgVoiceBar.h"

const int const_video_width = 320;
const int const_video_height = 240;

// CDlgVideo dialog
#define FFT_LEN  1024

class CDlgVideo : public CDialog
{
	DECLARE_DYNAMIC(CDlgVideo)

public:
	CDlgVideo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgVideo();

	static void FFTdataProcess(char *pdata, int length, void* udata);
	void AddFFTSampleData(const char* pdata, int length);
	void OnMove(void);
	void BuildVideoWindow(SHORT videoType);
	void DrawVideo(void * videodata, int len );
	bool Photo(const char* lspzSaveTo);
// Dialog Data
	enum { IDD = IDD_DLG_VIDEO };

protected:
#ifdef USES_EBCOM_TEST
	CWnd m_pUserVideo;
	CComPtr<IEBVWindow> m_pUserVideoInterface;
#else
	Cvideowindow * m_pUserVideo;
#endif
	int m_nVideoUserId;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CDlgVoiceBar *	m_pVoiceBar;

	//FFT data process callback.
	void ProcessFFTData(void);
	short m_SampleArrry[FFT_LEN];
	int   m_nSampleLength;
	double finleft[FFT_LEN/2],fout[FFT_LEN], foutimg[FFT_LEN],fdraw[FFT_LEN/2];
	int		m_nVoicePos;


	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnMove(int x, int y);
};
