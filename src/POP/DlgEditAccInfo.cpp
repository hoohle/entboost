// DlgEditAccInfo.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgEditAccInfo.h"


// CDlgEditAccInfo dialog

IMPLEMENT_DYNAMIC(CDlgEditAccInfo, CEbDialogBase)

CDlgEditAccInfo::CDlgEditAccInfo(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgEditAccInfo::IDD, pParent)
{

}

CDlgEditAccInfo::~CDlgEditAccInfo()
{
}

void CDlgEditAccInfo::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ACCOUNT, m_editAccount);
	DDX_Control(pDX, IDC_EDIT_USERNAME, m_editUserName);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
}


BEGIN_MESSAGE_MAP(CDlgEditAccInfo, CEbDialogBase)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CDlgEditAccInfo message handlers

BOOL CDlgEditAccInfo::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	this->SetTransparentType(CEbDialogBase::TT_STATIC);
	this->SetMouseMove(FALSE);

	m_editAccount.SetReadOnly(TRUE);
	Load();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEditAccInfo::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CDlgEditAccInfo::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}

void CDlgEditAccInfo::OnLButtonDown(UINT nFlags, CPoint point)
{
	this->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgEditAccInfo::Load(void)
{
#ifdef USES_EBCOM_TEST
	this->GetDlgItem(IDC_EDIT_ACCOUNT)->SetWindowText(CEBString(theEBClientCore->EB_LogonAccount.GetBSTR()).c_str());
	this->GetDlgItem(IDC_EDIT_USERNAME)->SetWindowText(CEBString(theEBClientCore->EB_UserName.GetBSTR()).c_str());
	this->GetDlgItem(IDC_EDIT_DESCRIPTION)->SetWindowText(CEBString(theEBClientCore->EB_Description.GetBSTR()).c_str());
#else
	this->GetDlgItem(IDC_EDIT_ACCOUNT)->SetWindowText(theEBAppClient.EB_GetLogonAccount().c_str());
	this->GetDlgItem(IDC_EDIT_USERNAME)->SetWindowText(theEBAppClient.EB_GetUserName().c_str());
	this->GetDlgItem(IDC_EDIT_DESCRIPTION)->SetWindowText(theEBAppClient.EB_GetDescription().c_str());
#endif
}

void CDlgEditAccInfo::Save(void)
{
	CString sUserName;
	this->GetDlgItem(IDC_EDIT_USERNAME)->GetWindowText(sUserName);
	if (sUserName.IsEmpty())
	{
		this->GetDlgItem(IDC_EDIT_USERNAME)->SetFocus();
		CDlgMessageBox::EbMessageBox(this,_T("���������ƣ�"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
#ifdef USES_EBCOM_TEST
	const CEBString sCurrentUserName = theEBClientCore->EB_UserName.GetBSTR();
	const CEBString sCurrentDescription = theEBClientCore->EB_Description.GetBSTR();
	CString sDescription;
	this->GetDlgItem(IDC_EDIT_DESCRIPTION)->GetWindowText(sDescription);
	if (sCurrentUserName != (LPCTSTR)sUserName)
	{
		theEBClientCore->EB_UserName = (LPCTSTR)sUserName;
	}
	if (sCurrentDescription != (LPCTSTR)sDescription)
	{
		theEBClientCore->EB_Description = (LPCTSTR)sDescription;
	}
#else
	std::string sCurrentUserName = theEBAppClient.EB_GetUserName();
	std::string sCurrentDescription = theEBAppClient.EB_GetDescription();
	CString sDescription;
	this->GetDlgItem(IDC_EDIT_DESCRIPTION)->GetWindowText(sDescription);
	if (sCurrentUserName != (LPCTSTR)sUserName)
	{
		theEBAppClient.EB_SetUserName(sUserName);
	}
	if (sCurrentDescription != (LPCTSTR)sDescription)
	{
		theEBAppClient.EB_SetDescription(sDescription);
	}
#endif
}

