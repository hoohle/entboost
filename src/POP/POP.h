
// POP.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "DlgEmotionSelect.h"
#include "DlgDialog.h"
#include "DlgMsgRecord.h"
#include "DlgResourceMgr.h"
#include "EBCCallInfo.h"
#include "DlgContactInfo.h"
#include "DlgMessageBox.h"

CString GetLineStateText(EB_USER_LINE_STATE nLineState);
CString GetGroupTypeText(EB_GROUP_TYPE nGroupType, bool bShortName=false);

//class CPOPConfigInfo
//{
//public:
//	CPOPConfigInfo(void)
//		: m_sServer("192.168.0.208:8012")
//		//: m_sServer("myphone.pop.com")
//	{
//	}
//
//	void SetServer(const tstring & value) {m_sServer = value;}
//	const tstring & GetServer(void) const {return m_sServer;}
//
//private:
//	tstring m_sServer;
//};

typedef enum MSG_RECORD_TYPE
{
	MRT_TITLE	// 谁对谁说
	, MRT_TEXT
	, MRT_JPG
	, MRT_FILE
	, MRT_WAV

};

// CPOPApp:
// 有关此类的实现，请参阅 POP.cpp
//

class CPOPApp : public CWinApp
	, public CTreeCallback
{
public:
	CPOPApp();

	//Gdiplus::Image * m_imageEbIcon;
	//Gdiplus::Image * m_imageEntLogo;
	Gdiplus::Image * m_image1Group;
	Gdiplus::Image * m_image1People;
	Gdiplus::Image * m_image1Cloud;
	Gdiplus::Image * m_image2Cloud;
	Gdiplus::Image * m_imageDefaultVisitor;
	Gdiplus::Image * m_imageDefaultOrg;
	Gdiplus::Image * m_imageDefaultContact;
	Gdiplus::Image * m_imageDefaultMember;
	Gdiplus::Image * m_imageDefaultDepartment;
	Gdiplus::Image * m_imageDefaultProject;
	Gdiplus::Image * m_imageDefaultGroup;
	Gdiplus::Image * m_imageDefaultTempGroup;
	Gdiplus::Image * m_imageStateAway;
	Gdiplus::Image * m_imageStateBusy;

	CDlgEmotionSelect* m_pDlgEmotionSelect;
	void ShowImageWindow(bool bShow, CImageSelectCallback* pCallback,LPRECT lpRect);
	static void RunEBSC(const CString& sParameter);
	static void InvalidateParentRect(const CWnd* pWnd);
	static int CALLBACK TreeCmpFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	void SetLineState2Db(EB_USER_LINE_STATE nLineState);
	void DeleteDbRecord(eb::bigint sId, bool bAccount);	// 删除聊天记录和临时图片文件
	void OpenGroupShareWindow(eb::bigint sGroupCode,const CWnd* pWndLayout);
	void OpenMyShareWindow(const CWnd* pWndLayout);
	void OpenSubscribeFuncWindow(const EB_SubscribeFuncInfo& pSubscribeFuncInfo);

	CString GetSettingFile(void) const;
	CString GetSettingIniFile(void) const;
	void EBC_SetProfileString(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,LPCTSTR lpszValue);
	CString EBC_GetProfileString(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,LPCTSTR lpszDefault=NULL);
	void EBC_SetProfileInt(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,UINT nValue);
	UINT EBC_GetProfileInt(LPCTSTR lpszAppName,LPCTSTR lpszKeyName,UINT nDefault=0);

	HICON GetFileHIcon(const std::string& sFilePath);
	CLockMap<tstring,HICON> m_pFileHIcon;				// ext->

	//CPOPConfigInfo	m_config;
	CHARFORMAT		m_cfDefault;
	bo::CBodbHandler::pointer m_pBoEB;
	bo::CBodbHandler::pointer m_pBoUsers;

	CLockMap<eb::bigint,CDlgMsgRecord*> m_pMsgRecord;				// account/groupcode->
	CLockMap<eb::bigint,CDlgResourceMgr*> m_pResourceMgr;			// account/groupcode->
	CLockMap<eb::bigint,CEBCCallInfo::pointer> m_pCallList;		// callid->
	CLockMap<eb::bigint, CDlgDialog::pointer> m_pDialogList;	// callid->
	CLockList<CDlgDialog::pointer> m_pCloseDialog;			// 
	CLockList<CWnd*> m_pCloseWnd;							// 
	std::vector<EB_SubscribeFuncInfo> m_pSubscribeFuncList;	// for menu
	void ClearSubscribeSelectInfo(void);
	eb::bigint m_nSelectCallId;
	eb::bigint m_nSelectUserId;
	std::string m_sSelectAccount;
	eb::bigint m_nSelectGroupId;

	//std::string GetFullAccount(const char* lpszAccount) const;
	//int GetManagerLevel(const char* sGroupCode) const;
	static void GetScreenSize(int & pWidth, int & pHeight);
	const CString & GetAppPath(void) const {return m_sAppPath;}
	const CString & GetAppDataPath(void) const {return m_sAppDataPath;}
	const CString & GetUserMainPath(void) const {return m_sUserMainPath;}
	const CString & GetUserImagePath(void) const {return m_sUserImagePath;}
	const CString & GetUserFilePath(void) const {return m_sUserFilePath;}
	const CString& GetLogonAccount(void) const {return m_sLogonAccount;}
	eb::bigint GetLogonUserId(void) const {return m_nLogonUserId;}
	const CString& GetLogonPassword(void) const {return m_sLogonPassword;}
	//const HICON GetIconAway(void) const {return m_iconAway;}
	//const HICON GetIconBusy(void) const {return m_iconBusy;}
	//const HICON GetIconEnt(void) const {return m_iconEnterprise;}
	//const HICON GetIconDep(void) const {return m_iconDepartment;}
	//const HICON GetIconCon(void) const {return m_iconContact;}
	void SetMainColor(COLORREF v) {m_nMainColor=v;}
	COLORREF GetMainColor(void) const {return m_nMainColor;}

	void NewEmployeeInfo(CWnd* pParent,const CTreeItemInfo * pTreeItemInfo);
#ifdef USES_EBCOM_TEST
	void EditEmployeeInfo(CWnd* pParent,IEB_MemberInfo* pMemberInfo);
#else
	void EditEmployeeInfo(CWnd* pParent,const EB_MemberInfo* pMemberInfo);
#endif
	bool IsLogonVisitor(void) const;
private:
	CString m_sAppPath;
	CString m_sAppDataPath;
	CString m_sUserMainPath;
	CString m_sUserImagePath;
	CString m_sUserFilePath;
	CString m_sLogonAccount;
	eb::bigint m_nLogonUserId;
	CString m_sLogonPassword;
	//HICON m_iconAway;
	//HICON m_iconBusy;
	//HICON m_iconEnterprise;
	//HICON m_iconDepartment;
	//HICON m_iconContact;
	COLORREF m_nMainColor;

	virtual bool GetItemImage(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem,Gdiplus::Image*& pImage1,Gdiplus::Image*& pImage2,int& pState) const;
	virtual bool GetItemDrawOpenClose(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem) const;

// 重写
	//bool openebuser(void);
	virtual BOOL InitInstance();
	void ReadUserSetting(void);
	void SaveUserSetting(void);

// 实现

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern CPOPApp theApp;
#ifdef USES_EBCOM_TEST
extern CComPtr<IEBParseSetting>	theEBSetting;
extern CComPtr<IEBClientCore>	theEBClientCore;
//extern CEBAppClient theEBAppClient;
#else
extern CEBParseSetting theSetting;
extern CEBAppClient theEBAppClient;
#endif