// DlgEditAccSetting.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgEditAccSetting.h"


// CDlgEditAccSetting dialog

IMPLEMENT_DYNAMIC(CDlgEditAccSetting, CEbDialogBase)

CDlgEditAccSetting::CDlgEditAccSetting(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgEditAccSetting::IDD, pParent)
{

}

CDlgEditAccSetting::~CDlgEditAccSetting()
{
}

void CDlgEditAccSetting::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_ENABLE_OUTENT_CALL, m_bEnableOutEntCall);
	DDX_Control(pDX, IDC_CHECK_ENABLE_OUTENT_ACCEPT, m_bEnableOutEntAccept);
	DDX_Control(pDX, IDC_CHECK_ENABLE_USER_CALL, m_bEnableUserCall);
	DDX_Control(pDX, IDC_CHECK_ENABLE_USER_ACCEPT, m_bEnableUserAccept);
	DDX_Control(pDX, IDC_CHECK_ENABLE_VISITOR_CALL, m_bEnableVisitorCall);
	DDX_Control(pDX, IDC_CHECK_ENABLE_VISITOR_ACCEPT, m_bEnableVisitorAccept);
	DDX_Control(pDX, IDC_CHECK_ENABLE_OFF_CALL, m_bEnableOffCall);
	DDX_Control(pDX, IDC_CHECK_ENABLE_OFF_FILE, m_bEnableOffFile);
	DDX_Control(pDX, IDC_CHECK_CONNECTED_OPEN_CHAT, m_bConnectedOpenChat);
}


BEGIN_MESSAGE_MAP(CDlgEditAccSetting, CEbDialogBase)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CDlgEditAccSetting message handlers

BOOL CDlgEditAccSetting::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	this->SetTransparentType(CEbDialogBase::TT_STATIC);
	this->SetMouseMove(FALSE);

	m_bEnableOutEntCall.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableOutEntCall.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableOutEntCall.SetTextLeft(16);
	m_bEnableOutEntCall.SetAlignText(CLabelEx::Align_Left);
	m_bEnableOutEntAccept.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableOutEntAccept.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableOutEntAccept.SetTextLeft(16);
	m_bEnableOutEntAccept.SetAlignText(CLabelEx::Align_Left);
	m_bEnableUserCall.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableUserCall.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableUserCall.SetTextLeft(16);
	m_bEnableUserCall.SetAlignText(CLabelEx::Align_Left);
	m_bEnableUserAccept.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableUserAccept.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableUserAccept.SetTextLeft(16);
	m_bEnableUserAccept.SetAlignText(CLabelEx::Align_Left);
	m_bEnableVisitorCall.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableVisitorCall.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableVisitorCall.SetTextLeft(16);
	m_bEnableVisitorCall.SetAlignText(CLabelEx::Align_Left);
	m_bEnableVisitorAccept.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableVisitorAccept.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableVisitorAccept.SetTextLeft(16);
	m_bEnableVisitorAccept.SetAlignText(CLabelEx::Align_Left);
	m_bEnableOffCall.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableOffCall.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableOffCall.SetTextLeft(16);
	m_bEnableOffCall.SetAlignText(CLabelEx::Align_Left);
	m_bEnableOffFile.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bEnableOffFile.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bEnableOffFile.SetTextLeft(16);
	m_bEnableOffFile.SetAlignText(CLabelEx::Align_Left);
	m_bConnectedOpenChat.LoadLabel(IDB_PNG_CHECKBOX,-1,0,2);
	m_bConnectedOpenChat.SetCheckBoxHoverBorder(TRUE,RGB(0,128,255));
	m_bConnectedOpenChat.SetTextLeft(16);
	m_bConnectedOpenChat.SetAlignText(CLabelEx::Align_Left);

	Load();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEditAccSetting::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CDlgEditAccSetting::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}
void CDlgEditAccSetting::OnLButtonDown(UINT nFlags, CPoint point)
{
	this->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgEditAccSetting::Load(void)
{
#ifdef USES_EBCOM_TEST
	long nSettingValue = theEBClientCore->EB_MyAccountSetting;
#else
	int nSettingValue = theEBAppClient.EB_GetMyAccountSetting();
#endif
	m_bEnableOutEntCall.SetCheck((nSettingValue&EB_SETTING_ENABLE_OUTENT_CALL)==EB_SETTING_ENABLE_OUTENT_CALL);
	m_bEnableOutEntAccept.SetCheck((nSettingValue&EB_SETTING_AUTO_OUTENT_ACCEPT)==EB_SETTING_AUTO_OUTENT_ACCEPT);
	m_bEnableUserCall.SetCheck((nSettingValue&EB_SETTING_ENABLE_USER_CALL)==EB_SETTING_ENABLE_USER_CALL);
	m_bEnableUserAccept.SetCheck((nSettingValue&EB_SETTING_AUTO_USER_ACCEPT)==EB_SETTING_AUTO_USER_ACCEPT);
	m_bEnableVisitorCall.SetCheck((nSettingValue&EB_SETTING_ENABLE_VISITOR_CALL)==EB_SETTING_ENABLE_VISITOR_CALL);
	m_bEnableVisitorAccept.SetCheck((nSettingValue&EB_SETTING_AUTO_VISITOR_ACCEPT)==EB_SETTING_AUTO_VISITOR_ACCEPT);
	m_bEnableOffCall.SetCheck((nSettingValue&EB_SETTING_ENABLE_OFF_CALL)==EB_SETTING_ENABLE_OFF_CALL);
	m_bEnableOffFile.SetCheck((nSettingValue&EB_SETTING_ENABLE_OFF_FILE)==EB_SETTING_ENABLE_OFF_FILE);
	m_bConnectedOpenChat.SetCheck((nSettingValue&EB_SETTING_CONNECTED_OPEN_CHAT)==EB_SETTING_CONNECTED_OPEN_CHAT);
}

void CDlgEditAccSetting::Save(void)
{
	UpdateData();
	int nSettingValue = 0;
	nSettingValue |= m_bEnableOutEntCall.GetCheck()?EB_SETTING_ENABLE_OUTENT_CALL:0;
	nSettingValue |= m_bEnableOutEntAccept.GetCheck()?EB_SETTING_AUTO_OUTENT_ACCEPT:0;
	nSettingValue |= m_bEnableUserCall.GetCheck()?EB_SETTING_ENABLE_USER_CALL:0;
	nSettingValue |= m_bEnableUserAccept.GetCheck()?EB_SETTING_AUTO_USER_ACCEPT:0;
	nSettingValue |= m_bEnableVisitorCall.GetCheck()?EB_SETTING_ENABLE_VISITOR_CALL:0;
	nSettingValue |= m_bEnableVisitorAccept.GetCheck()?EB_SETTING_AUTO_VISITOR_ACCEPT:0;
	nSettingValue |= m_bEnableOffCall.GetCheck()?EB_SETTING_ENABLE_OFF_CALL:0;
	nSettingValue |= m_bEnableOffFile.GetCheck()?EB_SETTING_ENABLE_OFF_FILE:0;
	nSettingValue |= m_bConnectedOpenChat.GetCheck()?EB_SETTING_CONNECTED_OPEN_CHAT:0;
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_MyAccountSetting = nSettingValue;
#else
	theEBAppClient.EB_SetMyAccountSetting(nSettingValue);
#endif
}

