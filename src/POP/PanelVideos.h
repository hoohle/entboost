#pragma once
//#include "videoroom.h"
#include "afxwin.h"
#include "Core/DlgVoiceBar.h"
#include "DlgVideo.h"

// CPanelVideos dialog
#define MAX_VIDEO_COUNT 2
#define FFT_LEN  1024

class CPanelVideos : public CEbDialogBase
#ifdef USES_EBCOM_TEST
	, public CEBVideoDataEventsSink
	//, public CEBVRoomEventsSink
#endif
{
	DECLARE_DYNAMIC(CPanelVideos)

public:
	CPanelVideos(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPanelVideos();

	EB_CallInfo m_pCallInfo;

	void OnMove(void);
	int GetVideoCount(void) const;
	void ExitChat(bool bHangup);

// Dialog Data
	enum { IDD = IDD_PAN_VIDEOS };

#ifdef USES_EBCOM_TEST
	// CEBVRoomEventsSink
	//virtual void Fire_onVideoStream(ULONG nUserId, BYTE* pVideoData, ULONG nVideoSize, ULONG nUserData);
	virtual void Fire_onVideoData(unsigned long nUserVideoId, const BYTE* pData, DWORD nSize, DWORD dwParam);
	virtual void Fire_onAudioData(const BYTE* pData, DWORD nSize, DWORD dwParam);

	void VRequestResponse(IEB_VideoInfo* pVideoInfo,int nStateValue);
	void VAckResponse(IEB_VideoInfo* pVideoInfo,int nStateValue);
	void VideoRequest(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo);
	void VideoAccept(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo);
	void VideoCancel(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo);
	void VideoEnd(IEB_VideoInfo* pVideoInfo,IEB_UserVideoInfo* pUserVideoInfo);
#else
	void VRequestResponse(const EB_VideoInfo* pVideoInfo,int nStateValue);
	void VAckResponse(const EB_VideoInfo* pVideoInfo,int nStateValue);
	void VideoRequest(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo);
	void VideoAccept(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo);
	void VideoCancel(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo);
	void VideoEnd(const EB_VideoInfo* pVideoInfo, const EB_UserVideoInfo* pUserVideoInfo);
#endif

	//static void ProcessVideoStream(DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD udata);
	//static void FFTdataProcess(char *pdata, int length, void* udata);
	virtual void DrawVideoData(unsigned int nuserid, void * pdata, int len);
protected:
	std::vector<std::string> m_pVideoDevices;
	int m_nLocalUserId;
	CDlgVideo * m_pUserVideo[MAX_VIDEO_COUNT];
	int m_nVideoUserId[MAX_VIDEO_COUNT];
	CRect m_rectUser[MAX_VIDEO_COUNT];

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CTraButton m_btnVideoEnd;
	CTraButton m_btnVideoAccept;
	CTraButton m_btnVideoCancel;

	bool IsMyOnVideo(void) const;
	int GetVideoIndex(int nVideoUserId) const;
	int GetNullVideoIndex(void) const;
	void OpenUserVideo(int nVideoUserId, bool bLocalUser);
	void DoVideoDisonnecte(void);
	void DoVideoConnected(void);

	// video
//#ifndef USES_EBCOM1
//	LRESULT OnMessageVRequestResponse(WPARAM wParam, LPARAM lParam);
//	LRESULT OnMessageVAckResponse(WPARAM wParam, LPARAM lParam);
//	LRESULT OnMessageVideoRequest(WPARAM wParam, LPARAM lParam);
//	LRESULT OnMessageVideoAccept(WPARAM wParam, LPARAM lParam);
//	LRESULT OnMessageVideoCancel(WPARAM wParam, LPARAM lParam);
//	LRESULT OnMessageVideoEnd(WPARAM wParam, LPARAM lParam);
//#endif
	LRESULT OnMsgBuildUserVideo(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonVideoEnd();
	afx_msg void OnBnClickedButtonVideoAccept();
	afx_msg void OnBnClickedButtonVideoCancel();
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
