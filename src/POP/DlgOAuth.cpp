// DlgOAuth.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgOAuth.h"

#define OAUTH_WINDOW_TEXT _T("__entboost_oauth_window__")

// CDlgOAuth dialog

IMPLEMENT_DYNAMIC(CDlgOAuth, CDialog)

CDlgOAuth::CDlgOAuth(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgOAuth::IDD, pParent)
{

}

CDlgOAuth::~CDlgOAuth()
{
}

void CDlgOAuth::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgOAuth, CDialog)

#ifndef USES_EBCOM_TEST
	ON_MESSAGE(EB_WM_OAUTH_FORWARD, OnMessageOAuthForward)
	ON_MESSAGE(EB_WM_LOGON_SUCCESS, OnMessageLogonSuccess)
	ON_MESSAGE(EB_WM_LOGON_TIMEOUT, OnMessageLogonTimeout)
	ON_MESSAGE(EB_WM_LOGON_ERROR, OnMessageLogonError)
#endif
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDlgOAuth message handlers
void CDlgOAuth::RunEBC(void)
{
	char pFileName[MAX_PATH] = {0};
	GetModuleFileName(NULL, pFileName, MAX_PATH);
	ShellExecute(NULL, "open", pFileName, _T(""), theApp.GetAppPath(), SW_SHOW);
}

#ifdef USES_EBCOM_TEST
void CDlgOAuth::Fire_onOAuthForward(BSTR sOAuthUrl)
{
	// 打开恩布验证程序
	char pFileName[MAX_PATH] = {0};
	GetModuleFileName(NULL, pFileName, MAX_PATH);
	CString sEBLoginExe = theApp.GetAppPath()+_T("\\eblogin.exe");
	CString sParameter;
	sParameter.Format(_T("%d \"%s\" \"%s\" \"%s\""), (int)this->GetSafeHwnd(),CEBString(sOAuthUrl).c_str(),m_sAppParameter0.c_str(),pFileName);	// m_sAppParameter0=="logout"，是注销
	ShellExecute(NULL,  "open", sEBLoginExe, sParameter, theApp.GetAppPath(), SW_SHOW);
	// 这里用浏览器实现
	//ShellExecuteW(NULL, L"open", sOAuthUrl, NULL, NULL,SW_SHOW);
}
#else
LRESULT CDlgOAuth::OnMessageOAuthForward(WPARAM wParam, LPARAM lParam)
{
	const char* sOAuthUrl = (const char*)wParam;
	// 打开恩布验证程序
	char pFileName[MAX_PATH] = {0};
	GetModuleFileName(NULL, pFileName, MAX_PATH);
	CString sEBLoginExe = theApp.GetAppPath()+_T("\\eblogin.exe");
	CString sParameter;
	sParameter.Format(_T("%d \"%s\" \"%s\" \"%s\""), (int)this->GetSafeHwnd(),sOAuthUrl,m_sAppParameter0.c_str(),pFileName);	// m_sAppParameter0=="logout"，是注销
	ShellExecute(NULL,  "open", sEBLoginExe, sParameter, theApp.GetAppPath(), SW_SHOW);
	// 这里用浏览器实现
	//ShellExecute(NULL, "open", sOAuthUrl, NULL, NULL,SW_SHOW);
	return 0;
}
#endif

#ifdef USES_EBCOM_TEST
void CDlgOAuth::Fire_onLogonSuccess(IEB_AccountInfo* pAccountInfo)
{
	this->PostMessage(WM_CLOSE, 0, 0);
	//AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//CEbDialogBase::OnOK();
}
#else
LRESULT CDlgOAuth::OnMessageLogonSuccess(WPARAM wParam, LPARAM lParam)
{
	this->PostMessage(WM_CLOSE, 0, 0);
	//CDialog::OnOK();
	return 0;
}
#endif

#ifdef USES_EBCOM_TEST
void CDlgOAuth::Fire_onLogonTimeout(IEB_AccountInfo * pAccountInfo)
{
	//this->ShowWindow(SW_SHOW);
	CDlgMessageBox::EbDoModal(this,_T("登录超时！"),_T("请重新登录。"),CDlgMessageBox::IMAGE_ERROR);
	this->PostMessage(WM_CLOSE, 0, 0);
	RunEBC();
}
#else
LRESULT CDlgOAuth::OnMessageLogonTimeout(WPARAM wParam, LPARAM lParam)
{
	//this->ShowWindow(SW_SHOW);
	CDlgMessageBox::EbDoModal(this,_T("登录超时！"),_T("请重新登录。"),CDlgMessageBox::IMAGE_ERROR);
	this->PostMessage(WM_CLOSE, 0, 0);
	RunEBC();
	return 0;
}
#endif

#ifdef USES_EBCOM_TEST
void CDlgOAuth::Fire_onLogonError(IEB_AccountInfo * pAccountInfo, EB_STATE_CODE nState)
{
	//this->ShowWindow(SW_SHOW);
	if (nState == EB_STATE_UNAUTH_ERROR)
		CDlgMessageBox::EbDoModal(this,_T("该帐号邮箱未通过验证："),_T("请查收邮件，点击验证链接，完成注册！"),CDlgMessageBox::IMAGE_ERROR);
	else
		CDlgMessageBox::EbDoModal(this,_T("登录失败！"),_T("请重新登录。"),CDlgMessageBox::IMAGE_ERROR);
	this->PostMessage(WM_CLOSE, 0, 0);
	RunEBC();
}
#else
LRESULT CDlgOAuth::OnMessageLogonError(WPARAM wParam, LPARAM lParam)
{
	int nErrorCode = lParam;
	//this->ShowWindow(SW_SHOW);
	if (nErrorCode == EB_STATE_UNAUTH_ERROR)
		CDlgMessageBox::EbDoModal(this,_T("该帐号邮箱未通过验证："),_T("请查收邮件，点击验证链接，完成注册！"),CDlgMessageBox::IMAGE_ERROR);
	else
		CDlgMessageBox::EbDoModal(this,_T("登录失败！"),_T("请重新登录。"),CDlgMessageBox::IMAGE_ERROR);
	this->PostMessage(WM_CLOSE, 0, 0);
	RunEBC();
	return 0;
}
#endif

bool CDlgOAuth::StartClient(void)
{
	//AfxMessageBox(sAddress.c_str());
#ifdef USES_EBCOM_TEST
	const CEBString sAddress = theEBSetting->ServerAddress.GetBSTR();
	//const CEBString sAddress = theEBClientCore->GetAccountAddress(lpszAccount).GetBSTR();
	theEBClientCore->EB_Init(sAddress.c_str(),"");
	return theEBClientCore->EB_IsInited==VARIANT_TRUE?true:false;
#else
	const CEBString sAddress = theSetting.GetServerAddress();
	//std::string sAddress;
	//entboost::GetAccountAddress(lpszAccount, sAddress);
	//CDlgMessageBox::EbDoModal(this,_T("服务器地址！"),sAddress.c_str(),CDlgMessageBox::IMAGE_INFORMATION);
	//theEBAppClient.Stop();
	theEBAppClient.EB_Init(sAddress.c_str());
	theEBAppClient.EB_SetMsgHwnd(this->GetSafeHwnd());
	return theEBAppClient.EB_IsInited();
#endif
}

BOOL CDlgOAuth::OnInitDialog()
{
	CDialog::OnInitDialog();
	ModifyStyleEx(WS_EX_APPWINDOW,WS_EX_TOOLWINDOW,SWP_FRAMECHANGED);	// 隐藏工具栏
	// 隐藏窗口（无标题窗口有效）
	WINDOWPLACEMENT wp;
	wp.length=sizeof(WINDOWPLACEMENT);
	wp.flags=WPF_RESTORETOMAXIMIZED;
	wp.showCmd=SW_HIDE;
	SetWindowPlacement(&wp);

	if (!StartClient())
	{
		CDlgMessageBox::EbDoModal(this,_T("本地服务启动失败！"),_T("请检查本地网络后再试。"),CDlgMessageBox::IMAGE_ERROR);
		this->PostMessage(WM_CLOSE, 0, 0);
		return FALSE;
	}
	this->SetWindowText(OAUTH_WINDOW_TEXT);

	const eb::bigint theDevAppId = 278573612908;
	const CEBString theDevAppKey = "ec1b9c69094db40d9ada80d657e08cc6";
#ifdef USES_EBCOM_TEST
	this->DispEventAdvise(theEBClientCore);
	bool ret = theEBClientCore->EB_SetDevAppId(theDevAppId,theDevAppKey.c_str(),VARIANT_TRUE)==S_OK;
#else
	bool ret = theEBAppClient.EB_SetDevAppId(theDevAppId,theDevAppKey.c_str(),true)>=0;
#endif
	const CString theAutoRunAccount = theApp.EBC_GetProfileString(_T("system"),_T("auto-run-account"));
	if (!ret)
	{
		// 本地服务有问题，检查是否自动登录，
		// 自动登录，继续等待
		// 手工登录，提示错误
		if (theAutoRunAccount.IsEmpty())
		{
			CDlgMessageBox::EbDoModal(this,_T("连接服务器失败！"),_T("请重新登录；如果出现同样错误提示，请检查本地网络后再试。"),CDlgMessageBox::IMAGE_ERROR);
			this->PostMessage(WM_CLOSE, 0, 0);
			return FALSE;
		}else
		{
			// 自动登录，一直等待
			while (true)
			{
#ifdef USES_EBCOM_TEST
				bool ret = theEBClientCore->EB_SetDevAppId(theDevAppId,theDevAppKey.c_str(),VARIANT_TRUE)==S_OK;
#else
				bool ret = theEBAppClient.EB_SetDevAppId(theDevAppId,theDevAppKey.c_str(),true)>=0;
#endif
				if (ret)
					break;
				Sleep(3000);
			}
		}
	}

	for (int i=0; i<30; i++)
	{
#ifdef USES_EBCOM_TEST
		const CEBString sAppOnlineKey = theEBClientCore->EB_GetDevAppOnlineKey().GetBSTR();
#else
		const CEBString sAppOnlineKey = theEBAppClient.EB_GetDevAppOnlineKey();
#endif
		if (!sAppOnlineKey.empty())
		{
			break;
		}
		Sleep(500);
		if (i+1==30)
		{
			if (theAutoRunAccount.IsEmpty())
			{
				// 网络有问题，提示失败。
				CDlgMessageBox::EbDoModal(this,_T("服务器连接失败！！"),_T("请重新登录；如果出现同样错误提示，请检查本地网络后再试。"),CDlgMessageBox::IMAGE_ERROR);
				this->PostMessage(WM_CLOSE, 0, 0);
				return FALSE;
			}else
			{
				// 自动登录，重设，继续等待；
				i=0;
			}
		}
	}

#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_LogonOAuth("",EB_LINE_STATE_ONLINE);
#else
	theEBAppClient.EB_LogonOAuth("");
#endif
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgOAuth::OnDestroy()
{
#ifdef USES_EBCOM_TEST
	this->DispEventUnadvise(theEBClientCore);
#else
	theEBAppClient.EB_SetMsgHwnd(NULL);
#endif

	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}
