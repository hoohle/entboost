// DlgVideo.cpp : implementation file
//

#include "stdafx.h"
#include "DlgVideo.h"
#include "Core/Fourier.h"


// CDlgVideo dialog

IMPLEMENT_DYNAMIC(CDlgVideo, CDialog)

CDlgVideo::CDlgVideo(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgVideo::IDD, pParent)
{
#ifndef USES_EBCOM_TEST
	m_pUserVideo = NULL;
#endif
	m_nVideoUserId = 0;
	m_nSampleLength = 0;
	m_nVoicePos = 0;
	m_pVoiceBar = NULL;

}

CDlgVideo::~CDlgVideo()
{
}

void CDlgVideo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgVideo, CDialog)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_MOVE()
END_MESSAGE_MAP()


// CDlgVideo message handlers

BOOL CDlgVideo::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_pVoiceBar = new CDlgVoiceBar(this);
	m_pVoiceBar->Create(CDlgVoiceBar::IDD, this);
	OnMove();

	SetTimer( 50 , 50 , NULL );
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgVideo::OnDestroy()
{
#ifdef USES_EBCOM_TEST
	m_pUserVideo.DestroyWindow();
	m_pUserVideoInterface.Release();
#else
	if (m_pUserVideo)
	{
		m_pUserVideo->VW_StopGraph();
		m_pUserVideo->VW_ReleaseGraph();
		delete m_pUserVideo;
		m_pUserVideo = NULL;
	}
#endif
	if (m_pVoiceBar)
	{
		m_pVoiceBar->DestroyWindow();
		delete m_pVoiceBar;
		m_pVoiceBar = NULL;
	}

	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CDlgVideo::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CDialog::OnCancel();
}

void CDlgVideo::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CDialog::OnOK();
}

void CDlgVideo::BuildVideoWindow(SHORT videoType)
{
#ifdef USES_EBCOM_TEST
	if (m_pUserVideo.GetSafeHwnd() == NULL)
	{
		m_pUserVideo.CreateControl(__uuidof(EBVWindow),NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,10,10),this,1111);
		LPUNKNOWN pUnknown = m_pUserVideo.GetControlUnknown();
		if (pUnknown == NULL)
			return;
		pUnknown->QueryInterface(__uuidof(IEBVWindow),(void**)&m_pUserVideoInterface);
		if (pUnknown==NULL)
			return;
		m_pUserVideoInterface->VW_BuildVideoWindow(videoType, const_video_width, const_video_height);
		m_pUserVideoInterface->VW_BuildGraph(VARIANT_FALSE);
		m_pUserVideoInterface->VW_StartGraph();
	}
#else
	if (m_pUserVideo == NULL)
	{
		m_pUserVideo = new Cvideowindow(this->GetSafeHwnd(),(VIDEO_RGB_TYPE)videoType, const_video_width, const_video_height);
		m_pUserVideo->VW_BuildGraph();
		m_pUserVideo->VW_StartGraph();
	}
#endif
	//OnMove();
}

void CDlgVideo::DrawVideo(void * pdata, int len)
{
#ifdef USES_EBCOM_TEST
	if (m_pUserVideoInterface != NULL)
	{
		m_pUserVideoInterface->VW_DrawVideo((unsigned char*)pdata,(unsigned long)len);
	}
#else
	if (m_pUserVideo != NULL)
	{
		m_pUserVideo->VW_DrawVideo(pdata, len);
	}
#endif
}
bool CDlgVideo::Photo(const char* lspzSaveTo)
{
#ifdef USES_EBCOM_TEST
	if (m_pUserVideoInterface != NULL)
	{
		return SUCCEEDED(m_pUserVideoInterface->VW_SaveBitmapFile(lspzSaveTo));
	}
#else
	if (m_pUserVideo!=NULL)
		return m_pUserVideo->VW_SaveBitmapFile(lspzSaveTo);
#endif
	return false;
}

void CDlgVideo::OnMove(void)
{
#ifdef USES_EBCOM_TEST
	if (m_pUserVideo.GetSafeHwnd())
	{
		CRect rect;
		this->GetWindowRect(&rect);
		this->ScreenToClient(&rect);
		m_pUserVideo.MoveWindow(rect.left,rect.top,rect.Width(),rect.Height());
	}
#else
	if (this->GetSafeHwnd()!=NULL && m_pUserVideo!=NULL)
	{
		CRect rect;
		this->GetWindowRect(&rect);
		this->ScreenToClient(&rect);
		m_pUserVideo->VW_MoveWindow(rect.left,rect.top,rect.right,rect.bottom);
		//CRect rectVideo;
		//rectVideo.left = 0;
		//rectVideo.right = rectVideo.left+rect.Width();
		//rectVideo.top = 0;
		//rectVideo.bottom = rectVideo.top+rect.Height();
		//m_pUserVideo->MoveWindow(rectVideo.left,rectVideo.top,rectVideo.right,rectVideo.bottom);
	}
#endif

	if (m_pVoiceBar != NULL && m_pVoiceBar->GetSafeHwnd())
	{
		CRect rectWindow;
		//this->ClientToScreen(&rectWindow);
		this->GetWindowRect(&rectWindow);

		const long const_VoiceBar_Width = 50;
		const long const_VoiceBar_Height = 40;
		CRect rect;
		rect.left = rectWindow.left + (5+1);
		rect.right = rect.left + const_VoiceBar_Width;
		rect.bottom = rectWindow.bottom - (5+1);
		rect.top = rect.bottom - const_VoiceBar_Height;
		m_pVoiceBar->MoveWindow(&rect);
	}
}

void CDlgVideo::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	OnMove();
}

void CDlgVideo::FFTdataProcess(char *pdata, int length, void* udata)
{
	CDlgVideo *pDlg = reinterpret_cast<CDlgVideo *>(udata);
	if(pDlg) {
		pDlg->AddFFTSampleData( pdata, length);
	}
}
void CDlgVideo::AddFFTSampleData(const char* pdata, int length)
{
	if (m_nSampleLength >= FFT_LEN)
		return;
	int lenToWrite = (m_nSampleLength +length) > FFT_LEN ? (FFT_LEN - m_nSampleLength) : length;
	memcpy(m_SampleArrry, pdata, lenToWrite);
	m_nSampleLength +=lenToWrite;

	if (m_nSampleLength == FFT_LEN) {
		ProcessFFTData();
		m_nSampleLength = 0;
	}
}
inline double GetFrequencyIntensity(double re, double im)
{
	return sqrt((re*re)+(im*im));
}
void CDlgVideo::ProcessFFTData(void)
{
	//CHerbDlg *pMainDlg = static_cast<CHerbDlg *>(theHerbDlg);
	//CClientMediaManager *pMediaMgr = pMainDlg->GetMediaManager();
	//if (pMediaMgr && this->m_bLocalVideo && pMediaMgr->m_bSendAudio == FALSE)
	//{
	//	// �����Ѿ�������Ƶ
	//	return;
	//}

	int i;
	int limit = FFT_LEN/2;
	for(i =0; i< limit; ++i) 
	{
		finleft[i] = (double)(m_SampleArrry[i]);
	}
	//Perform FFT on left channel
	fft_double(FFT_LEN/2,0,finleft,NULL,fout,foutimg);
	float re,im,fmax=-99999.9f,fmin=99999.9f;
	for(i=1;i < FFT_LEN/4;i++)//Use FFT_LEN/4 since the data is mirrored within the array.
	{
		re = (float)fout[i];
		im = (float)foutimg[i];
		//get amplitude and scale to 0..256 range
		double temp = GetFrequencyIntensity( re , im );
		temp = temp/(FFT_LEN/2);
		fdraw[i]=(int)temp%200;
		if (fdraw[i] > fmax)
		{
			fmax = (float)fdraw[i];
		}
		if (fdraw[i] < fmin)
		{
			fmin = (float)fdraw[i];
		}
		if( fdraw[i] != 0 )
		{
			m_nVoicePos = (int)fdraw[i];
			break;
		}
	}
}

void CDlgVideo::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (50 == nIDEvent)
	{
		if (m_pVoiceBar != NULL && m_pVoiceBar->GetSafeHwnd())
		{
			if (m_nVoicePos > 0 && !m_pVoiceBar->IsWindowVisible())
			{
				m_pVoiceBar->ShowWindow(SW_SHOW);
			}
			if (!m_pVoiceBar->SetVoiceValue(m_nVoicePos))
			{
				m_nVoicePos = 0;
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CDlgVideo::OnMove(int x, int y)
{
	CDialog::OnMove(x, y);
	this->OnMove();
}
