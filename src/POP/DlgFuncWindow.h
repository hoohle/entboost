#pragma once
#include "Core/Explorer.h"
#include "BroadcastView.h"

// CDlgFuncWindow dialog

class CDlgFuncWindow : public CEbDialogBase
	, public CBroadcastViewCallback
{
	DECLARE_DYNAMIC(CDlgFuncWindow)

public:
	CDlgFuncWindow(CWnd* pParent = NULL,bool bDeleteThis=false);   // standard constructor
	virtual ~CDlgFuncWindow();

	bool m_bBroadcastMsg;
	std::string m_sTitle;
	std::string m_sFuncUrl;
	size_t m_nWidth;
	size_t m_nHeight;

// Dialog Data
	enum { IDD = IDD_DLG_FUNC_WINDOW };

protected:
	bool m_bDeleteThis;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CTraButton m_btnMin;
	CTraButton m_btnMax;
	CTraButton m_btnClose;
	CExplorer m_pExplorer;
	CBroadcastView* m_pBroadcastView;

	virtual void OnBeforeNavigate(BOOL* bCancel,LPCTSTR szURL);
	DECLARE_EVENTSINK_MAP()
	afx_msg void OnBeforeNavigate2IeLobby(LPDISPATCH pDisp, VARIANT FAR* URL, VARIANT FAR* Flags, VARIANT FAR* TargetFrameName, VARIANT FAR* PostData, VARIANT FAR* Headers, BOOL FAR* Cancel);

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonMin();
	afx_msg void OnBnClickedButtonMax();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
