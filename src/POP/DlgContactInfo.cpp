// DlgContactInfo.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgContactInfo.h"

#define TIMERID_LOAD_CONTACT_LIST 200

// CDlgContactInfo dialog

IMPLEMENT_DYNAMIC(CDlgContactInfo, CEbDialogBase)

CDlgContactInfo::CDlgContactInfo(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgContactInfo::IDD, pParent)
	, m_sContact(_T(""))
	, m_sGroup(_T(""))
	, m_sName(_T(""))
	, m_sPhone(0)
	, m_sCompany(_T(""))
	, m_sJobTitle(_T(""))
	, m_sUrl(_T(""))
	, m_sTel(_T(""))
	, m_sFax(_T(""))
	, m_sEmail(_T(""))
	, m_sAddress(_T(""))
	, m_sDescription(_T(""))
{

}

CDlgContactInfo::~CDlgContactInfo()
{
}

void CDlgContactInfo::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//DDX_Text(pDX, IDC_EDIT_CONTACT_ACCOUNT, m_sContact);
	DDX_CBString(pDX, IDC_COMBO_CONTACT, m_sContact);
	DDX_Control(pDX, IDC_COMBO_CONTACT, m_comboContact);
	DDX_CBString(pDX, IDC_COMBO_GROUP, m_sGroup);
	DDX_Control(pDX, IDC_COMBO_GROUP, m_comboGroup);
	DDX_Text(pDX, IDC_EDIT_CONTACT_NAME, m_sName);
	DDX_Text(pDX, IDC_EDIT_CONTACT_PHONE, m_sPhone);
	DDX_Text(pDX, IDC_EDIT_COMPANY, m_sCompany);
	DDX_Text(pDX, IDC_EDIT_JOB_TITLE2, m_sJobTitle);
	DDX_Text(pDX, IDC_EDIT_URL, m_sUrl);
	DDX_Text(pDX, IDC_EDIT_TEL, m_sTel);
	DDX_Text(pDX, IDC_EDIT_FAX2, m_sFax);
	DDX_Text(pDX, IDC_EDIT_CONTACT_EMAIL, m_sEmail);
	DDX_Text(pDX, IDC_EDIT_CONTACT_ADDRESS, m_sAddress);
	DDX_Text(pDX, IDC_EDIT_CONTACT_DESCRIPTION, m_sDescription);
	DDX_Control(pDX, IDC_EDIT_CONTACT_NAME, m_editName);
	DDX_Control(pDX, IDC_EDIT_CONTACT_PHONE, m_editPhone);
	DDX_Control(pDX, IDC_EDIT_COMPANY, m_editCompany);
	DDX_Control(pDX, IDC_EDIT_JOB_TITLE2, m_editJobTitle);
	DDX_Control(pDX, IDC_EDIT_URL, m_editUrl);
	DDX_Control(pDX, IDC_EDIT_TEL, m_editTel);
	DDX_Control(pDX, IDC_EDIT_FAX2, m_editFax);
	DDX_Control(pDX, IDC_EDIT_CONTACT_EMAIL, m_editEmail);
	DDX_Control(pDX, IDC_EDIT_CONTACT_ADDRESS, m_editAddress);
	DDX_Control(pDX, IDC_EDIT_CONTACT_DESCRIPTION, m_editDescription);
}


BEGIN_MESSAGE_MAP(CDlgContactInfo, CEbDialogBase)
	ON_BN_CLICKED(IDOK, &CDlgContactInfo::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO_CONTACT, &CDlgContactInfo::OnCbnSelchangeComboContact)
	ON_CBN_EDITCHANGE(IDC_COMBO_CONTACT, &CDlgContactInfo::OnCbnEditchangeComboContact)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDlgContactInfo::OnBnClickedButtonClose)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgContactInfo message handlers

BOOL CDlgContactInfo::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	this->SetTransparentType(CEbDialogBase::TT_STATIC);

	m_btnClose.SetAutoSize(false);
	m_btnClose.Load(IDB_PNG_CLOSE);
	m_btnClose.SetToolTipText(_T("关闭"));
	m_btnOk.SetAutoSize(false);
	m_btnOk.Load(IDB_PNG_81X32A);
	m_btnOk.SetWindowText(_T("保存"));
	m_btnCancel.SetAutoSize(false);
	m_btnCancel.Load(IDB_PNG_81X32B);
	m_btnCancel.SetWindowText(_T("关闭"));

	m_comboContact.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);
	m_comboGroup.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);

	SetTimer(TIMERID_LOAD_CONTACT_LIST,10,NULL);

	const int POS_DLG_WIDTH = 612;
	const int POS_DLG_HEIGHT = 410;
	CRect rectClient;
	this->GetWindowRect(&rectClient);
	rectClient.right = rectClient.left + POS_DLG_WIDTH;
	rectClient.bottom = rectClient.top + POS_DLG_HEIGHT;
	this->MoveWindow(&rectClient);
	SetCircle();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgContactInfo::OnBnClickedOk()
{
	UpdateData();
	if (this->m_sContact.IsEmpty())
	{
		m_comboContact.SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入联系人帐号！"),_T(""),CDlgMessageBox::IMAGE_WARNING);
		return;
	}
	if (m_sName.IsEmpty())
	{
		m_editName.SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入联系人姓名！"),_T(""),CDlgMessageBox::IMAGE_WARNING);
		return;
	}
	OnOK();
}

#ifdef USES_EBCOM_TEST
void CDlgContactInfo::Data2Ctrl(IEB_ContactInfo* pPopContactInfo)
{
	if (pPopContactInfo != NULL)
	{
		m_sContact = pPopContactInfo->Contact.GetBSTR();
		m_sGroup = pPopContactInfo->Group.GetBSTR();
		m_sName = pPopContactInfo->Name.GetBSTR();
		m_sPhone = pPopContactInfo->Phone;
		m_sEmail = pPopContactInfo->Email.GetBSTR();
		m_sAddress = pPopContactInfo->Address.GetBSTR();
		m_sDescription = pPopContactInfo->Description.GetBSTR();
		UpdateData(FALSE);
	}
}
#else
void CDlgContactInfo::Data2Ctrl(const EB_ContactInfo* pPopContactInfo)
{
	if (pPopContactInfo != NULL)
	{
		m_sContact = pPopContactInfo->m_sContact.c_str();
		m_sGroup = pPopContactInfo->m_sGroup.c_str();
		m_sName = pPopContactInfo->m_sName.c_str();
		m_sPhone = pPopContactInfo->m_sPhone;
		m_sEmail = pPopContactInfo->m_sEmail.c_str();
		m_sAddress = pPopContactInfo->m_sAddress.c_str();
		m_sDescription = pPopContactInfo->m_sDescription.c_str();
		UpdateData(FALSE);
	}
}
#endif

void CDlgContactInfo::OnCbnSelchangeComboContact()
{
	int nitem = m_comboContact.GetCurSel();
	if (nitem >= 0)
	{
		const std::string sContact = m_pContactList[nitem];
#ifdef USES_EBCOM_TEST
		CComPtr<IEB_ContactInfo> pEBContactInfo = theEBClientCore->EB_GetContactInfo(sContact.c_str());
		Data2Ctrl(pEBContactInfo);
#else
		EB_ContactInfo pPopContactInfo;
		if (theEBAppClient.EB_GetContactInfo(sContact.c_str(), &pPopContactInfo))
		{
			Data2Ctrl(&pPopContactInfo);
		}
#endif
	}
}

void CDlgContactInfo::OnCbnEditchangeComboContact()
{
	UpdateData();
	if (!m_sContact.IsEmpty())
	{
#ifdef USES_EBCOM_TEST
		CComPtr<IEB_ContactInfo> pEBContactInfo = theEBClientCore->EB_GetContactInfo((LPCTSTR)m_sContact);
		Data2Ctrl(pEBContactInfo);
#else
		EB_ContactInfo pPopContactInfo;
		if (theEBAppClient.EB_GetContactInfo(m_sContact,&pPopContactInfo))
		{
			Data2Ctrl(&pPopContactInfo);
		}
#endif
	}
}

void CDlgContactInfo::OnPaint()
{
	if (IsIconic())
	{
		//CPaintDC dc(this); // 用于绘制的设备上下文

		//SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		//// 使图标在工作矩形中居中
		//int cxIcon = GetSystemMetrics(SM_CXICON);
		//int cyIcon = GetSystemMetrics(SM_CYICON);
		//CRect rect;
		//GetClientRect(&rect);
		//int x = (rect.Width() - cxIcon + 1) / 2;
		//int y = (rect.Height() - cyIcon + 1) / 2;

		//// 绘制图标
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc2(this);
		CRect rectClient;
		this->GetClientRect(&rectClient);
		CSkinMemDC memDC(&dc2, rectClient);
		this->ClearBgDrawInfo();
		this->AddBgDrawInfo(CEbBackDrawInfo(42,0.7,true));
		this->AddBgDrawInfo(CEbBackDrawInfo(315,0.95,false));
		this->AddBgDrawInfo(CEbBackDrawInfo(0,0.85,false));
		this->DrawPopBg(&memDC, theApp.GetMainColor());
		Gdiplus::Graphics graphics(memDC.m_hDC);
		//Gdiplus::Image * pImage;
		//if (libEbc::ImageFromIDResource(IDB_PNG_IMG1_PEOPLE,_T("png"),pImage))
		//{
		//	graphics.DrawImage(pImage,PointF(10,7));
		//	delete pImage;
		//}
		graphics.DrawImage(theApp.m_image1People,PointF(10,7));
		// 写标题
		const FontFamily fontFamily(theFontFamily.c_str());
		const Gdiplus::Font fontEbTitle(&fontFamily, 13, FontStyleBold, UnitPixel);
		Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(38,38,38));
		const Gdiplus::PointF pointTitle(40,13);
		graphics.DrawString(L"联系人资料",-1,&fontEbTitle,pointTitle,&brushEbTitle);

		//this->DrawPopBg(&dc2, theApp.GetMainColor());
		//CRect rectClient;
		//this->GetClientRect(&rectClient);
		//HDC m_hdcMemory = dc2.m_hDC;
		//// 写标题
		//CFont pNewFontTitle;
		//pNewFontTitle.CreatePointFont(115, _T("宋体"));//创建显示文本的字体
		//HGDIOBJ pOldFond = SelectObject(m_hdcMemory, pNewFontTitle.m_hObject);
		//::SetBkMode(m_hdcMemory, TRANSPARENT);
		//SetTextColor(m_hdcMemory, RGB(0, 0, 0));	// 黑色
		//CString sOutText = _T("通讯录资料");
		//TextOut(m_hdcMemory, 15, 15, sOutText, sOutText.GetLength());
		//SelectObject(m_hdcMemory, pOldFond);

	}
}

void CDlgContactInfo::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);

	int btnx = 0;
	btnx = cx-m_btnClose.GetImgWidth()-2;
	m_btnClose.MovePoint(btnx, 2);

	const int const_btn_width = m_btnCancel.GetImgWidth();// 80;
	const int const_btn_height = m_btnCancel.GetImgHeight();// 28;
	const int const_btn_intever = 10;
	int x = cx-const_btn_width-const_btn_intever;	// 最右边间隔
	const int const_btn_y = cy-const_btn_height-10;	// 最下面间隔
	m_btnCancel.MovePoint(x, const_btn_y);
	x -= (const_btn_width+const_btn_intever);
	m_btnOk.MovePoint(x, const_btn_y);
}

void CDlgContactInfo::OnBnClickedButtonClose()
{
	this->OnCancel();
}

void CDlgContactInfo::OnTimer(UINT_PTR nIDEvent)
{
	if (TIMERID_LOAD_CONTACT_LIST==nIDEvent)
	{
		KillTimer(nIDEvent);
		if (!m_sContact.IsEmpty() && m_sName.IsEmpty())
		{
			m_editName.SetFocus();
		}

#ifdef USES_EBCOM_TEST
		_variant_t pContactList = theEBClientCore->EB_GetContactList();
		if (pContactList.vt!=VT_EMPTY && pContactList.parray != NULL)
		{
			CComSafeArray<VARIANT> m_sa(pContactList.parray);
			for (ULONG i=0;i<m_sa.GetCount();i++)
			{
				CComVariant var = m_sa.GetAt(i);
				if (var.vt != VT_DISPATCH)
					continue;
				CComPtr<IEB_ContactInfo> pContactInfo;
				var.pdispVal->QueryInterface(__uuidof(IEB_ContactInfo),(void**)&pContactInfo);
				if (pContactInfo == NULL) continue;

				const CEBString sName(pContactInfo->Name.GetBSTR());
				const CEBString sContact(pContactInfo->Contact.GetBSTR());
				const CEBString sGroup(pContactInfo->Group.GetBSTR());
				CString sAddStringText;
				sAddStringText.Format(_T("%s(%s)"), sName.c_str(), sContact.c_str());
				int nitem = m_comboContact.AddString(sContact.c_str());
				m_pContactList[nitem] = sContact;
				if (sContact == (LPCTSTR)m_sContact)
				{
					m_comboContact.SetCurSel(nitem);
				}
				if (!sGroup.empty())
				{
					if (m_comboGroup.FindString(0, sGroup.c_str()) < 0)
					{
						m_comboGroup.AddString(sGroup.c_str());
					}
				}
			}
		}
#else
		std::vector<EB_ContactInfo> pContactList;
		theEBAppClient.EB_GetContactList(pContactList);
		for (size_t i=0; i<pContactList.size(); i++)
		{
			const EB_ContactInfo& pPopContactInfo = pContactList[i];
			CString sAddStringText;
			sAddStringText.Format(_T("%s(%s)"), pPopContactInfo.m_sName.c_str(), pPopContactInfo.m_sContact.c_str());
			int nitem = m_comboContact.AddString(pPopContactInfo.m_sContact.c_str());
			m_pContactList[nitem] = pPopContactInfo.m_sContact;
			if (pPopContactInfo.m_sContact == (LPCTSTR)m_sContact)
			{
				m_comboContact.SetCurSel(nitem);
			}
			if (!pPopContactInfo.m_sGroup.empty())
			{
				if (m_comboGroup.FindString(0, pPopContactInfo.m_sGroup.c_str()) < 0)
				{
					m_comboGroup.AddString(pPopContactInfo.m_sGroup.c_str());
				}
			}
		}
#endif
	}

	CEbDialogBase::OnTimer(nIDEvent);
}
