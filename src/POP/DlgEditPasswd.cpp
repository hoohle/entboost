// DlgEditPasswd.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgEditPasswd.h"


// CDlgEditPasswd dialog

IMPLEMENT_DYNAMIC(CDlgEditPasswd, CEbDialogBase)

CDlgEditPasswd::CDlgEditPasswd(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgEditPasswd::IDD, pParent)
{

}

CDlgEditPasswd::~CDlgEditPasswd()
{
}

void CDlgEditPasswd::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_OLDPWD, m_editOldPwd);
	DDX_Control(pDX, IDC_EDIT_NEWPWD, m_editNewPwd);
	DDX_Control(pDX, IDC_EDIT_CONFIRM, m_editConfirm);
}


BEGIN_MESSAGE_MAP(CDlgEditPasswd, CEbDialogBase)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CDlgEditPasswd message handlers

void CDlgEditPasswd::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CDlgEditPasswd::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}

BOOL CDlgEditPasswd::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	this->SetTransparentType(CEbDialogBase::TT_STATIC);
	this->SetMouseMove(FALSE);

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEditPasswd::OnLButtonDown(UINT nFlags, CPoint point)
{
	this->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgEditPasswd::Save(void)
{
#ifdef USES_EBCOM_TEST
	const CEBString sCurrentUserName = theEBClientCore->EB_UserName.GetBSTR();
	const CEBString sCurrentPasswd = theEBClientCore->EB_Password.GetBSTR();
#else
	std::string sCurrentUserName = theEBAppClient.EB_GetUserName();
	std::string sCurrentPasswd = theEBAppClient.EB_GetPassword();
#endif
	CString sOldPwd;
	this->GetDlgItem(IDC_EDIT_OLDPWD)->GetWindowText(sOldPwd);
	if (sCurrentPasswd != (LPCTSTR)sOldPwd)
	{
		this->GetDlgItem(IDC_EDIT_OLDPWD)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入当前密码！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
	CString sNewPwd;
	CString sConfirm;
	this->GetDlgItem(IDC_EDIT_NEWPWD)->GetWindowText(sNewPwd);
	this->GetDlgItem(IDC_EDIT_CONFIRM)->GetWindowText(sConfirm);
	if (sNewPwd.IsEmpty())
	{
		this->GetDlgItem(IDC_EDIT_NEWPWD)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入新密码！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}else if (sNewPwd != sConfirm)
	{
		this->GetDlgItem(IDC_EDIT_NEWPWD)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("新密码不一致，请重新输入！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}else if (sOldPwd == sNewPwd)
	{
		this->GetDlgItem(IDC_EDIT_NEWPWD)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("新密码跟当前密码一样，请重新输入！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_Password = (LPCTSTR)sNewPwd;
#else
	theEBAppClient.EB_SetPassword(sNewPwd);
#endif
	if (theApp.m_pBoEB->use("eb"))
	{
		CString sSql;
		sSql.Format(_T("update user_login_record_t set password='%s' where account='%s' AND password='%s'"),
			sNewPwd,theApp.GetLogonAccount(),sCurrentPasswd.c_str());
		theApp.m_pBoEB->execsql(sSql);
		theApp.m_pBoEB->close();
	}
}

