#pragma once
#include "DlgVideo.h"

// CDlgVideoPhoto dialog

class CDlgVideoPhoto : public CEbDialogBase
#ifdef USES_EBCOM_TEST
	, public CEBVRoomEventsSink
#endif
{
	DECLARE_DYNAMIC(CDlgVideoPhoto)

public:
	CDlgVideoPhoto(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgVideoPhoto();

	tstring m_sSavePhotoFile;
// Dialog Data
	enum { IDD = IDD_DLG_VIDEO_PHOTO };

protected:
	CRect m_rectHead;
	CRect m_rectBig;
	Gdiplus::Image * m_imageHead;

	int m_nLocalId;
#ifdef USES_EBCOM_TEST
	CComPtr<IEBVRoom> m_pVideoRoom;
#else
	Cvideoroom * m_pVideoRoom;
#endif
	CDlgVideo * m_pMyVideo;

#ifdef USES_EBCOM_TEST
	virtual void Fire_onVideoStream(ULONG nUserId, BYTE* pVideoData, ULONG nVideoSize, ULONG nUserData);
#else
	static void ProcessVideoStream(DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD udata);
#endif
	void OpenLocalVideo(void);
	void CreateVideoRoom(void);
	void CloseVideo(void);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CTraButton m_btnClose;
	CTraButton m_btnOk;
	CTraButton m_btnCancel;
	CTraButton m_btnPhoto;

	void DrawInfo(void);

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonClose();
public:
	afx_msg void OnBnClickedButtonPhoto();
	afx_msg void OnMove(int x, int y);
};
