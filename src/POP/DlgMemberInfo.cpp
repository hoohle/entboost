// DlgMemberInfo.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgMemberInfo.h"

#define TIMERID_CHECK_RESOURCEFILE 100

// CDlgMemberInfo dialog

IMPLEMENT_DYNAMIC(CDlgMemberInfo, CEbDialogBase)

CDlgMemberInfo::CDlgMemberInfo(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgMemberInfo::IDD, pParent)
	, m_sGroupCode(0)
	, m_sMemberCode(0)
	, m_sMemberUserId(0)
	, m_sMemberAccount(_T(""))
	, m_sUserName(_T(""))
	, m_sJobTitle(_T(""))
	, m_nJobPosition(0)
	, m_sCellPhone(_T(""))
	, m_sWorkPhone(_T(""))
	, m_sEmail(_T(""))
	, m_sFax(_T(""))
	, m_sAddress(_T(""))
	, m_sDescription(_T(""))
	, m_bNewEemployee(false)
	, m_nGender(0)
	, m_nBirthday(0)
{
	m_imageHead = NULL;
	m_pDlgChangeHead = NULL;
	m_nOldFileSize = 0;
	m_bSetTimer = false;
}

CDlgMemberInfo::~CDlgMemberInfo()
{
}

void CDlgMemberInfo::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);

	DDX_Text(pDX, IDC_EDIT_ACCOUNT, m_sMemberAccount);
	DDX_Text(pDX, IDC_EDIT_USERNAME, m_sUserName);
	DDX_Text(pDX, IDC_EDIT_DEPNAME, m_sGroupName);
	DDX_Text(pDX, IDC_EDIT_JOB_TITLE, m_sJobTitle);
	DDX_Text(pDX, IDC_EDIT_JOB_POSITION, m_nJobPosition);
	DDX_Text(pDX, IDC_EDIT_CELL_PHONE, m_sCellPhone);
	DDX_Text(pDX, IDC_EDIT_WORK_PHONE, m_sWorkPhone);
	DDX_Text(pDX, IDC_EDIT_EMAIL, m_sEmail);
	DDX_Text(pDX, IDC_EDIT_FAX, m_sFax);
	DDX_Text(pDX, IDC_EDIT_ADDRESS, m_sAddress);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_sDescription);
	DDX_Control(pDX, IDC_COMBO_GENDER, m_comboGender);
	DDX_CBIndex(pDX, IDC_COMBO_GENDER, m_nGender);
	DDX_Control(pDX, IDC_COMBO_BIR_YEAR, m_comboBirYear);
	//DDX_CBString(pDX, IDC_COMBO_BIR_YEAR, m_nBirYear);
	DDX_Control(pDX, IDC_COMBO_BIR_MONTH, m_comboBirMonth);
	//DDX_CBString(pDX, IDC_COMBO_BIR_MONTH, m_nBirMonth);
	DDX_Control(pDX, IDC_COMBO_BIR_DAY, m_comboBirDay);
	//DDX_CBString(pDX, IDC_COMBO_BIR_DAY, m_nBirDay);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON_CHANGEHEAD, m_btnChangeHead);
	DDX_Control(pDX, IDC_BUTTON_DEFAULT_EMP, m_btnDefaultEmp);

	DDX_Control(pDX, IDC_EDIT_ACCOUNT, m_editMemberAccount);
	DDX_Control(pDX, IDC_EDIT_DEPNAME, m_editGroupName);
	DDX_Control(pDX, IDC_EDIT_USERNAME, m_editUserName);
	DDX_Control(pDX, IDC_EDIT_JOB_TITLE, m_editJobTitle);
	DDX_Control(pDX, IDC_EDIT_JOB_POSITION, m_editJobPosition);
	DDX_Control(pDX, IDC_EDIT_CELL_PHONE, m_editCellPhone);
	DDX_Control(pDX, IDC_EDIT_WORK_PHONE, m_editWorkPhone);
	DDX_Control(pDX, IDC_EDIT_EMAIL, m_editEmail);
	DDX_Control(pDX, IDC_EDIT_FAX, m_editFax);
	DDX_Control(pDX, IDC_EDIT_ADDRESS, m_editAddress);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
}


BEGIN_MESSAGE_MAP(CDlgMemberInfo, CEbDialogBase)
	ON_BN_CLICKED(IDOK, &CDlgMemberInfo::OnBnClickedOk)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDlgMemberInfo::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_DEFAULT_EMP, &CDlgMemberInfo::OnBnClickedButtonDefaultEmp)
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CHANGEHEAD, &CDlgMemberInfo::OnBnClickedButtonChangehead)
END_MESSAGE_MAP()


// CDlgMemberInfo message handlers

void CDlgMemberInfo::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	if (m_sMemberAccount.IsEmpty())
	{
		this->GetDlgItem(IDC_EDIT_ACCOUNT)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入登录帐号！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
	if (m_sUserName.IsEmpty())
	{
		this->GetDlgItem(IDC_EDIT_USERNAME)->SetFocus();
		CDlgMessageBox::EbDoModal(this,_T("请输入姓名！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
	if (m_bNewEemployee)
	{
#ifdef USES_EBCOM_TEST
		if (theEBClientCore->EB_IsExistMemberByAccount(m_sGroupCode, (LPCTSTR)m_sMemberAccount))
#else
		if (theEBAppClient.EB_IsExistMemberByAccount(m_sGroupCode, m_sMemberAccount))
#endif
		{
			this->GetDlgItem(IDC_EDIT_ACCOUNT)->SetFocus();
			CString sText;
			sText.Format(_T("[%s]，请重新输入！"), m_sMemberAccount);
			CDlgMessageBox::EbDoModal(this,_T("帐号已经存在："),sText,CDlgMessageBox::IMAGE_ERROR);
			return;
		}
	}
	// 计算生日整数
	CString stext;
	m_comboBirYear.GetWindowText(stext);
	int nBirYear = atoi(stext);
	m_comboBirMonth.GetWindowText(stext);
	int nBirMonth = atoi(stext);
	m_comboBirDay.GetWindowText(stext);
	int nBirDay = atoi(stext);
	m_nBirthday = nBirYear*10000+nBirMonth*100+nBirDay;

	OnOK();
}

BOOL CDlgMemberInfo::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	ModifyStyle(0, WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SYSMENU);
	SetTransparentType(CEbDialogBase::TT_STATIC);

	//CRect rect;
	//m_editMemberAccount.GetRect(&rect);
	//rect.top += 4;
	//m_editMemberAccount.SetRectNP(&rect);
	m_editMemberAccount.ModifyStyleEx(0,WS_EX_CLIENTEDGE);
	//this->GetDlgItem(IDC_EDIT_ACCOUNT)->ModifyStyleEx(0,WS_EX_CLIENTEDGE);

	m_btnClose.SetAutoSize(false);
	m_btnClose.Load(IDB_PNG_CLOSE);
	m_btnClose.SetToolTipText(_T("关闭"));
	m_btnOk.SetAutoSize(false);
	m_btnOk.Load(IDB_PNG_81X32A);
	m_btnOk.SetWindowText(_T("保存"));
	m_btnCancel.SetAutoSize(false);
	m_btnCancel.Load(IDB_PNG_81X32B);
	m_btnCancel.SetWindowText(_T("关闭"));
	m_btnChangeHead.SetToolTipText(_T("更换群（部门）显示头像"));
	m_btnChangeHead.SetNorTextColor(RGB(0,128,255));
	m_btnChangeHead.SetHotTextColor(RGB(0,128,255));
	m_btnDefaultEmp.SetToolTipText(_T("设置该名片为我的默认名片"));
	m_btnDefaultEmp.SetNorTextColor(RGB(0,128,255));
	m_btnDefaultEmp.SetHotTextColor(RGB(0,128,255));
	//m_btnDefaultEmp.Load(IDB_PNG_89X35);
	//m_btnDefaultEmp.Load(IDB_PNG_BTN);

	m_comboGender.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);
	m_comboGender.SetHighlightColor( RGB(115,138,174),RGB(255,255,255));
	m_comboGender.SetNormalPositionColor( RGB(255,255,255),RGB(0,0,0));

	m_comboBirYear.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);
	m_comboBirMonth.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);
	m_comboBirDay.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);

	this->GetDlgItem(IDC_EDIT_DEPNAME)->EnableWindow(FALSE);
	if (!m_sMemberAccount.IsEmpty())
	{
		m_bNewEemployee = false;
		this->GetDlgItem(IDC_EDIT_ACCOUNT)->EnableWindow(FALSE);
	}else
	{
		m_bNewEemployee = true;
		this->GetDlgItem(IDC_BUTTON_CHANGEHEAD)->EnableWindow(FALSE);
		this->GetDlgItem(IDC_BUTTON_DEFAULT_EMP)->EnableWindow(FALSE);
		this->GetDlgItem(IDC_EDIT_ACCOUNT)->EnableWindow(TRUE);
		m_editMemberAccount.SetReadOnly(FALSE);
		this->EnableToolTips();
		this->SetToolTipText(IDC_EDIT_ACCOUNT,_T("请输入邮箱地址，用于恩布登录帐号！"));
	}
	// 性别
	m_comboGender.AddString(_T(""));
	m_comboGender.AddString(_T("男性"));
	m_comboGender.AddString(_T("女性"));
	CString stext;
	// 年
	for (int year=1960; year<=2012; year++)
	{
		stext.Format(_T("%04d年"), year);
		m_comboBirYear.AddString(stext);
	}
	int nBirYear = m_nBirthday/10000;
	if (nBirYear > 0)
	{
		stext.Format(_T("%04d年"), nBirYear);
		m_comboBirYear.SetWindowText(stext);
	}
	// 月
	for (int month=1; month<=12; month++)
	{
		stext.Format(_T("%02d月"), month);
		m_comboBirMonth.AddString(stext);
	}
	int nBirMonth = (m_nBirthday%10000)/100;
	if (nBirMonth > 0)
	{
		stext.Format(_T("%02d月"), nBirMonth);
		m_comboBirMonth.SetWindowText(stext);
	}
	// 日
	for (int day=1; day<=31; day++)
	{
		stext.Format(_T("%02d日"), day);
		m_comboBirDay.AddString(stext);
	}
	int nBirDay = m_nBirthday%100;
	if (nBirDay > 0)
	{
		stext.Format(_T("%02d月"), nBirDay);
		m_comboBirDay.SetWindowText(stext);
	}

	const int POS_DLG_WIDTH = 612;
	const int POS_DLG_HEIGHT = 445;
	CRect rectClient;
	this->GetWindowRect(&rectClient);
	rectClient.right = rectClient.left + POS_DLG_WIDTH;
	rectClient.bottom = rectClient.top + POS_DLG_HEIGHT;
	this->MoveWindow(&rectClient);
	SetCircle();

	m_rectHead.left = 25;
	m_rectHead.right = m_rectHead.left + 80;
	m_rectHead.top = 55;
	m_rectHead.bottom = m_rectHead.top + 80;
	//m_sHeadResourceFile = theApp.GetAppPath()+_T("\\img\\myimg.png");
	//USES_CONVERSION;
	//if (!m_sHeadResourceFile.IsEmpty() && PathFileExists(m_sHeadResourceFile))
	//{
	//	m_imageHead = new Gdiplus::Image((const WCHAR*)T2W(m_sHeadResourceFile));   
	//}

	if (!m_bNewEemployee)
	{
		UpdateData(FALSE);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgMemberInfo::OnPaint()
{
	if (IsIconic())
	{
		//CPaintDC dc(this); // 用于绘制的设备上下文

		//SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		//// 使图标在工作区矩形中居中
		//int cxIcon = GetSystemMetrics(SM_CXICON);
		//int cyIcon = GetSystemMetrics(SM_CYICON);
		//CRect rect;
		//GetClientRect(&rect);
		//int x = (rect.Width() - cxIcon + 1) / 2;
		//int y = (rect.Height() - cyIcon + 1) / 2;

		//// 绘制图标
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//CEbDialogBase::OnPaint();
		DrawInfo();
	}
}

void CDlgMemberInfo::DrawInfo(void)
{
	CPaintDC dc2(this); // 用于绘制的设备上下文
	CRect rectClient;
	this->GetClientRect(&rectClient);
	CSkinMemDC memDC(&dc2, rectClient);
	this->ClearBgDrawInfo();
	this->AddBgDrawInfo(CEbBackDrawInfo(42,0.7,true));
	this->AddBgDrawInfo(CEbBackDrawInfo(350,0.95,false));
	this->AddBgDrawInfo(CEbBackDrawInfo(0,0.85,false));
	this->DrawPopBg(&memDC, theApp.GetMainColor());
	Gdiplus::Graphics graphics(memDC.m_hDC);
	//Gdiplus::Image * pImage;
	//if (libEbc::ImageFromIDResource(IDB_PNG_IMG1_PEOPLE,_T("png"),pImage))
	//{
	//	graphics.DrawImage(pImage,PointF(10,7));
	//	delete pImage;
	//}
	graphics.DrawImage(theApp.m_image1People,PointF(10,7));
	USES_CONVERSION;
	const FontFamily fontFamily(theFontFamily.c_str());

	// 写标题
	const Gdiplus::Font fontEbTitle(&fontFamily, 13, FontStyleBold, UnitPixel);
	Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(38,38,38));
	//graphics.DrawImage(theApp.m_imageEbIcon, Gdiplus::RectF(3,3,20,20));
	//const Gdiplus::PointF pointTitle(25,7);
	const Gdiplus::PointF pointTitle(40,13);
	graphics.DrawString(L"成员资料",-1,&fontEbTitle,pointTitle,&brushEbTitle);

	const tstring sHeadResourceFile = m_pMemberInfo.m_sHeadResourceFile;
	if (!sHeadResourceFile.empty() && PathFileExists(sHeadResourceFile.c_str()))
	{
		if (m_nOldFileSize==0)
		{
			m_nOldFileSize = libEbc::GetFileSize(sHeadResourceFile.c_str());
		}
		Gdiplus::Image imageHead((const WCHAR*)T2W(sHeadResourceFile.c_str()));   
		graphics.DrawImage(&imageHead, m_rectHead.left, m_rectHead.top, m_rectHead.Width(), m_rectHead.Height());
	}else
	{
		Image * pImage = theApp.m_imageDefaultMember->Clone();;
		graphics.DrawImage(pImage, m_rectHead.left, m_rectHead.top, m_rectHead.Width(), m_rectHead.Height());
		delete pImage;
	}
	if (m_bNewEemployee)
	{
		Gdiplus::SolidBrush brushTip(Gdiplus::Color(255,0,128));
		graphics.DrawString(L"添加成功，请接收验证邮件，验证恩布帐号！",-1,&fontEbTitle,PointF(310,63),&brushTip);
	}
	//if (m_imageHead!=NULL)
	//{
	//	graphics.DrawImage(m_imageHead, m_rectHead.left, m_rectHead.top, m_rectHead.Width(), m_rectHead.Height());
	//}

	//CPaintDC dc(this); // 用于绘制的设备上下文
	//this->DrawPopBg(&dc, theApp.GetMainColor());
	//CRect rectClient;
	//this->GetClientRect(&rectClient);
	//HDC m_hdcMemory = dc.m_hDC;

	//// 写标题
	//CFont pNewFontTitle;
	//pNewFontTitle.CreatePointFont(115, _T("宋体"));//创建显示文本的字体
	//HGDIOBJ pOldFond = SelectObject(m_hdcMemory, pNewFontTitle.m_hObject);
	//::SetBkMode(m_hdcMemory, TRANSPARENT);
	//SetTextColor(m_hdcMemory, RGB(0, 0, 0));	// 黑色
	//CString sOutText = _T("群名片");
	//TextOut(m_hdcMemory, 6, 5, sOutText, sOutText.GetLength());
	//SelectObject(m_hdcMemory, pOldFond);

}

void CDlgMemberInfo::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);

	int btnx = 0;
	btnx = cx-m_btnClose.GetImgWidth()-2;
	m_btnClose.MovePoint(btnx, 2);

	const int const_btn_width = m_btnCancel.GetImgWidth();// 60;
	const int const_btn_height = m_btnCancel.GetImgHeight();// 24;
	int x = cx-const_btn_width-10;					// 最右边间隔
	const int const_btn_y = cy-const_btn_height-10;	// 最下面间隔
	m_btnCancel.MovePoint(x, const_btn_y);
	x -= (const_btn_width+10);						// 按钮间隔
	m_btnOk.MovePoint(x, const_btn_y);
	x = 20;
	m_btnDefaultEmp.MovePoint(x, const_btn_y,const_btn_width,const_btn_height);
}

void CDlgMemberInfo::OnBnClickedButtonClose()
{
	PostMessage(WM_CLOSE, 0, 0);
}

void CDlgMemberInfo::OnBnClickedButtonDefaultEmp()
{
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_MyDefaultMemberCode = m_sMemberCode;
#else
	theEBAppClient.EB_SetMyDefaultMemberCode(m_sMemberCode);
#endif
}

void CDlgMemberInfo::OnDestroy()
{
	if (m_pDlgChangeHead)
	{
		m_pDlgChangeHead->DestroyWindow();
		delete m_pDlgChangeHead;
		m_pDlgChangeHead = NULL;
	}
	CEbDialogBase::OnDestroy();
	if (m_imageHead!=NULL)
	{
		delete m_imageHead;
		m_imageHead = NULL;
	}
}

void CDlgMemberInfo::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	//if (!m_pMemberInfo.m_sHeadResourceFile.empty())
	if (!m_bNewEemployee)
	{
		CPoint pos;
		GetCursorPos(&pos);
		ScreenToClient(&pos);
		if (m_rectHead.PtInRect(pos))
		{
			::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_HAND));
			return;
		}
	}
	CEbDialogBase::OnMouseMove(nFlags, point);
}

void CDlgMemberInfo::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (!m_bNewEemployee)
	{
		ReleaseCapture();
		CPoint pos;
		GetCursorPos(&pos);
		ScreenToClient(&pos);
		if (m_rectHead.PtInRect(pos))
		{
			OnBnClickedButtonChangehead();
		}
	}
	CEbDialogBase::OnLButtonUp(nFlags, point);
}

void CDlgMemberInfo::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (!m_bNewEemployee)
	{
		CPoint pos;
		GetCursorPos(&pos);
		ScreenToClient(&pos);
		if (m_rectHead.PtInRect(pos))
		{
			this->SetCapture();
		}
	}
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgMemberInfo::OnTimer(UINT_PTR nIDEvent)
{
	if (TIMERID_CHECK_RESOURCEFILE == nIDEvent)
	{
		//if (m_pMemberInfo.get()!=NULL)
		{
			const tstring sHeadResourceFile = m_pMemberInfo.m_sHeadResourceFile;
			if (!sHeadResourceFile.empty() && PathFileExists(sHeadResourceFile.c_str()))
			{
				unsigned int nFileSize = libEbc::GetFileSize(sHeadResourceFile.c_str());
				if (nFileSize>0 && nFileSize!=m_nOldFileSize)
				{
					m_nOldFileSize = nFileSize;
					this->Invalidate();
				}
			}
		}
	}
	CEbDialogBase::OnTimer(nIDEvent);
}

void CDlgMemberInfo::OnBnClickedButtonChangehead()
{
	//if (m_pMemberInfo.m_sHeadResourceFile..get()!=NULL)
	if (!m_bNewEemployee)
	{
		if (!m_bSetTimer)
		{
			m_bSetTimer = true;
			SetTimer(TIMERID_CHECK_RESOURCEFILE,6*1000,NULL);
		}
		if (m_pDlgChangeHead==NULL)
		{

			m_pDlgChangeHead = new CDlgChangeHead(this);
			m_pDlgChangeHead->m_sGroupCode = this->m_sGroupCode;
			m_pDlgChangeHead->m_sHeadResourceFile = m_pMemberInfo.m_sHeadResourceFile;
			m_pDlgChangeHead->Create(CDlgChangeHead::IDD,this);
			m_pDlgChangeHead->SetCallback(this);	// **必须放在这后面
		}
		m_pDlgChangeHead->ShowWindow(SW_SHOW);
	}
}

void CDlgMemberInfo::OnSelectedImageInfo(const CEBImageDrawInfo& pSelectedImageInfo)
{
#ifdef USES_EBCOM_TEST
	int ret = theEBClientCore->EB_SetMyGroupHeadRes(m_sGroupCode,pSelectedImageInfo.m_sResId);
#else
	int ret = theEBAppClient.EB_SetMyGroupHeadRes(m_sGroupCode,pSelectedImageInfo.m_sResId);
#endif
	if (ret == 0)
	{
		m_pDlgChangeHead->ShowWindow(SW_HIDE);
		m_pMemberInfo.m_sHeadResourceFile = pSelectedImageInfo.m_sResFile;
		m_nOldFileSize = libEbc::GetFileSize(m_pMemberInfo.m_sHeadResourceFile.c_str());
		this->Invalidate();
	}
}
