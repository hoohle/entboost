// DlgEditVideo.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgEditVideo.h"

//#import "..\build\ebvideoroomd.dll" 

// CDlgEditVideo dialog

IMPLEMENT_DYNAMIC(CDlgEditVideo, CEbDialogBase)

CDlgEditVideo::CDlgEditVideo(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgEditVideo::IDD, pParent)
{
	m_nLocalId = 0;
#ifndef USES_EBCOM_TEST
	m_pVideoRoom = NULL;
#endif
	m_pMyVideo = NULL;

}

CDlgEditVideo::~CDlgEditVideo()
{
}

void CDlgEditVideo::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_VIDEOS, m_comboVideos);
	DDX_Control(pDX, IDC_BUTTON_LOCAL_VIDEO, m_btnLocalVideo);
}


BEGIN_MESSAGE_MAP(CDlgEditVideo, CEbDialogBase)
	ON_WM_LBUTTONDOWN()
	ON_CBN_SELCHANGE(IDC_COMBO_VIDEOS, &CDlgEditVideo::OnCbnSelchangeComboVideos)
	ON_BN_CLICKED(IDC_BUTTON_LOCAL_VIDEO, &CDlgEditVideo::OnBnClickedButtonLocalVideo)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_MOVE()
END_MESSAGE_MAP()


// CDlgEditVideo message handlers

void CDlgEditVideo::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnCancel();
}

void CDlgEditVideo::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CEbDialogBase::OnOK();
}

BOOL CDlgEditVideo::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	//this->SetTransparentType(CEbDialogBase::TT_STATIC);
	this->SetMouseMove(FALSE);

	this->EnableToolTips();
	this->SetToolTipText(IDC_COMBO_VIDEOS,_T("选择视频设备，打开预览视频，保存默认视频通话设备。"));

	//m_btnLocalVideo.SetAutoSize(false);
	m_btnLocalVideo.Load(IDB_PNG_81X32B);
	m_btnLocalVideo.SetWindowText(_T("打开"));
	m_btnLocalVideo.SetToolTipText(_T("打开本地预览视频"));
	m_btnLocalVideo.SetTextTop(-1);

	m_comboVideos.SetComboBitmap(IDB_COMBO_LEFT, IDB_COMBO_RIGHT, IDB_COMBO_CENTER);
	m_comboVideos.SetHighlightColor( RGB(115,138,174),RGB(255,255,255));
	m_comboVideos.SetNormalPositionColor( RGB(255,255,255),RGB(0,0,0));

	const int nLocalVideoIndex = (int)theApp.GetProfileInt(_T("param"),_T("local-video-index"),0);
#ifdef USES_EBCOM_TEST
	_variant_t pVideoDeviceList = theEBClientCore->EB_GetVideoDeviceList();
	if (pVideoDeviceList.vt!=VT_EMPTY && pVideoDeviceList.parray != NULL)
	{
		CComSafeArray<VARIANT> m_sa(pVideoDeviceList.parray);
		for (ULONG i=0;i<m_sa.GetCount();i++)
		{
			CComVariant var = m_sa.GetAt(i);
			if (var.vt != VT_BSTR)
				continue;
			const CEBString sVideoDevice(var.bstrVal);
			m_comboVideos.AddString(sVideoDevice.c_str());
		}
	}
#else
	std::vector<std::string> pVideoDevices;
	CEBAppClient::EB_GetVideoDeviceList(pVideoDevices);
	for (size_t i=0; i<pVideoDevices.size(); i++)
	{
		const std::string sVideoDevice = pVideoDevices[i];
		m_comboVideos.AddString(sVideoDevice.c_str());
	}
#endif

	if (m_comboVideos.GetCount()>nLocalVideoIndex)
		m_comboVideos.SetCurSel(nLocalVideoIndex);
	else if (m_comboVideos.GetCount()>0)
		m_comboVideos.SetCurSel(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEditVideo::OnLButtonDown(UINT nFlags, CPoint point)
{
	this->GetParent()->PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));	
	CEbDialogBase::OnLButtonDown(nFlags, point);
}

void CDlgEditVideo::OnCbnSelchangeComboVideos()
{
	const int nLocalVideoIndex = m_comboVideos.GetCurSel();
	theApp.WriteProfileInt(_T("param"),_T("local-video-index"),nLocalVideoIndex);
}

#ifdef USES_EBCOM_TEST
void CDlgEditVideo::Fire_onVideoStream(ULONG nUserId, BYTE* pVideoData, ULONG nVideoSize, ULONG nUserData)
{
	if (nUserData == 0) return;
	((CDlgVideo*)nUserData)->DrawVideo(pVideoData, nVideoSize);
}
void CDlgEditVideo::Fire_onAudioStream(ULONG nUserId, BYTE* pAudioData, ULONG nAudioSize, ULONG nUserData)
{
	if (nUserData == 0) return;
	((CDlgVideo*)nUserData)->AddFFTSampleData((const char*)pAudioData, nAudioSize);
}

#else
void CDlgEditVideo::ProcessVideoStream(DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD udata)
{
	if (udata == 0) return;
	((CDlgVideo*)udata)->DrawVideo(pFrame->data, pFrame->usedlen);
}
#endif

void CDlgEditVideo::OnMove(void)
{
	if (m_pMyVideo!=NULL && m_pMyVideo->GetSafeHwnd())
	{
		m_pMyVideo->OnMove();
	}

}

void CDlgEditVideo::CloseVideo(void)
{
#ifdef USES_EBCOM_TEST
	DispEventUnadvise(m_pVideoRoom);
#endif
	if (m_pVideoRoom != NULL)
	{
		// 关闭本地视频
#ifdef USES_EBCOM_TEST
		m_pVideoRoom->VR_StartVideoCapture = VARIANT_FALSE;
		m_pVideoRoom->VR_StartAudioCapture = VARIANT_FALSE;
#else
		m_pVideoRoom->VR_StopVideoCapture();
		m_pVideoRoom->VR_StopAudioCapture();
#endif
		m_pVideoRoom->VR_DelVideoStream(m_nLocalId);
		m_pVideoRoom->VR_DelAudioStream(m_nLocalId);
		m_pVideoRoom->VR_UnInit();
#ifdef USES_EBCOM_TEST
		m_pVideoRoom.Release();
#else
		delete m_pVideoRoom;
		m_pVideoRoom = NULL;
#endif
	}
	if (m_pMyVideo != NULL)
	{
		m_pMyVideo->DestroyWindow();
		delete m_pMyVideo;
		m_pMyVideo = NULL;
	}
	m_btnLocalVideo.SetWindowText(_T("打开"));
	m_btnLocalVideo.SetToolTipText(_T("打开本地视频"));
	m_comboVideos.EnableWindow(TRUE);
}

void CDlgEditVideo::CreateVideoRoom(void)
{
#ifdef USES_EBCOM_TEST
	if (m_pVideoRoom == NULL)
	{
		CoCreateInstance(__uuidof(EBVRoom),NULL,CLSCTX_INPROC,__uuidof(IEBVRoom),(void**)&m_pVideoRoom);
		if (m_nLocalId==0)
			m_nLocalId = 181101345+rand()%100;
		m_pVideoRoom->VR_Init(m_nLocalId);
		m_pVideoRoom->VR_VideoSend = VARIANT_FALSE;
		m_pVideoRoom->VR_AudioSend = VARIANT_FALSE;

		// OK
		DispEventAdvise(m_pVideoRoom);
	}
#else
	if (m_pVideoRoom == NULL)
	{
		if (m_nLocalId==0)
			m_nLocalId = 181101345+rand()%100;
		m_pVideoRoom = new Cvideoroom();
		m_pVideoRoom->VR_Init(m_nLocalId, ProcessVideoStream);
		m_pVideoRoom->VR_SetVideoSend(FALSE);
		m_pVideoRoom->VR_SetAudioSend(FALSE);
	}
#endif
}

void CDlgEditVideo::OnBnClickedButtonLocalVideo()
{
	if (m_comboVideos.GetCount() == 0)
	{
		CDlgMessageBox::EbMessageBox(this,_T("本地找不到视频设备！"),_T(""),CDlgMessageBox::IMAGE_ERROR);
		return;
	}
	CreateVideoRoom();
	if (m_pMyVideo == NULL)
	{
		// 打开本地视频
		int nvideoindex = m_comboVideos.GetCurSel();
		m_pVideoRoom->VR_InitVideoCapture(nvideoindex,10);
		m_pVideoRoom->VR_InitAudioCapture();

		CRect m_rectMy;
		m_comboVideos.GetWindowRect(&m_rectMy);
		this->ScreenToClient(&m_rectMy);
		m_rectMy.right = m_rectMy.left+const_video_width;
		m_rectMy.top += 28;
		m_rectMy.bottom = m_rectMy.top + const_video_height;

#ifdef USES_EBCOM_TEST
		USHORT videoType = m_pVideoRoom->VR_VideoType;
#else
		USHORT videoType = m_pVideoRoom->VR_GetVideoType();
#endif
		m_pMyVideo = new CDlgVideo(this);
		m_pMyVideo->Create(CDlgVideo::IDD,this);
		m_pMyVideo->BuildVideoWindow(videoType);
		m_pMyVideo->MoveWindow(&m_rectMy);
		m_pMyVideo->ShowWindow(SW_SHOW);
		m_pVideoRoom->VR_AddVideoStream(m_nLocalId, (DWORD)m_pMyVideo, true);
#ifdef USES_EBCOM_TEST
		m_pVideoRoom->VR_SetLocalAudiolStream(m_nLocalId, (ULONG)m_pMyVideo);
		m_pVideoRoom->VR_StartVideoCapture = VARIANT_TRUE;
		m_pVideoRoom->VR_StartAudioCapture = VARIANT_TRUE;
#else
		m_pVideoRoom->VR_SetLocalAudiolStream(m_nLocalId, CDlgVideo::FFTdataProcess,(void*)m_pMyVideo);
		m_pVideoRoom->VR_StartVideoCapture();
		m_pVideoRoom->VR_StartAudioCapture();
#endif
		m_btnLocalVideo.SetWindowText(_T("关闭"));
		m_btnLocalVideo.SetToolTipText(_T("关闭本地视频"));
		m_comboVideos.EnableWindow(FALSE);
	}else
	{
		// 关闭本地视频
		CloseVideo();
	}
	theApp.InvalidateParentRect(this);
}

void CDlgEditVideo::OnDestroy()
{
	CloseVideo();
	CEbDialogBase::OnDestroy();
}

void CDlgEditVideo::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);

	//m_btnLocalVideo.MovePoint(220, 5;
}

void CDlgEditVideo::OnTimer(UINT_PTR nIDEvent)
{
	CEbDialogBase::OnTimer(nIDEvent);
}

void CDlgEditVideo::OnMove(int x, int y)
{
	CEbDialogBase::OnMove(x, y);
}
