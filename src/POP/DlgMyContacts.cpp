// DlgMyContacts.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgMyContacts.h"
#include "DlgViewContactInfo.h"

const std::string const_default_group_name = "未分组联系人";
//#define TIMERID_DELETE_ITEM 100
#define TIMERID_EDIT_ITEM 101

// CDlgMyContacts dialog

IMPLEMENT_DYNAMIC(CDlgMyContacts, CDialog)

CDlgMyContacts::CDlgMyContacts(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMyContacts::IDD, pParent)
{
	m_pViewContactInfo = NULL;
}

CDlgMyContacts::~CDlgMyContacts()
{
}

void CDlgMyContacts::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_CONTACTS, m_treeContacts);
}


BEGIN_MESSAGE_MAP(CDlgMyContacts, CDialog)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_COMMAND(EB_MSG_VIEW_MSG_RECORD, &CDlgMyContacts::OnViewMsgRecord)
	ON_COMMAND(EB_COMMAND_DELETE_MSG_RECORD, &CDlgMyContacts::OnDeleteMsgRecord)
	ON_COMMAND(EB_COMMAND_NEW_CONTACT, &CDlgMyContacts::OnContactNew)
	ON_NOTIFY(NM_RCLICK, IDC_TREE_CONTACTS, &CDlgMyContacts::OnNMRClickTreeContacts)
	ON_COMMAND(EB_COMMAND_EDIT_CONTACT, &CDlgMyContacts::OnContactEdit)
	ON_COMMAND(EB_COMMAND_DELETE_CONTACT, &CDlgMyContacts::OnContactDelete)
	ON_COMMAND(EB_COMMAND_CALL_USER, &CDlgMyContacts::OnCallUser)
	ON_MESSAGE(WM_ITEM_DOUBLE_CLICK, OnTreeItemDblclk)
	ON_MESSAGE(WM_ITEM_TRACK_HOT, OnTreeItemTrackHot)
	ON_WM_TIMER()
	ON_COMMAND_RANGE(EB_COMMAND_SUBSCRIBE_FUNC,EB_COMMAND_SUBSCRIBE_FUNC+0x20,OnSubscribeFunc)
END_MESSAGE_MAP()


// CDlgMyContacts message handlers

BOOL CDlgMyContacts::OnInitDialog()
{
	CDialog::OnInitDialog();
    CMenuXP::SetXPLookNFeel (this);
    CMenuXP::UpdateMenuBar (this);

	//m_btnDeleteTrack.Create(_T(""),WS_CHILD|WS_VISIBLE, CRect(0,0,1,1), &m_treeContacts, 0xffff);
	//m_btnDeleteTrack.SetAutoSize(false);
	//m_btnDeleteTrack.SetAutoFocus(true);
	//m_btnDeleteTrack.Load(IDB_PNG_HOT_DELETE);
	//m_btnDeleteTrack.SetToolTipText(_T("删除"));
	m_btnEditTrack.Create(_T(""),WS_CHILD|WS_VISIBLE, CRect(0,0,1,1), &m_treeContacts, 0xffff);
	m_btnEditTrack.SetAutoSize(false);
	m_btnEditTrack.SetAutoFocus(true);
	m_btnEditTrack.Load(IDB_PNG_HOT_EDIT);
	m_btnEditTrack.SetToolTipText(_T("修改"));
	m_btnCallTrack.Create(_T(""),WS_CHILD|WS_VISIBLE, CRect(0,0,1,1), &m_treeContacts, 0xffff);
	m_btnCallTrack.SetAutoSize(false);
	m_btnCallTrack.SetAutoFocus(true);
	m_btnCallTrack.Load(IDB_PNG_HOT_CALL);
	m_btnCallTrack.SetToolTipText(_T("打开会话"));
	m_treeContacts.SetCallback((CTreeCallback*)&theApp);
	m_treeContacts.ModifyStyle(0, TVS_SINGLEEXPAND);
	m_treeContacts.SetTreeOpenClosedBmp(IDB_TREE_OPENED, IDB_TREE_CLOSED);
	m_treeContacts.SetItemHeight(40);
	m_treeContacts.SetIconSize(32,32);
	//m_treeContacts.SetItemIcon( theApp.GetIconCon() );
	//m_icon = AfxGetApp()->LoadIcon(IDI_DEPARTMENT);
	//m_treeContacts.SetRootIcon( m_icon );
	//m_icon = AfxGetApp()->LoadIcon(IDI_DEPARTMENT);
	//m_treeContacts.SetParentIcon( m_icon );

	HTREEITEM pGroupTreeItem = m_treeContacts.InsertItem(const_default_group_name.c_str());
	CTreeItemInfo::pointer pContactInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_GROUP,pGroupTreeItem);
	pContactInfo->m_sName = const_default_group_name;
	m_treeContacts.SetItemData(pGroupTreeItem,(DWORD)pContactInfo.get());
	m_pGroupItemInfo.insert(const_default_group_name, pContactInfo);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgMyContacts::OnDestroy()
{
	CDialog::OnDestroy();
	//m_btnDeleteTrack.DestroyWindow();
	m_btnEditTrack.DestroyWindow();
	m_btnCallTrack.DestroyWindow();
	if (m_pViewContactInfo)
	{
		m_pViewContactInfo->DestroyWindow();
		delete m_pViewContactInfo;
		m_pViewContactInfo = NULL;
	}
}

void CDlgMyContacts::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if (m_treeContacts.GetSafeHwnd())
		m_treeContacts.MoveWindow(0, 0, cx, cy);
}

void CDlgMyContacts::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	//CDialog::OnCancel();
}

void CDlgMyContacts::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	//CDialog::OnOK();
}

#ifdef USES_EBCOM_TEST
void CDlgMyContacts::ContactInfo(IEB_ContactInfo* pPopContactInfo)
{
	const CEBString sGroup = pPopContactInfo->Group.GetBSTR();
	const CEBString sName = pPopContactInfo->Name.GetBSTR();
	const CEBString sContact = pPopContactInfo->Contact.GetBSTR();
	const CEBString sGroupName = sGroup.empty()?const_default_group_name:sGroup;
	CString sGroupText;
	CTreeItemInfo::pointer pGroupItemInfo;
	if (!m_pGroupItemInfo.find(sGroupName,pGroupItemInfo))
	{
		sGroupText.Format(_T("%s(0)"), sGroupName.c_str());
		HTREEITEM pGroupTreeItem = m_treeContacts.InsertItem(sGroupText);
		pGroupItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_GROUP,pGroupTreeItem);
		pGroupItemInfo->m_sGroupName = sGroup;
		pGroupItemInfo->m_sName = sGroupName;
		m_pGroupItemInfo.insert(sGroupName,pGroupItemInfo);
		m_treeContacts.SetItemData(pGroupTreeItem, (DWORD)pGroupItemInfo.get());
		m_treeContacts.SortChildren(TVI_ROOT);
	}
	CString sContactText;
	sContactText.Format(_T("%s(%s)"), sName.c_str(), sContact.c_str());
	CTreeItemInfo::pointer pContactItemInfo;
	bool bChangeGroupCount = false;
	if (!m_pContactItemInfo.find(sContact, pContactItemInfo))
	{
		pGroupItemInfo->m_dwItemData++;
		bChangeGroupCount = true;
		HTREEITEM hContactItem = m_treeContacts.InsertItem(sContactText, pGroupItemInfo->m_hItem);
		pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
		pContactItemInfo->m_sGroupName = pGroupItemInfo->m_sGroupName;
		pContactItemInfo->m_sName = sName;
		pContactItemInfo->m_sAccount = sContact;
		m_pContactItemInfo.insert(sContact,pContactItemInfo);
		m_treeContacts.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());
	}else if (pContactItemInfo->m_sGroupName != sGroup)
	//}else if (pContactItemInfo->m_sGroupCode != sGroupName)
	{
		// 换group
		HTREEITEM hParentItem = m_treeContacts.GetParentItem(pContactItemInfo->m_hItem);
		CTreeItemInfo* pOldGroupItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hParentItem);
		if (pOldGroupItemInfo!=NULL && pOldGroupItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
		{
			if (pOldGroupItemInfo->m_dwItemData>1 || pOldGroupItemInfo->m_sGroupName.empty())	// empty是默认分组
			{
				pOldGroupItemInfo->m_dwItemData--;
				sGroupText.Format(_T("%s(%d)"), pOldGroupItemInfo->m_sName.c_str(),pOldGroupItemInfo->m_dwItemData);
				m_treeContacts.SetItemText(pOldGroupItemInfo->m_hItem,sGroupText);
			}else// if (pOldGroupItemInfo->m_dwItemData<=0 && !pOldGroupItemInfo->m_sGroupCode.empty())
			{
				// 无用分组，删除
				m_treeContacts.DeleteItem(pOldGroupItemInfo->m_hItem);
				m_pGroupItemInfo.remove(pOldGroupItemInfo->m_sGroupName);
			}
		}
		m_treeContacts.DeleteItem(pContactItemInfo->m_hItem);
		m_pContactItemInfo.remove(sContact);

		pGroupItemInfo->m_dwItemData++;
		bChangeGroupCount = true;
		HTREEITEM hContactItem = m_treeContacts.InsertItem(sContactText, pGroupItemInfo->m_hItem);
		pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
		pContactItemInfo->m_sGroupName = pGroupItemInfo->m_sGroupName;
		pContactItemInfo->m_sName = sName;
		pContactItemInfo->m_sAccount = sContact;
		m_pContactItemInfo.insert(sContact,pContactItemInfo);
		m_treeContacts.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());
	}else
	{
		m_treeContacts.SetItemText(pContactItemInfo->m_hItem, sContactText);
	}
	m_treeContacts.Expand(pGroupItemInfo->m_hItem, TVE_EXPAND);
	m_treeContacts.SelectItem(pContactItemInfo->m_hItem);
	if (bChangeGroupCount)
	{
		sGroupText.Format(_T("%s(%d)"), sGroupName.c_str(), pGroupItemInfo->m_dwItemData);
		m_treeContacts.SetItemText(pGroupItemInfo->m_hItem, sGroupText);
	}
	if (pGroupItemInfo->m_dwItemData>1)
		m_treeContacts.SortChildren(pGroupItemInfo->m_hItem);
}
#else
void CDlgMyContacts::ContactInfo(const EB_ContactInfo* pPopContactInfo)
{
	const std::string sGroupName = pPopContactInfo->m_sGroup.empty()?const_default_group_name:pPopContactInfo->m_sGroup;
	CString sGroupText;
	CTreeItemInfo::pointer pGroupItemInfo;
	if (!m_pGroupItemInfo.find(sGroupName,pGroupItemInfo))
	{
		sGroupText.Format(_T("%s(0)"), sGroupName.c_str());
		HTREEITEM pGroupTreeItem = m_treeContacts.InsertItem(sGroupText);
		pGroupItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_GROUP,pGroupTreeItem);
		pGroupItemInfo->m_sGroupName = pPopContactInfo->m_sGroup;
		pGroupItemInfo->m_sName = sGroupName;
		m_pGroupItemInfo.insert(sGroupName,pGroupItemInfo);
		m_treeContacts.SetItemData(pGroupTreeItem, (DWORD)pGroupItemInfo.get());
		m_treeContacts.SortChildren(TVI_ROOT);
	}
	CString sContactText;
	sContactText.Format(_T("%s - %s"), pPopContactInfo->m_sName.c_str(), pPopContactInfo->m_sContact.c_str());
	CTreeItemInfo::pointer pContactItemInfo;
	bool bChangeGroupCount = false;
	if (!m_pContactItemInfo.find(pPopContactInfo->m_sContact, pContactItemInfo))
	{
		pGroupItemInfo->m_dwItemData++;
		bChangeGroupCount = true;
		HTREEITEM hContactItem = m_treeContacts.InsertItem(sContactText, pGroupItemInfo->m_hItem);
		pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
		pContactItemInfo->m_sGroupName = pGroupItemInfo->m_sGroupName;
		pContactItemInfo->m_sName = pPopContactInfo->m_sName;
		pContactItemInfo->m_sAccount = pPopContactInfo->m_sContact;
		m_pContactItemInfo.insert(pPopContactInfo->m_sContact,pContactItemInfo);
		m_treeContacts.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());
	}else if (pContactItemInfo->m_sGroupName != pPopContactInfo->m_sGroup)
	//}else if (pContactItemInfo->m_sGroupCode != sGroupName)
	{
		// 换group
		HTREEITEM hParentItem = m_treeContacts.GetParentItem(pContactItemInfo->m_hItem);
		CTreeItemInfo* pOldGroupItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hParentItem);
		if (pOldGroupItemInfo!=NULL && pOldGroupItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
		{
			if (pOldGroupItemInfo->m_dwItemData>1 || pOldGroupItemInfo->m_sGroupCode==0)	// empty是默认分组
			{
				pOldGroupItemInfo->m_dwItemData--;
				sGroupText.Format(_T("%s(%d)"), pOldGroupItemInfo->m_sName.c_str(),pOldGroupItemInfo->m_dwItemData);
				m_treeContacts.SetItemText(pOldGroupItemInfo->m_hItem,sGroupText);
			}else// if (pOldGroupItemInfo->m_dwItemData<=0 && !pOldGroupItemInfo->m_sGroupCode.empty())
			{
				// 无用分组，删除
				m_treeContacts.DeleteItem(pOldGroupItemInfo->m_hItem);
				m_pGroupItemInfo.remove(pOldGroupItemInfo->m_sGroupName);
			}
		}
		m_treeContacts.DeleteItem(pContactItemInfo->m_hItem);
		m_pContactItemInfo.remove(pPopContactInfo->m_sContact);

		pGroupItemInfo->m_dwItemData++;
		bChangeGroupCount = true;
		HTREEITEM hContactItem = m_treeContacts.InsertItem(sContactText, pGroupItemInfo->m_hItem);
		pContactItemInfo = CTreeItemInfo::create(CTreeItemInfo::ITEM_TYPE_CONTACT,hContactItem);
		pContactItemInfo->m_sGroupName = pGroupItemInfo->m_sGroupName;
		pContactItemInfo->m_sName = pPopContactInfo->m_sName;
		pContactItemInfo->m_sAccount = pPopContactInfo->m_sContact;
		m_pContactItemInfo.insert(pPopContactInfo->m_sContact,pContactItemInfo);
		m_treeContacts.SetItemData(hContactItem,(DWORD)pContactItemInfo.get());
	}else
	{
		m_treeContacts.SetItemText(pContactItemInfo->m_hItem, sContactText);
	}
	m_treeContacts.Expand(pGroupItemInfo->m_hItem, TVE_EXPAND);
	m_treeContacts.SelectItem(pContactItemInfo->m_hItem);
	if (bChangeGroupCount)
	{
		sGroupText.Format(_T("%s(%d)"), sGroupName.c_str(), pGroupItemInfo->m_dwItemData);
		m_treeContacts.SetItemText(pGroupItemInfo->m_hItem, sGroupText);
	}
	if (pGroupItemInfo->m_dwItemData>1)
		m_treeContacts.SortChildren(pGroupItemInfo->m_hItem);
}
#endif

#ifdef USES_EBCOM_TEST
void CDlgMyContacts::DeleteContact(IEB_ContactInfo* pPopContactInfo)
{
	const CEBString sContact = pPopContactInfo->Contact.GetBSTR();
	CTreeItemInfo::pointer pContactItemInfo;
	if (m_pContactItemInfo.find(sContact, pContactItemInfo, true))
	{
		HTREEITEM hGroupItem = m_treeContacts.GetParentItem(pContactItemInfo->m_hItem);
		CTreeItemInfo* pGroupItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hGroupItem);
		if (pGroupItemInfo != NULL)
		{
			pGroupItemInfo->m_dwItemData--;
			const std::string sGroupName = pGroupItemInfo->m_sName;
			if (pGroupItemInfo->m_dwItemData == 0 && sGroupName != const_default_group_name)
			{
				// delete group item
				m_pGroupItemInfo.remove(sGroupName);
				m_treeContacts.DeleteItem(hGroupItem);
			}else
			{
				CString sGroupText;
				sGroupText.Format(_T("%s(%d)"),sGroupName.c_str(),pGroupItemInfo->m_dwItemData);
				m_treeContacts.SetItemText(hGroupItem, sGroupText);
			}
			m_treeContacts.Expand(hGroupItem, TVE_EXPAND);
		}
		m_treeContacts.DeleteItem(pContactItemInfo->m_hItem);
	}
}
#else
void CDlgMyContacts::DeleteContact(const EB_ContactInfo* pPopContactInfo)
{
	CTreeItemInfo::pointer pContactItemInfo;
	if (m_pContactItemInfo.find(pPopContactInfo->m_sContact, pContactItemInfo, true))
	{
		HTREEITEM hGroupItem = m_treeContacts.GetParentItem(pContactItemInfo->m_hItem);
		CTreeItemInfo* pGroupItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hGroupItem);
		if (pGroupItemInfo != NULL)
		{
			pGroupItemInfo->m_dwItemData--;
			std::string sGroupName = pGroupItemInfo->m_sName;
			if (pGroupItemInfo->m_dwItemData == 0 && sGroupName != const_default_group_name)
			{
				// delete group item
				m_pGroupItemInfo.remove(sGroupName);
				m_treeContacts.DeleteItem(hGroupItem);
			}else
			{
				CString sGroupText;
				sGroupText.Format(_T("%s(%d)"),sGroupName.c_str(),pGroupItemInfo->m_dwItemData);
				m_treeContacts.SetItemText(hGroupItem, sGroupText);
			}
			m_treeContacts.Expand(hGroupItem, TVE_EXPAND);
		}
		m_treeContacts.DeleteItem(pContactItemInfo->m_hItem);
	}
}
#endif

//CTreeItemInfo::pointer CDlgMyContacts::GetGroupItemInfo(HTREEITEM hItem) const
//{
//	boost::mutex::scoped_lock lock(const_cast<boost::mutex&>(m_pGroupItemInfo.mutex()));
//	CLockMap<std::string,CTreeItemInfo::pointer>::const_iterator pIter;
//	for (pIter=m_pGroupItemInfo.begin(); pIter!=m_pGroupItemInfo.end(); pIter++)
//	{
//		CTreeItemInfo::pointer pContactItemInfo = pIter->second;
//		if (pContactItemInfo->m_hItem == hItem)
//			return pContactItemInfo;
//	}
//	return NullTreeItemInfo;
//}
//CTreeItemInfo::pointer CDlgMyContacts::GetContactItemInfo(HTREEITEM hItem) const
//{
//	boost::mutex::scoped_lock lock(const_cast<boost::mutex&>(m_pContactItemInfo.mutex()));
//	CLockMap<std::string,CTreeItemInfo::pointer>::const_iterator pIter;
//	for (pIter=m_pContactItemInfo.begin(); pIter!=m_pContactItemInfo.end(); pIter++)
//	{
//		CTreeItemInfo::pointer pContactItemInfo = pIter->second;
//		if (pContactItemInfo->m_hItem == hItem)
//			return pContactItemInfo;
//	}
//	return NullTreeItemInfo;
//}

void CDlgMyContacts::DeleteItem(HTREEITEM hItem)
{
	if (hItem==NULL) return;
	CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
	CString sText;
	sText.Format(_T("[%s(%s)]吗？"), pContactItemInfo->m_sName.c_str(), pContactItemInfo->m_sAccount.c_str());
	if (CDlgMessageBox::EbDoModal(this,"你确定删除联系人：",sText,CDlgMessageBox::IMAGE_QUESTION)==IDOK)
	//if (MessageBox(sText, _T("删除好友"), MB_OKCANCEL|MB_ICONQUESTION) == IDOK)
	{
#ifdef USES_EBCOM_TEST
		theEBClientCore->EB_DeleteContact(pContactItemInfo->m_sAccount.c_str());
#else
		theEBAppClient.EB_DeleteContact(pContactItemInfo->m_sAccount.c_str());
#endif
	}
}

void CDlgMyContacts::EditItem(HTREEITEM hItem)
{
	if (hItem==NULL) return;
	CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
#ifdef USES_EBCOM_TEST
	CComPtr<IEB_ContactInfo> pContactInfo = theEBClientCore->EB_GetContactInfo(pContactItemInfo->m_sAccount.c_str());
	if (pContactInfo==NULL)
		return;
	CDlgContactInfo pDlgContactInfo(this);
	pDlgContactInfo.m_sContact = pContactInfo->Contact.GetBSTR();
	pDlgContactInfo.m_sGroup = pContactInfo->Group.GetBSTR();
	pDlgContactInfo.m_sName = pContactInfo->Name.GetBSTR();
	pDlgContactInfo.m_sPhone = pContactInfo->Phone;
	pDlgContactInfo.m_sCompany = pContactInfo->Company.GetBSTR();
	pDlgContactInfo.m_sJobTitle = pContactInfo->JobTitle.GetBSTR();
	pDlgContactInfo.m_sUrl = pContactInfo->Url.GetBSTR();
	pDlgContactInfo.m_sTel = pContactInfo->Tel.GetBSTR();
	pDlgContactInfo.m_sFax = pContactInfo->Fax.GetBSTR();
	pDlgContactInfo.m_sEmail = pContactInfo->Email.GetBSTR();
	pDlgContactInfo.m_sAddress = pContactInfo->Address.GetBSTR();
	pDlgContactInfo.m_sDescription = pContactInfo->Description.GetBSTR();
	if (pDlgContactInfo.DoModal() == IDOK)
	{
		pContactInfo->Contact = (LPCTSTR)pDlgContactInfo.m_sContact;
		pContactInfo->Group = (LPCTSTR)pDlgContactInfo.m_sGroup;
		pContactInfo->Name = (LPCTSTR)pDlgContactInfo.m_sName;
		pContactInfo->Phone = pDlgContactInfo.m_sPhone;
		pContactInfo->Company = (LPCTSTR)pDlgContactInfo.m_sCompany;
		pContactInfo->JobTitle = (LPCTSTR)pDlgContactInfo.m_sJobTitle;
		pContactInfo->Url = (LPCTSTR)pDlgContactInfo.m_sUrl;
		pContactInfo->Tel = (LPCTSTR)pDlgContactInfo.m_sTel;
		pContactInfo->Fax = (LPCTSTR)pDlgContactInfo.m_sFax;
		pContactInfo->Email = (LPCTSTR)pDlgContactInfo.m_sEmail;
		pContactInfo->Address = (LPCTSTR)pDlgContactInfo.m_sAddress;
		pContactInfo->Description = (LPCTSTR)pDlgContactInfo.m_sDescription;
		theEBClientCore->EB_EditContact(pContactInfo);
	}
#else
	EB_ContactInfo pContactInfo;
	if (!theEBAppClient.EB_GetContactInfo(pContactItemInfo->m_sAccount.c_str(),&pContactInfo))
		return;
	CDlgContactInfo pDlgContactInfo(this);
	pDlgContactInfo.m_sContact = pContactInfo.m_sContact.c_str();
	pDlgContactInfo.m_sGroup = pContactInfo.m_sGroup.c_str();
	pDlgContactInfo.m_sName = pContactInfo.m_sName.c_str();
	pDlgContactInfo.m_sPhone = pContactInfo.m_sPhone;
	pDlgContactInfo.m_sCompany = pContactInfo.m_sCompany.c_str();
	pDlgContactInfo.m_sJobTitle = pContactInfo.m_sJobTitle.c_str();
	pDlgContactInfo.m_sUrl = pContactInfo.m_sUrl.c_str();
	pDlgContactInfo.m_sTel = pContactInfo.m_sTel.c_str();
	pDlgContactInfo.m_sFax = pContactInfo.m_sFax.c_str();
	pDlgContactInfo.m_sEmail = pContactInfo.m_sEmail.c_str();
	pDlgContactInfo.m_sAddress = pContactInfo.m_sAddress.c_str();
	pDlgContactInfo.m_sDescription = pContactInfo.m_sDescription.c_str();
	if (pDlgContactInfo.DoModal() == IDOK)
	{
		pContactInfo.m_sContact = (LPCTSTR)pDlgContactInfo.m_sContact;
		pContactInfo.m_sGroup = (LPCTSTR)pDlgContactInfo.m_sGroup;
		pContactInfo.m_sName = (LPCTSTR)pDlgContactInfo.m_sName;
		pContactInfo.m_sPhone = pDlgContactInfo.m_sPhone;
		pContactInfo.m_sCompany = (LPCTSTR)pDlgContactInfo.m_sCompany;
		pContactInfo.m_sJobTitle = (LPCTSTR)pDlgContactInfo.m_sJobTitle;
		pContactInfo.m_sUrl = (LPCTSTR)pDlgContactInfo.m_sUrl;
		pContactInfo.m_sTel = (LPCTSTR)pDlgContactInfo.m_sTel;
		pContactInfo.m_sFax = (LPCTSTR)pDlgContactInfo.m_sFax;
		pContactInfo.m_sEmail = (LPCTSTR)pDlgContactInfo.m_sEmail;
		pContactInfo.m_sAddress = (LPCTSTR)pDlgContactInfo.m_sAddress;
		pContactInfo.m_sDescription = (LPCTSTR)pDlgContactInfo.m_sDescription;
		theEBAppClient.EB_EditContact(&pContactInfo);
	}
#endif
}

void CDlgMyContacts::CallItem(HTREEITEM hItem)
{
	const CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_CallAccount(pContactItemInfo->m_sAccount.c_str(),0);	// ??
#else
	theEBAppClient.EB_CallAccount(pContactItemInfo->m_sAccount.c_str(),0);	// ??
#endif
}

//bool CDlgMyContacts::GetItemImage(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem,Gdiplus::Image*& pImage1,Gdiplus::Image*& pImage2,int& pState) const
//{
//	const CTreeItemInfo * pTreeItemInfo = (const CTreeItemInfo*)pTreeCtrl.GetItemData(hItem);
//	if (pTreeItemInfo == NULL || pTreeItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return false;
//
//	CEBString sImagePath;
//	bool bIsMemberAccount = false;
//	EB_USER_LINE_STATE pOutLineState = EB_LINE_STATE_UNKNOWN;
//#ifdef USES_EBCOM_TEST
//	CComPtr<IEB_MemberInfo> pMemberInfo = theEBClientCore->EB_GetMemberInfoByAccount2(pTreeItemInfo->m_sAccount.c_str());
//	if (pMemberInfo != NULL)
//	{
//		bIsMemberAccount = true;
//		pOutLineState = pMemberInfo->LineState;
//		const CEBString sMemberHeadFile = pMemberInfo->HeadResourceFile.GetBSTR();
//		if (PathFileExists(sMemberHeadFile.c_str()))
//		{
//			sImagePath = sMemberHeadFile;
//		}
//	}
//#else
//	EB_MemberInfo pMemberInfo;
//	if (theEBAppClient.EB_GetMemberInfoByAccount2(&pMemberInfo,pTreeItemInfo->m_sAccount.c_str()))
//	{
//		bIsMemberAccount = true;
//		pOutLineState = pMemberInfo.m_nLineState;
//		if (PathFileExists(pMemberInfo.m_sHeadResourceFile.c_str()))
//		{
//			sImagePath = pMemberInfo.m_sHeadResourceFile;
//		}
//	}
//#endif
//	if (bIsMemberAccount)
//	{
//		switch (pOutLineState)
//		{
//		case EB_LINE_STATE_UNKNOWN:
//		case EB_LINE_STATE_OFFLINE:
//			pState = 0;
//			break;
//		case EB_LINE_STATE_ONLINE:
//			break;
//		case EB_LINE_STATE_BUSY:
//			{
//				pImage2 = theApp.m_imageStateBusy->Clone();
//			}break;
//		case EB_LINE_STATE_AWAY:
//			{
//				pImage2 = theApp.m_imageStateAway->Clone();
//			}break;
//		default:
//			break;
//		}
//		if (!sImagePath.empty())
//		{
//			USES_CONVERSION;
//			pImage1 = new Gdiplus::Image((const WCHAR*)T2W(sImagePath.c_str()));
//			return true;
//		}else
//		{
//			pImage1 = theApp.m_imageDefaultMember->Clone();
//		}
//	}else
//	{
//		pImage1 = theApp.m_imageDefaultContact->Clone();
//		return true;
//	}
//}

//int CDlgMyContacts::GetItemState(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem, HICON& pOutIcon) const
//{
//	// 全部显示为灰色
//	return 0;
//}
//bool CDlgMyContacts::GetItemDrawOpenClose(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem) const
//{
//	const CTreeItemInfo * pTreeItemInfo = (const CTreeItemInfo*)pTreeCtrl.GetItemData(hItem);
//	if (pTreeItemInfo != NULL && pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP) return true;
//	return false;
//}

void CDlgMyContacts::OnViewMsgRecord()
{
	const HTREEITEM hItem = m_treeContacts.GetTrackItem();
	const CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
	const eb::bigint sId = pContactItemInfo->m_nUserId;
	const tstring sName = pContactItemInfo->m_sName;

	CDlgMsgRecord * pDlgMsgRecord = NULL;
	if (!theApp.m_pMsgRecord.find(sId,pDlgMsgRecord))
	{
		CWnd * pParent = CWnd::FromHandle(::GetDesktopWindow());
		pDlgMsgRecord = new CDlgMsgRecord(pParent);
		pDlgMsgRecord->Create(CDlgMsgRecord::IDD,pParent);
		theApp.m_pMsgRecord.insert(sId,pDlgMsgRecord);
	}
	CRect rect;
	this->GetParent()->GetWindowRect(&rect);
	rect.left = rect.right;
	rect.right = rect.left + 320;
	pDlgMsgRecord->MoveWindow(&rect);
	pDlgMsgRecord->SetCircle();
	pDlgMsgRecord->LoadAccountMsgRecord(sId,sName);
}
void CDlgMyContacts::OnDeleteMsgRecord()
{
	const HTREEITEM hItem = m_treeContacts.GetTrackItem();
	const CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
	const eb::bigint sId = pContactItemInfo->m_nUserId;
	const tstring sName = pContactItemInfo->m_sName;

	CString sText;
	sText.Format(_T("[%s(%lld)]吗？"), sName.c_str(),sId);
	if (CDlgMessageBox::EbDoModal(this,"你确定清空聊天记录：",sText,CDlgMessageBox::IMAGE_QUESTION)==IDOK)
	//if (MessageBox(sText, _T("清空聊天记录"), MB_OKCANCEL|MB_ICONQUESTION) == IDOK)
	{
		theApp.DeleteDbRecord(sId,true);
	}
}

void CDlgMyContacts::OnContactNew()
{
	std::string sGroupName;
	const HTREEITEM hSelectItem = m_treeContacts.GetSelectedItem();
	if (hSelectItem != NULL && m_treeContacts.GetParentItem(hSelectItem) == NULL)
	{
		CTreeItemInfo* pGroupItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hSelectItem);
		if (pGroupItemInfo!=NULL)
			sGroupName = pGroupItemInfo->m_sName;
	}
	CDlgContactInfo pDlgContactInfo(this);
	if (sGroupName != const_default_group_name)
	{
		pDlgContactInfo.m_sGroup = sGroupName.c_str();
	}
	if (pDlgContactInfo.DoModal() == IDOK)
	{
		EB_ContactInfo pPopContactInfo((LPCTSTR)pDlgContactInfo.m_sContact);
		pPopContactInfo.m_sGroup = (LPCTSTR)pDlgContactInfo.m_sGroup;
		pPopContactInfo.m_sName = (LPCTSTR)pDlgContactInfo.m_sName;
		pPopContactInfo.m_sPhone = pDlgContactInfo.m_sPhone;
		pPopContactInfo.m_sCompany = (LPCTSTR)pDlgContactInfo.m_sCompany;
		pPopContactInfo.m_sJobTitle = (LPCTSTR)pDlgContactInfo.m_sJobTitle;
		pPopContactInfo.m_sUrl = (LPCTSTR)pDlgContactInfo.m_sUrl;
		pPopContactInfo.m_sTel = (LPCTSTR)pDlgContactInfo.m_sTel;
		pPopContactInfo.m_sFax = (LPCTSTR)pDlgContactInfo.m_sFax;
		pPopContactInfo.m_sEmail = (LPCTSTR)pDlgContactInfo.m_sEmail;
		pPopContactInfo.m_sAddress = (LPCTSTR)pDlgContactInfo.m_sAddress;
		pPopContactInfo.m_sDescription = (LPCTSTR)pDlgContactInfo.m_sDescription;
#ifdef USES_EBCOM_TEST
		CIEB_ContactInfo * pIEBContactInfo = new CIEB_ContactInfo(pPopContactInfo);
		CComPtr<IEB_ContactInfo> pInterface;
		pIEBContactInfo->QueryInterface(__uuidof(IEB_ContactInfo),(void**)&pInterface);
		theEBClientCore->EB_EditContact(pInterface);
#else
		theEBAppClient.EB_EditContact(&pPopContactInfo);
#endif
	}
}

void CDlgMyContacts::OnNMRClickTreeContacts(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
	EB_FUNC_LOCATION nFuncLocation = EB_FUNC_LOCATION_UNKNOWN;
	theApp.ClearSubscribeSelectInfo();
	if (!theApp.IsLogonVisitor())
	{
		CMenu m_menu2;
		m_menu2.CreatePopupMenu();
		HTREEITEM hSelItem = m_treeContacts.GetSelectedItem();
		const CTreeItemInfo * pTreeItemInfo = hSelItem==NULL?NULL:(const CTreeItemInfo*)m_treeContacts.GetItemData(hSelItem);
		if (pTreeItemInfo==NULL || pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_GROUP)
		{
			m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_NEW_CONTACT,_T("添加"));
		}else if (pTreeItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_CONTACT)
		{
			nFuncLocation = EB_FUNC_LOCATION_RIGHT_CLICK_MENU_USER;
			theApp.m_nSelectUserId = pTreeItemInfo->m_nUserId;
			theApp.m_sSelectAccount = pTreeItemInfo->m_sAccount;
			m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_CALL_USER,_T("打开会话"));
			// 判断聊天记录
			CString sSql;
			sSql.Format(_T("select msg_type FROM msg_record_t WHERE dep_code=0 AND (from_uid=%lld OR to_uid=%lld) "),
				pTreeItemInfo->m_nUserId,pTreeItemInfo->m_nUserId);
			int ret = theApp.m_pBoUsers->execsql(sSql);
			if (ret>0)
			{
				m_menu2.AppendMenu(MF_BYCOMMAND,EB_MSG_VIEW_MSG_RECORD,_T("查看聊天记录"));
				m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_DELETE_MSG_RECORD,_T("清空聊天记录"));
			}
			m_menu2.AppendMenu(MF_SEPARATOR);
			m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_NEW_CONTACT,_T("添加"));
			m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_EDIT_CONTACT,_T("编辑"));
			m_menu2.AppendMenu(MF_BYCOMMAND,EB_COMMAND_DELETE_CONTACT,_T("删除"));
		}

		// 应用功能菜单
		if (nFuncLocation != EB_FUNC_LOCATION_UNKNOWN)
		//if (theApp.m_nSelectUserId > 0 || !theApp.m_sSelectAccount.empty())
		{
			CMenu pPopupMenu;
			pPopupMenu.CreatePopupMenu();
#ifdef USES_EBCOM_TEST
			_variant_t pSubscribeFuncList = theEBClientCore->EB_GetSubscribeFuncList(nFuncLocation);
			if (pSubscribeFuncList.vt!=VT_EMPTY && pSubscribeFuncList.parray != NULL)
			{
				CComSafeArray<VARIANT> m_sa(pSubscribeFuncList.parray);
				for (ULONG i=0;i<m_sa.GetCount();i++)
				{
					CComVariant var = m_sa.GetAt(i);
					if (var.vt != VT_DISPATCH)
						continue;
					CComPtr<IEB_SubscribeFuncInfo> pSubscribeFuncInfo;
					var.pdispVal->QueryInterface(__uuidof(IEB_SubscribeFuncInfo),(void**)&pSubscribeFuncInfo);
					if (pSubscribeFuncInfo == NULL) continue;
					theApp.m_pSubscribeFuncList.push_back(EB_SubscribeFuncInfo(pSubscribeFuncInfo));
				}
			}
#else
			theEBAppClient.EB_GetSubscribeFuncList(nFuncLocation,theApp.m_pSubscribeFuncList);
#endif
			if (!theApp.m_pSubscribeFuncList.empty())
			{
				for (size_t i=0;i<theApp.m_pSubscribeFuncList.size();i++)
				{
					const EB_SubscribeFuncInfo & pSubscribeFuncInfo = theApp.m_pSubscribeFuncList[i];
					pPopupMenu.AppendMenu(MF_BYCOMMAND,EB_COMMAND_SUBSCRIBE_FUNC+i,pSubscribeFuncInfo.m_sFunctionName.c_str());
				}
				m_menu2.AppendMenu(MF_SEPARATOR);
				m_menu2.AppendMenu(MF_POPUP|MF_BYPOSITION,(UINT)pPopupMenu.m_hMenu,_T("应用功能"));
			}
		}

		CPoint point;
		GetCursorPos(&point);
		m_menu2.TrackPopupMenu(TPM_LEFTBUTTON|TPM_LEFTALIGN,point.x,point.y,this);
	}
}
void CDlgMyContacts::OnSubscribeFunc(UINT nID)
{
	size_t nIndex = nID-EB_COMMAND_SUBSCRIBE_FUNC;
	if (nIndex>=0 && nIndex<theApp.m_pSubscribeFuncList.size())
	{
		const EB_SubscribeFuncInfo& pSubscribeFuncInfo = theApp.m_pSubscribeFuncList[nIndex];
		theApp.OpenSubscribeFuncWindow(pSubscribeFuncInfo);
	}
}

void CDlgMyContacts::OnContactEdit()
{
	EditItem(m_treeContacts.GetSelectedItem());
}

void CDlgMyContacts::OnContactDelete()
{
	DeleteItem(m_treeContacts.GetSelectedItem());
}

void CDlgMyContacts::OnCallUser()
{
	const HTREEITEM hSelectItem = m_treeContacts.GetSelectedItem();
	if (hSelectItem==NULL) return;
	CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(hSelectItem);
	if (pContactItemInfo == NULL || pContactItemInfo->m_nItemType != CTreeItemInfo::ITEM_TYPE_CONTACT) return;
#ifdef USES_EBCOM_TEST
	theEBClientCore->EB_CallAccount(pContactItemInfo->m_sAccount.c_str(),0);	// ??
#else
	theEBAppClient.EB_CallAccount(pContactItemInfo->m_sAccount.c_str(),0);	// ??
#endif
}

LRESULT CDlgMyContacts::OnTreeItemDblclk(WPARAM wp, LPARAM lp)
{
	const HTREEITEM hDblClkItem = (HTREEITEM)wp;
	this->CallItem(hDblClkItem);
	return 0;
}
LRESULT CDlgMyContacts::OnTreeItemTrackHot(WPARAM wp, LPARAM lp)
{
	HTREEITEM item = (HTREEITEM)wp;
	if (item == NULL)
	{
		if (m_pViewContactInfo && m_pViewContactInfo->IsWindowVisible())
		{
			m_pViewContactInfo->HideReset();
		}
		//m_btnDeleteTrack.ShowWindow(SW_HIDE);
		m_btnEditTrack.ShowWindow(SW_HIDE);
		m_btnCallTrack.ShowWindow(SW_HIDE);
	}else if (m_btnCallTrack.GetSafeHwnd() != NULL)
	{
		CTreeItemInfo* pContactItemInfo = (CTreeItemInfo*)m_treeContacts.GetItemData(item);
		if (pContactItemInfo == NULL) return 1;
		if (pContactItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_CONTACT)
		{
#ifdef USES_EBCOM_TEST
			CComPtr<IEB_ContactInfo> pContactInfo = theEBClientCore->EB_GetContactInfo(pContactItemInfo->m_sAccount.c_str());
			if (pContactInfo==NULL)
				return 1;
#else
			EB_ContactInfo pContactInfo;
			if (!theEBAppClient.EB_GetContactInfo(pContactItemInfo->m_sAccount.c_str(),&pContactInfo))
				return 1;
#endif
			if (m_pViewContactInfo == NULL)
			{
				m_pViewContactInfo = new CDlgViewContactInfo(this);
				m_pViewContactInfo->Create(CDlgViewContactInfo::IDD,this);
			}
			const int const_dlg_width = 380;
			const int const_dlg_height = 180;
			CRect rect;
			this->GetWindowRect(&rect);
			CPoint pos;
			GetCursorPos(&pos);
			CRect rectViewContactInfo;
			rectViewContactInfo.top = pos.y-80;
			rectViewContactInfo.bottom = rectViewContactInfo.top+const_dlg_height;
			if (rect.left-const_dlg_width > 0)
			{
				rectViewContactInfo.right = rect.left-5;
				rectViewContactInfo.left = rectViewContactInfo.right-const_dlg_width;
			}else
			{
				rectViewContactInfo.left = rect.right+5;
				rectViewContactInfo.right = rectViewContactInfo.left+const_dlg_width;
			}
			m_pViewContactInfo->MoveWindow(&rectViewContactInfo);
			m_pViewContactInfo->SetCircle();
#ifdef USES_EBCOM_TEST
			m_pViewContactInfo->SetContactInfo(pContactInfo);
#else
			m_pViewContactInfo->SetContactInfo(&pContactInfo);
#endif
			m_pViewContactInfo->ShowWindow(SW_SHOW);
		}else if (m_pViewContactInfo!=NULL)
		{
			m_pViewContactInfo->ShowWindow(SW_HIDE);
		}
		if (pContactItemInfo->m_nItemType==CTreeItemInfo::ITEM_TYPE_CONTACT)
		{
			CRect rect;
			m_treeContacts.GetItemRect(item, &rect, TRUE);
			rect.right = m_treeContacts.GetHSize();
			const int const_btn_width = m_btnCallTrack.GetImgWidth();
			//const int const_btn_height = 24;
			//m_btnDeleteTrack.MovePoint(rect.right-const_btn_width, rect.top);
			//m_btnDeleteTrack.ShowWindow(SW_SHOW);
			//m_btnDeleteTrack.Invalidate();
			m_btnEditTrack.MovePoint(rect.right-const_btn_width, rect.top);
			m_btnEditTrack.ShowWindow(SW_SHOW);
			m_btnEditTrack.Invalidate();
			m_btnCallTrack.MovePoint(rect.right-const_btn_width*2, rect.top);
			m_btnCallTrack.ShowWindow(SW_SHOW);
			m_btnCallTrack.Invalidate();
		}else
		{
			m_btnCallTrack.ShowWindow(SW_HIDE);
			//m_btnDeleteTrack.ShowWindow(SW_HIDE);
			m_btnEditTrack.ShowWindow(SW_HIDE);
		}
	}
	return 0;
}

BOOL CDlgMyContacts::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	//if (pMsg->message == WM_LBUTTONUP && pMsg->hwnd == m_btnDeleteTrack.GetSafeHwnd())
	//{
	//	SetTimer(TIMERID_DELETE_ITEM,1,NULL);
	//}else
	if (pMsg->message == WM_LBUTTONUP && pMsg->hwnd == m_btnEditTrack.GetSafeHwnd())
	{
		SetTimer(TIMERID_EDIT_ITEM,1,NULL);
	}else if (pMsg->message == WM_LBUTTONUP && pMsg->hwnd == m_btnCallTrack.GetSafeHwnd())
	{
		CallItem(m_treeContacts.GetTrackItem());
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgMyContacts::OnTimer(UINT_PTR nIDEvent)
{
	//if (TIMERID_DELETE_ITEM == nIDEvent)
	//{
	//	KillTimer(nIDEvent);
	//	DeleteItem(m_treeContacts.GetTrackItem());
	//}else 
	if (TIMERID_EDIT_ITEM == nIDEvent)
	{
		KillTimer(nIDEvent);
		EditItem(m_treeContacts.GetTrackItem());
	}

	__super::OnTimer(nIDEvent);
}
