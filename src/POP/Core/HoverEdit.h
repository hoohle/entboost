#if !defined(AFX_HOVEREDIT_H__372AC76D_6B84_435C_8300_9519EB021C8C__INCLUDED_)
#define AFX_HOVEREDIT_H__372AC76D_6B84_435C_8300_9519EB021C8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HoverEdit.h : header file
//
#include "TrackControl.h"
/////////////////////////////////////////////////////////////////////////////
// CHoverEdit window

class CHoverEdit : public CTrackControl<CEdit>
{
// Construction
public:
	CHoverEdit();

// Attributes
public:
	void SetRectangleColor(COLORREF crHover,COLORREF crNormal=0) {m_colorHoverRectangle=crHover; m_colorNormalRectangle=crNormal;}
	COLORREF GetHoverRectangleColor(void) const {return m_colorHoverRectangle;}
	COLORREF GetNormalRectangleColor(void) const {return m_colorNormalRectangle;}

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHoverEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual void OnHoverLeave();
	virtual void OnHoverEnter();
	virtual ~CHoverEdit();

	// Generated message map functions
protected:
	COLORREF m_colorHoverRectangle;
	COLORREF m_colorNormalRectangle;
	//{{AFX_MSG(CHoverEdit)
	afx_msg void OnNcPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	inline void Redraw();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOVEREDIT_H__372AC76D_6B84_435C_8300_9519EB021C8C__INCLUDED_)
