// HoverEdit.cpp : implementation file
//

#include "stdafx.h"
#include "HoverEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHoverEdit

CHoverEdit::CHoverEdit()
{
	m_colorHoverRectangle = RGB(0,128,255);
	//m_colorNormalRectangle = 0;
	m_colorNormalRectangle = RGB(120,176,228);
}

CHoverEdit::~CHoverEdit()
{
}


BEGIN_MESSAGE_MAP(CHoverEdit, CEdit)
//{{AFX_MSG_MAP(CHoverEdit)
ON_WM_NCPAINT()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHoverEdit message handlers

void CHoverEdit::OnHoverEnter()
{
	Redraw();
}

void CHoverEdit::OnHoverLeave()
{
	Redraw();
}

void CHoverEdit::Redraw()
{
	RedrawWindow(NULL,NULL,RDW_FRAME|RDW_INVALIDATE);
}

void CHoverEdit::OnNcPaint() 
{
	//DWORD nStyle = this->GetStyle();
	//if (nStyle & ES_READONLY)
	//{
	//	return;
	//}

	CWindowDC DC(this);
	CRect Rect;
	GetWindowRect(&Rect);
	if (IsHover())
	{
		CPen BorderPen,*pOldPen;
		BorderPen.CreatePen(PS_SOLID,1, m_colorHoverRectangle);		//�߿���ɫ
		pOldPen = DC.SelectObject(&BorderPen);
		DC.Rectangle(0,0,Rect.Width(),Rect.Height());
		DC.SelectObject(pOldPen);
	}else if (m_colorNormalRectangle != 0)
	{
		CPen BorderPen,*pOldPen;
		BorderPen.CreatePen(PS_SOLID,1, m_colorNormalRectangle);	//�߿���ɫ
		pOldPen = DC.SelectObject(&BorderPen);
		DC.Rectangle(0,0,Rect.Width(),Rect.Height());
		DC.SelectObject(pOldPen);
	}else
	{
		DC.DrawEdge(CRect(0,0,Rect.Width(),Rect.Height()),EDGE_SUNKEN,BF_FLAT|BF_RECT);
	}
}
