// DlgMsgRecord.cpp : implementation file
//

#include "stdafx.h"
#include "POP.h"
#include "DlgMsgRecord.h"
#include "Core/SkinMemDC.h"

#define TIMERID_LOAD_MSG_RECORD 100

// CDlgMsgRecord dialog

IMPLEMENT_DYNAMIC(CDlgMsgRecord, CEbDialogBase)

CDlgMsgRecord::CDlgMsgRecord(CWnd* pParent /*=NULL*/)
	: CEbDialogBase(CDlgMsgRecord::IDD, pParent)
{
	m_sAccount = 0;
	m_sGroupCode = 0;
}

CDlgMsgRecord::~CDlgMsgRecord()
{
}

void CDlgMsgRecord::DoDataExchange(CDataExchange* pDX)
{
	CEbDialogBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_MAX, m_btnMax);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	DDX_Control(pDX, IDC_EDIT_SEARCH, m_editSearch);
	DDX_Control(pDX, IDC_BUTTON_DELETEALL, m_btnDeleteAll);
}


BEGIN_MESSAGE_MAP(CDlgMsgRecord, CEbDialogBase)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CDlgMsgRecord::OnBnClickedButtonMin)
	ON_BN_CLICKED(IDC_BUTTON_MAX, &CDlgMsgRecord::OnBnClickedButtonMax)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDlgMsgRecord::OnBnClickedButtonClose)
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_DELETEALL, &CDlgMsgRecord::OnBnClickedButtonDeleteall)
	ON_EN_CHANGE(IDC_EDIT_SEARCH, &CDlgMsgRecord::OnEnChangeEditSearch)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CDlgMsgRecord message handlers

BOOL CDlgMsgRecord::OnInitDialog()
{
	CEbDialogBase::OnInitDialog();
	ModifyStyle(0, WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_SYSMENU);
	this->SetSplitterBorder();
	//this->SetTransparentType(CEbDialogBase::TT_STATIC);

	this->EnableToolTips();
	this->SetToolTipText(IDC_EDIT_SEARCH,_T("搜索聊天记录"));
	this->SetToolTipText(IDC_BUTTON_DELETEALL,_T("清空聊天记录"));

	m_btnMin.SetAutoSize(false);
	m_btnMin.Load(IDB_PNG_MIN);
	m_btnMax.SetAutoSize(false);
	m_btnMax.Load(IDB_PNG_MAX);
	m_btnClose.SetAutoSize(false);
	m_btnClose.Load(IDB_PNG_CLOSE);

	if (!m_pMrFrameControl.CreateControl(__uuidof(EBRichMessage),NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,10,10),this,1111))
		return FALSE;
	LPUNKNOWN pUnknown = m_pMrFrameControl.GetControlUnknown();
	if (pUnknown == NULL)
		return FALSE;
	pUnknown->QueryInterface(__uuidof(IEBRichMessage),(void**)&m_pMrFrameInterface);
	if (pUnknown==NULL)
		return FALSE;
	m_pMrFrameInterface->SetLineInterval(3);

	m_editSearch.SetRectangleColor(RGB(0,128,255),RGB(144,174,205));
	m_btnDeleteAll.SetAutoSize(false);
	m_btnDeleteAll.Load(IDB_PNG_81X32B);

	//m_pMrFrame.Create(CRect(0,0,0,0),this);
	//m_pMrFrame.SetScrollBarBmp(IDB_VERTICAL_SCROLLBAR_UPARROW, IDB_VERTICAL_SCROLLBAR_SPAN, IDB_VERTICAL_SCROLLBAR_DOWNARROW,
	//	IDB_VERTICAL_SCROLLBAR_THUMB, IDB_VERTICAL_SCROLLBAR_TOP, IDB_VERTICAL_SCROLLBAR_BOTTOM );
	//m_pMrFrame.SetLineAlignment(3);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgMsgRecord::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMERID_LOAD_MSG_RECORD)
	{
		KillTimer(TIMERID_LOAD_MSG_RECORD);
		CString sWindowText;
		sWindowText.Format(_T("%s聊天信息"),m_sName.c_str());
		this->SetWindowText(sWindowText);
		CString sSql;
		if (m_sAccount>0)
		{
			sSql.Format(_T("select msg_time,msg_id,off_time,from_uid,from_name,to_uid,to_name,private,msg_type,msg_name,msg_text FROM msg_record_t \
						   WHERE dep_code=0 AND (from_uid=%lld OR to_uid=%lld) ORDER BY msg_time"),
						   m_sAccount,m_sAccount);
			LoadMsgRecord(sSql,false);
		}else if (m_sGroupCode>0)
		{
			sSql.Format(_T("select msg_time,msg_id,off_time,from_uid,from_name,to_uid,to_name,private,msg_type,msg_name,msg_text FROM msg_record_t \
						   WHERE dep_code=%lld ORDER BY msg_time"),
						   m_sGroupCode);
			LoadMsgRecord(sSql,true);
		}
	}
	CEbDialogBase::OnTimer(nIDEvent);
}

void CDlgMsgRecord::OnSize(UINT nType, int cx, int cy)
{
	CEbDialogBase::OnSize(nType, cx, cy);
	const int const_minbtn_width = m_btnMin.GetImgWidth();
	const int const_minbtn_height = m_btnMin.GetImgHeight();
	int btnx = 0;
	btnx = cx-m_btnClose.GetImgWidth()-2;
	m_btnClose.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;
	m_btnMax.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;
	m_btnMin.MovePoint(btnx, 2);
	btnx -= const_minbtn_width;

	int y = 52;
	if (m_editSearch.GetSafeHwnd())
	{
		m_editSearch.MoveWindow(10,y,196,22);
	}
	const int const_btn_width = m_btnDeleteAll.GetImgWidth();
	m_btnDeleteAll.MovePoint(cx-const_btn_width-10,y-5);
	
	const int const_interval = 1;
	y = 84;
	if (m_pMrFrameControl.GetSafeHwnd())
	{
		m_pMrFrameControl.MoveWindow(const_interval,y,cx-const_interval*2,cy-y-const_interval);	// 15 scroll
		if (m_pMrFrameInterface != NULL)
			m_pMrFrameInterface->ScrollToPos(-1);
	}
}

void CDlgMsgRecord::OnDestroy()
{
	CEbDialogBase::OnDestroy();
	m_pMrFrameInterface.Release();
	m_pMrFrameControl.DestroyWindow();

	delete this;
}

void CDlgMsgRecord::OnBnClickedButtonMin()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CDlgMsgRecord::OnBnClickedButtonMax()
{
	int m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度   
	//int m_nScreenHeight = GetSystemMetrics(SM_CYSCREEN); //屏幕高度
	int m_nScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	m_nScreenHeight += GetSystemMetrics(SM_CYCAPTION);

	static CRect theRestoreRect;
	CRect rect;
	GetWindowRect(&rect);
	if (rect.Width() == m_nScreenWidth)
	{
		this->SetToolTipText(IDC_BUTTON_MAX,_T("最大化"));
		m_btnMax.Load(IDB_PNG_MAX);
		MoveWindow(&theRestoreRect);
	}else
	{
		this->SetToolTipText(IDC_BUTTON_MAX,_T("向下还原"));
		m_btnMax.Load(IDB_PNG_RESTORE);
		theRestoreRect = rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = rect.left + m_nScreenWidth;
		rect.bottom = rect.top + m_nScreenHeight;
		MoveWindow(&rect);
	}
	//m_pMrFrame.Scroll();
}

void CDlgMsgRecord::OnBnClickedButtonClose()
{
	this->PostMessage(WM_CLOSE, 0, 0);
}

void CDlgMsgRecord::OnPaint()
{
	if (IsIconic())
	{
		//CPaintDC dc(this); // 用于绘制的设备上下文

		//SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		//// 使图标在工作区矩形中居中
		//int cxIcon = GetSystemMetrics(SM_CXICON);
		//int cyIcon = GetSystemMetrics(SM_CYICON);
		//CRect rect;
		//GetClientRect(&rect);
		//int x = (rect.Width() - cxIcon + 1) / 2;
		//int y = (rect.Height() - cyIcon + 1) / 2;

		//// 绘制图标
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this); // 用于绘制的设备上下文
		CRect rectClient;
		this->GetClientRect(&rectClient);
		CSkinMemDC memDC(&dc, rectClient);
		this->ClearBgDrawInfo();
		this->AddBgDrawInfo(CEbBackDrawInfo(42,0.7,true));
		this->AddBgDrawInfo(CEbBackDrawInfo(42,0.85,false));
		this->AddBgDrawInfo(CEbBackDrawInfo(0,0.99,false));
		this->DrawPopBg(&memDC, theApp.GetMainColor());
		Gdiplus::Graphics graphics(memDC.m_hDC);

		CString sWindowText;
		sWindowText.Format(_T("%s聊天信息"),m_sName.c_str());
		USES_CONVERSION;
		const FontFamily fontFamily(theFontFamily.c_str());
		const Gdiplus::Font fontEbTitle(&fontFamily, 15, FontStyleBold, UnitPixel);
		Gdiplus::SolidBrush brushEbTitle(Gdiplus::Color(38,38,38));
		const Gdiplus::PointF pointTitle(13,13);
		graphics.DrawString(T2W(sWindowText),-1,&fontEbTitle,pointTitle,&brushEbTitle);
	}
}

void CDlgMsgRecord::LoadAccountMsgRecord(eb::bigint sAccount, const tstring& sName)
{
	m_sAccount = sAccount;
	m_sGroupCode = 0;
	m_sName = sName;
	SetTimer(TIMERID_LOAD_MSG_RECORD,100,NULL);
}

void CDlgMsgRecord::LoadDepartmentMsgRecord(eb::bigint sDepCode, const tstring& sName)
{
	m_sAccount = 0;
	m_sGroupCode = sDepCode;
	m_sName = sName;
	SetTimer(TIMERID_LOAD_MSG_RECORD,100,NULL);
}

void CDlgMsgRecord::LoadMsgRecord(const CString& sSql, bool bIsDepartment)
{
	if (m_pMrFrameInterface==NULL) return;
	bo::PRESULTSET pResltSet = NULL;
	int ret = theApp.m_pBoUsers->execsql(sSql, &pResltSet);
	if (pResltSet != NULL)
	{
		USES_CONVERSION;
		//CString sMsgTime;
		time_t nLocalMsgTime = 0;
		for (int i=0; i<pResltSet->rscount; i++)
		{
			const time_t nMsgTime = pResltSet->rsvalues[i]->fieldvalues[0]->v.timestampVal.time;
			const short nTimezone = pResltSet->rsvalues[i]->fieldvalues[0]->v.timestampVal.timezone;
			const eb::bigint sMsgId = pResltSet->rsvalues[i]->fieldvalues[1]->v.bigintVal;
			const tstring soffTime = BODB_BUFFER_TEXT(pResltSet->rsvalues[i]->fieldvalues[2]->v.varcharVal);
			const eb::bigint sFromAccount = pResltSet->rsvalues[i]->fieldvalues[3]->v.bigintVal;
			const tstring sFromName = BODB_BUFFER_TEXT(pResltSet->rsvalues[i]->fieldvalues[4]->v.varcharVal);
			const eb::bigint sToAccount = pResltSet->rsvalues[i]->fieldvalues[5]->v.bigintVal;
			const tstring sToName = BODB_BUFFER_TEXT(pResltSet->rsvalues[i]->fieldvalues[6]->v.varcharVal);
			const int nPrivate = pResltSet->rsvalues[i]->fieldvalues[7]->v.tinyintVal;
			const int nMsgType = pResltSet->rsvalues[i]->fieldvalues[8]->v.tinyintVal;
			const tstring sMsgName = BODB_BUFFER_TEXT(pResltSet->rsvalues[i]->fieldvalues[9]->v.varcharVal);
			tstring sMsgText = BODB_BUFFER_TEXT(pResltSet->rsvalues[i]->fieldvalues[10]->v.varcharVal);

			//// 兼容旧版本
			//if (sFromName.empty())
			//	sFromName = sFromAccount;
			//if (sToName.empty())
			//	sToName = sToAccount;
			CString sWindowText;
			if (MRT_TITLE==nMsgType)
			{
				if (nLocalMsgTime > 0)
				{
					m_pMrFrameInterface->WriteTime(nLocalMsgTime);
				}
				nLocalMsgTime = nMsgTime+nTimezone*60;
				if (!soffTime.empty())
					libEbc::ChangeTime(soffTime.c_str(),nLocalMsgTime);

				CString sPrivateText;
				CString sToText;
				if (bIsDepartment)
				{
					if (nPrivate==1)
						sPrivateText = _T("[私聊]");
					if (sToAccount == theApp.GetLogonUserId())
						sToText.Format(_T("对你说"));
					else if (sToAccount>0)
						sToText.Format(_T("对%s说"),sToName.c_str());
				}
				m_pMrFrameInterface->AddLine();
				if (sFromAccount==theApp.GetLogonUserId())
				{
					m_pMrFrameInterface->SetAlignmentFormat(2);
				}
				sWindowText.Format(_T("%s%s%s"),sPrivateText,sFromName.c_str(),sToText);
				m_pMrFrameInterface->WriteString((LPCTSTR)sWindowText);

				m_pMrFrameInterface->AddLine();
				m_pMrFrameInterface->SetFrameArc(theArcOffset,thePoloygonWidth,thePoloygonHeight);
				m_pMrFrameInterface->SetFrameBorderColor(128,128,128);
				if (sFromAccount==theApp.GetLogonUserId())
				{
					m_pMrFrameInterface->SetAlignmentFormat(2);
					m_pMrFrameInterface->SetFrameBackGroundColor(211,211,211);
				}else
				{
					m_pMrFrameInterface->SetFrameBackGroundColor(183,253,159);
				}

				//const COLORREF crTextColor = (sFromAccount==theEBAppClient.EB_GetLogonAccount())?RGB(66, 180, 117):RGB(0, 110, 254);
				//sWindowText.Format(_T("<font color=#%02X%02X%02X>%s%s%s</font><font color=#C0C0C0 style=\"FONT-SIZE:10pt\" > %s</font>"),
				//	GetRValue(crTextColor),GetGValue(crTextColor),GetBValue(crTextColor),sPrivateText,sFromAccount.c_str(),sToText,sMsgTime);
				//AddMessage(sWindowText,FALSE);
			}else if (MRT_TEXT==nMsgType)
			{
				bo::bodb_escape_string_out(sMsgText);
				m_pMrFrameInterface->WriteString(sMsgText.c_str());

				//sWindowText.Format(_T("&nbsp;%s"), sMsgText.c_str());
				////if (bHasCharFormat)
				////	sWindowText = CharFormat2Html(cf, sWindowText);
				//AddMessage(sWindowText,FALSE);
			}else if (MRT_JPG==nMsgType)
			{
				m_pMrFrameInterface->WriteImage(sMsgName.c_str());
			}else if (MRT_WAV==nMsgType)
			{
				//WriteFileHICON(sMsgName.c_str());
				m_pMrFrameInterface->WriteWav("播放语音",sMsgName.c_str());
			}else if (MRT_FILE==nMsgType)
			{
				bool bReceive = sFromAccount!=theApp.GetLogonUserId();
				////m_pMrFrame.AddLine();
				//m_pMrFrame.SetAlignment(bReceive?MR_ALIGNMENT_LEFT:MR_ALIGNMENT_RIGHT);
				//if (bReceive)
				//	m_pMrFrame.WriteArc(6,8,16,Gdiplus::Color::Gray,Gdiplus::Color(183,253,159));
				//else
				//	m_pMrFrame.WriteArc();

				const std::wstring sImagePath = A2W(sMsgText.c_str());
				Gdiplus::Image * image = Gdiplus::Image::FromFile(sImagePath.c_str());
				bool bIsImage = (bool)(image->GetType()!= Gdiplus::ImageTypeUnknown);
				delete image;
				if (bIsImage)
				{
					m_pMrFrameInterface->WriteImage(sMsgText.c_str());
				}else
				{
					WriteFileHICON(sMsgText.c_str());
					const tstring sFileName = libEbc::GetFileName(sMsgText);
					m_pMrFrameInterface->WriteUrl(sFileName.c_str(), sMsgText.c_str());
				}
				m_pMrFrameInterface->WriteLine();
				m_pMrFrameInterface->WriteText(L" ");
				m_pMrFrameInterface->WriteOpenFile(L"打开",sMsgText.c_str());
				m_pMrFrameInterface->WriteText(L" ");
				m_pMrFrameInterface->WriteOpenDir(L"打开目录",sMsgText.c_str());
			}
		}
		bo::bodb_free(pResltSet);

		if (nLocalMsgTime > 0)
		{
			m_pMrFrameInterface->WriteTime(nLocalMsgTime);
			m_pMrFrameInterface->UpdateSize(VARIANT_TRUE);
			m_pMrFrameInterface->ScrollToPos(-1);
		}
	}
	ShowWindow(SW_SHOW);
	SetForegroundWindow();
}
void CDlgMsgRecord::WriteFileHICON(const char* lpszFilePath)
{
	SHFILEINFO sfi; 
	ZeroMemory(&sfi,sizeof(sfi)); 
	DWORD ret = SHGetFileInfo(lpszFilePath,
		FILE_ATTRIBUTE_NORMAL, 
		&sfi, 
		sizeof(sfi), 
		SHGFI_USEFILEATTRIBUTES|SHGFI_ICON);
	if (ret == 1)
	{
		m_pMrFrameInterface->WriteHICON((ULONG*)sfi.hIcon,lpszFilePath);
	}
}

void CDlgMsgRecord::OnClose()
{
	if (m_sAccount>0)
		theApp.m_pMsgRecord.remove(m_sAccount);
	if (m_sGroupCode>0)
		theApp.m_pMsgRecord.remove(m_sGroupCode);
	theApp.m_pCloseWnd.add(this);
	//CEbDialogBase::OnClose();
	this->EndDialog(IDOK);
}

BOOL CDlgMsgRecord::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	return TRUE;
	return CEbDialogBase::OnEraseBkgnd(pDC);
}

void CDlgMsgRecord::OnBnClickedButtonDeleteall()
{
	bool bIsAccount = m_sAccount>0;
	eb::bigint sId = bIsAccount?m_sAccount:m_sGroupCode;
	CString sText;
	if (bIsAccount)
		sText.Format(_T("[%s(%lld)]吗？"), m_sName.c_str(),sId);
	else
		sText.Format(_T("[%s]吗？"), m_sName.c_str());	// 群组（部门）不需要显示group_code
	if (CDlgMessageBox::EbDoModal(this,"你确定清空聊天记录：",sText,CDlgMessageBox::IMAGE_QUESTION)==IDOK)
	{
		theApp.DeleteDbRecord(sId,bIsAccount);
		m_pMrFrameInterface->Clear();
		//m_pMrFrameControl.Invalidate();	// 没用
	}
}

void CDlgMsgRecord::OnEnChangeEditSearch()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CEbDialogBase::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sSearchString;
	m_editSearch.GetWindowText(sSearchString);
	if (!sSearchString.IsEmpty())
	{
		// ???

	}
}

void CDlgMsgRecord::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	OnBnClickedButtonMax();
	CEbDialogBase::OnLButtonDblClk(nFlags, point);
}
