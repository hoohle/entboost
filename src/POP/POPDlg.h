// POPDlg.h : 头文件
//

#pragma once

#include "stl/lockmap.h"
#include "DlgDialog.h"
#include "DlgIncomingCall.h"
#include "DlgAlertingCall.h"
#include "afxwin.h"
class CDlgMyEnterprise;
class CDlgMyGroup;
class CDlgMyContacts;
class CDlgMySession;
class CDlgEditInfo;
class CDlgFileManager;

// CPOPDlg 对话框
class CPOPDlg : public CEbDialogBase
	, public CTreeCallback
#ifdef USES_EBCOM_TEST
	, public CEBCoreEventsSink
	, public CEBSearchEventsSink
#else
	, public CEBSearchCallback
#endif
{
// 构造
public:
	CPOPDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_POP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	CTraButton m_btnSkin;
	CTraButton m_btnMin;
	CTraButton m_btnMax;
	CTraButton m_btnClose;
	CTraButton m_btnLineState;
	CTraButton m_btnMyCenter;
	CTraButton m_btnMyShare;
	CTraButton m_btnFileMgr;
	//CString m_sCallNumber;
	//CComboBox m_comboNumbers;
	//CxSkinButton m_btnCallUser;
	CTraButton m_btnMyEnterprise;
	CTraButton m_btnMyDepartment;
	CTraButton m_btnMyContacts;
	CTraButton m_btnMySession;
	VividTree m_treeSearch;				// itemdata:1=employee;2=contact
	CTraButton m_btnSearchTrackCall;	// search call
	CHoverEdit m_editSearch;
	CEdit m_editDescription;

//#ifdef USES_EBCOM
//	DECLARE_INTERFACE_MAP()
//	DECLARE_DISPATCH_MAP()
//
//	//// Connection point for ISample interface
//	//BEGIN_CONNECTION_PART(CPOPDlg, SampleConnPt)
//	//	CONNECTION_IID(__uuidof(_IEBClientCoreEvents))
//	//END_CONNECTION_PART(SampleConnPt)
//	//DECLARE_CONNECTION_MAP()
//
//	void onCallConnected2(IDispatch * pCallInfo, long nConnectFlag)
//	{
//		CComPtr<IEB_CallInfo> pInterface;
//		pCallInfo->QueryInterface(__uuidof(IEB_CallInfo),(void**)&pInterface);
//		if (pInterface != NULL)
//		{
//			::SendMessage(this->GetSafeHwnd(),EB_WM_CALL_CONNECTED,(WPARAM)(IEB_CallInfo*)pInterface,(LPARAM)nConnectFlag);
//		}
//	}
//	void onGroupInfo2(IDispatch * pGroupInfo, VARIANT_BOOL bIsMyGroup)
//	{
//		CComPtr<IEB_GroupInfo> pInterface;
//		pGroupInfo->QueryInterface(__uuidof(IEB_GroupInfo),(void**)&pInterface);
//		if (pInterface != NULL)
//		{
//		AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			//if (m_hFireWnd)
//				::SendMessage(GetSafeHwnd(),EB_WM_GROUP_INFO,(WPARAM)(IEB_GroupInfo*)pInterface,bIsMyGroup?1:0);
//			//else
//				//Fire_onGroupInfo(pInterface, bIsMyGroup?true:false);
//		}
//	}
//	void onMemberInfo2(IDispatch * pMemberInfo, VARIANT_BOOL bIsMyDefaultMember)
//	{
//		CComPtr<IEB_MemberInfo> pInterface;
//		pMemberInfo->QueryInterface(__uuidof(IEB_MemberInfo),(void**)&pInterface);
//		if (pInterface != NULL)
//		{
//		AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			//if (m_hFireWnd)
//				::SendMessage(GetSafeHwnd(),EB_WM_MEMBER_INFO,(WPARAM)(IEB_MemberInfo*)pInterface,(LPARAM)0);
//			//else
//				//Fire_onMemberInfo(pInterface, bIsMyDefaultMember?true:false);
//		}
//	}
//#endif

// 实现
protected:
	CMenu m_menuState;
	CMenu m_menuTray;
	CMenu m_menuSkin;
	NOTIFYICONDATA m_trayIconData;
	std::vector<EB_MemberInfo> m_pMyEmployeeInfo;			// for menu
	std::vector<EB_SubscribeFuncInfo> m_pSubscribeFuncList;	// for menu

	//HICON m_hIcon;
	CDlgEditInfo* m_pDlgEditInfo;
	CDlgMyEnterprise* m_pDlgMyEnterprise;
	CDlgMyGroup* m_pDlgMyGroup;
	CDlgMyContacts* m_pDlgMyContacts;
	CDlgMySession* m_pDlgMySession;
	CLockMap<eb::bigint, CDlgIncomingCall::pointer> m_pIncomingCallList;	// fromaccount->
	CLockMap<eb::bigint, CDlgAlertingCall::pointer> m_pAlertingCallList;	// fromaccount->
	CLockMap<std::string, CTreeItemInfo::pointer> m_pContactItem;		// contact->
	CLockMap<eb::bigint, CTreeItemInfo::pointer> m_pMemberItem;		// emp_code->
	//CLockMap<HTREEITEM, std::string> m_pSearchItem2;		//

	void SaveCallRecord(eb::bigint nCallId,eb::bigint sGroupCode,const EB_AccountInfo& pFromAccountInfo);
	void DeleteDlgIncomingCall(eb::bigint sFromAccount);
	void DeleteDlgAlertingCall(eb::bigint sFromAccount);

	CDlgDialog::pointer GetDlgDialog(const CEBCCallInfo::pointer& pEbCallInfo);

	// file manager
	CDlgFileManager* m_pDlgFileManager;
	void ShowDlgFileManager(void);
#ifdef USES_EBCOM_TEST
	virtual void Fire_onSearchGroupInfo(IEB_GroupInfo* pGroupInfo, IEB_EnterpriseInfo* pEnterpriseInfo, ULONG dwParam) {}
	virtual void Fire_onSearchMemberInfo(IEB_GroupInfo* pGroupInfo, IEB_MemberInfo* pMemberInfo, ULONG dwParam);
	virtual void Fire_onSearchContactInfo(IEB_ContactInfo* pContactInfo, ULONG dwParam);
#else
	virtual void onGroupInfo(const EB_GroupInfo* pGroupInfo,const EB_EnterpriseInfo* pEnterpriseInfo,unsigned long dwParam) {}
	virtual void onMemberInfo(const EB_GroupInfo* pGroupInfo, const EB_MemberInfo* pMemberInfo, unsigned long dwParam);
	virtual void onContactInfo(const EB_ContactInfo* pContactInfo, unsigned long dwParam);
#endif
	// chatroom
	//LRESULT OnMessageEnterRoom(WPARAM wParam, LPARAM lParam);
	//LRESULT OnMessageExitRoom(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageUserEnterRoom(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageUserExitRoom(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageReceiveRich(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageSendRich(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageSendingFile(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageSendedFile(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageCancelFile(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageReceivingFile(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageReceivedFile(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageFilePercent(WPARAM wParam, LPARAM lParam);

	LRESULT OnMessageVRequestResponse(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageVAckResponse(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageVideoRequest(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageVideoAccept(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageVideoCancel(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageVideoEnd(WPARAM wParam, LPARAM lParam);

	LRESULT OnMessageBroadcastMsg(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageNewVersion(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageServerChange(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageOnlineAnother(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogoutResponse(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonSuccess(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonTimeout(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonError(WPARAM wParam, LPARAM lParam);

	LRESULT OnMessageSInfoResponse(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageUserStateChange(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageCallConnected(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageCallHangup(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageAlertingCall(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageCallError(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageIncomingCall(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageContactInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageContactDelete(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageEnterpriseInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageGroupInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageGroupDelete(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageGroupEditError(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageRemoveGroup(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageExitGroup(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageRequestJoin2Group(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageInviteJoin2Group(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageMemberInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageMemberDelete(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageMemberEditError(WPARAM wParam, LPARAM lParam);
	//LRESULT OnMessageAppMsgInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageResourceInfo(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageDeleteResource(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageMoveResource(WPARAM wParam, LPARAM lParam);
//#endif
	LRESULT OnMessageFileManager(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageHotKey(WPARAM wParam, LPARAM lParam);

	//LRESULT OnMessageWindowResize(WPARAM wParam, LPARAM lParam);
	LRESULT OnTreeItemDoubleClieck(WPARAM wp, LPARAM lp);
	LRESULT OnTreeItemTrackHot(WPARAM wp, LPARAM lp);
	LRESULT OnIconNotification(WPARAM wp, LPARAM lp);

	void DrawInfo(void);
	void CallItem(HTREEITEM hItem);

	void InitData(void);
	HWND m_pCurrentLabel;
	HWND m_pOldCurrentLabel;
	void RefreshLabelWindow(void);
	void RefreshEditDescription(void);
	void ChangeTrayText(void);

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	//afx_msg void OnCbnSelchangeComboNumbers();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//afx_msg void OnBnClickedButtonCalluser();
	afx_msg void OnBnClickedButtonEnterprise();
	afx_msg void OnBnClickedButtonDepartment();
	afx_msg void OnBnClickedButtonContact();
	afx_msg void OnBnClickedButtonMax();
	afx_msg void OnBnClickedButtonMin();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnEnChangeEditSearch();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnExitApp();
	afx_msg void OnLogout();
	afx_msg void OnBnClickedButtonLinestate();
	afx_msg void OnStateOnline();
	afx_msg void OnStateAway();
	afx_msg void OnStateBusy();
	afx_msg void OnAutoLogin();	// 取消自动登录
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnEnKillfocusEditDescription();
	afx_msg void OnBnClickedButtonSession();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMySetting();
	afx_msg void OnFileManager();
	afx_msg void OnMyShare();
	afx_msg void OnBnClickedButtonSkin2();
	afx_msg void OnSkinSelect(UINT nID);
	afx_msg void OnOpenMain();
	afx_msg void OnMyEmployeeInfo(UINT nID);
	afx_msg void OnSubscribeFunc(UINT nID);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
public:
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonMyCenter();
	afx_msg void OnBnClickedButtonMyShare();
	afx_msg void OnBnClickedButtonFileMgr();
};
