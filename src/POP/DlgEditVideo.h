#pragma once
//#include "videoroom.h"
#include "Core/DlgVoiceBar.h"
#include "DlgVideo.h"


// CDlgEditVideo dialog

class CDlgEditVideo : public CEbDialogBase
#ifdef USES_EBCOM_TEST
	, public CEBVRoomEventsSink
#endif
{
	DECLARE_DYNAMIC(CDlgEditVideo)

public:
	CDlgEditVideo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgEditVideo();

	void OnMove(void);
	void CloseVideo(void);
// Dialog Data
	enum { IDD = IDD_DLG_EDIT_VIDEO };

protected:
	int m_nLocalId;
#ifdef USES_EBCOM_TEST
	CComPtr<IEBVRoom> m_pVideoRoom;
#else
	Cvideoroom * m_pVideoRoom;
#endif
	CDlgVideo * m_pMyVideo;

#ifdef USES_EBCOM_TEST
	virtual void Fire_onVideoStream(ULONG nUserId, BYTE* pVideoData, ULONG nVideoSize, ULONG nUserData);
	virtual void Fire_onAudioStream(ULONG nUserId, BYTE* pAudioData, ULONG nAudioSize, ULONG nUserData);
#else
	static void ProcessVideoStream(DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD udata);
#endif

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CBitComboBox m_comboVideos;
	CTraButton m_btnLocalVideo;

	void CreateVideoRoom(void);

	DECLARE_MESSAGE_MAP()
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnCbnSelchangeComboVideos();
	afx_msg void OnBnClickedButtonLocalVideo();
	afx_msg void OnDestroy();
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnMove(int x, int y);
};
