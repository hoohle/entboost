#pragma once

#include "afxwin.h"

// CDlgUserInfo dialog

class CDlgUserInfo : public CEbDialogBase
{
	DECLARE_DYNAMIC(CDlgUserInfo)

public:
	CDlgUserInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgUserInfo();

	EB_CallInfo m_pCallInfo;
	EB_AccountInfo m_pFromAccountInfo;

// Dialog Data
	enum { IDD = IDD_DLG_USERINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void InvalidateParentRect(UINT nID);
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnEnKillfocusEditName();
	afx_msg void OnEnKillfocusEditEnterprise();
	afx_msg void OnEnKillfocusEditTel();
	afx_msg void OnEnKillfocusEditEmail();
	afx_msg void OnEnKillfocusEditJobtitle();
	afx_msg void OnEnKillfocusEditDepartment();
};
