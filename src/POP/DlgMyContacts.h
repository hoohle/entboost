#pragma once
#include "afxcmn.h"

class CDlgViewContactInfo;

// CDlgMyContacts dialog

class CDlgMyContacts : public CDialog
	//, public CTreeCallback
{
	DECLARE_DYNAMIC(CDlgMyContacts)

public:
	CDlgMyContacts(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgMyContacts();

#ifdef USES_EBCOM_TEST
	void ContactInfo(IEB_ContactInfo* pPopContactInfo);
	void DeleteContact(IEB_ContactInfo* pPopContactInfo);
#else
	void ContactInfo(const EB_ContactInfo* pPopContactInfo);
	void DeleteContact(const EB_ContactInfo* pPopContactInfo);
#endif

// Dialog Data
	enum { IDD = IDD_DLG_MY_CONTACTS };

protected:
	CDlgViewContactInfo* m_pViewContactInfo;
	CLockMap<std::string,CTreeItemInfo::pointer> m_pGroupItemInfo;	// groupname->
	CLockMap<std::string,CTreeItemInfo::pointer> m_pContactItemInfo;	// contact->
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//CTraButton m_btnDeleteTrack;
	CTraButton m_btnEditTrack;
	CTraButton m_btnCallTrack;
	VividTree m_treeContacts;

	//CTreeItemInfo::pointer GetGroupItemInfo(HTREEITEM hItem) const;
	//CTreeItemInfo::pointer GetContactItemInfo(HTREEITEM hItem) const;
	//static int CALLBACK TreeCmpFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

	void DeleteItem(HTREEITEM hItem);
	void EditItem(HTREEITEM hItem);
	void CallItem(HTREEITEM hItem);
	//virtual bool GetItemImage(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem,Gdiplus::Image*& pImage1,Gdiplus::Image*& pImage2,int& pState) const;
	//virtual int GetItemState(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem,HICON& pOutIcon) const;
	//virtual bool GetItemDrawOpenClose(const CTreeCtrl& pTreeCtrl,HTREEITEM hItem) const;
	LRESULT OnTreeItemDblclk(WPARAM wp, LPARAM lp);
	LRESULT OnTreeItemTrackHot(WPARAM wp, LPARAM lp);

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnViewMsgRecord();
	afx_msg void OnDeleteMsgRecord();
	afx_msg void OnContactNew();
	afx_msg void OnNMRClickTreeContacts(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnContactEdit();
	afx_msg void OnContactDelete();
	afx_msg void OnCallUser();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSubscribeFunc(UINT nID);
};
