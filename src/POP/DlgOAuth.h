#pragma once

// CDlgOAuth dialog

class CDlgOAuth : public CDialog
#ifdef USES_EBCOM_TEST
	, public CEBCoreEventsSink
#endif
{
	DECLARE_DYNAMIC(CDlgOAuth)

public:
	CDlgOAuth(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgOAuth();

	std::string m_sAppParameter0;
// Dialog Data
	enum { IDD = IDD_DLG_OAUTH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	static void RunEBC(void);

#ifdef USES_EBCOM_TEST
	virtual void Fire_onOAuthForward(BSTR sOAuthUrl);
	virtual void Fire_onLogonSuccess(IEB_AccountInfo * pAccountInfo);
	virtual void Fire_onLogonTimeout(IEB_AccountInfo * pAccountInfo);
	virtual void Fire_onLogonError(IEB_AccountInfo * pAccountInfo, EB_STATE_CODE nState);
#else
	LRESULT OnMessageOAuthForward(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonSuccess(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonTimeout(WPARAM wParam, LPARAM lParam);
	LRESULT OnMessageLogonError(WPARAM wParam, LPARAM lParam);
#endif
	bool StartClient(void);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
};
