#include "StdAfx.h"
#include "ebc_public.h"

std::wstring theFontFamily = L"Times New Roman";

namespace libEbc
{
long GetFileSize(const char* lpszFile)
{
	long result = 0;
	FILE * f = fopen(lpszFile, "rb");
	if (f != NULL)
	{
		fseek(f, 0, SEEK_END);
		result = ftell(f);
		fclose(f);
	}
	return result;
}

std::string GetFileName(const std::string & sPathName)
{
	std::string sFileName(sPathName);
	int find = sPathName.rfind("\\");
	if (find < 0)
		find = sPathName.rfind("/");
	if (find > 0)
		sFileName = sPathName.substr(find+1);
	return sFileName;
}
void GetFileExt(const std::string & sFileName, std::string & sOutName, std::string & sOutExt)
{
	int find = sFileName.rfind(".");
	if (find > 0)
	{
		sOutName = sFileName.substr(0, find);
		sOutExt = sFileName.substr(find+1);
	}else
	{
		sOutName = sFileName;
	}
}


int   GetCodecClsid(const   WCHAR*   format,   CLSID*   pClsid)
{
	UINT     num   =   0;                     //   number   of   image   encoders
	UINT     size   =   0;                   //   size   of   the   image   encoder   array   in   bytes

	Gdiplus::ImageCodecInfo*   pImageCodecInfo   =   NULL;

	Gdiplus::GetImageEncodersSize(&num,   &size);
	if(size   ==   0)
		return   -1;     //   Failure

	pImageCodecInfo   =   (Gdiplus::ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo   ==   NULL)
		return   -1;     //   Failure

	Gdiplus::GetImageEncoders(num,   size,   pImageCodecInfo);
	for(UINT   j   =   0;   j   <   num;   ++j)
	{
		if(   wcscmp(pImageCodecInfo[j].MimeType,   format)   ==   0   )
		{
			*pClsid   =   pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return   j;     //   Success
		}        
	}   //   for
	free(pImageCodecInfo);
	return   -1;     //   Failure

}   //   GetCodecClsid
int bmp_2_jpg(const WCHAR * sBmpFile, const WCHAR * sJpgFile, long quality)
{
	CLSID                           codecClsid;
	Gdiplus::EncoderParameters		encoderParameters;
	Gdiplus::Status					stat;

	//   Get   an   image   from   the   disk.
	Gdiplus::Image   image(sBmpFile);

	//   Get   the   CLSID   of   the   JPEG   codec.
	GetCodecClsid(L"image/jpeg",   &codecClsid);

	//   Before   we   call   Image::Save,   we   must   initialize   an
	//   EncoderParameters   object.   The   EncoderParameters   object
	//   has   an   array   of   EncoderParameter   objects.   In   this
	//   case,   there   is   only   one   EncoderParameter   object   in   the   array.
	//   The   one   EncoderParameter   object   has   an   array   of   values.
	//   In   this   case,   there   is   only   one   value   (of   type   LONG)
	//   in   the   array.   We   will   set   this   value   to   0,   50,   and   100.

	encoderParameters.Count   =   1;
	encoderParameters.Parameter[0].Guid   =   Gdiplus::EncoderQuality;
	encoderParameters.Parameter[0].Type   =   Gdiplus::EncoderParameterValueTypeLong;
	encoderParameters.Parameter[0].NumberOfValues   =   1;

	//   Save   the   image   as   a   JPEG   with   quality   level   0.
	encoderParameters.Parameter[0].Value   =   &quality;
	stat   =   image.Save(sJpgFile,   &codecClsid,   &encoderParameters);
	return stat == Gdiplus::Ok ? 0 : -1;
	/*
	if(stat   == Gdiplus::Ok)
		wprintf(L"%s   saved   successfully.\n ",   L"Shapes001.jpg ");
	else
		wprintf(L"%d     Attempt   to   save   %s   failed.\n ",   stat,   L"Shapes001.jpg ");

	//   Save   the   image   as   a   JPEG   with   quality   level   50.
	quality   =   50;
	encoderParameters.Parameter[0].Value   =   &quality;
	stat   =   image.Save(L"d:\\Shapes050.jpg ",   &codecClsid,   &encoderParameters);

	if(stat   ==   Gdiplus::Ok)
		wprintf(L"%s   saved   successfully.\n ",   L"Shapes050.jpg ");
	else
		wprintf(L"%d     Attempt   to   save   %s   failed.\n ",   stat,   L"Shapes050.jpg ");

	//   Save   the   image   as   a   JPEG   with   quality   level   100.
	quality   =   100;
	encoderParameters.Parameter[0].Value   =   &quality;
	stat   =   image.Save(L"d:\\Shapes100.jpg ",   &codecClsid,   &encoderParameters);

	if(stat   ==   Gdiplus::Ok)
		wprintf(L"%s   saved   successfully.\n ",   L"Shapes100.jpg ");
	else
		wprintf(L"%d     Attempt   to   save   %s   failed.\n ",   stat,   L"Shapes100.jpg ");
*/
	return   0; 
}
bool ChangeTime(const char* sTimeString, time_t& pOutTime, int* pOutMS)
{
	int nMS = 0;
	tm pTm;
	memset(&pTm,0,sizeof(pTm));
	int nRet = ::sscanf(sTimeString,"%d-%d-%d %d:%d:%d.%d",&pTm.tm_year,&pTm.tm_mon,&pTm.tm_mday,&pTm.tm_hour,&pTm.tm_min,&pTm.tm_sec,&nMS);
	if (nRet != EOF)
	{
		pTm.tm_year -= 1900;
		pTm.tm_mon -= 1;
		pOutTime = mktime(&pTm);
		if (pOutMS != NULL)
			*pOutMS = nMS;
		return true;
	}
	return false;
}

std::string str_convert(const char * strSource, int sourceCodepage, int targetCodepage)
{
	int unicodeLen = MultiByteToWideChar(sourceCodepage, 0, strSource, -1, NULL, 0);
	if (unicodeLen <= 0) return "";

	wchar_t * pUnicode = new wchar_t[unicodeLen];
	memset(pUnicode,0,(unicodeLen)*sizeof(wchar_t));

	MultiByteToWideChar(sourceCodepage, 0, strSource, -1, (wchar_t*)pUnicode, unicodeLen);

	char * pTargetData = 0;
	int targetLen = WideCharToMultiByte(targetCodepage, 0, (wchar_t*)pUnicode, -1, (char*)pTargetData, 0, NULL, NULL);
	if (targetLen <= 0)
	{
		delete[] pUnicode;
		return "";
	}

	pTargetData = new char[targetLen+1];
	memset(pTargetData, 0, targetLen);
	pTargetData[targetLen] = '0';

	WideCharToMultiByte(targetCodepage, 0, (wchar_t*)pUnicode, -1, (char *)pTargetData, targetLen, NULL, NULL);

	std::string result(pTargetData);
	//std::string result(pTargetData,targetLen);
	delete[] pUnicode;
	delete[] pTargetData;
	return   result;
}
std::string ACP2UTF8(const char* sString)
{
	return str_convert(sString,CP_ACP,CP_UTF8);
}
std::string UTF82ACP(const char* sString)
{
	return str_convert(sString,CP_UTF8,CP_ACP);
}
BOOL ImageFromIDResource(UINT nID, LPCTSTR sTR, Image * & pImg)
{
	pImg = NULL;
    HINSTANCE hInst = AfxGetResourceHandle();
    HRSRC hRsrc = ::FindResource (hInst,MAKEINTRESOURCE(nID),sTR); // type
    if (!hRsrc)
        return FALSE;
    // load resource into memory
    DWORD len = SizeofResource(hInst, hRsrc);
    BYTE* lpRsrc = (BYTE*)LoadResource(hInst, hRsrc);
    if (!lpRsrc)
        return FALSE;
    // Allocate global memory on which to create stream
    HGLOBAL m_hMem = GlobalAlloc(GMEM_FIXED, len);
    BYTE* pmem = (BYTE*)GlobalLock(m_hMem);
    memcpy(pmem,lpRsrc,len);
    IStream* pstm;
    CreateStreamOnHGlobal(m_hMem,FALSE,&pstm);
    // load from stream
    pImg=Gdiplus::Image::FromStream(pstm);
    // free/release stuff
    pstm->Release();
    GlobalUnlock(m_hMem);
    GlobalFree(m_hMem);
    FreeResource(lpRsrc);
    return TRUE;
}

};
