
// ebc_public.h file here
#ifndef __ebc_public_h__
#define __ebc_public_h__

#include "ebstring.h"
extern std::wstring theFontFamily;	// "Times New Roman"
const std::wstring theFontFamily0 = L"微软雅黑";
const std::wstring theFontFamily1 = L"宋体";
const int theMaxDatabaseTextSize = 6000;
const short theArcOffset = 6;		// 聊天边框圆弧
const short thePoloygonWidth = 12;	// 0
const short thePoloygonHeight = 16;	// 0
const int theColorSkinSize = 10;
const COLORREF theColorSkinsValue[] = {RGB(218,219,223),RGB(86,190,245),RGB(1,103,204),RGB(162,237,119),RGB(255,111,128),RGB(244,236,138),RGB(208,177,94),RGB(246,176,80),RGB(193,195,194),RGB(162,161,167)};
//const COLORREF theColorSkinsValue[] = {RGB(120,170,100),RGB(246,176,80),RGB(23,167,237),RGB(240,240,240)};
const CString theColorSkinsString[] = {_T("白色"),_T("蓝色"),_T("深蓝"),_T("绿色"),_T("红色"),_T("黄色"),_T("金色"),_T("紫色"),_T("银色"),_T("黑色")};
// 灰色 = 240,240,240
// 蓝色 = 23,167,237

#ifndef bigint
typedef __int64				bigint;
#endif

typedef enum EB_COMMAND_ID
{
	EB_COMMAND_LOGOUT			= WM_USER+100
	, EB_COMMAND_EXIT_APP
	, EB_MSG_EXIT_CHAT
	, EB_COMMAND_SELECTED_IMAGE
	//, EB_MSG_SELECTED_IMAGE
	, EB_MSG_EBSC
	, EB_MSG_EBSC_OK
	, EB_MSG_VIEW_MSG_RECORD
	, EB_COMMAND_VIEW_GROUP_SHARE
	, EB_COMMAND_DELETE_MSG_RECORD
	, EB_COMMAND_FILE_MANAGER
	, EB_COMMAND_OPEN_MAIN
	, EB_COMMAND_MY_SETTING
	, EB_COMMAND_AUTO_LOGIN
	, EB_COMMAND_MY_SHARE
	, EB_COMMAND_SYS_SETTING
	, EB_COMMAND_DELETE_ACCOUNT

	, EB_MSG_SELECTED_EMP
	, EB_COMMAND_NEW_DEPARTMENT1	// 部门
	, EB_COMMAND_NEW_DEPARTMENT2	// 项目组
	, EB_COMMAND_NEW_DEPARTMENT3	// 个人群组
	, EB_COMMAND_EDIT_DEPARTMENT
	, EB_COMMAND_DELETE_DEPARTMENT
	, EB_COMMAND_EXIT_DEPARTMENT
	, EB_COMMAND_EDIT_ENTERPRISE
	, EB_COMMAND_DEPARTMENT_DEL_EMP
	, EB_COMMAND_DEPARTMENT_ADD_EMP
	, EB_COMMAND_DEPARTMENT_EDIT_EMP

	, EB_COMMAND_NEW_CONTACT
	, EB_COMMAND_EDIT_CONTACT
	, EB_COMMAND_DELETE_CONTACT
	, EB_COMMAND_CALL_USER

	, EB_COMMAND_DELETE_SESSION
	, EB_COMMAND_CLEAR_SESSION
	, EB_COMMAND_NEW_DIR_RES	= WM_USER+150
	, EB_COMMAND_DELETE_DIR_RES
	, EB_COMMAND_EDIT_DIR_RES
	, EB_COMMAND_NEW_SUBDIR_RES
	, EB_COMMAND_NEW_FILE_RES
	, EB_COMMAND_NEW_NOTE_RES
	, EB_COMMAND_DELETE_ITEM_RES
	, EB_COMMAND_EDIT_ITEM_RES
	, EB_COMMAND_DOWNLOAD_FILE
	, EB_COMMAND_MOVE_2_DIR0	= WM_USER+170
	, EB_COMMAND_MOVE_2_DIRX	= WM_USER+190
	, EB_COMMAND_STATE_ONLINE	= WM_USER+200
	, EB_COMMAND_STATE_AWAY
	, EB_COMMAND_STATE_BUSY
	, EB_COMMAND_MY_EMPLOYEE	= WM_USER+300	// 后面至少留50个
	, EB_COMMAND_SUBSCRIBE_FUNC	= WM_USER+350	// 后面至少留50个
	, EB_COMMAND_SKIN_SETTING	= WM_USER+400
	, EB_COMMAND_SKIN_1
	, EB_COMMAND_SKIN_2			= EB_COMMAND_SKIN_1+10


};

class CTreeItemInfo
{
public:
	typedef boost::shared_ptr<CTreeItemInfo> pointer;
	typedef enum ITEM_TYPE
	{
		ITEM_TYPE_ENTERPRISE
		, ITEM_TYPE_GROUP
		, ITEM_TYPE_MEMBER
		, ITEM_TYPE_CONTACT
		, ITEM_TYPE_DIR
		, ITEM_TYPE_FILE
		, ITEM_TYPE_NOTE
		, ITEM_TYPE_OTHERRES
	};
	static CTreeItemInfo::pointer create(ITEM_TYPE nItemType,int nIndex)
	{
		return CTreeItemInfo::pointer(new CTreeItemInfo(nItemType,nIndex));
	}
	static CTreeItemInfo::pointer create(ITEM_TYPE nItemType,HTREEITEM hItem)
	{
		return CTreeItemInfo::pointer(new CTreeItemInfo(nItemType,hItem));
	}
	void operator =(const CTreeItemInfo* pItemInfo)
	{
		if (pItemInfo)
		{
			m_nItemType = pItemInfo->m_nItemType;
			m_nSubType = pItemInfo->m_nSubType;
			m_hItem = pItemInfo->m_hItem;
			m_nIndex = pItemInfo->m_nIndex;
			m_sEnterpriseCode = pItemInfo->m_sEnterpriseCode;
			m_sGroupCode = pItemInfo->m_sGroupCode;
			m_sMemberCode = pItemInfo->m_sMemberCode;
			m_sName = pItemInfo->m_sName;
			m_sAccount = pItemInfo->m_sAccount;
			m_dwItemData = pItemInfo->m_dwItemData;;
		}
	}
	ITEM_TYPE m_nItemType;
	int m_nSubType;
	HTREEITEM m_hItem;		// for tree
	int m_nIndex;			// for list
	bigint m_sEnterpriseCode;
	bigint m_sGroupCode;
	bigint m_sMemberCode;
	CEBString m_sGroupName;
	CEBString m_sName;	// group_name,emp_name,res_name...
	CEBString m_sAccount;
	bigint m_nUserId;
	bigint m_nBigId;
	bigint m_sId;			// resid,...
	bigint m_sParentId;
	DWORD m_dwItemData;

	CTreeItemInfo(ITEM_TYPE nItemType,int nIndex)
		: m_nItemType(nItemType),m_nSubType(0)
		, m_hItem(NULL),m_nIndex(nIndex)
		, m_sEnterpriseCode(0),m_sGroupCode(0),m_sMemberCode(0)
		, m_nUserId(0),m_nBigId(0)
		, m_sId(0),m_sParentId(0)
		, m_dwItemData(0)
	{
	}
	CTreeItemInfo(ITEM_TYPE nItemType,HTREEITEM hItem)
		: m_nItemType(nItemType),m_nSubType(0)
		, m_hItem(hItem),m_nIndex(0)
		, m_sEnterpriseCode(0),m_sGroupCode(0),m_sMemberCode(0)
		, m_nUserId(0),m_nBigId(0)
		, m_sId(0),m_sParentId(0)
		, m_dwItemData(0)
	{
	}
	CTreeItemInfo(void)
		: m_nItemType(ITEM_TYPE_CONTACT),m_nSubType(0)
		, m_hItem(NULL),m_nIndex(0)
		, m_sEnterpriseCode(0),m_sGroupCode(0),m_sMemberCode(0)
		, m_nUserId(0),m_nBigId(0)
		, m_sId(0),m_sParentId(0)
		, m_dwItemData(0)
	{
	}
};
const CTreeItemInfo::pointer NullTreeItemInfo;

namespace libEbc
{
	long GetFileSize(const char* lpszFile);
	std::string GetFileName(const std::string & sPathName);
	void GetFileExt(const std::string & sFileName, std::string & sOutName, std::string & sOutExt);

	int bmp_2_jpg(const WCHAR * sBmpFile, const WCHAR * sJpgFile, long quality = 100);
	BOOL ImageFromIDResource(UINT nID, LPCTSTR sTR, Image * & pImg);
	bool ChangeTime(const char* sTimeString, time_t& pOutTime, int* pOutMS = NULL);	// YYYY-mm-dd HH:MM:SS.XXXXX -> time_t

	std::string ACP2UTF8(const char* sString);
	std::string UTF82ACP(const char* sString);

};


#endif //__ebc_public_h__
