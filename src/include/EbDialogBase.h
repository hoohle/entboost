#pragma once

#define POP_WM_WINDOW_SIZE (WM_USER+101)

// CEbDialogBase dialog
class CEbBackDrawInfo
{
public:
	int m_nHeight;		// �߶�
	float m_fLight;		// ����
	bool m_bGradient;	// ����

	CEbBackDrawInfo(int nHeight,float fLight,bool bGradient)
		: m_nHeight(nHeight), m_fLight(fLight), m_bGradient(bGradient)
	{}
	CEbBackDrawInfo(void)
		: m_nHeight(0)
		, m_fLight(0.0)
		, m_bGradient(false)
	{
	}
};

class CEbDialogBase : public CDialog
{
	DECLARE_DYNAMIC(CEbDialogBase)

public:
	CEbDialogBase(CWnd* pParent = NULL);   // standard constructor
	CEbDialogBase(UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor
	virtual ~CEbDialogBase();

	enum TransparentType
	{
		TT_MSGBOX		= 0x1
		, TT_EDIT		= 0x2
		, TT_LISTBOX	= 0x4
		, TT_BTN		= 0x8
		, TT_DLG		= 0x10
		, TT_SCROLLBAR	= 0x20
		, TT_STATIC		= 0x40
		, TT_MAX		= 0x80
	};

	static void GradientFillRect( CDC *pDC, CRect &rect, COLORREF col_from, COLORREF col_to, bool vert_grad );

	void SetTransparentType(UINT nTransType = TT_STATIC|TT_DLG) {m_nTransType = nTransType;}
	void SetBgColor(COLORREF colorBg);
	void SetMouseMove(BOOL bEnable) {m_bMouseMove = bEnable;}
	void SetCircle(bool bCircle = true);
	void SetSplitterBorder(void);

	void ClearBgDrawInfo(void) {m_pBgDrawInfo.clear();}
	void AddBgDrawInfo(const CEbBackDrawInfo& pDrawInfo) {m_pBgDrawInfo.push_back(pDrawInfo);}
	void DrawPopBg(CDC * pDC, COLORREF colorBg, int nBorder=2);
	void SetToolTipText(UINT nID,LPCTSTR sToolTip);
	//void SetToolTipColor(COLORREF crText,COLORREF crBkgnd);

protected:
	HICON m_hIcon;
	UINT m_nTransType;
	CBrush m_brush;
	BOOL m_bMouseMove;
	CSplitterControl m_wndSplitterLeft;
	CSplitterControl m_wndSplitterTop;
	CSplitterControl m_wndSplitterRight;
	CSplitterControl m_wndSplitterBottom;
	std::map<UINT,CString> m_pToolTip;
	std::vector<CEbBackDrawInfo> m_pBgDrawInfo;
	bool m_bCircle;

	//virtual void OnDraw(void) {}
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void init();

	LRESULT OnMessageWindowResize(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnTipNotify(UINT id,NMHDR*pNMHDR,LRESULT*pResult);
	virtual BOOL OnInitDialog();
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnPaint();
};
