// videoroom.h file here
#ifndef __videoroom_h__
#define __videoroom_h__

#ifdef VIDEOROOM_EXPORTS
#define VIDEOROOM_API __declspec(dllexport)
#else
#define VIDEOROOM_API __declspec(dllimport)
#endif

#include <string>
#include <vector>
#include "videoroomdefine.h"

class VIDEOROOM_API Cvideoroom
{
public:
	Cvideoroom(void);
	~Cvideoroom(void);
	static void VR_GetVideoDevices(std::vector<std::string>& pOutDevices);

	int VR_Init(unsigned int nlocalid, VideoStreamCallBack cb);
	//void setlocalid(unsigned int nlocalid);
	void VR_UnInit(void);

	void VR_SetVideoServer(const char* sAddress, unsigned short nPort);
	void VR_SetAudioServer(const char* sAddress, unsigned short nPort);

	// VideoQuality:0-10; default 3
	bool VR_InitVideoCapture(unsigned short nvideoindex,int nVideoQuality=3);
	bool VR_InitAudioCapture(void);
	VIDEO_RGB_TYPE VR_GetVideoType(void) const;
	
	void VR_SetVideoSend(bool bSend);
	void VR_SetAudioSend(bool bSend);
	bool VR_GetVideoSend(void) const;
	bool VR_GetAudioSend(void) const;

	void VR_StartVideoCapture(void);
	void VR_StopVideoCapture(void);
	void VR_StartAudioCapture(void);
	void VR_StopAudioCapture(void);

	// 我看谁
	void VR_AddMeLooks(unsigned int nuserid);
	void VR_DelMeLook(unsigned int nuserid);
	// 谁看我
	void VR_AddLookMes(unsigned int nuserid);
	void VR_DelLookMes(unsigned int nuserid);

	void VR_AddVideoStream(unsigned int nuserid, unsigned long udata, bool bLocalVideo);
	void VR_DelVideoStream(unsigned int nuserid);
	// 本地音量条
	void VR_SetLocalAudiolStream(unsigned int nuserid, AudioStreamCallBack cb, void * udata);
	void VR_AddAudioStream(unsigned int nuserid, AudioStreamCallBack cb, void * udata);
	void VR_DelAudioStream(unsigned int nuserid);
	void VR_PauseAudioStream(unsigned int nuserid);
	void VR_PlayAudioStream (unsigned int nuserid);

private:
	void * m_handle;
};

class VIDEOROOM_API Cvideowindow
{
public:
	Cvideowindow(HWND hwnd, 
		VIDEO_RGB_TYPE nRGB,
		int videoWidth,
		int VideoHeight,
		int left = 0, 
		int top = 0,
		int right = 0,
		int bottom = 0);
	~Cvideowindow(void);

	HWND VW_GetParentWnd(void) const;
	HRESULT	VW_BuildGraph(bool bDefaultRender = false);
	HRESULT	VW_StartGraph(void);
	HRESULT VW_StopGraph(void);
	HRESULT	VW_ReleaseGraph(void);

	void VW_MoveWindow( int left, int top, int right, int bottom);
	int VW_DrawVideo(void * videodata, int len );
	void VW_ShowVideoWindow(bool show);
	//void NotifyOwnerMessage(HWND hWnd, long uMsg,LONG_PTR wParam,LONG_PTR lParam);
	//HRESULT ProcessGraphNotify(long *lEventCode, LONG_PTR *lParam1, LONG_PTR *lParam2);

	bool VW_SaveBitmapFile(const char * sSaveToFile);
private:
	void * m_handle;
};

#endif // __videoroom_h__
