// videoroomdefine.h file here
#ifndef __videoroomdefine_h__
#define __videoroomdefine_h__

#ifndef VIDEOROOM_EXPORTS
typedef enum {
	VIDEO_RGB24  = 0
	, VIDEO_RGB32
	, VIDEO_I420
	, VIDEO_H264
	, VIDEO_WMV9
	, VIDEO_RV40
	, VIDEO_XVID
	, VIDEO_YUY2
}VIDEO_RGB_TYPE;

typedef struct tagVIDEOFRAME
{
	DWORD type;    //frame type
	WORD  width;
	WORD  height;
	BYTE  *data;
	DWORD len;
	DWORD usedlen;
	DWORD timastamp;
}VIDEOFRAME;
#endif

typedef void (*VideoStreamCallBack) (DWORD nId, VIDEOFRAME *pFrame, void *lParam, DWORD uData);
typedef void (*AudioStreamCallBack) (char *pdata, int length, void* uData);

#endif // __videoroomdefine_h__

