// ebstring.h file here
#ifndef __ebstring_h__
#define __ebstring_h__

#include <string>
#ifdef EBSTRING_USES_BSTR

#define Z_A2W_CP_EX(lpa, nChars, cp) (\
	((_lpa_ex = lpa) == NULL) ? NULL : (\
	_convert_ex = (::MultiByteToWideChar(cp, 0, _lpa_ex, (int)(strlen(_lpa_ex)), NULL, 0) + 1), \
	FAILED(::ATL::AtlMultiply(&_convert_ex, _convert_ex, static_cast<int>(sizeof(WCHAR)))) ? NULL : \
	ATLA2WHELPER(	\
	(LPWSTR)_ATL_SAFE_ALLOCA(_convert_ex, _ATL_SAFE_ALLOCA_DEF_THRESHOLD), \
	_lpa_ex, \
	_convert_ex, \
	(cp))))

#define Z_W2A_CP_EX(lpw, nChars, cp) (\
	((_lpw_ex = lpw) == NULL) ? NULL : (\
	_convert_ex = (::WideCharToMultiByte(cp, 0, _lpw_ex, (int)(wcslen(_lpw_ex)), NULL, 0, NULL, NULL) + 1), \
	ATLW2AHELPER(	\
	(LPSTR)_ATL_SAFE_ALLOCA(_convert_ex, _ATL_SAFE_ALLOCA_DEF_THRESHOLD), \
	_lpw_ex, \
	_convert_ex, \
	(cp))))

#undef USES_CONVERSION
#undef A2W
#undef W2A
#undef A2W_CP
#undef W2A_CP
#undef A2W_CP_EX
#undef W2A_CP_EX
// 强制使用安全版本
#define USES_CONVERSION	USES_CONVERSION_EX
#define A2W(lpa)	A2W_EX(lpa, 0)
#define W2A(lpw)	W2A_EX(lpw, 0)
#define A2W_CP_EX(lpa, cp)	Z_A2W_CP_EX(lpa, 0, cp)
#define W2A_CP_EX(lpw, cp)	Z_W2A_CP_EX(lpw, 0, cp)
#define A2W_CP(lpa, cp)	A2W_CP_EX(lpa, cp)
#define W2A_CP(lpw, cp)	W2A_CP_EX(lpw, cp)

#define A2W_UTF8(lpa)	A2W_CP_EX(lpa, CP_UTF8)
#define W2A_UTF8(lpw)	W2A_CP_EX(lpw, CP_UTF8)

inline BSTR A2BSTR_CP(LPCSTR lpa, UINT cp)
{
	USES_CONVERSION;
	return CComBSTR(A2W_CP_EX(lpa, cp)).Detach();
}

inline BSTR A2BSTR_UTF8(LPCSTR lpa)
{
	return A2BSTR_CP(lpa, CP_UTF8);
}

#define BSTR2A_EX(bstr) (bstr==NULL?"":W2A_EX(bstr,0))

#endif

class CEBString : public std::string
{
public:
#ifdef EBSTRING_USES_BSTR
	const CEBString& operator=(BSTR bstr)
	{
		USES_CONVERSION_EX;
		this->assign(BSTR2A_EX(bstr));
		return *this;
	}
	const CEBString& operator=(const CEBString& s)
	{
		this->assign(s);
		return *this;
	}
	BSTR CopyBSTR(int cp=CP_ACP) const
	{
		USES_CONVERSION_EX;
		return ::SysAllocString(A2W_CP_EX(this->c_str(),cp));
	}
	//void CopyTo(VARIANT* pDest) const
	//{
	//	USES_CONVERSION;
	//	return ::SysAllocString(A2W(this->c_str()));
	//}
#endif
	CEBString(void)
	{
	}
	CEBString(const char* str)
		: std::string(str)
	{		
	}
	//CEBString(const CEBString& str)
	//	: std::string(str.c_str())
	//{		
	//}
	CEBString(const std::string& str)
		: std::string(str)
	{		
	}
#ifdef EBSTRING_USES_BSTR
	CEBString(BSTR bstr)
	{
		CEBString::operator=(bstr);
	}
	CEBString(BSTR bstr, UINT cp, bool bFreeBSTR=false)
	{
		USES_CONVERSION_EX;
		CEBString::operator=(W2A_CP_EX(bstr,cp));
		if (bFreeBSTR && bstr != NULL)
		{
			::SysFreeString(bstr);
		}
	}
#endif

};


#endif // __ebstring_h__
